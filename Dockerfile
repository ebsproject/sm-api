FROM maven:3.6.3-jdk-11-openj9 as build
COPY src src
COPY pom.xml .

RUN mvn clean install

#uncomment to check if the binaries you expect are really being built - we saw ebs-sg-sm.jar get renamed as we are doing this build
#RUN ls -lht target

FROM openjdk:11-jre

ENV spring_datasource_url=jdbc:postgresql://{hostname}:{port}/{databasename}
ENV spring_datasource_username=username
ENV spring_datasource_password=password

ENV ebs_cs_graphql_endpoint=http://csapi.local/graphql
ENV ebs_cb_api_endpoint=http://cbapi.local/v3/
ENV ebs_file_api_endpoint=http://fileapi-SERVER:8080
ENV ebs_marker_graphql_endpoint=http://markerapi-SERVER:8080

#Env variables to authenticate with apis from other micro components
ENV ebs_cs_auth_client_id=some_client_id
ENV ebs_cs_auth_client_secret=some_client_secret
ENV ebs_cs_auth_username=some_username
ENV ebs_cs_auth_password=some_password
ENV ebs_cs_auth_endpoint=https://sg.SOME_SERVER:PORT/oauth2/token

#Env variables to set gigwa loader properties
ENV ebs_gigwa_loader_enabled=false
ENV ebs_gigwa_loader_delay_time_seconds=15
ENV ebs_gigwa_username=username
ENV ebs_gigwa_password=password
ENV ebs_gigwa_api_endpoint=http://gigwa-SERVER:8080/gigwa/rest

COPY --from=build target/ebs-sg-sm.jar ./app.jar

ENTRYPOINT ["java", "-Xshare:off", "-XX:+UseContainerSupport", "-XX:G1HeapRegionSize=4M", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=200", "-XX:ConcGCThreads=2", "-XX:ParallelGCThreads=4", "-XX:InitialRAMPercentage=50.0", "-XX:MaxRAMPercentage=80.0", "-Xms524m", "-Xmx1524m", "-jar", "/app.jar"]




package org.ebs;

import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.ebs.util.DateCoercing;
import org.ebs.util.DateTimeCoercing;
import org.ebs.util.UUIDCoercing;
import org.ebs.util.brapi.BrapiClient;
import org.ebs.util.brapi.TokenGenerator;
import org.ebs.util.client.CBGraphClient;
import org.ebs.util.client.FileClient;
import org.ebs.util.client.GigwaClient;
import org.ebs.util.client.GigwaTokenGenerator;
import org.ebs.util.client.GraphQLResolverClient;
import org.ebs.util.client.MarkerClient;
import org.ebs.util.client.RestCsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

import graphql.schema.GraphQLScalarType;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	public static ThreadLocal<String> REQUEST_TOKEN = new ThreadLocal<>();
    public static Map<String, String> TOKEN_MAP = new ConcurrentHashMap<>();
    public static ThreadLocal<String> REQUEST_USER = new ThreadLocal<>();

	@Value("${ebs.cs.graphql.endpoint}")
	private String csGraphqlEndpoint;
	
	@Value("${ebs.marker.graphql.endpoint}")
	private String markerGraphqlEndpoint;
	
	@Value("${ebs.cb.graphql.endpoint}")
	private String cbGraphqlEndpoint;

	@Value("${ebs.cb.api.endpoint}")
    private String cbApiEndpoint;

    @Value("${ebs.gigwa.api.endpoint}")
    private String gigwaRestEndpoint;

    @Value("${ebs.file.api.endpoint}")
    private String fileRestEndpoint;

	@Bean
	public GraphQLScalarType dateScalar() {
		return GraphQLScalarType.newScalar()
			.name("Date")
			.description("Date Scalar, custom implementation")
			.coercing(new DateCoercing())
			.build();
	}

	@Bean
	public GraphQLScalarType datetimeScalar() {
		return GraphQLScalarType.newScalar()
			.name("Datetime")
			.description("Datetime Scalar, custom implementation")
			.coercing(new DateTimeCoercing())
			.build();
	}

    @Bean
    public GraphQLScalarType uuidScalar() {
        return GraphQLScalarType.newScalar()
            .name("UUID")
            .description("Uuid Scalar, custom implementation")
            .coercing(new UUIDCoercing())
            .build();
    }

	@Bean @Primary
	@Autowired
    BrapiClient brapiClientCB(TokenGenerator tokenGenerator) {
        return new BrapiClient(URI.create(cbApiEndpoint), tokenGenerator);
    }

    @Bean
	@Autowired
	FileClient fileClient(RestTemplateBuilder restTemplateBuilder, TokenGenerator tokenGenerator) {
		return new FileClient(restTemplateBuilder.build(), URI.create(fileRestEndpoint), tokenGenerator);
	}

	@Bean
	@Autowired
    GraphQLResolverClient graphqlClientCS(TokenGenerator tokenGenerator) {
        return new GraphQLResolverClient(URI.create(csGraphqlEndpoint), tokenGenerator);
    }
    
    @Bean
    GigwaClient brapiClientGigwa(RestTemplateBuilder restTemplateBuilder,
            GigwaTokenGenerator tokenGenerator) {
        return new GigwaClient(restTemplateBuilder.build(), URI.create(gigwaRestEndpoint), tokenGenerator);
    }

    @Bean
    @Autowired
    MarkerClient markerClient (RestTemplateBuilder restTemplateBuilder,
            TokenGenerator tokenGenerator) {
        return new MarkerClient (restTemplateBuilder.build(), URI.create(markerGraphqlEndpoint), tokenGenerator);
    }
    
    @Bean
    @Autowired
    CBGraphClient cbGraphClient (RestTemplateBuilder restTemplateBuilder, TokenGenerator tokenGenerator) {
        return new CBGraphClient(restTemplateBuilder.build(), URI.create(cbGraphqlEndpoint), tokenGenerator);
    }
    
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    RestCsClient restCsClient(TokenGenerator tokenGenerator) {
        return new RestCsClient(tokenGenerator, csGraphqlEndpoint);
    }
}

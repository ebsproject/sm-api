package org.ebs.util.brapi;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BrStudy {
    private String studyName;
    private String studyType;
    private String trialDbId;
    private String studyDbId;

    public BrStudy(String studyName) {
        this.studyName = studyName;
    }
}

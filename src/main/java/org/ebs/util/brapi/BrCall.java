package org.ebs.util.brapi;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor @AllArgsConstructor
public class BrCall {
    private String callSetDbId;
    private String variantDbId;
    private Map<String,String> additionalInfo;
    private BrGenotype genotype;

    public BrCall(String callSetDbId) {
        this.callSetDbId = callSetDbId;
    }
}

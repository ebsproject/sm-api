package org.ebs.util.brapi;

import static java.lang.String.format;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Base64.getEncoder;
import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpStatus.OK;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.security.MessageDigest;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
@Primary
public class TokenGenerator {

    @Value("${ebs.cs.auth.client_id}")
    private String clientId;
    @Value("${ebs.cs.auth.client_secret}")
    private String clientSecret;
    @Value("${ebs.cs.auth.username}")
    private String userName;
    @Value("${ebs.cs.auth.password}")
    private String password;
    @Value("${ebs.cs.auth.endpoint}")
    private String authenticationEndpoint;

    private final ObjectMapper mapper;

    private HttpClient _client = HttpClient.newBuilder().build();
    private HttpRequest _req;

    private String token = null;
    private Instant expiration = now();

    public String getToken() {

        if (token == null || now().plus(2, MINUTES).isAfter(expiration)) {
            AuthenticationResponse auth = authenticate().orElseThrow(() -> {
                token = null;
                throw new RuntimeException("Could not generate token");
            });
            token = auth.getAuthenticationResult().getIdToken();
            expiration = now().plus(auth.getAuthenticationResult().getExpiresIn(), SECONDS);
            log.debug("Authentication generated. Valid from {} to {}", now(), expiration);

        }
        return token;
    }

    public String getSessionID() {
        return null;
    }

    Optional<AuthenticationResponse> authenticate() {
        log.debug("Generating new authentication token...");
        AuthenticationResponse auth = null;
        try {
            HttpResponse<String> response = _client.send(authenticationRequest(false),
                    BodyHandlers.ofString());
            if (response.statusCode() == OK.value()) {
                auth = mapper.readValue(response.body(), AuthenticationResponse.class);
            } else {
                log.warn("Authentication without secret hash failed for {} with status: {}. Detail: {}", userName,
                    HttpStatus.valueOf(response.statusCode()), response.body());
                response = _client.send(authenticationRequest(true),
                        BodyHandlers.ofString());
                if (response.statusCode() == OK.value()) {
                    auth = mapper.readValue(response.body(), AuthenticationResponse.class);
                    log.warn("Authentication wih secret hash failed for {} with status: {}. Detail: {}", userName,
                        HttpStatus.valueOf(response.statusCode()), response.body());
                }
            }

        } catch (IOException | InterruptedException e) {
            log.warn("Authentication failed for {}. Detail: {}", userName, e.getMessage());
        }
        return ofNullable(auth);
    }


    String requestBody(boolean includeSecretHash) throws Exception {
        if (includeSecretHash) {
            return format(
                    "{\"AuthParameters\":{\"USERNAME\": \"%s\",\"PASSWORD\": \"%s\"," +
                            "\"SECRET_HASH\":\"%s\"}," +
                            "\"AuthFlow\": \"USER_PASSWORD_AUTH\"," + "\"ClientId\": \"%s\"}",
                    userName, password, getSecretHash(), clientId);
        } else {
            return format(
                    "{\"AuthParameters\":{\"USERNAME\": \"%s\",\"PASSWORD\": \"%s\"}," +
                            "\"AuthFlow\": \"USER_PASSWORD_AUTH\"," + "\"ClientId\": \"%s\"}",
                    userName, password, clientId);
        }
    }

    String getSecretHash() throws Exception{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(clientSecret.getBytes());
        byte[] value = (userName + clientId).getBytes("UTF-8");
        return getEncoder().encodeToString(md.digest(value));
    }

    HttpRequest authenticationRequest(boolean includeSecretHash) {
        try {
            return HttpRequest.newBuilder(URI.create(authenticationEndpoint))
                    .timeout(Duration.ofSeconds(5))
                    .header("X-Amz-Target", "AWSCognitoIdentityProviderService.InitiateAuth")
                    .header(HttpHeaders.CONTENT_TYPE, "application/x-amz-json-1.1")
                    .POST(ofString(requestBody(includeSecretHash))).build();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Could not create authentication request: " + e.getMessage());
        }
        return null;
}
}
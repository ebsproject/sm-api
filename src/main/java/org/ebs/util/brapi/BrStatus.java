package org.ebs.util.brapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Models status section of a BrAPI response as well as status codes and
 * messages
 * 
 * @author JAROJAS
 *
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BrStatus {

    public static final int SUCCESS_CODE = 0;
    public static final int ERROR_CODE = 1;
    public static final BrStatus SUCCESS = new BrStatus(SUCCESS_CODE, "success");
    public static final BrStatus ERROR = new BrStatus(ERROR_CODE, "server error");

    private int code;
    private String message;

}

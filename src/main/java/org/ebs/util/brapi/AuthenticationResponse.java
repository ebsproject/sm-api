package org.ebs.util.brapi;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AuthenticationResponse {

    @JsonProperty("AuthenticationResult")
    private AuthenticationResult authenticationResult;
}
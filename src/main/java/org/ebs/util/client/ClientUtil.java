package org.ebs.util.client;

import static java.util.Optional.ofNullable;

import org.ebs.security.EbsUser;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.security.core.context.SecurityContextHolder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ClientUtil {
    /**
     * Returns the Authorization heaver set with the current request token, or a server-side generated one if there is no authority in the security context
     * @param tokenGenerator the generator for a new token when there is no security context
     * @param prefixBearer true if 'Bearer' is going to be added to the header
     * @return a string with a Bearer token value for an authorization header
     */
    public static String getAuthorizationHeaderValue(TokenGenerator tokenGenerator, boolean prefixBearer,
        boolean forceTokenGenerator) {

            
        String value = (prefixBearer ? "Bearer " : "") +
            (forceTokenGenerator ? tokenGenerator.getToken() :
                ofNullable(SecurityContextHolder.getContext())
                        .map(c -> c.getAuthentication())
                        .map(a -> (EbsUser) a.getPrincipal())
                        .map(u -> u.getRequestToken())
                        .orElse(tokenGenerator.getToken()));
        log.trace("setting authorization header value to: {}", value);

        return value;
    }

    public static String getAuthorizationHeaderValue(TokenGenerator tokenGenerator) {
        return getAuthorizationHeaderValue(tokenGenerator, true, false);
    }

    public static String getAuthorizationHeaderValue(TokenGenerator tokenGenerator, boolean prefixBearer) {
        return getAuthorizationHeaderValue(tokenGenerator, prefixBearer, false);
    }

}
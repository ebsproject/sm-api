package org.ebs.util.client;    

import java.util.List;

import org.ebs.services.to.custom.AssayTo;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
class AssaySearchResponse {
    private List<AssayTo> content;
    int numberOfElements;
    int totalPages;
}
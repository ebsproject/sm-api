package org.ebs.util.client;

import java.net.URI;
import java.util.Map;

import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class GigwaTokenGenerator extends TokenGenerator{
    
    @Value("${ebs.gigwa.username}")
    private String username = "";
    @Value("${ebs.gigwa.password}")
    private String password = "";
    @Value("${ebs.gigwa.api.endpoint}")
    private String gigwaRestEndpoint = "";
    
    private RestTemplate client = null;
    private String sessionId = null;

    @Autowired
    public GigwaTokenGenerator(ObjectMapper mapper, RestTemplateBuilder restBuilder) {
        super(mapper);
        client = restBuilder.build();
    }

    @Override
    public String getToken() {
        try {
            Map<String, Object> params = Map.of("username", username, "password", password);
            ResponseEntity<TokenResponse> tokenResponse = client.postForEntity(
                    gigwaRestEndpoint + "/gigwa/generateToken", params, TokenResponse.class);

            String token = tokenResponse.getBody().getToken();
            log.trace("gigwa token generated: {}", token);
            return token;
        } catch (Exception e) {
            log.error("Cannot generate token: {}", e.getMessage());
            throw new RuntimeException("Gigwa authentication failed for user " + username);
        }
    }
    
    @Override
    public String getSessionID() {
        if (sessionId != null) {
            return sessionId;
        }
        
        try {
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("username", username);
            params.add("password", password);

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONNECTION, "keep-alive");
            headers.add(HttpHeaders.ACCEPT, "*/*");
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            
            HttpEntity<Object> entity = new HttpEntity<Object>(params, headers);
    
            ResponseEntity<String> response = client.exchange(
                   URI.create( gigwaRestEndpoint.replace("/rest", "/login")), HttpMethod.POST,
                 entity, String.class);
            
            sessionId = response.getHeaders().getFirst(HttpHeaders.SET_COOKIE);
            sessionId = sessionId.substring(0, sessionId.indexOf(";"));
            return sessionId;
        } catch (Exception e) {
            log.error("Cannot generate cookie: {}", e.getMessage());
            throw new RuntimeException("Gigwa sessionID failed for user " + username);
        }
    }

}

/**
 * Models the response of the resource generating gigwa tokens
 */
@Data
class TokenResponse {
    private String token;
}

package org.ebs.util.client;

import static org.ebs.util.client.ClientUtil.getAuthorizationHeaderValue;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.ebs.services.to.custom.AssayTo;
import org.ebs.util.brapi.TokenGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class MarkerClient {
	
	private static final Logger log = LoggerFactory.getLogger(GraphQLResolverClient.class);
	private final RestTemplate restTemplate;
    private final MultiValueMap<String, String> headers;
    private final URI endpoint;
    private final TokenGenerator tokenGenerator;
    
    public MarkerClient(RestTemplate restClient, URI endpoint, TokenGenerator tokenGenerator) {
        
        restTemplate = restClient;
        headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
        this.endpoint = endpoint;
        this.tokenGenerator = tokenGenerator;
    }
    
    public <T> T findOneEntity(String query, ParameterizedTypeReference<Map<String,Map<String,T>>> responseType) {
        T result = null;
        try {
        
        	headers.remove(HttpHeaders.AUTHORIZATION);
        	headers.add(HttpHeaders.AUTHORIZATION, getAuthorizationHeaderValue(tokenGenerator));
            log.trace("Calling {} from external service: {}", query, endpoint);
            String body = String.format("{\"query\":\"{%s}\"}", query);
            HttpEntity<String> req = new HttpEntity<>(body,headers);
            ResponseEntity<Map<String,Map<String,T>>> response= restTemplate.exchange(endpoint,
                    HttpMethod.POST, req, responseType);
                    String queryName = query.split("\\(|\\{",2)[0];
            return response.getBody().get("data").get(queryName);
        } catch(Exception e) {
            log.error("### Error on query -> {}, error: {}", query, e.getMessage());
            // e.printStackTrace();
        }
        return result;
    }
    
    public <T> List<T> findEntityList(String query,
            ParameterizedTypeReference<Map<String, Map<String, Map<String, List<T>>>>> responseType) {
        List<T> result = null;
        try {

            headers.remove(HttpHeaders.AUTHORIZATION);
            headers.add(HttpHeaders.AUTHORIZATION, getAuthorizationHeaderValue(tokenGenerator));
            log.trace("Calling {} from external service: {}", query, endpoint);
            String body = String.format("{\"query\":\"{%s}\"}", query.replaceAll("\\\"", "\\\\\""));
            HttpEntity<String> req = new HttpEntity<>(body, headers);
            ResponseEntity<Map<String, Map<String, Map<String, List<T>>>>> response = restTemplate
                    .exchange(endpoint, HttpMethod.POST, req, responseType);

            String queryName = query.split("\\(|\\{", 2)[0];
            return response.getBody().get("data").get(queryName).get("content");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public <T> T modifyEntity(String query, ParameterizedTypeReference<Map<String, Map<String, T>>> responseType) {
        try {

            headers.remove(HttpHeaders.AUTHORIZATION);
            headers.add(HttpHeaders.AUTHORIZATION, getAuthorizationHeaderValue(tokenGenerator));
            log.trace("Calling {} from external service: {}", query, endpoint);
            String body = String.format("{\"query\":\"%s\"}", query.replaceAll("\\\"", "\\\\\""));
            HttpEntity<String> req = new HttpEntity<>(body, headers);
            ResponseEntity<Map<String, Map<String, T>>> response = restTemplate.exchange(endpoint, HttpMethod.POST, req,
                    responseType);
            String queryName = query.split("\\(|\\{", 2)[0];
            return response.getBody().get("data").get(queryName);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<AssayTo> findAssaysByMarkerIdsAndTechnolyServiceProvider(List<Integer> markerIds, int technolyServiceProvider) {
        String body = "{" +
                        "    \"filters\": [{\"entity\": \"MARKER\", \"attribute\": \"id\", \"logicOperatorMod\": \"AND\", \"filterMod\": \"IN\", \"values\": "+markerIds.toString()+"},\r\n" +
                        "                {\"entity\": \"TECHNOLOGY_SERVICE_PROVIDER\", \"attribute\": \"id\", \"logicOperatorMod\": \"AND\", \"filterMod\": \"EQ\", \"values\": [ "+technolyServiceProvider+" ]}],\r\n" +
                        "    \"page\": { \"number\": {page}, \"size\": 200 },\r\n" +
                        "    \"sorts\": [{ \"entity\": \"ASSAY\", \"column\": \"id\", \"mod\": \"ASC\" }]\r\n" +
                        "}";
        return searchAssays(body);
    }

    public List<AssayTo> findAssaysByMarkerGroupAndTechnolyServiceProvider(int markerGroupId, int technolyServiceProvider) {
        
        String body = "{" +
                        "    \"filters\": [{\"entity\": \"MARKER_GROUP\", \"attribute\": \"id\", \"logicOperatorMod\": \"AND\", \"filterMod\": \"EQ\", \"values\": [ "+markerGroupId+" ]}," +
                        "                {\"entity\": \"TECHNOLOGY_SERVICE_PROVIDER\", \"attribute\": \"id\", \"logicOperatorMod\": \"AND\", \"filterMod\": \"EQ\", \"values\": [ "+technolyServiceProvider+" ]}]," +
                        "    \"page\": { \"number\": {page}, \"size\": 200 }," +
                        "    \"sorts\": [{ \"entity\": \"ASSAY\", \"column\": \"id\", \"mod\": \"ASC\" }]" +
                        "}";

        return searchAssays(body);

    }

    List<AssayTo> searchAssays(String requestBody) {
        List<AssayTo> responseList = new ArrayList<>();
        headers.remove(HttpHeaders.AUTHORIZATION);
        headers.add(HttpHeaders.AUTHORIZATION, getAuthorizationHeaderValue(tokenGenerator));
        int page =1;
        boolean hasMoreElements = true;
        String path = endpoint.toString().replace("graphql","assays/search");
        while(hasMoreElements){
            HttpEntity<String> entity = new HttpEntity<>(requestBody.replace("{page}", "" + page), headers);
            log.debug("path: {}, body: {}", path, entity.getBody());
            ResponseEntity<AssaySearchResponse> response = restTemplate.exchange(path, HttpMethod.POST, entity, AssaySearchResponse.class);
            log.trace("Response from marker client: {}:{}", response.getHeaders(), response.getBody().getContent().size());
            responseList.addAll(response.getBody().getContent());
            hasMoreElements = response.getBody().getTotalPages() > page;
            page++;
        }
        log.debug("Assays found in {} pages: {}", page-1, responseList.size());
        return responseList;
    }

}

package org.ebs.util.client;

import java.util.List;

import org.springframework.core.io.FileSystemResource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileUploadForm {
    private int tenantId;
    private FileSystemResource resource;
    private List<String> tags;
    private String description;
    private int status;
}

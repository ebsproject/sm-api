package org.ebs.util.client;

import static java.util.Collections.singletonList;
import static org.ebs.services.GigwaService.DEFAULT_MODULE_NAME;
import static org.ebs.util.client.ClientUtil.getAuthorizationHeaderValue;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.ebs.util.brapi.BrCall;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrSample;
import org.ebs.util.brapi.BrStudy;
import org.ebs.util.brapi.BrapiClient;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GigwaClient extends BrapiClient {
    // endpoint of giwa's private api
    private URI privateEndpoint; 

    public GigwaClient(RestTemplate restClient, URI brEndpoint, GigwaTokenGenerator tokenGenerator) {
        super(brEndpoint, tokenGenerator, restClient);
        this.privateEndpoint = URI.create(brEndpoint.toString().replace("/rest", "/private"));

    }

    HttpEntity<String> authorizedEntity() {
        HttpHeaders privateHeaders = new HttpHeaders();
        privateHeaders.add(HttpHeaders.COOKIE, tokenGenerator.getSessionID());
        privateHeaders.add(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        return new HttpEntity<String>(null, privateHeaders);
    }

    public String sendImport(FileSystemResource resource) {
        log.trace("calling POST {}{}", brEndpoint, "/gigwa/genotypeImport");
        
        return template.postForEntity(brEndpoint + "/gigwa/genotypeImport", prepareRequest(resource),
                String.class).getBody();
    }

    private HttpEntity<MultiValueMap<String, Object>> prepareRequest(FileSystemResource resource) {
        String fileName = resource.getFilename();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        int separator = fileName.lastIndexOf(".");

        body.add("module", DEFAULT_MODULE_NAME);
        body.add("project", "project-" + fileName.substring(0, separator));
        body.add("run", "run-01");
        body.add("projectDesc", "auto loaded project");
        body.add("file[0]", resource);

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(tokenGenerator.getToken());
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        return new HttpEntity<MultiValueMap<String, Object>>(body, headers);
    }

    public String getProcessStatus(String progressId) {
        log.info("Looking for progress {} status", progressId);
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(progressId);
        ResponseEntity<String> response = template.exchange(String.format(brEndpoint + "/gigwa/progress?progressToken=%s", progressId) ,
                HttpMethod.GET, new HttpEntity<MultiValueMap<String, Object>>(null, headers), String.class);
        log.info("HttpStatus code {}", response.getStatusCode());
        log.info("Progress status esponse: {}", response.getBody());
        try {
            if (response.getStatusCode().equals(HttpStatus.OK)) {
                ObjectMapper om = new ObjectMapper();
                JsonNode data = om.readTree(response.getBody());
                if (data.hasNonNull("error")) {
                    return "error:" + data.get("error").asText();
                } else {
                    return "complete:" + data.get("complete").asText();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "error:" + e.getMessage();
        }
        return "No content";
    }

    /**
     * Returns the study with the gien name
     * NOTE: currently gigwa does not support filtering by name, so we must iterate
     * as it does not have proper pagination, we must keep track of records
     * @param studyName as defined by gigwa
     * @return
     */
    public Optional<BrStudy> getStudy(String studyName) {

        Optional<BrStudy> study = Optional.empty();
        BrPagedResponse<BrStudy> response = null;
        int page = 0;

        //TODO improve gigwa api to filter by studyName
        log.info("Looking for project {}", studyName);
        try {
            while (study.isEmpty() && (page == 0
                    || page < response.getMetadata().getPagination().getTotalPages())) {

                response = super.getList("/brapi/v2/studies?page=" + page++, BrStudy.class);
                log.info("total studies in gigwa: {}", response.getResult().getData().size());

                study = response.getResult().getData().stream().filter(s -> {
                    log.info("{} vs {}: {}", studyName, s.getStudyName(),
                            studyName.equals(s.getStudyName()));

                    return studyName.equals(s.getStudyName());
                }).findFirst();

                page++;
            }
        } catch (Exception e) {
            log.warn("Could not retrieve studies: {}", e.getMessage());
            e.printStackTrace();
        }
        log.info("study returned: {}", study);
        return study;
    }

    public List<BrSample> getSamplesByGermplasm(List<String> germplasmDbId) {
        int page = 0;
        Map<String, Object> body = new HashMap<>();
        body.put("germplasmDbIds", germplasmDbId.toArray(new String[] {}));
        body.put("pageSize", 100000);
        BrPagedResponse<BrSample> response = null;
        List<BrSample> samples = new ArrayList<>();

        while (page == 0 || page < response.getMetadata().getPagination().getTotalPages()) {
            body.put("page", page);
            response = super.post("/brapi/v2/search/samples", body, BrSample.class);
            samples.addAll(response.getResult().getData());
            page++;
        }
        log.trace("samples found for {} germplasm: {}", germplasmDbId.size(), samples.size());

        return samples;
    }

    public List<BrSample> getSamples(String studyId) {
        int page = 0;
        Map<String, Object> body = new HashMap<>();
        body.put("studyDbIds", new String[]{studyId});
        body.put("pageSize", 100000);
        BrPagedResponse<BrSample> response = null;
        List<BrSample> samples = new ArrayList<>();

        while (page == 0 || page < response.getMetadata().getPagination().getTotalPages()) {
            body.put("page", page);
            response = super.post("/brapi/v2/search/samples", body, BrSample.class);
            samples.addAll(response.getResult().getData());
            page++;
        }
        log.trace("samples found for study {}: {}",studyId, samples.size());

        return samples;
    }

    public List<BrCall> getCalls(List<String> callsetIds) {
        Map<String, Object> body = new HashMap<>();
        body.put("callSetDbIds", callsetIds.toArray(new String[] {}));
        body.put("pageSize", 100000);
        body.put("expandHomozygotes", "true");
        BrPagedResponse<BrCall> response = null;
        List<BrCall> calls = new ArrayList<>();
        
        int page = 0;
        List<BrCall> pageCalls = Collections.emptyList();
        while (page == 0 || !pageCalls.isEmpty()) {
            body.put("pageToken", page);
            response = super.post("/brapi/v2/search/calls", body, BrCall.class);
            pageCalls = Optional.ofNullable(response)
                    .map(r -> r.getResult())
                    .map(r -> r.getData())
                    .orElse(Collections.emptyList());
            calls.addAll(pageCalls);
            page++;
        }
        log.info("calls found: {}", calls.size());
        return calls;
    }

    public Map<String, GigwaModule> listModules() {
        
        ParameterizedTypeReference<Map<String, GigwaModule>> responseType = new ParameterizedTypeReference<>() {};

        ResponseEntity<Map<String, GigwaModule>> response = template.exchange(privateEndpoint + "/listModules.json_",
                HttpMethod.GET, authorizedEntity(), responseType);
        log.trace("modules found: {}", response.getBody().size());
        return response.getBody();
    }

    public String createModule(String module) {
        ResponseEntity<String> response = template.exchange(String.format("%s/createModule.json_?module=%s&host=defaultMongoHost", privateEndpoint, module) ,
                HttpMethod.GET, authorizedEntity(), String.class);
        log.info("module '{}' created: {}", module, response.getBody());
        return response.getBody();
    }

    public String setModuleVisibility(String module, boolean isPublic, boolean isHidden) {
        log.trace("setting module '{}' visibility: public={} hidden={}", module, isPublic, isHidden);
        ResponseEntity<String> response = template.exchange(String.format("%s/moduleVisibility.json_?module=%s&public=%s&hidden=%s", privateEndpoint, module, isPublic, isHidden) ,
                HttpMethod.GET, authorizedEntity(), String.class);
        log.info("module '{}' visibility set: {}", module, response.getBody());
        return response.getBody();
    }
    
    /**
     * Normal request of any client tries to use the request token.
     * For gigwa calls it's needed to use a token from a 
     * (gigwa) token generator, instead
     */
    @Override
    protected HttpEntity<Object> requestFor(Object body, boolean recordsBody) {
        String authHeader = getAuthorizationHeaderValue(tokenGenerator, true, true);
        log.trace("requestFor authHeader: {}", authHeader);
        super.headers.replace(AUTHORIZATION, singletonList(authHeader));
        return new HttpEntity<>(recordsBody ? Collections.singletonMap("records", body) : body,
                headers);
    }

}
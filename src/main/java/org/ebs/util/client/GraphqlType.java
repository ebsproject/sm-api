package org.ebs.util.client;

import java.util.List;
import java.util.Map;

import org.springframework.core.ParameterizedTypeReference;

public class GraphqlType{
    
    public static final ParameterizedTypeReference<Map<String, Map<String, Map<String, List<SmSample>>>>> SAMPLE = new ParameterizedTypeReference<>(){};
}

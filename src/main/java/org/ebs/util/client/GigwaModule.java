package org.ebs.util.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GigwaModule {
    int size;
    boolean isPublic;
    boolean hidden;
    String host;
}

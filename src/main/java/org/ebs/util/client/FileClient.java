package org.ebs.util.client;

import static java.util.Collections.singletonList;
import static org.ebs.util.client.ClientUtil.getAuthorizationHeaderValue;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.UUID;

import org.ebs.rest.to.FileObject;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrapiClient;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.ResolvableType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StreamUtils;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileClient extends BrapiClient{

    public static final Integer STATUS_UPLOADED_NEEDS_CONFIRMATION = -1;
    public static final Integer STATUS_UPLOADED = 1;
    public static final Integer STATUS_GIGWA_LOADED = 2;
    public static final Integer STATUS_GIGWA_SPLIT = 3;
    public static final Integer STATUS_GIGWA_SPLIT_ERROR = 4;
    public static final Integer STATUS_GIGWA_LOADED_ERROR = 5;

    private final RestTemplate client;
    private final TokenGenerator tokenGenerator;

    MultiValueMap<String, String> headers;
    
    @Autowired
    public FileClient(RestTemplate template, URI fileRestEndpoint,
            TokenGenerator tokenGenerator) {
        super(fileRestEndpoint, tokenGenerator);
        this.client = template;
        this.tokenGenerator = tokenGenerator;

        headers = new LinkedMultiValueMap<>();
        headers.add(CONTENT_TYPE, "application/json");
        headers.add(AUTHORIZATION, "");
    }

    public byte[] download(int tenantId, UUID uuid) throws IllegalStateException, IOException {
        headers.replace(AUTHORIZATION, singletonList(getAuthorizationHeaderValue(tokenGenerator, true)));
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        String path = String.format("%s/%s/objects/%s/download", brEndpoint, tenantId, uuid);

        ResponseEntity<byte[]> response = client.exchange(path, GET, entity, byte[].class);
        log.trace("Response from file client: {}", response.getHeaders());

        return response.getBody();
    }
    
    public File downloadFile(int tenantId, UUID uuid) throws IllegalStateException, IOException {
        headers.replace(AUTHORIZATION, singletonList(getAuthorizationHeaderValue(tokenGenerator, true)));

        String path = String.format("%s/%s/objects/%s/download", brEndpoint, tenantId, uuid);

        RequestCallback callback = request -> request.getHeaders().setBearerAuth(getAuthorizationHeaderValue(tokenGenerator, false));

        File response = client.execute(path, GET, callback, clientHttpResponse -> {
            File ret = File.createTempFile("download", "tmp");
            StreamUtils.copy(clientHttpResponse.getBody(), new FileOutputStream(ret));
            return ret;
        });

        log.trace("Response from file client: {}", response.getName());

        return response;
    }

    public FileObject uploadFile(FileUploadForm form) throws RuntimeException {
        log.info(">> Uploading {} with tags {}", form.getResource().getFilename(), form.getTags());
        HttpEntity<MultiValueMap<String, Object>> entity = prepareRequest(form);
        String path = String.format("%s/%s/objects", brEndpoint, form.getTenantId());

        ResolvableType resolvableType = ResolvableType
                .forClassWithGenerics(BrPagedResponse.class, FileObject.class);
        ParameterizedTypeReference<BrPagedResponse<FileObject>> typeRef = ParameterizedTypeReference
                .forType(resolvableType.getType());
        ResponseEntity<BrPagedResponse<FileObject>> response = client.exchange( path,  POST, entity, typeRef);
        if (response.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
            
            if (!response.getBody().getMetadata().getStatus().isEmpty()) {
                throw new RuntimeException(response.getBody().getMetadata().getStatus().get(0).getMessage());
            }
            else 
                throw new RuntimeException("Error occurred while trying to upload file");
        }
        log.info("Response from file client: {}", response.getBody().getResult().getData());

        return response.getBody().getResult().getData().get(0);
    }
    
    private HttpEntity<MultiValueMap<String, Object>> prepareRequest(FileUploadForm form) {
        if (form.getResource() == null || form.getTenantId() <= 0) {
            throw new RuntimeException(
                    "At least a fileResource and a tenantId are necessary to upload a file");
        }

        String fileName = form.getResource().getFilename();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        log.info("preparing to send: {}", fileName);

        body.add("tenantId", form.getTenantId());
        body.add("file", form.getResource());
        body.addAll("tags", form.getTags());
        body.add("description", form.getDescription());
        body.add("status", form.getStatus());

        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(getAuthorizationHeaderValue(tokenGenerator, false));
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        log.info("object {} created with headers {}", body, headers);
        return new HttpEntity<MultiValueMap<String, Object>>(body, headers);
    }

    public String deleteFile(String key) {
        headers.replace(AUTHORIZATION, singletonList(getAuthorizationHeaderValue(tokenGenerator, true)));
        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        String path = String.format("%s/1/objects/%s", brEndpoint, key);
        ResolvableType resolvableType = ResolvableType.forClassWithGenerics(BrResponse.class,
                String.class);
        ParameterizedTypeReference<BrResponse<String>> typeRef = ParameterizedTypeReference
                .forType(resolvableType.getType());
        ResponseEntity<BrResponse<String>> response = client.exchange(path, DELETE, entity, typeRef);
        log.trace("Response from file client: {}", response.getHeaders());

        return response.getBody().getResult();

    }
}

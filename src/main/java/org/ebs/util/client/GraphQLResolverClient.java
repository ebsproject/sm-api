package org.ebs.util.client;

import static org.ebs.util.client.ClientUtil.getAuthorizationHeaderValue;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.ebs.util.brapi.TokenGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class GraphQLResolverClient {

    private static final Logger log = LoggerFactory.getLogger(GraphQLResolverClient.class);

    private final RestTemplate plantilla;
    private final MultiValueMap<String, String> headers;
    private final URI endpoint;
    private final TokenGenerator tokenGenerator;

    public GraphQLResolverClient(URI endpoint, TokenGenerator tokenGenerator) {
        plantilla = new RestTemplate();
        headers = new LinkedMultiValueMap<>();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");
        this.endpoint = endpoint;
        this.tokenGenerator = tokenGenerator;
    }
    
    public <T> T findOneEntity(String query, ParameterizedTypeReference<Map<String,Map<String,T>>> responseType) {
        T result = null;
        try {
        
        	headers.remove(HttpHeaders.AUTHORIZATION);
        	headers.add(HttpHeaders.AUTHORIZATION, getAuthorizationHeaderValue(tokenGenerator));
            log.trace("Calling {} from external service: {}", query, endpoint);
            String body = String.format("{\"query\":\"{%s}\"}", query);
            HttpEntity<String> req = new HttpEntity<>(body,headers);
            ResponseEntity<Map<String,Map<String,T>>> response= plantilla.exchange(endpoint,
                    HttpMethod.POST, req, responseType);
            String queryName = query.split("\\(|\\{",2)[0];
            return response.getBody().get("data").get(queryName);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public <T> List<T> findEntityList(String query,
            ParameterizedTypeReference<Map<String, Map<String, Map<String, List<T>>>>> responseType) {
        List<T> result = null;
        try {

            headers.remove(HttpHeaders.AUTHORIZATION);
            headers.add(HttpHeaders.AUTHORIZATION, getAuthorizationHeaderValue(tokenGenerator));
            log.trace("Calling {} from external service: {}", query, endpoint);
            String body = String.format("{\"query\":\"{%s}\"}", query.replaceAll("\\\"", "\\\\\""));
            HttpEntity<String> req = new HttpEntity<>(body, headers);
            ResponseEntity<Map<String, Map<String, Map<String, List<T>>>>> response = plantilla
                    .exchange(endpoint, HttpMethod.POST, req, responseType);

            String queryName = query.split("\\(|\\{", 2)[0];
            return response.getBody().get("data").get(queryName).get("content");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public <T> T modifyEntity(String query, ParameterizedTypeReference<Map<String,Map<String,T>>> responseType) {
        try {
        
        	headers.remove(HttpHeaders.AUTHORIZATION);
        	headers.add(HttpHeaders.AUTHORIZATION, getAuthorizationHeaderValue(tokenGenerator, true, true));
            log.trace("Calling {} from external service: {}", query, endpoint);
            String body = String.format("{\"query\":\"%s\"}", query.replaceAll("\\\"", "\\\\\""));
            HttpEntity<String> req = new HttpEntity<>(body,headers);
            ResponseEntity<Map<String,Map<String,T>>> response= plantilla.exchange(endpoint,HttpMethod.POST, req, responseType);
            String queryName = query.split("\\(|\\{",2)[0];
            return response.getBody().get("data").get(queryName);

        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
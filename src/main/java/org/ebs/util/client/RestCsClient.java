package org.ebs.util.client;

import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import org.ebs.rest.to.GigwaLoadTO;
import org.ebs.util.brapi.TokenGenerator;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
public class RestCsClient {

    public static final int BATCH_RULE_ID = 2;
    public static final int REQUEST_RULE_ID = 3;
    public static final int PLATE_RULE_ID = 4;
    public static final int VENDOR_FILE_RULE_ID = 5;
    public static final int SAMPLE_RULE_ID = 6;
    public static final int BLANK_SAMPLE_RULE_ID = 9;
    public static final int CONTROL_SAMPLE_RULE_ID = 10;
    private final TokenGenerator tokenGenerator;
    private final String csGraphqlEndpoint;

    public String getSequence(int ruleId, Map<String, String> args) {
        log.debug("generating sequence for rule {} with args {}", ruleId, args);
        WebClient client = WebClient.builder()
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " +  tokenGenerator.getToken())
                .build();
        
        return client
            .post()
            .uri(csGraphqlEndpoint.replaceFirst("/graphql", "/sequence/next/" + ruleId) )
            .bodyValue( args)
            .retrieve().bodyToMono(String.class)
            .blockOptional()
            .orElseThrow(exceptionFor("Could not generate file name with rule (" + ruleId + ") and args " + args));

    }

    public String[] getSequence(int ruleId, Map<String, String> args, int batchSize) {
        if(batchSize == 0) {
            log.debug("sequence rule (" + ruleId + ") for 0 elements. Discarding");
            return null;
        }
        log.debug("generating sequence for rule {} with args {}", ruleId, args);
        WebClient client = WebClient.builder()
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " +  tokenGenerator.getToken())
                .exchangeStrategies(ExchangeStrategies
                    .builder()
                    .codecs(codecs -> codecs
                            .defaultCodecs()
                            .maxInMemorySize(5000*1024))
                            .build()
                            )
                .build();
        
        return client
            .post()
            .uri(csGraphqlEndpoint.replaceFirst("/graphql", "/sequence/next/" + ruleId + "/batch/" + batchSize) )
            .bodyValue( args)
            .retrieve().bodyToMono(String[].class)
            .blockOptional()
            .orElseThrow(exceptionFor("Could not generate code with rule (" + ruleId + ") and args " + args));
    }
    
    /**
     * Allows to generate error messages using variables outside of the lambda expression,
     * like instance variables or non final arguments
     * @param message
     * @return
     */
    private Supplier<RuntimeException> exceptionFor(String message) {
        return () -> new RuntimeException(message);
    }

    public CompositeBatchRef getBatchCodes(Map<String, String> args, int plateNumber, int sampleNumber) {
        return getBatchCodes(args,plateNumber,sampleNumber,0);
    }
    public CompositeBatchRef getBatchCodes(Map<String, String> args, int plateNumber, int sampleNumber, int blankSampleNumber) {
        CompositeBatchRef refs = new CompositeBatchRef();
        refs.setPlateCodeList(List.of(getSequence(PLATE_RULE_ID, args, plateNumber)));
        refs.setSampleCodeList(List.of(getSequence(SAMPLE_RULE_ID, args, sampleNumber)));
        return refs;
    }

    public String getPrograms(int id) {

        WebClient client = WebClient.builder()
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + tokenGenerator.getToken())
                .build();

        return client
                .get()
                .uri(csGraphqlEndpoint.replaceFirst("/graphql", "/user/" + id + "/programs"))
                .retrieve().bodyToMono(String.class)
                .blockOptional()
                .orElseThrow(exceptionFor("Could not retrieve program under the user is (" + id + ") " + id));

    }
    
    public String callGigwaLoader(GigwaLoadTO params) {
        log.debug("Calling callGigwaLoader with: {}", params);
        WebClient client = WebClient.builder()
                .defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " +  tokenGenerator.getToken())
                .build();
        
        return client
            .post()
            .uri(csGraphqlEndpoint.replaceFirst("/graphql", "/queue/gigwa-loader") )
            .bodyValue(params)
            .retrieve().bodyToMono(String.class)
            .blockOptional()
                .orElseThrow(exceptionFor("Could not call /queue/gigwa-loader with " + params));

    }

}

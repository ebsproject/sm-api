package org.ebs.util.client;

import java.util.List;

import lombok.Data;

@Data
public class CompositeBatchRef {
    List<String> plateCodeList;
    List<String> sampleCodeList;
    List<String> blankSampleCodeList;
    
}

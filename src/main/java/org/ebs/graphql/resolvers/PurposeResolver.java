package org.ebs.graphql.resolvers;

import java.util.Set;

import org.ebs.services.PurposeService;
import org.ebs.services.to.PurposeTo;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.ServiceTypeTo;
import org.ebs.services.to.custom.MarkerPanelTo;
import org.ebs.services.to.custom.TraitCategoryTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;

/**
 * @author Juan Carlos Moreno Sanchez <j.m.sanchez@cgiar.org>
 * @version 2.0
 * @created 12-Mar-2021 11:46:19 AM
 * @since 13-01-202555
 */
@Component
@Validated
public class PurposeResolver implements GraphQLResolver<PurposeTo> {

	private PurposeService purposeService;

	/**
	 * 
	 * @param purpose
	 */
	public Set<RequestTo> getRequests(PurposeTo purpose) {
		return purposeService.findRequests(purpose.getId());
	}

	/**
	 * 
	 * @param purpose
	 */
	public Set<ServiceTo> getServices(PurposeTo purpose) {
		return purposeService.findServices(purpose.getId());
	}

	/**
	 * 
	 * @param purposeTo
	 */
	public ServiceTypeTo getServicetype(PurposeTo purposeTo) {
		return purposeService.findServiceType(purposeTo.getId()).get();
	}

	/**
	 * 
	 * @param purposeService
	 */
	@Autowired
	public PurposeResolver(PurposeService purposeService) {
		this.purposeService = purposeService;

	}

	/**
	 * 
	 * @param purpose
	 */
	public Set<MarkerPanelTo> getMarkerPanels(PurposeTo purpose) {
		Set<MarkerPanelTo> set = purposeService.findMarkerGroupPurposes(purpose.getId());
		return !set.isEmpty() ? set : null;
	}

	public Set<TraitCategoryTo> getTraitCategories(PurposeTo purpose) {
		Set<TraitCategoryTo> set = purposeService.findTriatCategoryPurposes(purpose.getId());
		return !set.isEmpty() ? set : null;

	}

}
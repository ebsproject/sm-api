package org.ebs.graphql.resolvers;

import org.ebs.services.RequestTraitService;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.RequestTraitTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@Component @Validated
public class RequestTraitResolver implements GraphQLResolver<RequestTraitTo> {

	public final RequestTraitService requestTraitService; 

	public RequestTo getRequest(RequestTraitTo requestTraitTo){
		return requestTraitService.findRequest(requestTraitTo.getId()).isPresent() ?
				requestTraitService.findRequest(requestTraitTo.getId()).get() : null;
	}
}

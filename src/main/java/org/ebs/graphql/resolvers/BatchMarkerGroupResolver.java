package org.ebs.graphql.resolvers;

import java.util.Set;

import org.ebs.services.BatchMarkerGroupService;
import org.ebs.services.custom.MarkerService;
import org.ebs.services.to.BatchMarkerGroupCustomTo;
import org.ebs.services.to.BatchMarkerGroupTo;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.custom.MarkerPanelTo;
import org.ebs.services.to.custom.TechnologyServiceProviderTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@Component @Validated
public class BatchMarkerGroupResolver implements GraphQLResolver<BatchMarkerGroupTo> {
	
	public final BatchMarkerGroupService batchMarkerGroupService;
	private final MarkerService markerService;
	
	public BatchTo getBatch(BatchMarkerGroupTo to) {
		return batchMarkerGroupService.findBatch(to.getId()).isPresent() ?
				batchMarkerGroupService.findBatch(to.getId()).get() : null;
		
	}

	public  Set <BatchMarkerGroupCustomTo> getBatchMarkerGroupCustoms(BatchMarkerGroupTo to){
		return batchMarkerGroupService.findBatchMarkerGroupCustoms(to.getId());
	}
	
	public  MarkerPanelTo getMarkerGroup (BatchMarkerGroupTo to) {
		return markerService.findMarkerGroupById(to.getMarkerGroupID(), false);
	}

	public TechnologyServiceProviderTo getTechnologyServiceProvider (BatchMarkerGroupTo to) {
		return markerService.findTechnologyAndSPBTechSerProviderId(to.getTechnologyServiceProviderId());
	}
}

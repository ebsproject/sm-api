package org.ebs.graphql.resolvers;

import org.ebs.services.BatchMarkerGroupCustomService;
import org.ebs.services.custom.MarkerService;
import org.ebs.services.to.BatchMarkerGroupCustomTo;
import org.ebs.services.to.BatchMarkerGroupTo;
import org.ebs.services.to.custom.AssayTo;
import org.ebs.services.to.custom.MarkerTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@Component @Validated
public class BatchMarkerGroupCustomResolver implements GraphQLResolver<BatchMarkerGroupCustomTo> {
	
	private final BatchMarkerGroupCustomService batchMarkerGroupCustomService;
	private final MarkerService markerService; 

	public BatchMarkerGroupTo getBatchMarkerGroup(BatchMarkerGroupCustomTo to) {
		return batchMarkerGroupCustomService.findBatchMarkerGroup(to.getId()).isPresent() ?
				batchMarkerGroupCustomService.findBatchMarkerGroup(to.getId()).get() : null;
		
	}

	public AssayTo getAssay(BatchMarkerGroupCustomTo to) {
		return markerService.findAssayById(to.getAssayId());
		
	}
	
	public MarkerTo getMarker(BatchMarkerGroupCustomTo to) {
		return markerService.findMarkerById(to.getMarkerId());
	}
}

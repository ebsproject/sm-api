///////////////////////////////////////////////////////////
//  TissueTypeResolver.java
//  Macromedia ActionScript Implementation of the Class TissueTypeResolver
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:42 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.graphql.resolvers;

import java.util.Set;

import org.ebs.services.TissueTypeService;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.TissueTypeTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:42 AM
 */
@Component @Validated
public class TissueTypeResolver implements GraphQLResolver<TissueTypeTo> {

	private TissueTypeService tissuetypeService;

	/**
	 * 
	 * @param tissuetype
	 */
	public Set<BatchTo> getBatchs(TissueTypeTo tissuetype){
		return tissuetypeService.findBatchs(tissuetype.getId());
	}

	/**
	 * 
	 * @param tissuetypeService
	 */
	@Autowired
	public TissueTypeResolver(TissueTypeService tissuetypeService){
		this.tissuetypeService = tissuetypeService; 
	
	}
	/**
	 * 
	 * @param tissuetype
	 */
	public Set<RequestTo> getRequests(TissueTypeTo tissuetype){
		return tissuetypeService.findRequests(tissuetype.getId());
	}

}
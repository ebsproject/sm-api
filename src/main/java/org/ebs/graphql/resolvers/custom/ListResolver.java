package org.ebs.graphql.resolvers.custom;

import java.util.ArrayList;
import java.util.List;

import org.ebs.services.RequestService;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.custom.CBListTo;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;

@Component @Validated @RequiredArgsConstructor
public class ListResolver implements GraphQLResolver<CBListTo> {

	private final RequestService requestService; 
	
	public List<RequestTo> getRequests (CBListTo to)
	{
		List<FilterInput> filters = new ArrayList<FilterInput>();
		FilterInput input = new FilterInput();
		 input.setCol("listId");
		 input.setVal(String.valueOf(to.getListDbId()));
		 input.setMod(FilterMod.EQ);
		 filters.add(input);
		return requestService.findRequests(null, null, filters, false).getContent();
	}
}

///////////////////////////////////////////////////////////
//  CollectionContainerTypeResolver.java
//  Macromedia ActionScript Implementation of the Class CollectionContainerTypeResolver
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:02 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.graphql.resolvers;

import java.util.Set;

import org.ebs.services.CollectionContainerTypeService;
import org.ebs.services.to.CollectionContainerTypeTo;
import org.ebs.services.to.CollectionLayoutTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:02 AM
 */
@Component @Validated
public class CollectionContainerTypeResolver implements GraphQLResolver<CollectionContainerTypeTo> {

	private CollectionContainerTypeService collectioncontainertypeService;

	/**
	 * 
	 * @param collectioncontainertypeService
	 */
	@Autowired
	public CollectionContainerTypeResolver(CollectionContainerTypeService collectioncontainertypeService){
		this.collectioncontainertypeService = collectioncontainertypeService; 
	
	}

	/**
	 * 
	 * @param collectioncontainertype
	 */
	public Set<CollectionLayoutTo> getCollectionlayouts(CollectionContainerTypeTo collectioncontainertype){
		return collectioncontainertypeService.findCollectionLayouts(collectioncontainertype.getId());
	}

}
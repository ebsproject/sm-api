///////////////////////////////////////////////////////////
//  ValueResolver.java
//  Macromedia ActionScript Implementation of the Class ValueResolver
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:19:10 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.graphql.resolvers;

import org.ebs.services.ValueService;
import org.ebs.services.to.FieldTo;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.ValueTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import graphql.kickstart.tools.GraphQLResolver;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:19:10 PM
 */
@Component @Validated
public class ValueResolver implements GraphQLResolver<ValueTo> {

	private ValueService valueService;

	/**
	 * 
	 * @param valueTo
	 */
	public FieldTo getField(ValueTo valueTo){
		return valueService.findField(valueTo.getId()).get();
	}

	/**
	 * 
	 * @param valueTo
	 */
	public RequestTo getRequest(ValueTo valueTo){
		return valueService.findRequest(valueTo.getId()).get();
	}

	/**
	 * 
	 * @param valueService
	 */
	@Autowired
	public ValueResolver(ValueService valueService){
		this.valueService = valueService; 
	
	}

}
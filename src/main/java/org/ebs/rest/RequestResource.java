package org.ebs.rest;

import org.ebs.util.brapi.BrResponse;
import org.springframework.web.bind.annotation.RequestParam;

public interface RequestResource {

	BrResponse<String>  createRequestCode();
	BrResponse<String> createRequestListMembers(@RequestParam int listID, @RequestParam int entityTypeId,@RequestParam int requestId );
}

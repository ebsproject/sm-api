package org.ebs.rest;

import org.ebs.services.to.custom.ReportIntertekTo;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("vendor-report")
public interface VendorReportIntertek {

    @PostMapping("createIntertekReport")
	ResponseEntity<InputStreamResource> createReportIntertekFormat(@RequestBody ReportIntertekTo to );
}

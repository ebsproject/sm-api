package org.ebs.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.ebs.rest.to.GigwaLoadTO;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@SecurityRequirement(name = "Bearer Token")
@Validated
@RequestMapping("gigwa")
public interface GigwaResource {

	
    @Operation(description = "Loads a file from File Manager into Gigwa and generate result files by request")
    @PostMapping(value =  "load", produces = APPLICATION_JSON_VALUE)
    ResponseEntity<String> getGenotypes(@RequestBody GigwaLoadTO loadParams);

}

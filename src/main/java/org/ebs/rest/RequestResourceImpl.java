package org.ebs.rest;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.ebs.services.RequestService;
import org.ebs.services.custom.CBGermplasmService;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("request")
@RequiredArgsConstructor
public class RequestResourceImpl implements RequestResource{
	
	private final RequestService requestService;
	private final CBGermplasmService CBGermplasmService;  

	private String pattern = "YYYYMMdd";
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	

	@Override
    @GetMapping("createRequestCode")
    @Deprecated
	public BrResponse<String> createRequestCode() {
		long number = requestService.findRequests(null, null, null, false).getTotalElements();
		String date = simpleDateFormat.format(new Date());
		
		return BrapiResponseBuilder.forData("RLB-"+date+"-"+(number+1))
		.withStatusSuccess()
		.build();
	}

	@Override
	@GetMapping("createRequestListMemberById")
	public BrResponse<String> createRequestListMembers(@RequestParam int listID, @RequestParam int entityTypeId,@RequestParam int requestId ){
		 CBGermplasmService.saveGermplasListFromCB(listID, entityTypeId, requestId);
		 String str = null;
		 return BrapiResponseBuilder.forData(str)
		.withStatusSuccess()
		.build();
	}
}

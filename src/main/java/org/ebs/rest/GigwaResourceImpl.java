package org.ebs.rest;

import java.util.UUID;

import org.ebs.rest.to.GigwaLoadTO;
import org.ebs.services.GigwaLoader;
import org.ebs.util.brapi.BrResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
public class GigwaResourceImpl implements GigwaResource {

    private final GigwaLoader loader;
    
    @Override
    public ResponseEntity<String> getGenotypes(GigwaLoadTO loadParams) {
        UUID uuid = UUID.fromString(loadParams.getId());
        boolean loaded = loader.loadUnprocessedFiles(uuid);
        if (loaded) {
            log.debug("calling generateRequestResultFiles({})", uuid.toString());
            BrResponse<String> resp = loader.generateRequestResultFiles(uuid);

            return ResponseEntity.ok().body(uuid.toString() + " - " + resp.getResult());
        }
        return ResponseEntity.ok().body(uuid.toString() + " - problem to upload");
    }

}

package org.ebs.rest;

import static org.ebs.model.RequestStatusModel.REQ_STATUS_DESIGN_CREATED;
import static org.ebs.model.RequestStatusModel.REQ_STATUS_FILE_UPLOADED;
import static org.ebs.services.GigwaLoader.getBatchIdFromTags;
import static org.ebs.util.brapi.BrStatus.ERROR_CODE;
import static org.ebs.util.brapi.BrStatus.SUCCESS_CODE;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

import org.ebs.rest.to.FileObject;
import org.ebs.rest.to.GenotypeSearchParams;
import org.ebs.rest.to.GigwaLoadTO;
import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.rest.validation.ResultFileValidator;
import org.ebs.security.EbsUser;
import org.ebs.services.BatchService;
import org.ebs.services.GigwaService;
import org.ebs.services.NotificationService;
import org.ebs.services.custom.BatchDesignService;
import org.ebs.services.custom.CSFileObject;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.SampleGermplasmDetailTo;
import org.ebs.services.to.custom.BatchCodeTo;
import org.ebs.services.to.custom.BatchDesignTo;
import org.ebs.services.to.custom.DesignTo;
import org.ebs.services.to.custom.RequestIDTo;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrStatus;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.ebs.util.client.FileClient;
import org.ebs.util.client.RestCsClient;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
public class BatchResourceImpl implements BatchResource {

    private final BatchService batchService;
    private final BatchDesignService batchDesignService;
    private final NotificationService notificationService;
    private final AsyncTaskExecutor taskExecutorPool;
    private final ResultFileValidator fileValidator;
    private final CSFileObject csFileObjectService;
    private final FileClient fileClient;
    private final GigwaService gigwaService;
    private final RestCsClient csClient;

    @Override
    @GetMapping("list-request-byBatchID")
    public BrPagedResponse<RequestIDTo> getListRequestByBatch(@RequestParam int batchId) {
        return BrapiResponseBuilder.forData(batchService.getRequesByBatchID(batchId))
                .withStatusSuccess()
                .build();
    }

    @Deprecated(since = "2024/03/15")
    @Override
    @PostMapping(value = "createSamplesdeprecated")
    public BrResponse<DesignTo> createSamples(@RequestBody BatchDesignTo bean) {
        return null;
    }

    @Override
    @PostMapping(value = "createSamplesTask")
    public BrPagedResponse<DesignTo> createSamplesByTask(@RequestBody BatchDesignTo bean, Authentication auth) {
        try {
            log.info("Saving batch >> ");
            BatchTo to = batchDesignService.saveBatch(bean);
            log.info("Saved batch >> ");
            // TaskExecutor theExecutor = new SimpleAsyncTaskExecutor();
            taskExecutorPool.execute(new Runnable() {
                @Override
                public void run() {
                    batchDesignService.createSamplesAsynchronous(bean, auth, to);
                    int requestorId = ((EbsUser) auth.getPrincipal()).getId();
                    notificationService.sendNotificationAsBatchManager(requestorId, to.getId(),
                            "The samples of batch [" + bean.getBatch().getName() + "] were created successfully");
                }
            });

        } catch (Exception ex) {
            log.error("Saved batch >> " + ex.getMessage());
            ex.printStackTrace();

            if (bean != null)
                batchService.deleteBatch(bean.getIdBatch());

            if (ex instanceof NoSuchElementException)
                throw new RuntimeException(
                        "Batch or Batch Code or Vendor Control or Collection Does not exist in the Database");
            else
                throw ex;
        }

        return BrapiResponseBuilder.forData(batchService.getListBatchForDesign(0))
                .withStatusSuccess()
                .build();
    }

    @Override
    @GetMapping("list-byBatchIDToDesign")
    public BrPagedResponse<DesignTo> getListBatchForDesign(@RequestParam int batchId) {
        return BrapiResponseBuilder.forData(batchService.getListBatchForDesign(batchId))
                .withStatusSuccess()
                .build();
    }

    @Override
    @GetMapping("createRequestListMemberById")
    public BrResponse<String> createRequestListMemeber(@RequestParam int batchId, @RequestParam int[] requestId) {
        batchDesignService.createRequestListMemberElements(batchId, requestId);
        String str = null;
        return BrapiResponseBuilder.forData(str)
                .withStatusSuccess()
                .build();
    }

    @Override
    @PostMapping("createBatchCode")
    public BrResponse<String> createBatchCode(@RequestBody BatchCodeTo bean) {
        return BrapiResponseBuilder.forData(batchDesignService.createBatchCode(bean))
                .withStatusSuccess()
                .build();
    }

    @Override
    public ResponseEntity<BrPagedResponse<FileObject>> postObject(int tenantId, UploadResultFileRequest data) {
        BrPagedResponse<FileObject> response = null;
        try {
            String errors = fileValidator.findErrors(data);
            log.trace("Errors after validation: {}", errors);
            boolean hasErrors = !errors.isBlank();

            //if has 500 Internal Server Errors that prevent further file processing, return 500 error
            if (hasErrors) {
                BrPagedResponse<FileObject> errorResponse;
                if (errors.startsWith("500: ")) {
                     errorResponse = BrapiResponseBuilder
                            .forData(new PageImpl<>(new ArrayList<FileObject>()))
                            .withStatusError(errors.replace("500: ", ""))
                            .build();
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorResponse);
                }
            }

            response = gigwaService.uploadFile(tenantId, data.getFile(), data.getTags(),
                    data.getDescription(), hasErrors);
            log.trace("File upload response {}", response);
            log.trace("File upload finished with status {}", response.getMetadata().getStatus());

            if (response.getMetadata().getStatus().get(0).getCode() == SUCCESS_CODE) {
                int batchId = getBatchIdFromTags(data.getTags().stream());
                batchDesignService.changeRequestStatusByBatch(batchId, tenantId, REQ_STATUS_FILE_UPLOADED, "");
            } else if (response.getMetadata().getStatus().get(0).getCode() == ERROR_CODE) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
            }
            //if has validation errors that need confirmation, return 409 conflict response, and wait for a postConfirmation
            if (hasErrors) {
                response.getMetadata().getStatus().add(new BrStatus(ERROR_CODE, errors));
                return ResponseEntity.status(HttpStatus.CONFLICT).body(response);
            }
        } catch (Exception e) {
            if (data.getFile() == null) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(BrapiResponseBuilder.forData(new PageImpl<>(new ArrayList<FileObject>()))
                        .withStatusError("No file was selected to upload")
                        .build());
            }
            String keyToDelete = Optional.ofNullable(response)
                    .map(r -> r.getResult())
                    .map(r -> r.getData())
                    .map(d -> d.get(0))
                    .map(f -> f.getKey())
                    .orElse("");
            if (!keyToDelete.isEmpty()) {
                keyToDelete = fileClient.deleteFile(keyToDelete);
                log.warn("removed file {} because preprocessing error: {}", keyToDelete, e.getMessage());
            }
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(BrapiResponseBuilder.forData(new PageImpl<>(new ArrayList<FileObject>()))
                    .withStatusError("Error during file preprocessing: " + e.getMessage())
                    .build());
        }
        // if no validaton errors nor exceptions, call gigwaLoader immediatly
        String fileId = response.getResult().getData().get(0).getKey();
        log.debug("calling loader for file {}:", fileId, response.getResult().getData().get(0));
        String callGwLoaderResp = csClient.callGigwaLoader(new GigwaLoadTO(fileId));
        log.debug("csClient.callGigwaLoader resp: {}", callGwLoaderResp);

        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @Override
    public BrResponse<FileObjectTo> postConfirmation(String fileKey, boolean accepted) {
        UUID uuid = UUID.fromString(fileKey);
        FileObjectTo file = csFileObjectService.findByKey(uuid);
        if (file == null) {
            throw new RuntimeException("File " + fileKey + " with confirmation pending does not exist.");
        } else {
            log.info("confirmation set to {} for file: {}", accepted, file);
        }

        if (accepted) {
            String[] tags = Arrays.copyOf(file.getTags(), file.getTags().length + 1);
            tags[tags.length - 1] = ResultFileValidator.WARNINGS_CONFIRMED_TAG;

            file.setTags(tags);
            file.setStatus(FileClient.STATUS_UPLOADED);
            csFileObjectService.modify(file);
            log.info("Accepting import of file {} with validation warnings. Calling gigwa Loader", file.getName());

            String callGwLoaderResp = csClient.callGigwaLoader(new GigwaLoadTO(fileKey));
            log.debug("csClient.callGigwaLoader resp: {}", callGwLoaderResp);
        } else {
            String deletedKey = fileClient.deleteFile(fileKey);
            log.info("removed file {} [{}] after rejecting upload", file.getName(), deletedKey);
            int batchId = getBatchIdFromTags(Arrays.stream(file.getTags()));
            batchDesignService.changeRequestStatusByBatch(batchId, file.getTenant(), REQ_STATUS_DESIGN_CREATED, "");
        }

        return BrapiResponseBuilder.forData(file).withStatusSuccess().build();
    }

    @Override
    public ResponseEntity<StreamingResponseBody> getGenotypes(GenotypeSearchParams params) {

        log.trace("searching genotypes by: {}", params);

        StreamingResponseBody stream = output -> {
            try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output))) {
                gigwaService.generateGenotypeMatrix(params, writer);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error in getGenotypes with params {} : {}", params, e.getMessage());
                throw e;
            }
        };

        return ResponseEntity.ok()
                .header(CONTENT_DISPOSITION, ContentDisposition.attachment()
                        .filename(gigwaService.createFileName(params))
                        .build().toString())
                .contentType(APPLICATION_OCTET_STREAM)
                .body(stream);
    }

    @Override
    public BrResponse<Map<Integer, List<SampleGermplasmDetailTo>>> getGermpalsmDetails(GenotypeSearchParams params) {
        return BrapiResponseBuilder.forData(batchService.findSampleGermplasmDetails(params)).build();
    }
}

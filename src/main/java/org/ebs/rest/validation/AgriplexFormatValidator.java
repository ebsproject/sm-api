package org.ebs.rest.validation;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.BatchService;
import org.ebs.services.GigwaLoader;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.BatchTo;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.monitorjbl.xlsx.StreamingReader;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class AgriplexFormatValidator implements FormatValidator {
    
    private final int markerColIndex = 4;
    private final int sampleColIndex = 2;
    private final int plateColIndex = 0;

    private final BatchService batchService;
    private final RequestListMemberService listService;

    @Override
    public String validate(UploadResultFileRequest data) {
        log.trace("Agriplex Validator");
        MultipartFile mpFile = data.getFile()[0];
        String validationErrors = "";
        log.trace("validating {} for Agriplex", mpFile.getOriginalFilename());

        try {
            Workbook listBook = StreamingReader.builder().rowCacheSize(50).bufferSize(8192).open(mpFile.getInputStream());
            log.trace("WB created with {} sheets", listBook.getNumberOfSheets());
            Sheet dataSheet = listBook.getSheetAt(0);

            String sample = null;
            boolean dataMatrixStarted = false;
            int totalCalls = 0;

            Map<String, Integer> dataMarkerCount = new HashMap<>(1000);
            Map<String, Integer> dataSampleCount = new HashMap<>(1000);
            List<String> markerList = emptyList();
            Set<String> plateSet = new HashSet<>();

            for (Row row : dataSheet) {
                String markerColValue = Optional.ofNullable(row.getCell(markerColIndex - 1))
                    .map(c -> c.getStringCellValue()).orElse("");
                
                if (markerColValue.equalsIgnoreCase("SNP_ID") || markerColValue.equalsIgnoreCase("AgriPlex ID")) {
                    dataMatrixStarted = true;
                    for (int i = row.getFirstCellNum() + 1; i < row.getLastCellNum(); i++) {
                        dataMarkerCount.merge(row.getCell(i).getStringCellValue(), 1, Integer::sum);
                    }
                    markerList = new ArrayList<>(dataMarkerCount.keySet());
                    log.info("Markers: {}", markerList.size());
                }
                
                String value = "";
                
                Cell c = row.getCell(sampleColIndex);
                if(c != null){
                    switch (c.getCellType()) {
                        case STRING:
                            value = c.getStringCellValue();
                            break;
                        case NUMERIC:
                            value = "" + (long) c.getNumericCellValue();
                            break;
                        default:
                            break;
                    }
                }

                sample = value;

                if (dataMatrixStarted && !(sample.isBlank() || sample.equalsIgnoreCase("Sample_ID"))) {
                    plateSet.add(row.getCell(plateColIndex).getStringCellValue());
                    dataSampleCount.merge(sample, 1, Integer::sum);
                    totalCalls += validateRowCalls(row, sample, markerList);
                }
            }
            log.info("Total plates: {}", plateSet.size());
            log.info("TOTAL Samples: {}", dataSampleCount.size());

            int batchId = GigwaLoader.getBatchIdFromTags(data.getTags().stream());
            BatchTo batch = batchService.findBatch(batchId).orElseThrow(() -> new RuntimeException("Batch not found: " + batchId));
            validationErrors += validateExistingSamples(dataSampleCount.keySet(), batch);
            validationErrors += validateExistingPlates(plateSet, batchId);
            validationErrors += validateExistingMarkers(dataMarkerCount.keySet(), batch);
            validationErrors += validateDuplicatedSamples(dataSampleCount) + validateDuplicatedMarkers(dataMarkerCount);
            
            if(validationErrors.isEmpty()){
                int expectedCalls = dataSampleCount.size() * dataMarkerCount.size();
                String missingCallsMessage = "The data matrix should contain %s elements, but %s were found";
                if (expectedCalls != totalCalls) {
                    validationErrors =  String.format(missingCallsMessage, expectedCalls, totalCalls);
                }
            }
            listBook.close();
        } catch (Exception e) {
            validationErrors = "Cannot validate for Agripplex: " + e.getMessage();
            e.printStackTrace();
        }

        return validationErrors;
    }
    
    File createTmpFile(MultipartFile mpFile) {
        Path p = Path.of(Thread.currentThread().getName() + ".xlsx");
        File tmp = p.toFile();
        try {
            Files.createFile(p);
            OutputStream os = new FileOutputStream(tmp, false);
            mpFile.getInputStream().transferTo(os);
            os.close();
            log.trace("Create file {}, size: {}", tmp.toPath(), tmp.length());
        } catch (IllegalStateException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Error creating tmp file: " + e.getMessage());
        }
        return tmp;
    }

    String validateDuplicatedSamples(Map<String, Integer> dataSampleCount) {
        String dupMessage = " Sample %s is repeated %s time(s).";
        String dupErrors = "";

        dupErrors = dataSampleCount.entrySet().stream()
                .filter(e -> e.getValue() > 1 && !e.getKey().isBlank())
                .map(e -> String.format(dupMessage, e.getKey(), e.getValue() - 1))
                .collect(joining());

        return dupErrors.trim();
    }
    
    String validateDuplicatedMarkers(Map<String, Integer> dataMarkerCount) {

        String dupMessage = " Marker %s is repeated %s time(s).";
        String dupErrors = "";
        dupErrors = dataMarkerCount.entrySet().stream()
                .filter(e -> e.getValue() > 1 && !e.getKey().isBlank())
                .map(e -> String.format(dupMessage, e.getKey(), e.getValue() - 1))
                .collect(joining());
        
        return dupErrors.trim();
    }
    
    int validateRowCalls(Row row, String sample, List<String> markerList) {
        int rowCalls = 0;
        String call = null;
        for (int i = sampleColIndex + 2; i < row.getLastCellNum(); i++) {
            call = Optional.ofNullable(row.getCell(i)).map(c -> c.getStringCellValue()).orElse("");
            if (call.isEmpty()) {
                log.warn("Empty value for sample[{}], maker[{}]", sample, markerList.get(i - (sampleColIndex + 2)));
            } else {
                rowCalls++;
            }
        }
        return rowCalls;
    }

    String validateExistingSamples(Set<String> fileSamples, BatchTo batch) {
        log.trace("validateExistingSamples fileSamples {}, batch {}", fileSamples, batch);
        final StringBuilder error = new StringBuilder("");
        batchService.getRequesByBatchID(batch.getId())
            .map(requestId -> requestId.getId())
            .forEach(reqId -> {
                    List<String> smSamples = listService.findAllSampleNames(batch.getId(), reqId);
                    log.trace("smSamples {}", smSamples.size());
                    if (!fileSamples.containsAll(smSamples)) {
                        smSamples.removeAll(fileSamples);
                        error.append(String.format("Request(id=%s) contains %s samples and results file has %s but there are missing ones: %s. "
                            , reqId, smSamples.size(), fileSamples.size(), smSamples.toString()));
                    }
            });

        log.trace("validateExistingSamples return : {}", error.toString());
        return error.toString();
    }

    String validateExistingMarkers(Set<String> fileMarkers, BatchTo batch) {
        log.trace("validateExistingMarkers fileMarkers {}, batch {}", fileMarkers, batch);
        final StringBuilder error = new StringBuilder("");
        List<String> smMarkers = batchService.findMarkers(batch.getId());

        int smMarkerCount = smMarkers.size();
        if (!fileMarkers.containsAll(smMarkers)) {
            smMarkers.removeAll(fileMarkers);
            error.append(String.format(
                    "Batch(id=%s) contains %s markers and results file has %s but there are missing ones: %s. ",
                    batch.getId(), smMarkerCount, fileMarkers.size(), smMarkers.toString()));
        }
        log.trace("validateExistingMarkers return : {}", error.toString());
        return error.toString();
    }

    String validateExistingPlates(Set<String> plateSet, int batchId) {
        log.trace("validateExistingPlates plateSet {}, batchId {}", plateSet, batchId);
        List<String> plates = batchService.getPlateListByBatchId(batchId);
        log.trace("plates {}", plates);
        plates.removeAll(plateSet);
        String missing = plates.stream()
            .sorted()
            .collect(Collectors.joining(","));
        log.trace("validateExistingPlates return : {}", missing.isEmpty() ? "" : "There are missing plates in the file: " + missing);
        return missing.isEmpty() ? "" : "There are missing plates in the file: " + missing;
    }

    @Override
    public Format key() {
        return Formats.AGRIPLEX;
    }

}

package org.ebs.rest.validation;

import org.ebs.rest.to.UploadResultFileRequest;

public interface FormatValidator {
    /**
     * Checks if a file passes the basic quality controls like no duplicates, complete data grid, etc.
     * @param data the file from a request to be validated plus metadata
     * @return a String with the error(s) found, or empty for a valid file.
     */
    String validate(UploadResultFileRequest data);

    /**
     * defines a unique Format value as key for this converter
     * @return Returns the format this converter supports
     */
    Format key();
}
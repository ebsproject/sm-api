package org.ebs.rest.validation;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.ebs.services.custom.MarkerService;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class Formats {

    private final MarkerService markerService;

    private static int VENDOR_AGRIPLEX_ID;
    private static int VENDOR_DARTAG;
    private static int VENDOR_INTERTEK_ID;

    public static final Format VCF = new Format(0,null);
    public static final Format INTERTEK = new Format(0,null);
    public static final Format AGRIPLEX = new Format(0,null);
    public static final Format DARTAG = new Format(0,null);
    public static final Format HAPMAP = new Format(0, null);
    private static final Map<Integer, Format> formatMap = new HashMap<>(0);
    private static boolean init = false;

    @PostConstruct
    private void init() {
        try {
            markerService.findServiceProviders("(filters:[{col:\"type\" val:\"External\", mod: EQ}])").forEach(sp -> {
                String vendor = sp.getName().toLowerCase();
                if (vendor.contains("agriplex")) {
                    VENDOR_AGRIPLEX_ID = sp.getId();
                    AGRIPLEX.setVendorId(VENDOR_AGRIPLEX_ID);
                    AGRIPLEX.setName(sp.getName());
                } else if (vendor.contains("dart")) {
                    VENDOR_DARTAG = sp.getId();
                    DARTAG.setVendorId(VENDOR_DARTAG);
                    DARTAG.setName(sp.getName());
                } else if (vendor.contains("intertek")) {
                    VENDOR_INTERTEK_ID = sp.getId();
                    VCF.setVendorId(sp.getId());
                    VCF.setName(sp.getName());
                    HAPMAP.setVendorId(sp.getId());
                    HAPMAP.setName(sp.getName());
                    INTERTEK.setVendorId(VENDOR_INTERTEK_ID);
                    INTERTEK.setName(sp.getName());

                }
            });
            formatMap.put(VCF.getVendorId(), VCF);
            formatMap.put(INTERTEK.getVendorId(), INTERTEK);
            formatMap.put(AGRIPLEX.getVendorId(), AGRIPLEX);
            formatMap.put(DARTAG.getVendorId(), DARTAG);
            formatMap.put(HAPMAP.getVendorId(), HAPMAP);
            log.info("Formats set up with MarkerDB info: {}", formatMap);
            init = true;
        } catch(Exception e) {
            log.warn("Could not initialize vendor formats on start up.");
            log.error(e.getMessage());
        }
    }
    
    public Format formatOf(int vendorId) {
        test();
        return formatMap.get(vendorId);
    }
    
    public final int getVENDOR_AGRIPLEX_ID() {
        test();
        return VENDOR_AGRIPLEX_ID;
    }
    
    public final int getVENDOR_DARTAG() {
        test();
        return VENDOR_DARTAG;
    }
    
    public final int getVENDOR_INTERTEK_ID() {
        test();
        return VENDOR_INTERTEK_ID;
    }
    
    void test(){
        if (!init) {
            init();
        }
        if (AGRIPLEX.getVendorId() == 0 || DARTAG.getVendorId() == 0 || INTERTEK.getVendorId() == 0) {
            String error = "Vendor Formats cannot be initialized. Check markerDB connection";
            log.error(error);
            throw new RuntimeException(error);
        }
    }
}

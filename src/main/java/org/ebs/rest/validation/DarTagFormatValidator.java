package org.ebs.rest.validation;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.BatchService;
import org.ebs.services.GigwaLoader;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.BatchTo;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class DarTagFormatValidator implements FormatValidator {

    private final BatchService batchService;
    private final RequestListMemberService listService;

    @Override
    public String validate(UploadResultFileRequest data) {
        MultipartFile value = data.getFile()[0];
        String validationErrors = "";
        try (Scanner scan = new Scanner(value.getInputStream())) {

            int sampleColIndex = 2;
            int sampleRowIndex = 1;
            int markerColIndex = 3;
            int markerRowIndex = 0;
            int plateColIndex = 0;

            List<String[]> rows = new ArrayList<>();
            while (scan.hasNextLine()) {
                rows.add(scan.nextLine().split(","));
            }
            
            Map<String, Integer> dataMarkerCount = new HashMap<>(1000);
            String markers[] = Arrays.copyOfRange(rows.get(markerRowIndex),markerColIndex,rows.get(markerColIndex).length);

            Arrays.stream(markers)
                .forEach(m -> dataMarkerCount.merge(m, 1, Integer::sum));
            
            log.trace("Markers: {}", dataMarkerCount.size());
            System.out.println("Markers: " + dataMarkerCount.size());
            
            Map<String, Integer> dataSampleCount = new HashMap<>(1000);
            Set<String> plateSet = new HashSet<>();

            rows.stream()
                .filter(r -> !r[0].equals("PLATE_ID"))
                .forEach(r -> {
                    dataSampleCount.merge(r[sampleColIndex], 1, Integer::sum);
                    plateSet.add(r[plateColIndex]);
                });
        
            log.info("Samples: {}", dataSampleCount.size());
            log.info("Plates: {}", plateSet.size());
            
            int batchId = GigwaLoader.getBatchIdFromTags(data.getTags().stream());
            BatchTo batch = batchService.findBatch(batchId).get();
            validationErrors += validateExistingSamples(dataSampleCount.keySet(), batch);
            validationErrors += validateExistingPlates(plateSet, batchId);
            validationErrors += validateExistingMarkers(dataMarkerCount.keySet(), batch);
            validationErrors += validateDuplicatedSamples(dataSampleCount) +
                validateDuplicatedMarkers(dataMarkerCount) +
                validateCallMatrix(rows, markerColIndex, sampleRowIndex, dataSampleCount.size() * dataMarkerCount.size(), sampleColIndex, markerRowIndex);


        } catch (IOException e) {
            validationErrors = "Cannot validate for DarTag: " + e.getMessage();
            e.printStackTrace();
        }
        return validationErrors;
    }
    
    String validateDuplicatedSamples(Map<String, Integer> dataSampleCount) {
        String dupMessage = " Sample %s is repeated %s time(s).";
        String dupErrors = "";

        dupErrors = dataSampleCount.entrySet().stream()
                .filter(e -> e.getValue() > 1)
                .map(e -> String.format(dupMessage, e.getKey(), e.getValue() - 1))
                .collect(joining());

        return dupErrors.trim();
    }
    
    String validateDuplicatedMarkers(Map<String, Integer> dataMarkerCount) {

        String dupMessage = " Marker %s is repeated %s time(s).";
        String dupErrors = "";
        dupErrors = dataMarkerCount.entrySet().stream()
                .filter(e -> e.getValue() > 1)
                .map(e -> String.format(dupMessage, e.getKey(), e.getValue() - 1))
                .collect(joining());

        return dupErrors.trim();
    }
    
    String validateCallMatrix(List<String[]> rows, int markerColIndex, int sampleRowIndex, int expectedCalls,
            int sampleColIndex, int markerRowIndex) {
        String missingMessage = "The data matrix should contain %s elements, but %s were found";
        int totalCalls = 0;

        for (int j = sampleRowIndex; j < rows.size(); j++) {
            for (int i = markerColIndex; i < rows.get(j).length; i++) {
                if (rows.get(j)[i].isEmpty()) {
                    log.warn("Empty value for sample[{}], maker[{}]",
                            rows.get(j)[sampleColIndex],
                            rows.get(markerRowIndex)[i]);
                } else {
                    totalCalls++;
                }

                //just for logging purposes
                if (rows.get(j).length < rows.get(markerRowIndex).length && i + 1 == rows.get(j).length) {
                    log.warn("Empty value for sample[{}], maker[{}]",
                            rows.get(j)[sampleColIndex],
                            rows.get(markerRowIndex)[rows.get(markerRowIndex).length - 1]);
                }
            }
        }

        if (expectedCalls != totalCalls) {
            return String.format(missingMessage, expectedCalls, totalCalls);
        }
        return "";

    }
    
    String validateExistingSamples(Set<String> fileSamples, BatchTo batch) {
        final StringBuilder error = new StringBuilder("");
        batchService.getRequesByBatchID(batch.getId())
            .map(requestId -> requestId.getId())
            .forEach(reqId -> {
                    List<String> smSamples = listService.findAllSampleNames(batch.getId(), reqId);
                    int smSampleCount = smSamples.size();
                    if (!fileSamples.containsAll(smSamples)) {
                        smSamples.removeAll(fileSamples);
                        error.append(String.format("Request(id=%s) contains %s samples and results file has %s but there are missing ones: %s. "
                            , reqId, smSampleCount, fileSamples.size(), smSamples.toString()));
                    }
            });

        return error.toString();
    }

    String validateExistingMarkers(Set<String> fileMarkers, BatchTo batch) {
        final StringBuilder error = new StringBuilder("");
        List<String> smMarkers = batchService.findMarkers(batch.getId());

        int smMarkerCount = smMarkers.size();
        if (!fileMarkers.containsAll(smMarkers)) {
            smMarkers.removeAll(fileMarkers);
            error.append(String.format(
                    "Batch(id=%s) contains %s markers and results file has %s but there are missing ones: %s. ",
                    batch.getId(), smMarkerCount, fileMarkers.size(), smMarkers.toString()));
        }
        return error.toString();
    }

    
    String validateExistingPlates(Set<String> plateSet, int batchId) {
        List<String> plates = batchService.getPlateListByBatchId(batchId);
        plates.removeAll(plateSet);
        String missing = plates.stream()
                .sorted()
                .collect(Collectors.joining(","));
        return missing.isEmpty() ? "" : "There are missing plates in the file: " + missing;
    }
    
    @Override
    public Format key() {
        return Formats.DARTAG;
    }

}

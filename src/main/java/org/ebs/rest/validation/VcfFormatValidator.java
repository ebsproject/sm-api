package org.ebs.rest.validation;

import static java.util.stream.Collectors.joining;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.ebs.rest.to.UploadResultFileRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class VcfFormatValidator implements FormatValidator {

    private static final String FORMAT_HEAD = "##fileformat";
    private static final String DATA_HEAD = "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO";

    @Override
    public String validate(UploadResultFileRequest data) {
        MultipartFile value = data.getFile()[0];
        int expectedSamples = 0;
        String validationErrors = "";
        try (Scanner scan = new Scanner(value.getInputStream())) {
            String line = null;
            while (scan.hasNextLine()) {
                line = scan.nextLine();
                if (line.startsWith(FORMAT_HEAD)) {
                    log.trace("format header found: {}", line.split("=")[1].toLowerCase());
                    if (!line.split("=")[1].toLowerCase().contains("vcf")) {
                        log.trace("returning...");
                        return "Unrecognized vcf file";
                    }
                } else if (line.startsWith(DATA_HEAD)) {
                    log.trace("data head found");
                    String[] samples = line.replace(DATA_HEAD, "").replace("\tFORMAT\t", "")
                            .split("\\t");
                    expectedSamples = samples.length;

                    Map<String, Integer> dataSampleCount = new HashMap<>();
                    for (String s : samples) {
                        dataSampleCount.merge(s, 1, Integer::sum);
                    }
                    log.trace("Effective samples: {}", dataSampleCount.size());

                    int expectedMarkers = 0;
                    Map<String, Integer> dataMarkerCount = new HashMap<>();
                    int totalCalls = 0;
                    while (scan.hasNext()) {
                        line = scan.nextLine();
                        String[] lineCalls = line.split("\\t");
                        if (Arrays.asList(Arrays.copyOfRange(lineCalls,9,lineCalls.length - 1)).contains("")) {
                            validationErrors += String.format("Empty call for %s", lineCalls[2]);
                        }

                        dataMarkerCount.merge(lineCalls[2], 1, Integer::sum);
                        expectedMarkers++;
                        totalCalls += lineCalls.length - 9; //9 is the number of header columns
                    }

                    validationErrors += validateDuplicatedSamples(dataSampleCount, expectedSamples)
                            + validateDuplicatedMarkers(dataMarkerCount, expectedMarkers)
                            + validateCallMatrix(expectedSamples, expectedMarkers, totalCalls);

                }
            }
            log.trace(validationErrors);
            return validationErrors;

        } catch (IOException e) {
            validationErrors = "Cannot validate VCF: " + e.getMessage();
            e.printStackTrace();
        }
        return validationErrors;
    }

    
    
    String validateDuplicatedSamples(Map<String, Integer> dataSampleCount, int expectedSamples) {
        String dupMessage = " Sample %s is repeated %s time(s).";
        String dupErrors = "";
        dupErrors = dataSampleCount.entrySet().stream()
            .filter(e -> e.getValue() > 1)
            .map(e -> String.format(dupMessage, e.getKey(), e.getValue() - 1))
            .collect(joining());
                        
        return dupErrors.trim();
    }

    String validateDuplicatedMarkers(Map<String, Integer> dataMarkerCount, int expectedMarkers) {
        String dupMessage = " Marker %s is repeated %s time(s).";
        String dupErrors = "";
        dupErrors = dataMarkerCount.entrySet().stream()
            .filter(e -> e.getValue() > 1)
            .map(e -> String.format(dupMessage, e.getKey(), e.getValue() - 1))
            .collect(joining());
                        
        return dupErrors.trim();
    }

    String validateCallMatrix(int expectedSamples, int expectedMarkers, int totalCalls) {
        int expectedCalls = 0;
        String missingMessage = "The data matrix should contain %s elements, but %s were found";

        log.trace("Expected samples: {}", expectedSamples);
        log.trace("Expected merkers: {}", expectedMarkers);
        expectedCalls = expectedMarkers * expectedSamples;
        log.trace("expected calls: {}", expectedCalls);

        if(expectedCalls != totalCalls) {
            return String.format(missingMessage, expectedCalls, totalCalls);
        }

        return "";
    }

    @Override
    public Format key() {
        return Formats.VCF;
    }
}

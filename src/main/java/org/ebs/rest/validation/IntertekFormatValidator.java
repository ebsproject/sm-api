package org.ebs.rest.validation;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.BatchService;
import org.ebs.services.GigwaLoader;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.BatchTo;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class IntertekFormatValidator implements FormatValidator{

    private static final String DNA_HEAD = "MasterPlate,Density,Barcode";
    private static final String SNP_HEAD = "SNPID,SNPNum,AlleleY,AlleleX,Sequence";
    private static final String DATA_HEAD = "DaughterPlate,MasterPlate,MasterWell,Call,X,Y,SNPID,SubjectID,Norm,Carrier,DaughterWell,LongID,AliquotID";
    private static final String EMPTY_SEQUENCE = ",,,,,,,,,,";

    private final BatchService batchService;
    private final RequestListMemberService listService;

    @Override
    public String validate(UploadResultFileRequest data) {
        MultipartFile value = data.getFile()[0];
        AtomicInteger expectedMarkers = new AtomicInteger(0);
        String validationErrors = "";
        int plateSize = 0;
        int expectedSamples = 0;
        Set<String> expectedPlates = new HashSet<>();

        try (Scanner scan = new Scanner(value.getInputStream())) {
            String line = null;
            while (scan.hasNextLine()) {
                line = scan.nextLine();
                if (line.startsWith("Master plate type")) {
                    plateSize = Integer.parseInt(line.split(",")[1]);
                    log.trace("plate size: {}", plateSize);
                } else if (line.startsWith(DNA_HEAD)) {

                    line = scan.nextLine();
                    while (lineNotEmpty(line)) {
                        String[] lineTokens = line.split(",");
                        expectedSamples += Integer.parseInt(lineTokens[1]);
                        expectedPlates.add(lineTokens[0]);
                        line = scan.nextLine();

                    }
                    log.trace("expected plates: {}", expectedPlates.size());
                }

                else if (line.startsWith(SNP_HEAD)) {

                    line = scan.nextLine();
                    while (lineNotEmpty(line)) {
                        line = scan.nextLine();
                        expectedMarkers.incrementAndGet();
                    }
                }

                else if (line.startsWith(DATA_HEAD)) {
                    Map<String, Map<String, Integer>> dataSampleCount = new HashMap<>();
                    Map<String, Map<String, Integer>> dataMarkerCount = new HashMap<>();
                    int totalCalls = 0;

                    while (scan.hasNextLine()) {
                        line = scan.nextLine();
                        String[] tokens = line.split(",");
                        dataMarkerCount.putIfAbsent(tokens[1], new HashMap<>());
                        dataMarkerCount.get(tokens[1]).merge(tokens[6], 1, Integer::sum);
                        
                        dataSampleCount.putIfAbsent(tokens[1], new HashMap<>());
                        dataSampleCount.get(tokens[1]).merge(tokens[7], 1, Integer::sum);
                        totalCalls++;
                        if (tokens[3].trim().isEmpty()) {
                            validationErrors += String.format("Empty call for %s %s. ", tokens[6], tokens[7]);
                        }

                    }

                    if (dataSampleCount.containsKey("")) {
                        log.trace("missing data samples: {}", dataSampleCount.get(""));
                        dataSampleCount.remove("");
                    }
                    log.trace("data plates: {}", dataSampleCount.size());
                    log.trace("data markers: {}", dataMarkerCount.size());

                    int totalSamples = dataSampleCount.values().stream().map(m -> m.size())
                            .reduce(Integer::sum).get();

                    log.trace("total samples: {}", totalSamples);
                    log.trace("total calls: {}", totalCalls);
                    int emptyCalls = dataSampleCount.values().stream()
                            .map(m -> m.entrySet().stream()
                                .map(e -> e.getKey().equals("") ? e.getValue() : 0)
                                .reduce(Integer::sum).get())
                            .reduce(Integer::sum).get();
                    log.trace("empty calls: {}", emptyCalls);

                    dataSampleCount.forEach((k, v) -> v.remove(""));

                    int realCalls = dataSampleCount.values().stream()
                            .map(m -> m.values().stream().reduce(Integer::sum).get())
                            .reduce(Integer::sum).get();
                    log.trace("actual calls: {}", realCalls);

                    int batchId = GigwaLoader.getBatchIdFromTags(data.getTags().stream());
                    BatchTo batch = batchService.findBatch(batchId).get();
                    validationErrors += validateExistingSamples(allValuesFrom(dataSampleCount), batch);
                    validationErrors += validateExistingMarkers(allValuesFrom(dataMarkerCount), batch);
                    validationErrors += validateExistingPlates(expectedPlates, batchId);
                    
                            validationErrors += validateDuplicatedSamples(dataSampleCount,expectedMarkers.get())
                            + validateDuplicatedMarkers(dataMarkerCount, expectedMarkers.get(), plateSize)
                            + validateCallMatrix(expectedSamples, expectedMarkers.get(), totalCalls);
                   
                    return validationErrors;
                }
            }

        } catch (IOException e) {
            validationErrors = "Cannot validate for Intertek: " + e.getMessage();
            e.printStackTrace();
        }
        return validationErrors;
    }
    
    private boolean lineNotEmpty(String line) {
        return !line.isEmpty() && !line.startsWith(EMPTY_SEQUENCE);
    }


    Set<String> allValuesFrom(Map<String, Map<String, Integer>> data) {
        Set<String> set = new HashSet<>();
        data.values().forEach(v -> set.addAll(v.keySet()));
        return set;
    }
    
    String validateDuplicatedSamples(Map<String, Map<String, Integer>> dataSampleCount, int expectedMarkers) {
        StringBuilder sb = new StringBuilder();
        String dupMessage = " Sample %s is repeated %s time(s) for plate %s.";

        dataSampleCount.entrySet().stream().forEach(p -> p.getValue().entrySet()
            .stream().filter(e -> e.getValue() > expectedMarkers)
            .forEach(e -> sb.append(String.format(dupMessage, e.getKey(),
                        e.getValue() - expectedMarkers, p.getKey()))));
                        
        return sb.toString().trim();
    }

    String validateDuplicatedMarkers(Map<String,Map<String, Integer>> dataMarkerCount, int expectedMarkers, int plateSize) {
        StringBuilder sb = new StringBuilder();
        String dupMessage = " Marker %s is repeated %s time(s) for plate %s";

        dataMarkerCount.entrySet().stream().forEach(p -> p.getValue().entrySet()
            .stream().filter(e -> e.getValue() > plateSize)
            .forEach(e -> sb.append(String.format(dupMessage, e.getKey(),
                        e.getValue() - plateSize, p.getKey()))));

        return sb.toString().trim();
    }

    String validateCallMatrix(int expectedSamples, int expectedMarkers, int totalCalls) {
        int expectedCalls = 0;
        String missingMessage = "The data matrix should contain %s elements, but %s were found";

        log.trace("expected samples: {}", expectedSamples);
        log.trace("expected markers: {}", expectedMarkers);
        expectedCalls = expectedMarkers * expectedSamples;
        log.trace("expected calls: {}", expectedCalls);

        if (expectedCalls != totalCalls) {
            return String.format(missingMessage, expectedCalls, totalCalls);
        }
        return "";
    }

    String validateExistingSamples(Set<String> fileSamples, BatchTo batch) {
        final StringBuilder error = new StringBuilder("");
        batchService.getRequesByBatchID(batch.getId())
            .map(requestId -> requestId.getId())
            .forEach(reqId -> {
                    List<String> smSamples = listService.findAllSampleNames(batch.getId(), reqId);
                    int smSampleCount = smSamples.size();
                    if (!fileSamples.containsAll(smSamples)) {
                        log.info("smSamples: " + smSamples);
                        log.info("fileSamples: " + fileSamples);
                        smSamples.removeAll(fileSamples);
                        error.append(String.format("Request(id=%s) contains %s samples and results file has %s but there are missing ones: %s. "
                            , reqId, smSampleCount, fileSamples.size(), smSamples.toString()));
                    }
            });

        return error.toString();
    }

    String validateExistingMarkers(Set<String> fileMarkers, BatchTo batch) {
        final StringBuilder error = new StringBuilder("");
        List<String> smMarkers = batchService.findMarkers(batch.getId());

        int smMarkerCount = smMarkers.size();
        if (!fileMarkers.containsAll(smMarkers)) {
            smMarkers.removeAll(fileMarkers);
            error.append(String.format(
                    "Batch(id=%s) contains %s markers and results file has %s but there are missing ones: %s. ",
                    batch.getId(), smMarkerCount, fileMarkers.size(), smMarkers.toString()));
        }
        return error.toString();
    }

    String validateExistingPlates(Set<String> plateSet, int batchId) {
        List<String> plates = batchService.getPlateListByBatchId(batchId);
        plates.removeAll(plateSet);
        String missing = plates.stream()
                .sorted()
                .collect(Collectors.joining(","));
        return missing.isEmpty() ? "" : "There are missing plates in the file: " + missing;
    }

    @Override
    public Format key() {
        return Formats.INTERTEK;
    }
}

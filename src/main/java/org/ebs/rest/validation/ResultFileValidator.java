package org.ebs.rest.validation;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.BatchService;
import org.ebs.services.GigwaLoader;
import org.ebs.services.to.BatchTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ResultFileValidator {

    private final Map<Format, FormatValidator> validatorsMap = new HashMap<>();
    private final BatchService batchService;
    private final Formats formats;
    private static final Pattern EXTENSION_PATTERN = Pattern.compile("\\.(\\w+)$");
    public static final String CONFIRM_WARNINGS_TAG = "confirm-warnings";
    public static final String WARNINGS_CONFIRMED_TAG = "warnings-confirmed";

    @Autowired
    public ResultFileValidator(List<FormatValidator> formatValidators, BatchService batchService, Formats formats) {
        formatValidators.forEach(f -> validatorsMap.putIfAbsent(f.key(), f));
        log.debug("File Format Validators registered: {}", formatValidators.size());

        this.batchService = batchService;
        this.formats = formats;
    }

    public String findErrors(UploadResultFileRequest data) {
        log.debug("looking up errors in {}", data);
        int batchId = GigwaLoader.getBatchIdFromTags(data.getTags().stream());
        Optional<BatchTo> batch = batchService.findBatch(batchId);
        String errors = "";
        if (batch.isEmpty()) {
            errors = "500: Batch does not exist: " + batchId;
            log.warn(errors);
            return errors;
        }
        Format format = inferFormat(data.getFile()[0]);
        log.trace("findErrors - format inferred: {}", format);
        if (format == null) {
            errors = "500: Cannot validate unrecognized file format";
            log.warn(errors);
            return errors;
        }
        
        if (batch.get().getVendorId() != format.getVendorId() && format != Formats.HAPMAP) {
            errors = "500: Batch uses vendor " + formats.formatOf(batch.get().getVendorId()).getName() + " but file has format " + format.getName();
            log.warn(errors);
            return errors;
        }
        errors = validatorsMap.get(format).validate(data);
        if (!errors.isEmpty()) {
            log.warn(errors);
        }
        return errors;
    }
    
    private Format inferFormat(MultipartFile file) {
        log.trace("Inferring file format for {}", file.getOriginalFilename());
        String fileExtension = getFileExtension(file);
        Format f = null;

        switch (fileExtension) {
            case "csv":
                f = isDarTagCsv(file) ? Formats.DARTAG : Formats.INTERTEK;                
                break;
            case "vcf":
                f = Formats.VCF;
                break;
            case "xlsx":
                f = Formats.AGRIPLEX;
                break;
            case "intertek":
                f = Formats.INTERTEK;
            case "hapmap":
                f = Formats.HAPMAP;
                break;
            default:
                break;
        }
        log.trace("inferred format for extension {}: {}", fileExtension, f);
        return f;
    }
    
    String getFileExtension(MultipartFile file) {
        Matcher matcher = EXTENSION_PATTERN.matcher(file.getOriginalFilename());
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return "";
        }
    }

    /**
     * Determines if a csv file is in darTag or intertek format
     * @param value
     * @return
     */
    boolean isDarTagCsv(MultipartFile file) {
        try (Scanner s = new Scanner(file.getInputStream())) {
            return s.hasNextLine() && s.nextLine().startsWith("PLATE_ID,WELL,SUBJECT_ID,");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}

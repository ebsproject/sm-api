package org.ebs.rest;

import org.ebs.services.to.custom.BatchDesignTo;

public interface LaboratoryReport {

	byte[]  createLaboratoryReport(BatchDesignTo bean);
}

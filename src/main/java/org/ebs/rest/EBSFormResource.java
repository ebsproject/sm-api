package org.ebs.rest;

import org.ebs.services.to.EbsFormTo;
import org.ebs.util.brapi.BrResponse;
import org.springframework.web.bind.annotation.RequestParam;
public interface EBSFormResource {

	
	BrResponse<EbsFormTo> getListEBSForm(@RequestParam int id);
	
}

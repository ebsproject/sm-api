package org.ebs.rest;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.Map;

import org.ebs.services.report.ServiceReportKBio;
import org.ebs.services.to.custom.ReportIntertekTo;
import org.ebs.util.client.RestCsClient;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class VendorReportIntertekImpl implements VendorReportIntertek{

    private final ServiceReportKBio serviceReportKBio;
    private final RestCsClient csClient;

    @Override
    public ResponseEntity<InputStreamResource> createReportIntertekFormat(ReportIntertekTo to) {
        return ResponseEntity.ok()
                .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION)
                .header(HttpHeaders.CONTENT_DISPOSITION, ContentDisposition.attachment()
                        .filename(getFileName(to))
                        .build().toString())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(new InputStreamResource(
                        new ByteArrayInputStream(serviceReportKBio.getBytesReportEIBDARTAGIntertekOrderExtraction(to))));
    }
    
    String getFileName(ReportIntertekTo to) {
        Map<String, String> args = new HashMap<>();
        args.put("12", String.valueOf(to.getBatchId()));
        args.put("16", String.valueOf(to.getVendorId()));

        return csClient.getSequence(RestCsClient.VENDOR_FILE_RULE_ID, args) + ".xls";
    }
}

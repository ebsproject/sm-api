package org.ebs.rest;

import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.ebs.rest.to.FileObject;
import org.ebs.rest.to.GenotypeSearchParams;
import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.SampleGermplasmDetailTo;
import org.ebs.services.to.custom.BatchCodeTo;
import org.ebs.services.to.custom.BatchDesignTo;
import org.ebs.services.to.custom.DesignTo;
import org.ebs.services.to.custom.RequestIDTo;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@SecurityRequirement(name = "Bearer Token")
@Validated
@RequestMapping("batch")
public interface BatchResource {

	
	BrPagedResponse<RequestIDTo>  getListRequestByBatch(int batchId);
	
	BrResponse<DesignTo> createSamples(BatchDesignTo bean);
	
	BrPagedResponse<DesignTo> createSamplesByTask(@RequestBody BatchDesignTo bean, Authentication principal);

	BrPagedResponse<DesignTo>  getListBatchForDesign (@RequestParam int batchId);
	
	BrResponse<String>  createRequestListMemeber(@RequestParam int batchId,@RequestParam int []  requestId ); 
	
    BrResponse<String> createBatchCode(BatchCodeTo bean);

    @Operation(description = "Processes and stores files")
    @PostMapping("upload-results/{tenantId}")
    ResponseEntity<BrPagedResponse<FileObject>> postObject(
        @PathVariable @Min(1) int tenantId, 
        @ModelAttribute UploadResultFileRequest data);

    @Operation(description = "Confirms that a file with validation errors must be imported in Gigwa")
    @PutMapping("upload-results/{fileKey}")
    BrResponse<FileObjectTo> postConfirmation(
        @PathVariable @NotEmpty String fileKey, @RequestParam(name = "accepted") boolean accepted);
    

    @Operation(description = "Gets a file with the data matrix for a given search criteria")
    @PostMapping(value =  "genotype", produces = APPLICATION_OCTET_STREAM_VALUE)
    ResponseEntity<StreamingResponseBody> getGenotypes(@RequestBody GenotypeSearchParams params);

    @Operation(description = "returns sample germplasm detail info for either a request, plots, packages or germplasm")
    @PostMapping(value =  "sample-germplasm")
    BrResponse<Map<Integer,List<SampleGermplasmDetailTo>>> getGermpalsmDetails(@RequestBody GenotypeSearchParams params);
    

}

package org.ebs.rest;

import org.ebs.services.custom.LaboratoryReportService;
import org.ebs.services.to.custom.BatchDesignTo;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("laboratory-report")
@RequiredArgsConstructor
public class LaboratoryReportImpl implements LaboratoryReport{

	private final LaboratoryReportService laboratoryReportService;
	
	@Override
	@PostMapping(value="createLaboratoryReport", consumes = MediaType.APPLICATION_JSON_VALUE)
	public byte[] createLaboratoryReport(@RequestBody BatchDesignTo bean) {
		return laboratoryReportService.createReportLaboratory(bean);
	}

}

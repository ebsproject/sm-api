package org.ebs.rest.to;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor
public class FileObject {
    
    private String key;
    private String name;
    private int ownerId;
    private long size;
    private List<String> tags;
}

package org.ebs.rest.to;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import lombok.ToString;

@Data
public class UploadResultFileRequest {
    @ToString.Exclude
    MultipartFile[] file;
    List<String> tags;
    String description;
}

package org.ebs.rest.to;

import java.util.HashSet;
import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenotypeSearchParams {
    Integer requestId;
    Set<Integer> plotIds = new HashSet<>();
    Set<Integer> packageIds = new HashSet<>();
    Set<Integer> germplasmIds = new HashSet<>();
    Set<String> sampleIds = new HashSet<>();

    public GenotypeSearchParams(Integer requestId) {
        this.requestId = requestId;
    }

    public boolean hasIdFilter() {
        return !plotIds.isEmpty() || !packageIds.isEmpty() || !germplasmIds.isEmpty() ||
            !sampleIds.isEmpty();
    }
    public boolean hasPlotFilter() {
        return !plotIds.isEmpty();
    }
    public boolean hasPackageFilter() {
        return !packageIds.isEmpty();
    }
    public boolean hasGermplasmFilter() {
        return !germplasmIds.isEmpty();
    }
    public boolean hasSampleFilter() {
        return !sampleIds.isEmpty();
    }
    public boolean hasRequestFilter() {
        return requestId != null && requestId > 0;
    }
}

package org.ebs.rest;

import org.ebs.services.EbsFormService;
import org.ebs.services.to.EbsFormTo;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("EbsForm")
@RequiredArgsConstructor
public class EBSFormResourceImpl implements EBSFormResource{

	private final EbsFormService ebsformService;

	@Override
	 @GetMapping("list-EBSForm")
	public BrResponse<EbsFormTo> getListEBSForm(@RequestParam int id) {
		try {
	
		return BrapiResponseBuilder.forData(ebsformService.findEbsFormById( id))
				 .withStatusSuccess()
		            .build();
		}catch(Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

}

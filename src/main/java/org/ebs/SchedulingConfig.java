package org.ebs;

import static java.util.Optional.ofNullable;
import static org.ebs.services.GigwaService.DEFAULT_MODULE_NAME;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.ebs.util.client.GigwaClient;
import org.ebs.util.client.GigwaModule;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
@Slf4j
public class SchedulingConfig {

    @Value("${ebs.gigwa.loader.enabled}")
    private boolean gigwaLoaderEnabled = false;
    private final GigwaClient gigwaClient;
    private boolean hasRun = false;

    @PostConstruct
    private void logState() {
        log.info("Gigwa Loader enabled: {}", gigwaLoaderEnabled);
        initGigwaDb();
    }
    
    void initGigwaDb() {
        try{
            Optional<GigwaModule> module = ofNullable(gigwaClient.listModules())
                .map(map -> map.get(DEFAULT_MODULE_NAME));
            
            if (module.isEmpty()) {
                gigwaClient.createModule(DEFAULT_MODULE_NAME);
            }
            gigwaClient.setModuleVisibility(DEFAULT_MODULE_NAME, false, true);
            hasRun = true;
        } catch (Exception e) {
            log.warn("Cannot connect to Gigwa for initial check up: {}", e.getMessage());
        }
    }

    @Scheduled(fixedDelayString = "300000")
    public void init() {
        if(!hasRun){
            initGigwaDb();
        }
    }



}

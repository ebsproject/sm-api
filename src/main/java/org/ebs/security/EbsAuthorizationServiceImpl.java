package org.ebs.security;

import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.util.Collections.singletonList;
import static org.ebs.Application.REQUEST_USER;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

import java.net.URI;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.ebs.security.to.EbsUserProfile;
import org.ebs.util.brapi.TokenGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class EbsAuthorizationServiceImpl implements EbsAuthorizationService {
    
    static private final Logger log = LoggerFactory.getLogger(EbsAuthorizationServiceImpl.class);

    private static Map<String, Entry<Instant, EbsUserProfile>> tmpProfile = new HashMap<>();

    @Value("${ebs.cs.graphql.endpoint}")
    private String profileEndpoint = null;
    private static final String profileResource = "/user/username/{username}";
    
    private RestTemplate template;
    private MultiValueMap<String, String> headers;

    private TokenGenerator tokenGenerator;
    
    @Autowired
    public EbsAuthorizationServiceImpl(TokenGenerator tokenGenerator) {
        this.tokenGenerator = tokenGenerator;

        template = new RestTemplate();
        headers = new LinkedMultiValueMap<>();
        headers.add(CONTENT_TYPE, "application/json");
        headers.add(AUTHORIZATION, "NO_AUTH");

    }

    /**
     * Get userpofile from EBS
     * @param token to be searched
     * @return user profile provided by EBS core services  or null if not found
     */
    @Override
    public EbsUserProfile getUserProfile() {
        String userName = REQUEST_USER.get() != null ?REQUEST_USER.get() :((EbsUser)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Entry<Instant,EbsUserProfile> cachedProfile = tmpProfile.getOrDefault(userName, null);
        
        if (cachedProfile == null
                || cachedProfile.getKey().plus(10, MINUTES).isBefore(now())) {
            tmpProfile.put(userName, Map.entry(Instant.now(), get(profileEndpoint.replace("/graphql",""), profileResource,
                    Map.of("username", userName), EbsUserProfile.class, tokenGenerator.getToken() )));
        }
        return tmpProfile.get(userName).getValue();
    }

    /**
     * Makes a GET request
     * @param endpoint base of the URI
     * @param resourcePath template of the web/api resource
     * @param variables to replace in resourcePath or to embed in request body
     * @param returnedClass class of the response type
     * @return an R instance of the returnedClass
     */
    private <R> R get(String endpoint, String resourcePath, Map<String,String> variables, Class<R> returnedClass, String token) {
        ResponseEntity<R> response = null;
        try {
            log.trace("Calling {}{}", endpoint, resourcePath);
            
            URI uri = uriFor(endpoint, resourcePath, variables);
            response = template.exchange(uri, HttpMethod.GET, requestFor(null, token), returnedClass);

        } catch(Exception e) {
            log.error("Could not invoke service: {}{}. Cause: {}", endpoint, resourcePath,
                    e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        return response.getBody();
    }


    /**
     * 
     * @param body of the request. Can be null for GET requests
     * @return HTTP Entity with authorization set
     */
    private HttpEntity<Object> requestFor(Object body, String token) {
        headers.replace(AUTHORIZATION, singletonList("Bearer " + token));
        return new HttpEntity<>(body, headers);
    }

    /**
     * Creates the URI for a GET call
     * @param endpoint base of the URI
     * @param resourcePath path of a specific API resource, may have placeholders. Example: api/{placeholder1}/{placeholder2}
     * @param variables values to be replaced in the resourcePath template, if any
     * @return a URI for the requested resource with resolved placeholders
     */
    private URI uriFor(String endpoint, String resourcePath, Map<String,String> variables) {
        String resolvedPath = resourcePath;
        for (String key : variables.keySet()) {
            resolvedPath = resolvedPath.replace("{" + key + "}", variables.get(key));
        }
        return URI.create(endpoint + resolvedPath);
    }

}

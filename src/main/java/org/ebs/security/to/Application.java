package org.ebs.security.to;

import java.util.List;

import lombok.Data;

@Data
public class Application {

    private int id;
    private String domain;
    private String prefix;
    private List<Product> products;    
}

package org.ebs.security.to;

import java.util.List;

import lombok.Data;

@Data
public class Permission {

    private List<Application> applications;
    private Membership memberOf;
    
}

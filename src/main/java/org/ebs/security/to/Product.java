package org.ebs.security.to;

import java.util.List;

import lombok.Data;

@Data
public class Product {
    
    private int id;
    private String name;
    private String description;
    private List<Action> dataActions;
    private List<Action> actions;
    
}



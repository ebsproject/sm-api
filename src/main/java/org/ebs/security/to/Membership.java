package org.ebs.security.to;

import java.util.List;

import lombok.Data;

@Data
public class Membership {
    private List<Member> units;
    private List<Member> programs;
    private List<String> roles;
}

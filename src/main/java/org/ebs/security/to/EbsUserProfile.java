package org.ebs.security.to;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class EbsUserProfile {
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("service_providers")
    private List<ServiceProvider> serviceProviders;
    private String account;
    private Permission permissions;
    private int dbId;
}

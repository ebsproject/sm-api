package org.ebs.security.to;

import java.util.List;

import lombok.Data;

/**
 * Abstract entity that represents
 */
@Data
public class Member {
    String id;
    String name;
    String code;
    List<String> teams;
}

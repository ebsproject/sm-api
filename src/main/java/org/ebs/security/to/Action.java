package org.ebs.security.to;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Action {
    @JsonProperty("General")
    private List<String> general;

}

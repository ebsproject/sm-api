package org.ebs.security;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.ebs.Application;
import org.ebs.security.to.EbsUserProfile;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * Service required by Spring Security to obtain additional information about
 * users
 *
 * @author jarojas
 *
 */
@Service
@Profile({ "default", "prod" })
@Log4j2
@RequiredArgsConstructor
class UserDetailsServiceImpl implements UserDetailsService {

    private final EbsAuthorizationService authorizationService;

    @PostConstruct
    void init() {
        log.info("Creating UserDetailsService filter for PRODUCTION");
    }

	@Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (!username.equals(Application.REQUEST_USER.get())) {
            throw new RuntimeException(
                    "Cannot load user details: Provided user is not the same authorizing the request.");
        }

        EbsUserProfile profile = authorizationService.getUserProfile();

        List<GrantedAuthority> auths = new ArrayList<>();

        profile.getPermissions().getMemberOf().getRoles().forEach(role -> {
            auths.add(new SimpleGrantedAuthority("ROLE_" + role.toUpperCase()));
        });
        
        EbsUser user = new EbsUser(profile.getDbId(), username, auths, true, Application.REQUEST_TOKEN.get());
        log.trace("Authorities set for {}", user);
        return user;
    }

}

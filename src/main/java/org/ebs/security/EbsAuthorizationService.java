package org.ebs.security;

import org.ebs.security.to.EbsUserProfile;

public interface EbsAuthorizationService {
    /**
     * Returns the user profile of the user performing the current request
     * @return the user profile from CS-API
     */
    EbsUserProfile getUserProfile();
}

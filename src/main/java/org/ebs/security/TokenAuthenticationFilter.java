package org.ebs.security;

import static java.time.Instant.now;
import static org.ebs.Application.REQUEST_TOKEN;
import static org.ebs.Application.REQUEST_USER;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;

/**
 * Helper Filter for authentication. If a valid token is found in the request, a
 * valid Authentication is set in Spring Security Context during the life of
 * this request.
 *
 * @author jarojas
 *
 */
@Component
@Profile({"default", "prod"})
class TokenAuthenticationFilter extends AbstractAuthenticationFilter {

    private final static Logger LOG = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

    private Set<String> excludedPaths = Set.of("/favicon.ico","/actuator","/playground","/api-doc","/swagger-ui");


	    @Autowired
	    public TokenAuthenticationFilter(UserDetailsService userDetailsService) {
			super(userDetailsService);
            LOG.info("Creating authentication filter for PRODUCTION");
		}

    @Override
    public String getUsername(HttpServletRequest request) {

        String username = null;
        REQUEST_TOKEN.set(null);
        REQUEST_USER.set(null);

        if (isExcludedRequest(request))   return null;
        try {
            
            String token = extractToken(request);

            if (isTokenExpired(token)) {
                throw new RuntimeException("Authorization token has expired");
            }

            REQUEST_TOKEN.set(token);
            DecodedJWT jwt = JWT.decode(REQUEST_TOKEN.get());
            username = Optional.ofNullable(jwt.getClaim("http://wso2.org/claims/emailaddress").asString())
                    .orElseGet(() -> jwt.getClaim("email").asString());

            if (username == null)
                throw new RuntimeException("Claim 'emailaddress' not found");
            REQUEST_USER.set(username.toLowerCase());
        } catch (Exception e) {
            REQUEST_TOKEN.set(null);
            REQUEST_USER.set(null);
             LOG.error("Error reading username from token: {}", e.getMessage());
        }

        return REQUEST_USER.get();
    }
    
    boolean isExcludedRequest(HttpServletRequest req) {
        
        if (req.getServletPath().startsWith("/actuator/loggers") && req.getMethod().equals("POST")) {
            return false;
        }
        
        boolean isExcluded = false;
        String referer = req.getHeader("referer");

        for(String ex : excludedPaths){
            if (req.getMethod().equals("OPTIONS")
                    || req.getServletPath().startsWith(ex) 
                    || (referer != null && referer.contains(ex))) {
                isExcluded = true;
                break;
            }
        }

        return isExcluded;
    }

    private String extractToken(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION))
                .filter(header -> header.startsWith("Bearer "))
                .map(header -> header.substring(7))
                .orElseThrow(() -> new IllegalArgumentException("Could not extract token from request, check Authorization header"));
    }
    
    private boolean isTokenExpired(String token) {
        Date expiration = JWT.decode(token).getExpiresAt();
        return expiration.toInstant().isBefore(now());
    }

}
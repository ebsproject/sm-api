package org.ebs.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:21 AM
 */
@Entity
@Table(name = "Request", schema = "sample")
@Getter
@Setter
@NoArgsConstructor
public class RequestModel extends Auditable {

    private static final long serialVersionUID = -136055010;
	
    public RequestModel(int id) {
        this.id = id;
    }

	@Column(name="admin_Contact_Id")
	private Integer adminContactId;
	
	@Column(name="requester_Owner_Id")
	private Integer requesterOwnerId;
	
	@Column (name= "germplasm_owner_id")
	private Integer germplasmOwnerId;
	
	@Column(name="completed_By")
	private Date completed_By;
	
	@Column(name="crop_id")
	private Integer idCrop;
	
	@Column(name="note")
	private String note;	
	
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	
	@Column(name="list_Id")
	private int listId;
	
	@Column(name="program_id")
	private Integer idProgram;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="purpose_id")
	private PurposeModel purpose;
	
	@Column(name="request_Code")
	private String requestCode;
	
	@OneToMany(mappedBy = "request",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<RequestListMemberModel> requestlistmembers;
	
	@OneToMany(mappedBy = "request",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<RequestStatusModel> requeststatuss;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="service_id")
	private ServiceModel service;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="service_provider_id")
	private ServiceProviderModel serviceprovider;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="service_type_id")
	private ServiceTypeModel servicetype;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="status_id")
	private StatusModel status;
	
	@Column(name="submition_Date")
	private Date submitionDate;
	
	@Column(name="tenant_id")
	private int tenant;
	
	@Column(name="total_Entities")
	private int totalEntities;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="form_id")
	private FormModel form;
	
	@OneToMany(mappedBy = "request",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ValueModel> values;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="tissue_type_id")
	private TissueTypeModel tissuetype;
	
	//@OneToOne(mappedBy = "request",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	@OneToMany(mappedBy = "request",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<RequestMarkerGroupModel> requestmarkergroups;

	@Column(name= "samples_per_item")
	private Integer samplesperItem;
	
	
	@OneToMany(mappedBy = "request",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<RequestTraitModel> requestTraits;
	
	@Override
	public String toString(){
		return "RequestModel [listId=" + listId + ",totalEntities=" + totalEntities + ",id=" + id + " samplesperItem > "+samplesperItem+",]";
	}

}
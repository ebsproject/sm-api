///////////////////////////////////////////////////////////
//  FormTypeModel.java
//  Macromedia ActionScript Implementation of the Class FormTypeModel
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:18:58 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:18:58 PM
 */
@Entity @Table(name="Form_Type",schema="ebsform") @Getter @Setter
public class FormTypeModel extends Auditable {

	@OneToMany(mappedBy = "formtype",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<EbsFormModel> ebsforms;
	@OneToMany(mappedBy = "formtype",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<FormModel> forms;
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	@Column(name="name")
	private String name;
	private static final long serialVersionUID = 175158771;
	@Column(name="tenant_id")
	private int tenant;

	@Override
	public String toString(){
		return "FormTypeModel [id=" + id + ",]";
	}

}
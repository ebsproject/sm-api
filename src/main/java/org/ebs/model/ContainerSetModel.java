///////////////////////////////////////////////////////////
//  ContainerSetModel.java
//  Macromedia ActionScript Implementation of the Class ContainerSetModel
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:04 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:04 AM
 */
@Entity @Table(name="Container_Set",schema="sample") @Getter @Setter
public class ContainerSetModel extends Auditable {

	@ManyToOne(fetch=FetchType.LAZY, optional =false) @JoinColumn(name="batch_id")
	private BatchModel batch;
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="labcontrol_id")
	private LabControlModel labcontrol;
	@Column(name="name")
	private String name;
	@OneToMany(mappedBy = "containerset",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<SampleDetailModel> sampledetails;
	private static final long serialVersionUID = 467515032;
	@OneToMany(mappedBy = "containerset",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<StockModel> stocks;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="storagelocation_id")
	private StorageLocationModel storagelocation;
	@Column(name="tenant_id")
	private int tenant;

	@Override
	public String toString(){
		return "ContainerSetModel [id=" + id + ",]";
	}

}
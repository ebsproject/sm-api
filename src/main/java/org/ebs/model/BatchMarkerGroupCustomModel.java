package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity @Table(name="batch_marker_group_custom",schema="sample") @Getter @Setter
public class BatchMarkerGroupCustomModel  extends Auditable {

	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	
	@Column(name="marker_id")
	private Integer MarkerId;
	
	@Column(name="assay_id")
	private Integer AssayId;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="batch_marker_group_id")
	private BatchMarkerGroupModel batchMarkerGroup;
	
	@Override
	public String toString(){
		return "BatchMarkerGroupCustom [MarkerId = "+MarkerId + " batchMarkerGroupl [id="+batchMarkerGroup.getId() +"]]";
	}
}

package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "trait_category_purpose", schema = "services")
@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor
public class TriatCategoryPurposeModel extends Auditable{

	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	
	@Column(name="trait_category_id")
	private Integer traitCategoryId;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =false) @JoinColumn(name="purpose_id")
	private PurposeModel purpose;

	@Override
	public String toString(){
		return "TriatCategoryPurposeModel [traitCategoryId=" + traitCategoryId + ",id=" + id + ",]";
	}
}

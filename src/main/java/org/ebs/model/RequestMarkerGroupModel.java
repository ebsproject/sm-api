package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity @Table(name="Request_Marker_Group",schema="sample") @Getter @Setter
public class RequestMarkerGroupModel  extends Auditable {

	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	
	
	@Column(name="marker_group_id")
	private Integer markerGroupID;
	
	//@OneToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="request_id")
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="request_id")
	private RequestModel request;

	@Override
	public String toString(){
		return "RequestMarkerGroupModel [MarkerGroupID = "+markerGroupID + " request [id="+request.getId() +"]]";
	}
}

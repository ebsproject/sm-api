///////////////////////////////////////////////////////////
//  FormModel.java
//  Macromedia ActionScript Implementation of the Class FormModel
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:18:57 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:18:57 PM
 */
@Entity @Table(name="Form",schema="customform") @Getter @Setter
public class FormModel extends Auditable {

	@Column(name="description")
	private String description;
	@OneToMany(mappedBy = "form",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<FieldModel> fields;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="form_type_id")
	private FormTypeModel formtype;
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	@Column(name="name")
	private String name;
	@OneToMany(mappedBy = "form",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<RequestModel> requests;
	private static final long serialVersionUID = -152621314;
	@Column(name="tenant_id")
	private int tenant;

	@Override
	public String toString(){
		return "FormModel [id=" + id + ",]";
	}

}
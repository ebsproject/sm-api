package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
@Entity @Table(name="batch_marker_group",schema="sample") @Getter @Setter
public class BatchMarkerGroupModel extends Auditable  {
	
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	
	@Column(name="marker_group_id")
	private Integer markerGroupID = 0;
	
	@Column(name="technology_service_provider_id")
	private Integer technologyServiceProviderId = 0;
	
	@Column(name="is_fixed")
	private  boolean isFixed;

	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="batch_id")
	private BatchModel batch;
	
	@OneToMany(mappedBy = "batchMarkerGroup",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<BatchMarkerGroupCustomModel> batchMarkerGroupCustom;
	
	@Override
	public String toString(){
		return "BatchMarkerGroupModel [markerGroupID = "+markerGroupID + " batch [id="+batch.getId() +"]]";
	}
}

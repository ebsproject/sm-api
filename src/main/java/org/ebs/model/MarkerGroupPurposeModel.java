package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Marker_Group_Purpose", schema = "services")
@Getter
@Setter
@NoArgsConstructor @AllArgsConstructor
public class MarkerGroupPurposeModel extends Auditable {
    
	private static final long serialVersionUID = 238853967;
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	@Column(name="marker_Group_Id")
	private int markerGroupId;
	@ManyToOne(fetch=FetchType.LAZY, optional =false) @JoinColumn(name="purpose_id")
	private PurposeModel purpose;
	//@Column(name="tenant_id")
	//private int tenant;

	@Override
	public String toString(){
		return "MarkerGroupPurposeModel [markerGroupId=" + markerGroupId + ",id=" + id + ",]";
	}
}

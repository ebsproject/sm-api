///////////////////////////////////////////////////////////
//  CollectionLayoutRepository.java
//  Macromedia ActionScript Implementation of the Interface CollectionLayoutRepository
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:03 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.CollectionLayoutModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:03 AM
 */
public interface CollectionLayoutRepository extends JpaRepository<CollectionLayoutModel,Integer>, RepositoryExt<CollectionLayoutModel> {

	/**
	 * 
	 * @param collectioncontainertypeId
	 */
	public List<CollectionLayoutModel> findByCollectioncontainertypeId(int collectioncontainertypeId);

}
package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.RequestTraitModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestTraitRepository extends JpaRepository<RequestTraitModel,Integer>, RepositoryExt<RequestTraitModel> {

	
	public List<RequestTraitModel> findByRequestId(int requestId);

}

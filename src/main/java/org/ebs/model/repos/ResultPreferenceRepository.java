///////////////////////////////////////////////////////////
//  ResultPreferenceRepository.java
//  Macromedia ActionScript Implementation of the Interface ResultPreferenceRepository
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:25 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.ResultPreferenceModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:25 AM
 */
public interface ResultPreferenceRepository extends JpaRepository<ResultPreferenceModel,Integer>, RepositoryExt<ResultPreferenceModel> {

	/**
	 * 
	 * @param serviceproviderId
	 */
	public List<ResultPreferenceModel> findByServiceproviderId(int serviceproviderId);

}
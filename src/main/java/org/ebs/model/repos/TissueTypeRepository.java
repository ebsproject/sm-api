///////////////////////////////////////////////////////////
//  TissueTypeRepository.java
//  Macromedia ActionScript Implementation of the Interface TissueTypeRepository
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:42 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model.repos;

import org.ebs.model.TissueTypeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:42 AM
 */
public interface TissueTypeRepository extends JpaRepository<TissueTypeModel,Integer>, RepositoryExt<TissueTypeModel> {

}
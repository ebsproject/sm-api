package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.MarkerGroupPurposeModel;
import org.ebs.model.TriatCategoryPurposeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TriatCategoryPurposeRepository extends JpaRepository<TriatCategoryPurposeModel,Integer>, RepositoryExt<MarkerGroupPurposeModel> {

	public List<TriatCategoryPurposeModel> findByPurposeId(int purposeId);
	
}

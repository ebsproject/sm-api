package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.MarkerGroupPurposeModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarkerGroupPurposeRepository extends JpaRepository<MarkerGroupPurposeModel,Integer>, RepositoryExt<MarkerGroupPurposeModel> {

	/**
	 * 
	 * @param purposeId
	 */
	public List<MarkerGroupPurposeModel> findByPurposeId(int purposeId);

}

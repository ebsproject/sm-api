package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.BatchMarkerGroupCustomModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BatchMarkerGroupCustomRepository extends JpaRepository<BatchMarkerGroupCustomModel,Integer>, RepositoryExt<BatchMarkerGroupCustomModel> {
	

	public List<BatchMarkerGroupCustomModel> findByBatchMarkerGroupId(int id);
}

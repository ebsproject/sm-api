package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.RequestMarkerGroupModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestMarkerGroupRepository extends JpaRepository<RequestMarkerGroupModel,Integer>, RepositoryExt<RequestMarkerGroupModel> {
	

	public List<RequestMarkerGroupModel> findByRequestId(int requestId);
}

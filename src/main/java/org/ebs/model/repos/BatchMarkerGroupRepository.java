package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.BatchMarkerGroupModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BatchMarkerGroupRepository
		extends JpaRepository<BatchMarkerGroupModel, Integer>, RepositoryExt<BatchMarkerGroupModel> {

	public List<BatchMarkerGroupModel> findByBatchId(int id);

}

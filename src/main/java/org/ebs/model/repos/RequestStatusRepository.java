///////////////////////////////////////////////////////////
//  RequestStatusRepository.java
//  Macromedia ActionScript Implementation of the Interface RequestStatusRepository
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:23 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model.repos;

import java.util.List;

import org.ebs.model.RequestStatusModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:23 AM
 */
public interface RequestStatusRepository extends JpaRepository<RequestStatusModel,Integer>, RepositoryExt<RequestStatusModel> {

	/**
	 * 
	 * @param requestId
	 */
	public List<RequestStatusModel> findByRequestId(int requestId);

	/**
	 * 
	 * @param statusId
	 */
	public List<RequestStatusModel> findByStatusId(int statusId);

}
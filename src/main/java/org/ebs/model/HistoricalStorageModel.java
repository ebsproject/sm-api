///////////////////////////////////////////////////////////
//  HistoricalStorageModel.java
//  Macromedia ActionScript Implementation of the Class HistoricalStorageModel
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:08 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:08 AM
 */
@Entity @Table(name="Historical_Storage",schema="sample") @Getter @Setter
public class HistoricalStorageModel extends Auditable {

	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	@Column(name="move_Date")
	private String moveDate;
	@Column(name="move_Type")
	private String moveType;
	@Column(name="name")
	private String name;
	private static final long serialVersionUID = 498029760;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="storagelocation_id")
	private StorageLocationModel storagelocation;
	@Column(name="tenant_id")
	private int tenant;

	@Override
	public String toString(){
		return "HistoricalStorageModel [id=" + id + ",]";
	}

}
///////////////////////////////////////////////////////////
//  InputPropModel.java
//  Macromedia ActionScript Implementation of the Class InputPropModel
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:19:01 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:19:01 PM
 */
@Entity @Table(name="Input_Prop",schema="ebsform") @Getter @Setter
public class InputPropModel extends Auditable {

	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="component_id")
	private ComponentModel component;
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	@Column(name="key")
	private String key;
	private static final long serialVersionUID = 390769532;
	@Column(name="tenant_id")
	private int tenant;
	@Column(name="value")
	private String value;

	@Override
	public String toString(){
		return "InputPropModel [id=" + id + ",]";
	}

}
///////////////////////////////////////////////////////////
//  FieldModel.java
//  Macromedia ActionScript Implementation of the Class FieldModel
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:18:55 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.ebs.util.Auditable;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:18:55 PM
 */
@Entity @Table(name="Field",schema="customform") @Getter @Setter
public class FieldModel extends Auditable {

	@ManyToOne(fetch=FetchType.LAZY, optional =false) @JoinColumn(name="component_id")
	private ComponentModel component;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="data_type_id")
	private DataTypeModel datatype;
	@Column(name="default_Value")
	private String defaultValue;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="form_id")
	private FormModel form;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="group_id")
	private GroupModel group;
	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	@Column(name="is_Base")
	private Boolean isBase;
	@Column(name="is_Required")
	private Boolean isRequired;
	@Column(name="label")
	private String label;
	@Column(name="name")
	private String name;
	@Column(name="\"order\"")
	private Integer order;
	private static final long serialVersionUID = 179905817;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="tab_id")
	private TabModel tab;
	@Column(name="tenant_id")
	private int tenant;
	@Column(name="tooltip")
	private String tooltip;
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="validation_regex_id")
	@NotFound (action=NotFoundAction.IGNORE)
	private ValidationRegexModel validationregex;
	@OneToMany(mappedBy = "field",fetch=FetchType.LAZY, cascade = CascadeType.ALL)
	private Set<ValueModel> values;

	@Override
	public String toString(){
		return "FieldModel [id=" + id + ",]";
	}

}
package org.ebs.model.custom;

public interface PlateModel {

	String getPlateName();
	Integer getPlateId();
	Integer getBatchId();
	
}

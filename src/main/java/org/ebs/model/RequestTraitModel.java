package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.ebs.util.Auditable;

import lombok.Getter;
import lombok.Setter;

@Entity @Table(name="request_trait",schema="sample") @Getter @Setter
public class RequestTraitModel extends Auditable {
	private static final long serialVersionUID = 1L;

	@GeneratedValue(strategy= GenerationType.IDENTITY) @Id @Column
	private int id;
	
	@Column(name="trait_id")
	private int traitId;
	
	@Column(name="marker_group_id")
	private Integer markerGroupId;
	
	@ManyToOne(fetch=FetchType.LAZY, optional =true) @JoinColumn(name="request_id")
	private RequestModel request;
	
	@Override
	public String toString(){
		return "RequestTraitModel [dataId=" + traitId + ",request=" + request + ",id=" + id + ",]";
	}
	
}

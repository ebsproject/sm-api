package org.ebs.services;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.ebs.rest.to.FileObject;
import org.ebs.rest.to.GenotypeSearchParams;
import org.ebs.services.to.FileObjectTo;
import org.ebs.util.brapi.BrCall;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrSample;
import org.ebs.util.brapi.BrStudy;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.multipart.MultipartFile;

public interface GigwaService {

    public static final String DEFAULT_MODULE_NAME = "ebs";

    /**
     * Checks if a file can be imported by gigwa. If not, converts the file to a supported format.
     * Additionally creates a backup of the converted file
     * @param file object to download and verify
     * @param vendorId id of the vendor's format to support
     * @param tmpPath path where the temporaty file must be created
     * @throws an exception if the appropriate file cannot be created in tmpPath
     */
    void prepareFile(FileObjectTo fileObject, int vendorId, Path tmpPath) throws IllegalStateException, IOException, RuntimeException;

    /**
     * Lists calls from gigwa in the given study, filtered for a specific batch and request
     * @param studyDbId as defined by gigwa
     * @param batchId
     * @param requestId
     * @return a list of Calls for which sample names are equivalent to SM-sample-names
     */
    List<BrCall> searchCalls(String studyDbId, int batchId, int requestId);

    /**
     * Lists calls from gigwa the given germplasmDbIds
     * @param germplasmIds as defined by gigwa. They are presumed to be unique for EBS
     * @return a list of Calls for which germplasm names match
     */
    List<BrCall> searchCalls(List<String> germplasmIds);

    /**
     * Returns the SM sample name from a gigwa sampleDbId
     * @param sample to be converted
     * @return the equivalent sm sample name
     */
    String smSampleName(BrSample sample);

    /**
     * Returns the SM sample name from a gigwa call
     * @param call to have its callsetDbId converted 
     * @return the equivalent sm sample name
     */
    String smSampleName(BrCall call);
    
    /**
     * Returns the SM marker name from a gigwa call
     * @param call to have its variantDbId converted
     * @return the equivalent sm marker name
     */
    String smMarkerName(BrCall call);

    /**
     * Returns the genotype value from a gigwa call
     * @param call to have its genotype extracted at position 0 (if multiple values exist)
     * @return the genotype value
     */
    String genotypeOf(BrCall call);

    /**
     * Returns Gigwa's study which maps to the provided (Genotype Results) File Name
     * @param fileName loaded for a Batch
     * @return corresponding Gigwa study
     */
    BrStudy getStudy(String fileName);

    /**
     * Invokes Gigwa File Import API
     * @param resource to be sent
     * @return gigwa's import response
     */
    String sendImport(FileSystemResource resource);

    /**
     * Returns a gigwa germplasm Id for the given SM sample
     * @param studyDbId from gigwa
     * @param smSampleId from SM
     * @return Gigwa germplasmDbId associated to the given sample
     */
    String gemplasmDbId(String studyDbId, String smSampleId);

     
    /**
     * Saves files into the File Manager and performs validations
     * @param tenantId
     * @param files
     * @param tags
     * @param description
     * @return
     */
    public BrPagedResponse<FileObject> uploadFile(int tenantId, MultipartFile[] files,
            List<String> tags, String description, boolean hasValidationErrors);

    /**
     * Creates a text csv-matrix where markers are columns and samples are rows
     * @param params to search for samples
     * @param writer where output matrix will be written
     * @throws IOException
     */
    public void generateGenotypeMatrix(GenotypeSearchParams params, BufferedWriter writer) throws IOException;

    /**
     * Generated the csv file name for a file that will contain genotype data
     * @param params used as input fo a query
     * @return the appropriate file name for give params with .csv extension
     */
    String createFileName(GenotypeSearchParams params);

    /**
     * Consults the status of a progressId
     * @param progressId identifier generated by Gigwa for tasks
     * like importing genotyping data, metadata, etc.
     * @return the status, and appropriate message if available
     */
    public String getProcessStatus(String progressId);
}


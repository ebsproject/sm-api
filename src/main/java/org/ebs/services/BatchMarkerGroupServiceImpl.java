package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.BatchMarkerGroupModel;
import org.ebs.model.repos.BatchMarkerGroupCustomRepository;
import org.ebs.model.repos.BatchMarkerGroupRepository;
import org.ebs.model.repos.BatchRepository;
import org.ebs.services.to.BatchMarkerGroupCustomTo;
import org.ebs.services.to.BatchMarkerGroupTo;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.Input.BatchMarkerGroupInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@Service @Transactional(readOnly = true)
public class BatchMarkerGroupServiceImpl implements BatchMarkerGroupService{
	
	private final ConversionService converter;
	private final BatchMarkerGroupRepository batchMarkerGroupRepository; 
	private final BatchRepository batchRepository; 

	private final BatchMarkerGroupCustomRepository batchMarkerGroupCustomRepository; 
	
	@Override @Transactional(readOnly = false)
	public BatchMarkerGroupTo createBatchMarkerGroup(BatchMarkerGroupInput input) {
		BatchMarkerGroupModel model = converter.convert(input, BatchMarkerGroupModel.class);	
		model.setId(0);
		if (input.getBatch() != null && input.getBatch().getId() > 0) {
			model.setBatch(batchRepository.findById(input.getBatch().getId()).get());
		}
		model = batchMarkerGroupRepository.save(model);
		return converter.convert(model,BatchMarkerGroupTo.class );
	}

	@Override @Transactional(readOnly = false)
	public BatchMarkerGroupTo modifyBatchMarkerGroup(BatchMarkerGroupInput input) {
		BatchMarkerGroupModel target = batchMarkerGroupRepository.findById(input.getId())
				.orElseThrow(() -> new RuntimeException("RequestTrait not found"));
		BatchMarkerGroupModel source =  converter.convert(input, BatchMarkerGroupModel.class);
		Utils.copyNotNulls(source,target);
		return converter.convert(batchMarkerGroupRepository.save(target), BatchMarkerGroupTo.class);
	}

	@Override @Transactional(readOnly = false)
	public int deleteBatchMarkerGroup(int id) {
		BatchMarkerGroupModel model = batchMarkerGroupRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("RequestTrait not found"));
		model.setDeleted(true);
		 batchMarkerGroupRepository.save(model);
		return id;
	}

	@Override
	public Optional<BatchMarkerGroupTo> findBatchMarkerGroup(int id) {
		if(id <1)
		 {return Optional.empty();}
		
		return batchMarkerGroupRepository.findById(id).filter(r -> !r.isDeleted()).map(r -> converter.convert(r,BatchMarkerGroupTo.class));
	}

	@Override
	public Page<BatchMarkerGroupTo> findBatchMarkerGroups(PageInput page, SortInput sort, List<FilterInput> filters) {
		return batchMarkerGroupRepository.findByCriteria(BatchMarkerGroupModel.class, filters, sort, page).map(r -> converter.convert(r,BatchMarkerGroupTo.class));
	}

	@Override
	public Optional<BatchTo> findBatch(int id) {
		return batchMarkerGroupRepository.findById(id).map(r -> converter.convert(r.getBatch(),BatchTo.class));
		
	}
	
	public Set <BatchMarkerGroupCustomTo> findBatchMarkerGroupCustoms(int id){
		try {
		return batchMarkerGroupCustomRepository.findByBatchMarkerGroupId(id).stream().map(e -> converter.convert(e,BatchMarkerGroupCustomTo.class)).collect(Collectors.toSet());
		}catch (Exception ex ) {
			ex.printStackTrace();
		}
		return null;
	}

}

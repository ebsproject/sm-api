package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.RequestMarkerGroupModel;
import org.ebs.model.repos.RequestMarkerGroupRepository;
import org.ebs.services.to.RequestMarkerGroupTo;
import org.ebs.services.to.RequestTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service @Transactional(readOnly = true)
public class RequestMarkerGroupServiceImpl implements RequestMarkerGroupService{
	
	private final RequestMarkerGroupRepository requestMarkerGroupRepository;
	private final ConversionService converter;
	
	public Optional <RequestTo> findRequest(int requestMarkerGroupId){
		return requestMarkerGroupRepository.findById(requestMarkerGroupId).map(r -> converter.convert(r.getRequest(),RequestTo.class));
	}
	
	
	@Override
	public Page<RequestMarkerGroupTo> findRequestMarkerGroups(PageInput page, SortInput sort, List<FilterInput> filters) {
		return requestMarkerGroupRepository.findByCriteria(RequestMarkerGroupModel.class, filters, sort, page).map(r -> converter.convert(r,RequestMarkerGroupTo.class));
	}

}

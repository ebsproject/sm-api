///////////////////////////////////////////////////////////
//  LabLayoutService.java
//  Macromedia ActionScript Implementation of the Interface LabLayoutService
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:11 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.CollectionLayoutTo;
import org.ebs.services.to.LabLayoutControlTo;
import org.ebs.services.to.LabLayoutTo;
import org.ebs.services.to.ServiceProviderTo;
import org.ebs.services.to.Input.LabLayoutInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:11 AM
 */
public interface LabLayoutService {

	/**
	 * 
	 * @param LabLayout
	 */
	public LabLayoutTo createLabLayout(LabLayoutInput LabLayout);

	/**
	 * 
	 * @param labLayoutId
	 */
	public int deleteLabLayout(int labLayoutId);

	/**
	 * 
	 * @param lablayoutId
	 */
	public Optional<CollectionLayoutTo> findCollectionLayout(int lablayoutId);

	/**
	 * 
	 * @param labLayoutId
	 */
	public Optional<LabLayoutTo> findLabLayout(int labLayoutId);

	/**
	 * 
	 * @param lablayoutId
	 */
	public Set<LabLayoutControlTo> findLabLayoutControls(int lablayoutId);

	/**
	 * 
	 * @param page
	 * @param sort
	 * @param filters
	 */
	public Page<LabLayoutTo> findLabLayouts(PageInput page, SortInput sort, List<FilterInput> filters);

	/**
	 * 
	 * @param lablayoutId
	 */
	public Optional<ServiceProviderTo> findServiceProvider(int lablayoutId);

	/**
	 * 
	 * @param labLayout
	 */
	public LabLayoutTo modifyLabLayout(LabLayoutInput labLayout);

}
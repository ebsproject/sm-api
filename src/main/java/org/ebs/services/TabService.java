///////////////////////////////////////////////////////////
//  TabService.java
//  Macromedia ActionScript Implementation of the Interface TabService
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:19:07 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.ComponentTo;
import org.ebs.services.to.EbsFormTo;
import org.ebs.services.to.FieldTo;
import org.ebs.services.to.TabTo;
import org.ebs.services.to.Input.TabInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:19:07 PM
 */
public interface TabService {

	/**
	 * 
	 * @param Tab
	 */
	public TabTo createTab(TabInput Tab);

	/**
	 * 
	 * @param tabId
	 */
	public int deleteTab(int tabId);

	/**
	 * 
	 * @param tabId
	 */
	public Set<ComponentTo> findComponents(int tabId);

	/**
	 * 
	 * @param tabId
	 */
	public Optional<EbsFormTo> findEbsForm(int tabId);

	/**
	 * 
	 * @param tabId
	 */
	public Set<FieldTo> findFields(int tabId);

	/**
	 * 
	 * @param tabId
	 */
	public Optional<TabTo> findTab(int tabId);

	/**
	 * 
	 * @param page
	 * @param sort
	 * @param filters
	 */
	public Page<TabTo> findTabs(PageInput page, SortInput sort, List<FilterInput> filters);

	/**
	 * 
	 * @param tab
	 */
	public TabTo modifyTab(TabInput tab);

}
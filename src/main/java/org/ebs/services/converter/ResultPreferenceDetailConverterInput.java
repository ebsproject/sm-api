///////////////////////////////////////////////////////////
//  ResultPreferenceDetailConverterInput.java
//  Macromedia ActionScript Implementation of the Class ResultPreferenceDetailConverterInput
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:24 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.ResultPreferenceDetailModel;
import org.ebs.services.to.Input.ResultPreferenceDetailInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:24 AM
 */
@Component
  class ResultPreferenceDetailConverterInput implements Converter<ResultPreferenceDetailInput,ResultPreferenceDetailModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public ResultPreferenceDetailModel convert(ResultPreferenceDetailInput source){
		ResultPreferenceDetailModel target = new  ResultPreferenceDetailModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
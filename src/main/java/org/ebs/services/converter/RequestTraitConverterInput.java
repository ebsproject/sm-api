package org.ebs.services.converter;

import org.ebs.model.RequestTraitModel;
import org.ebs.services.to.Input.RequestTraitInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class RequestTraitConverterInput implements Converter<RequestTraitInput,RequestTraitModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public RequestTraitModel convert(RequestTraitInput source){
		RequestTraitModel target = new  RequestTraitModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}

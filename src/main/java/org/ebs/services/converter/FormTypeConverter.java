///////////////////////////////////////////////////////////
//  FormTypeConverter.java
//  Macromedia ActionScript Implementation of the Class FormTypeConverter
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:18:58 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.FormTypeModel;
import org.ebs.services.to.FormTypeTo;
import org.ebs.util.Utils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:18:58 PM
 */
@Component
  class FormTypeConverter implements Converter<FormTypeModel,FormTypeTo> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public FormTypeTo convert(FormTypeModel source){
		FormTypeTo target = new  FormTypeTo(); 
		Utils.copyNotNulls(source, target); 
		return target;
	}

}
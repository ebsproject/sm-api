///////////////////////////////////////////////////////////
//  StockConverterInput.java
//  Macromedia ActionScript Implementation of the Class StockConverterInput
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:39 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.StockModel;
import org.ebs.services.to.Input.StockInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:39 AM
 */
@Component
  class StockConverterInput implements Converter<StockInput,StockModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public StockModel convert(StockInput source){
		StockModel target = new  StockModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
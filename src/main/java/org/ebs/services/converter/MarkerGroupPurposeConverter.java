package org.ebs.services.converter;

import org.ebs.model.MarkerGroupPurposeModel;
import org.ebs.services.to.MarkerGroupPurposeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class MarkerGroupPurposeConverter  implements Converter<MarkerGroupPurposeModel,MarkerGroupPurposeTo> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public MarkerGroupPurposeTo convert(MarkerGroupPurposeModel source){
		MarkerGroupPurposeTo target = new  MarkerGroupPurposeTo(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	

	}
}

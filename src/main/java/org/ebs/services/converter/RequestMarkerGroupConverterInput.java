package org.ebs.services.converter;

import org.ebs.model.RequestMarkerGroupModel;
import org.ebs.services.to.Input.RequestMarkerGroupInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author LPUEBLA
 * @version 1.0
 * @created 11-may.-2023 11:10:14 a. m.
 */
@Component
  class RequestMarkerGroupConverterInput implements Converter<RequestMarkerGroupInput,RequestMarkerGroupModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public RequestMarkerGroupModel convert(RequestMarkerGroupInput source){
		RequestMarkerGroupModel target = new  RequestMarkerGroupModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
package org.ebs.services.converter;

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DarTagToHapMapConverter implements FileConverter{
    private final int markerColIndex = 3;
    private final int markerRowIndex = 0;
    private final int sampleColIndex = 2;
    private final int sampleRowIndex = 1;
    private final int markerDataRowIndex = 1;
    private final String hapmapStandardHeader = "rs#	alleles	chrom	pos	strand	assembly#	center	protLSID	assayLSID	panelLSID	QCcode";

    public void convert(int batchId, Path tmpPath , byte[] data) {

        
        try (FileWriter writter = new FileWriter(tmpPath.toFile()); Scanner scan = new Scanner(new ByteArrayInputStream(data))) {
            List<String[]> rows = new ArrayList<>();
            while (scan.hasNextLine()) {
                rows.add(scan.nextLine().split(","));
            }
            
            writter.append(hapmapStandardHeader);
            for (int i = sampleRowIndex; i < rows.size(); i++) {
                if(rows.get(i).length > 0)
                    writter.append("\t")
                        .append(rows.get(i)[sampleColIndex]);
            }
            writter.append("\n");
            int variantPosKey = 1;
            //log.info("rows.get(sampleRowIndex).length -> {}", rows.get(sampleRowIndex).length);
            for (int j = markerColIndex; j < rows.get(sampleRowIndex).length; j++) {
                //log.info("reading marker # {}", j);
                String alleles = getAllelesString(rows, j);
                if (alleles.length() == 3) {
                    writter.append(getMarker(rows, j))
                        .append("\t")
                        .append(alleles)
                        .append("	" + batchId + "	" + variantPosKey++ + "	+	NA	NA	NA	NA	NA	NA	");
                    for (int i = markerDataRowIndex; i < rows.size(); i++) {
                        writter.append(convertGenoString(rows, i, j));
                        if (i < rows.size() - 1)
                            writter.append("\t");
                    }
                    writter.append("\n");
                }
            }

        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    private String getMarker(List<String[]> rows, int j){
        return rows.get(markerRowIndex)[j];
    }

    private String getAllelesString(List<String[]> rows, int colIndex) {
        Set<String> genotype = new HashSet<>();
        rows.stream()
            .filter(r -> !r[0].equalsIgnoreCase("PLATE_ID"))
            .map(r -> r[colIndex])
            .filter(s -> !s.contains("-") && s.length() == 3)
            .forEach(s -> {
                genotype.add(s.charAt(0)+"");
                genotype.add(s.charAt(2)+"");
            });
        //log.trace("genotype -> {}", genotype.toString());
        String genotypeString = genotype.stream()
            .sorted(new GenotypeComparator())
            .collect(Collectors.joining("/"));
        
        if (genotypeString.length() == 0)
            return "N/A";
        if (genotypeString.length() == 1)
            return genotypeString + "/N";
        return genotypeString;
    }

    private String convertGenoString(List<String[]> rows, int i, int j){
        return rows.get(i)[j].replace(":", "").replaceAll("-", "N");
    }

    enum Genotypes {
        A, T, C, G, N
    }

    private class GenotypeComparator implements Comparator<String> {

        @Override
        public int compare(String gen1, String gen2) {
            return Genotypes.valueOf(gen1).compareTo(Genotypes.valueOf(gen2));
        }

    }
    
}
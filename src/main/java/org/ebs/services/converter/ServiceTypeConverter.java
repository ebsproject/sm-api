///////////////////////////////////////////////////////////
//  ServiceTypeConverter.java
//  Macromedia ActionScript Implementation of the Class ServiceTypeConverter
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:34 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.ServiceTypeModel;
import org.ebs.services.to.ServiceTypeTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:34 AM
 */
@Component
  class ServiceTypeConverter implements Converter<ServiceTypeModel,ServiceTypeTo> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public ServiceTypeTo convert(ServiceTypeModel source){
		ServiceTypeTo target = new  ServiceTypeTo(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
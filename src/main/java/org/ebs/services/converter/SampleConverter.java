///////////////////////////////////////////////////////////
//  SampleConverter.java
//  Macromedia ActionScript Implementation of the Class SampleConverter
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:26 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.SampleModel;
import org.ebs.services.to.SampleTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:26 AM
 */
@Component
  class SampleConverter implements Converter<SampleModel,SampleTo> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public SampleTo convert(SampleModel source){
		SampleTo target = new  SampleTo(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
package org.ebs.services.converter;

import org.ebs.model.BatchMarkerGroupCustomModel;
import org.ebs.services.to.BatchMarkerGroupCustomTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class BatchMarkerGroupCustomConverter implements Converter<BatchMarkerGroupCustomModel,BatchMarkerGroupCustomTo> {

	@Override
	public BatchMarkerGroupCustomTo convert(BatchMarkerGroupCustomModel source) {
		BatchMarkerGroupCustomTo target = new  BatchMarkerGroupCustomTo(); 
		BeanUtils.copyProperties(source, target);
		target.setMarkerId(source.getMarkerId());
		target.setAssayId(source.getAssayId());
		return target;
	}

}

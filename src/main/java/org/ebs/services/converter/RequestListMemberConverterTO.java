package org.ebs.services.converter;

import org.ebs.model.RequestListMemberModel;
import org.ebs.services.to.RequestListMemberTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class RequestListMemberConverterTO implements Converter<RequestListMemberTo,RequestListMemberModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public RequestListMemberModel convert(RequestListMemberTo source){
		RequestListMemberModel target = new  RequestListMemberModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}

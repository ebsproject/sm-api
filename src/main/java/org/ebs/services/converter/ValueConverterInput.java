///////////////////////////////////////////////////////////
//  ValueConverterInput.java
//  Macromedia ActionScript Implementation of the Class ValueConverterInput
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:19:09 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.ValueModel;
import org.ebs.services.to.Input.ValueInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:19:09 PM
 */
@Component
  class ValueConverterInput implements Converter<ValueInput,ValueModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public ValueModel convert(ValueInput source){
		ValueModel target = new  ValueModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
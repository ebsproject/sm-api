///////////////////////////////////////////////////////////
//  LabLayoutConverter.java
//  Macromedia ActionScript Implementation of the Class LabLayoutConverter
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:10 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.LabLayoutModel;
import org.ebs.services.to.LabLayoutTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:10 AM
 */
@Component
  class LabLayoutConverter implements Converter<LabLayoutModel,LabLayoutTo> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public LabLayoutTo convert(LabLayoutModel source){
		LabLayoutTo target = new  LabLayoutTo(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
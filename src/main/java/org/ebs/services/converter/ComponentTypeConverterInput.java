///////////////////////////////////////////////////////////
//  ComponentTypeConverterInput.java
//  Macromedia ActionScript Implementation of the Class ComponentTypeConverterInput
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:18:52 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.ComponentTypeModel;
import org.ebs.services.to.Input.ComponentTypeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:18:52 PM
 */
@Component
  class ComponentTypeConverterInput implements Converter<ComponentTypeInput,ComponentTypeModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public ComponentTypeModel convert(ComponentTypeInput source){
		ComponentTypeModel target = new  ComponentTypeModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
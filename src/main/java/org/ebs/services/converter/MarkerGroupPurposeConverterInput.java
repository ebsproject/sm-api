package org.ebs.services.converter;

import org.ebs.model.MarkerGroupPurposeModel;
import org.ebs.services.to.Input.MarkerGroupPurposeInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;

class MarkerGroupPurposeConverterInput implements Converter<MarkerGroupPurposeInput,MarkerGroupPurposeModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public MarkerGroupPurposeModel convert(MarkerGroupPurposeInput source){
		MarkerGroupPurposeModel target = new  MarkerGroupPurposeModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}

package org.ebs.services.converter;

import java.nio.file.Path;

public interface FileConverter {

    /**
     * Changes data format of a result files
     * For importing purposes, chromosome will be the batchId to simulate variant position
     * and make each import to gigwa unique
    
     * @param batchId of the associated batch
     * @param tmpPath Path where the converted file will be
     * @param data original file
     */
    void convert(int batchId, Path tmpPath, byte[] data);

}

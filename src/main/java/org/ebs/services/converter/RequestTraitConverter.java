package org.ebs.services.converter;

import org.ebs.model.RequestTraitModel;
import org.ebs.services.to.RequestTraitTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class RequestTraitConverter implements Converter<RequestTraitModel,RequestTraitTo> {

	@Override
	public RequestTraitTo convert(RequestTraitModel source){
		RequestTraitTo target = new  RequestTraitTo(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}
}

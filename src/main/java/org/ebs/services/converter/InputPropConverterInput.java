///////////////////////////////////////////////////////////
//  InputPropConverterInput.java
//  Macromedia ActionScript Implementation of the Class InputPropConverterInput
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:19:01 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.converter;

import org.ebs.model.InputPropModel;
import org.ebs.services.to.Input.InputPropInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:19:01 PM
 */
@Component
  class InputPropConverterInput implements Converter<InputPropInput,InputPropModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public InputPropModel convert(InputPropInput source){
		InputPropModel target = new  InputPropModel(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}

}
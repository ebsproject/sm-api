package org.ebs.services.converter;

import static java.util.Optional.ofNullable;

import org.ebs.model.SampleDetailModel;
import org.ebs.services.to.SampleGermplasmDetailTo;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SampleDetailModelToSampleGermplasmDetailToConverter implements Converter<SampleDetailModel, SampleGermplasmDetailTo>{

    @Override
    public SampleGermplasmDetailTo convert(SampleDetailModel source) {
        SampleGermplasmDetailTo target = new SampleGermplasmDetailTo(source.getSampleCode(), source.getPlateName(), source.getRow() + source.getColumn(), source.getRow(), ""+source.getColumn(), source.getBatch().getId());
        target.setSampleNumber(source.getSampleNumber());
        target.setId(source.getId());
        target.setVendorControl(source.isVendorControl());
        target.setPlateId(source.getPlateId());
        target.setPlantNumber(ofNullable(source.getSampleItemId()).orElse(0));
        return target;
    }
    
}

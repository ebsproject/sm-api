package org.ebs.services.converter;

import java.io.ByteArrayInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AgriplexToHapMapConverter implements FileConverter{
    private final String hapmapStandardHeader = "rs#	alleles	chrom	pos	strand	assembly#	center	protLSID	assayLSID	panelLSID	QCcode";

    public void convert(int batchId, Path tmpPath , byte[] data) {
            XSSFWorkbook listBook;
        
        try(FileWriter writter = new FileWriter(tmpPath.toFile())) {
            listBook = new XSSFWorkbook(new ByteArrayInputStream(data));
            XSSFSheet dataSheet = listBook.getSheetAt(0);
            writter.append(hapmapStandardHeader);

            int markerColIndex = 4;
            int markerRowIndex = 5;
            int sampleColIndex = 2;
            int sampleRowIndex = 8;
    
            //set proper offset for one of the two formats of agriplex
            if (!dataSheet.getRow(markerRowIndex).getCell(markerColIndex-1).getStringCellValue()
                    .equalsIgnoreCase("SNP_ID")) {
                markerRowIndex--;
                sampleRowIndex--;
            }
            
            for (int i = sampleRowIndex; i <= dataSheet.getLastRowNum(); i++) {
                XSSFCell c = dataSheet.getRow(i).getCell(sampleColIndex);
                String value = null;
                switch (c.getCellType()) {
                    case STRING:
                        value = c.getStringCellValue();
                        break;
                    case NUMERIC:
                        value = "" + (long) c.getNumericCellValue();
                        break;
                    default:
                        throw new RuntimeException(String.format("Error while reading cell at row(%s), col(%s): Type unrecognized", i, sampleColIndex));
                }
                writter.append("\t").append(value);
            }
            writter.append("\n");
            int variantPosKey = 1;
            for (int j = markerColIndex; j < dataSheet.getRow(sampleRowIndex).getPhysicalNumberOfCells(); j++) {
                String alleles = convertAllelesString(dataSheet, j, markerRowIndex);
                if(alleles.length() == 3){
                    writter.append(getMarker(dataSheet, j, markerRowIndex))
                        .append("\t")
                        .append(alleles)
                        .append("	" + batchId + "	" + variantPosKey++ + "	+	NA	NA	NA	NA	NA	NA	");
                    for (int i = sampleRowIndex; i < dataSheet.getPhysicalNumberOfRows(); i++) {
                        writter.append(convertGenoString(dataSheet, i, j));
                        if (i < dataSheet.getPhysicalNumberOfRows() - 1)
                            writter.append("\t");
                    }
                    writter.append("\n");
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
    }

    private String getMarker(XSSFSheet dt, int j, int markerRowIndex){
        return dt.getRow(markerRowIndex).getCell(j).getStringCellValue();
    }

    private String convertAllelesString(XSSFSheet dt, int j, int markerRowIndex) {
        return String.format("%s/%s",
            dt.getRow(markerRowIndex + 1).getCell(j).getStringCellValue(),
            dt.getRow(markerRowIndex + 2).getCell(j).getStringCellValue()).replaceAll("-","N");
    }

    private String convertGenoString(XSSFSheet dt, int i, int j){
        String str = dt.getRow(i).getCell(j).getStringCellValue();
        if (str == "Fail" || str == "--")
            str = "NN";
        else if (str.length() == 5 && str.contains(" / "))
            str = str.replace(" / ", "");
        else if (str.length() == 1)
            str += str;
        else
            str = "NN";

        return str.replaceAll("-", "N");
    }

}

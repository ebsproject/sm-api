package org.ebs.services.converter;

import org.ebs.model.BatchMarkerGroupModel;
import org.ebs.services.to.Input.BatchMarkerGroupInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class BatchMarkerGroupConverterInput implements Converter<BatchMarkerGroupInput,BatchMarkerGroupModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public BatchMarkerGroupModel convert(BatchMarkerGroupInput source){
		BatchMarkerGroupModel target = new  BatchMarkerGroupModel(); 
		BeanUtils.copyProperties(source, target);
		target.setMarkerGroupID(source.getMarkerGroupID());
		target.setTechnologyServiceProviderId(source.getTechnologyServiceProviderId());
		return target;
	}

}

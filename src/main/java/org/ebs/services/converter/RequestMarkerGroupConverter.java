package org.ebs.services.converter;

import org.ebs.model.RequestMarkerGroupModel;
import org.ebs.services.to.RequestMarkerGroupTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class RequestMarkerGroupConverter implements Converter<RequestMarkerGroupModel,RequestMarkerGroupTo> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public RequestMarkerGroupTo convert(RequestMarkerGroupModel source){
		RequestMarkerGroupTo target = new  RequestMarkerGroupTo(); 
		BeanUtils.copyProperties(source, target); 
		return target;
	}
}

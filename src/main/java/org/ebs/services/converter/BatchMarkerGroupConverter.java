package org.ebs.services.converter;

import org.ebs.model.BatchMarkerGroupModel;
import org.ebs.services.to.BatchMarkerGroupTo;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class BatchMarkerGroupConverter implements Converter<BatchMarkerGroupModel,BatchMarkerGroupTo> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public BatchMarkerGroupTo convert(BatchMarkerGroupModel source){
		BatchMarkerGroupTo target = new  BatchMarkerGroupTo(); 
		BeanUtils.copyProperties(source, target);
		target.setMarkerGroupID(source.getMarkerGroupID());
		target.setTechnologyServiceProviderId(source.getTechnologyServiceProviderId());
		return target;
	}

}

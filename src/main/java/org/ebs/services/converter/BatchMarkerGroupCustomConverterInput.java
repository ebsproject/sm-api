package org.ebs.services.converter;

import org.ebs.model.BatchMarkerGroupCustomModel;
import org.ebs.services.to.Input.BatchMarkerGroupCustomInput;
import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class BatchMarkerGroupCustomConverterInput implements Converter<BatchMarkerGroupCustomInput,BatchMarkerGroupCustomModel> {

	/**
	 * 
	 * @param source
	 */
	@Override
	public BatchMarkerGroupCustomModel convert(BatchMarkerGroupCustomInput source){
		BatchMarkerGroupCustomModel target = new  BatchMarkerGroupCustomModel(); 
		BeanUtils.copyProperties(source, target);
		target.setMarkerId(source.getMarkerId());
		target.setAssayId(source.getAssayId());
		
		return target;
	}

}

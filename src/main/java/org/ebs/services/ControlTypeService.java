///////////////////////////////////////////////////////////
//  ControlTypeService.java
//  Macromedia ActionScript Implementation of the Interface ControlTypeService
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:05 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.ControlTypeTo;
import org.ebs.services.to.LabLayoutControlTo;
import org.ebs.services.to.SampleDetailTo;
import org.ebs.services.to.Input.ControlTypeInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:05 AM
 */
public interface ControlTypeService {

	/**
	 * 
	 * @param ControlType
	 */
	public ControlTypeTo createControlType(ControlTypeInput ControlType);

	/**
	 * 
	 * @param controlTypeId
	 */
	public int deleteControlType(int controlTypeId);

	/**
	 * 
	 * @param controlTypeId
	 */
	public Optional<ControlTypeTo> findControlType(int controlTypeId);

	/**
	 * 
	 * @param page
	 * @param sort
	 * @param filters
	 */
	public Page<ControlTypeTo> findControlTypes(PageInput page, SortInput sort, List<FilterInput> filters);

	/**
	 * 
	 * @param controltypeId
	 */
	public Set<LabLayoutControlTo> findLabLayoutControls(int controltypeId);

	/**
	 * 
	 * @param controltypeId
	 */
	public Set<SampleDetailTo> findSampleDetails(int controltypeId);

	/**
	 * 
	 * @param controlType
	 */
	public ControlTypeTo modifyControlType(ControlTypeInput controlType);

}
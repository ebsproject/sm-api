///////////////////////////////////////////////////////////
//  ValueServiceImpl.java
//  Macromedia ActionScript Implementation of the Class ValueServiceImpl
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:19:10 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.FieldModel;
import org.ebs.model.RequestModel;
import org.ebs.model.ValueModel;
import org.ebs.model.repos.FieldRepository;
import org.ebs.model.repos.RequestRepository;
import org.ebs.model.repos.ValueRepository;
import org.ebs.services.to.FieldTo;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.ValueTo;
import org.ebs.services.to.Input.ValueInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:19:10 PM
 */
@Service @Transactional(readOnly = true)
  class ValueServiceImpl implements ValueService {

	private ConversionService converter;
	private FieldRepository fieldRepository;
	private RequestRepository requestRepository;
	private ValueRepository valueRepository;

	/**
	 * 
	 * @param Value
	 */
	@Override @Transactional(readOnly = false)
	public ValueTo createValue(ValueInput Value){
		ValueModel model = converter.convert(Value,ValueModel.class); 
		 model.setId(0);
		 FieldModel fieldModel = fieldRepository.findById(Value.getField().getId()).get(); 
		model.setField(fieldModel); 
		RequestModel requestModel = requestRepository.findById(Value.getRequest().getId()).get(); 
		model.setRequest(requestModel); 
		 
		 model= valueRepository.save(model); 
		 return converter.convert(model, ValueTo.class); 
	}

	/**
	 * 
	 * @param valueId
	 */
	@Override @Transactional(readOnly = false)
	public int deleteValue(int valueId){
		ValueModel value = valueRepository.findById(valueId).orElseThrow(() -> new RuntimeException("Value not found")); 
		 value.setDeleted(true); 
		  valueRepository.save(value); 
		 return valueId;
	}

	/**
	 * 
	 * @param valueId
	 */
	public Optional<FieldTo> findField(int valueId){
		return valueRepository.findById(valueId).map(r -> converter.convert(r.getField(),FieldTo.class));
	}

	/**
	 * 
	 * @param valueId
	 */
	public Optional<RequestTo> findRequest(int valueId){
		return valueRepository.findById(valueId).map(r -> converter.convert(r.getRequest(),RequestTo.class));
	}

	/**
	 * 
	 * @param valueId
	 */
	@Override
	public Optional<ValueTo> findValue(int valueId){
		if(valueId <1) 
		 {return Optional.empty();} 
		 return valueRepository.findById(valueId).filter(r -> !r.isDeleted()).map(r -> converter.convert(r,ValueTo.class));
	}

	/**
	 * 
	 * @param page
	 * @param sort
	 * @param filters
	 */
	@Override
	public Page<ValueTo> findValues(PageInput page, SortInput sort, List<FilterInput> filters){
		return valueRepository.findByCriteria(ValueModel.class,filters,sort,page).map(r -> converter.convert(r,ValueTo.class));
	}

	/**
	 * 
	 * @param value
	 */
	@Override @Transactional(readOnly = false)
	public ValueTo modifyValue(ValueInput value){
		ValueModel target= valueRepository.findById(value.getId()).orElseThrow(() -> new RuntimeException("Value not found")); 
		 ValueModel source= converter.convert(value,ValueModel.class); 
		 Utils.copyNotNulls(source,target); 
		 return converter.convert(valueRepository.save(target), ValueTo.class);
	}

	/**
	 * 
	 * @param requestRepository
	 * @param fieldRepository
	 * @param converter
	 * @param valueRepository
	 */
	@Autowired
	public ValueServiceImpl(RequestRepository requestRepository, FieldRepository fieldRepository, ConversionService converter, ValueRepository valueRepository){
		this.valueRepository =valueRepository; 
		 this.converter = converter;
		 this.fieldRepository = fieldRepository;
		 this.requestRepository = requestRepository;
	}

}
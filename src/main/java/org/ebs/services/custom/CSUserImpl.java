package org.ebs.services.custom;

import java.util.List;

import org.ebs.services.to.custom.ContactByIDTo;
import org.ebs.services.to.custom.GraphqlTypeTo;
import org.ebs.services.to.custom.UserTo;
import org.ebs.util.client.GraphQLResolverClient;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CSUserImpl implements CSUser{

	private final GraphQLResolverClient client;
	
	@Override
	public UserTo findUserIdExternal(String filter) {
		//String query="findUserList(filters: {mod:EQ  col:\"externalId\" val:\""+554+"\"}){"
		//								  (filters: {mod:EQ  col:\\\"externalId\\\" val:\\\"\"+id+\"\\\"})
				String query="findUserList"+filter+"{"
				+ " content{"
				+ " id "
				+ " externalId "
			    + " userName "
			    + " contact{ "
			        + " person{ "
			        + " familyName "
			        + " fullName"
			        + " additionalName "
			        + " givenName "
			        +" } "
			      +" } "
			        +" }"
			+ "  }";
									
				List <UserTo> list = client.findEntityList(query,GraphqlTypeTo.USER_LIST);
				return list != null ? !list.isEmpty()? list.get(0) : null : null;
		// return client.findEntityList(query,GraphqlTypeTo.USER_LIST).i.get(0);
	}

	@Override
	public List<UserTo> findUsers(String filter) {
		String query="findUserList"+filter+"{"
				+ " content{"
				+ " id "
				+ " externalId "
			    + " userName "
			    + " contact{ "
			        + " person{ "
			        + " familyName "
			          + " additionalName "
			          + " givenName "
			        +" } "
			      +" } "
			        +" }"
			+ "  }";
											
		 return client.findEntityList(query,GraphqlTypeTo.USER_LIST);
	}

	public ContactByIDTo findContactByID(int id) {
		String query="findContact(id:"+id+"){"
				
				+ " id "
				+ " parents{"
				+ "      institution{"
				+ "              institution{"
				+ "                  id"
				+ "                  commonName"
				+ "                 }"
				+ "          }"
				+ "}"
			+ "  }";
		return client.findOneEntity(query,GraphqlTypeTo.CONTACT_BY_ID);
		
	}

}

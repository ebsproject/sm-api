package org.ebs.services.custom;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.custom.GraphqlTypeTo;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrPagination;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.client.FileClient;
import org.ebs.util.client.GraphQLResolverClient;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
	
@Service
@RequiredArgsConstructor
public class CSFileObjectImpl implements CSFileObject {

    private final GraphQLResolverClient client;
    private final FileClient fileClient;

	@Override
    public FileObjectTo modify(FileObjectTo fileObject) {
        String query = "mutation{modifyFileObject(fileObject:{ id:\""+fileObject.getKey() + "\" description:\""+Optional.ofNullable(fileObject.getDescription()).orElse("") +
        "\" status: " + fileObject.getStatus()+ " }){ id }}";
        return client.modifyEntity(query, GraphqlTypeTo.FILE_OBJECT);
    }

    @Override
    public List<FileObjectTo> findByStatus(int status) {
        List<FileObjectTo> fileObjects = new ArrayList<>();
        //TODO get list of tenants from CS and do iterate for all of them to avoid hardcoding it in response

        BrPagedResponse<FileObjectTo> fileObjectResponse = fileClient.getList("/1/objects?tags=gigwa&status=" + status, FileObjectTo.class);
        fileObjects.addAll(fileObjectResponse.getResult().getData());

        BrPagination brPage = fileObjectResponse.getMetadata().getPagination();
        while (brPage.getCurrentPage() < brPage.getTotalPages()) {
            fileObjectResponse = fileClient.getList("/1/objects?tags=gigwa&page="+(brPage.getCurrentPage()+1)+"&status=" + status, FileObjectTo.class);
            fileObjects.addAll(fileObjectResponse.getResult().getData());
            brPage = fileObjectResponse.getMetadata().getPagination();
        }
        fileObjects.forEach(f -> f.setTenant(1));
        return fileObjects;
    }

    @Override
    public FileObjectTo findByKey(UUID id) {
        if (id == null)
            return null;
        
        return Optional.ofNullable(fileClient.getList("/1/objects?tags=gigwa&id=" + id.toString(), FileObjectTo.class))
            .map(res -> res.getResult().getData())
            .map(data -> {
                if(data.isEmpty())
                    return null;
                else{
                    data.get(0).setTenant(1);
                    return data.get(0);
                }
                
            })
            .orElse(null);        
	}

}

package org.ebs.services.custom;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.ebs.model.BatchModel;
import org.ebs.model.SampleDetailModel;
import org.ebs.model.repos.SampleDetailRepository;
import org.ebs.services.to.custom.BatchDesignTo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Service @Transactional(readOnly = true)
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class LaboratoryReportServiceImpl implements LaboratoryReportService{

	
	private final SampleDetailRepository sampledetailRepository;
	


	private short cellKBiocontrolRandom = 52;


	private String [] headerList = {"experiment_name", "occurrence_code", "site_name","field_name", "experiment_year",
			"experiment_season", "experiment_entry_number", "plot_code", "germplasm_code", "pedigree", "seed_generation", "seed_code",
			"material_type", "X", "Y", "Plant #", "Plate_Name", "Well_Position", "Sample_Name", "Sample_ID", "UUID"};
	private XSSFCellStyle styleCellYellow;
	private XSSFCellStyle styleCellGreen;
	private XSSFCellStyle styleCellBlue;
	private XSSFCellStyle styleCellNormal;
	private static final Logger log = LoggerFactory.getLogger(LaboratoryReportServiceImpl.class);
	@Override
	public byte[] createReportLaboratory(BatchDesignTo bean) {
	
		return null;
		}

	private  List<SampleDetailModel> getSampleDetailListByBatchID (BatchDesignTo bean) {
		return   sampledetailRepository.findByBatchId(bean.getIdBatch());
	}


	private XSSFWorkbook createWorkbook(BatchDesignTo bean,  StudyLabReportBean studylabReportBean ) {

		XSSFWorkbook book = new XSSFWorkbook();  // or new XSSFWorkbook();
		XSSFSheet plateView = book.createSheet("Plate View");
		log.debug(plateView.toString());
		

		XSSFSheet sampleListView = book.createSheet("Sample List View");
		styleCellYellow = setStyleByColor(book, 1);
		styleCellGreen = setStyleByColor(book, 2);
		styleCellBlue = setStyleByColor(book, 3);
		styleCellNormal = setStyleByColor(book, 4);
		createSampleListView(sampleListView, book);

	return book;
	}

	private void createSampleListView (XSSFSheet sheet, XSSFWorkbook book) {
	
	}

	private void validateCells (XSSFRow rowData) {
		for (int index = 0 ; index < 21 ; index++) {
			if (rowData.getCell(index) == null)rowData.createCell(index);
		}
	}

	private void createHeader (XSSFSheet sheet, XSSFWorkbook book) {
		XSSFRow rowData = sheet.createRow(0);
		int index = 0;
		for (String header: headerList) {
			switch (index) {
			case 0:
				loadDataCellColor(rowData, header, index,  styleCellYellow, true);
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				loadDataCellColor(rowData, header, index, styleCellGreen, true);
				break;
			case 6 :
			case 7 :
				loadDataCellColor(rowData, header, index, styleCellYellow, true);
			break;
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
				loadDataCellColor(rowData, header, index, styleCellGreen, true);
				break;
			case 13 :
			case 14:
			case 15:
			case 16 :
			case 17:
				loadDataCellColor(rowData, header, index, styleCellYellow, true);
			break;
			case 18 :
			case 19:
			case 20 :
				loadDataCellColor(rowData, header, index, styleCellBlue, true);
			}
			index++;
		}
	}






	private byte[] inputStreamToBytes(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
		byte[] buffer = new byte[1024];
		int len;
		while((len = in.read(buffer)) >= 0)
		out.write(buffer, 0, len);
		in.close();
		out.close();
		return out.toByteArray();
	}

	private StudyLabReportBean createPlateMaps(List<SampleDetailModel> sampleDetailList, BatchModel batchModel ) {
		SortedMap <Integer, Map<String , SampleDetailModel>> mapPlate = new TreeMap <Integer, Map<String , SampleDetailModel>>();

		for (SampleDetailModel sample : sampleDetailList) {

			Integer numberPlate = Integer.parseInt(sample.getPlateName().substring(1, sample.getPlateName().length()));
			if (mapPlate.containsKey(numberPlate)){
				Map<String , SampleDetailModel> mapDetail =
				(Map<String , SampleDetailModel>)mapPlate.get(numberPlate);
				mapDetail.put(sample.getPlateName()+sample.getRow()+sample.getColumn(), sample);
			}else {
				Map<String , SampleDetailModel> mapDetail = new HashMap<String, SampleDetailModel>();
				mapDetail.put(sample.getPlateName()+sample.getRow()+sample.getColumn(), sample);
				mapPlate.put(numberPlate, mapDetail);
			}
		}

		StudyLabReportBean beanReport = new StudyLabReportBean ();
		beanReport.setMapPlateSamples(mapPlate);
		beanReport.setPrefix(batchModel.getName());
		beanReport.setPatternPlate(batchModel.getName());
		beanReport.setPadded(true);
		beanReport.setNameRow(PlateContentList.letters96);
		beanReport.setNumberColumn(PlateRow.COLS_PLATE_96);

		return beanReport;

	}

	private XSSFCellStyle styleCellNormally(XSSFWorkbook objLibro, boolean isHeader){
		XSSFFont sourceStyle = objLibro.createFont();
		sourceStyle.setFontHeightInPoints((short)11);
		sourceStyle.setBold(true);
		//sourceStyle.setBoldweight((short)11);
		sourceStyle.setFontName(HSSFFont.FONT_ARIAL);
		if (isHeader){
			sourceStyle.setBold(true);
			//sourceStyle.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		}
		XSSFCellStyle styleCell = objLibro.createCellStyle();
		styleCell.setWrapText(true);
		//styleCell.setAlignment(XSSFCellStyle.ALIGN_JUSTIFY);
		styleCell.setFillForegroundColor(cellKBiocontrolRandom);
		//styleCell.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);
		styleCell.setFont(sourceStyle);
		return styleCell;
	}

	private XSSFCellStyle getStyleCeldSolidForeground (XSSFWorkbook objBook, short foregroundColor){
		XSSFFont sourceStyle = objBook.createFont();
		sourceStyle.setFontHeightInPoints((short)11);
		sourceStyle.setFontName(HSSFFont.FONT_ARIAL);
		XSSFCellStyle stileCell = objBook.createCellStyle();
		stileCell.setWrapText(true);
		//stileCell.setAlignment(XSSFCellStyle. ALIGN_JUSTIFY);
		//stileCell.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);
		stileCell.setFont(sourceStyle);
		stileCell.setFillForegroundColor(foregroundColor);
		//stileCell.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		return stileCell;
	}

	private int createHeaderPlate (XSSFSheet sheet, int rowCounter, int numberColumn, XSSFCellStyle styleCellNormallyHeader, String plateName){
		XSSFRow rowData = sheet.createRow(rowCounter);
		for (int index = 1; index<= numberColumn; index++){
			loadDataCell(rowData,String.valueOf(index), index+1, styleCellNormallyHeader);
		}
		return ++rowCounter;
	}

	private XSSFCellStyle  setStyleByColor(XSSFWorkbook book, int color) {
		XSSFCellStyle styleCell = book.createCellStyle();
	 	switch (color ) {
      	case 1:
      		//yellow
      		styleCell.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 255, 0)));
      		styleCell.setFillPattern(FillPatternType.SOLID_FOREGROUND);
      	break;
      	case 2:
      		//green
      		styleCell.setFillForegroundColor(new XSSFColor(new java.awt.Color(112, 173, 71)));
      		styleCell.setFillPattern(FillPatternType.SOLID_FOREGROUND);
      		break;
      	case 3:
      		styleCell.setFillForegroundColor(new XSSFColor(new java.awt.Color(0, 176, 240)));
      		styleCell.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	 	}
	 	return styleCell;
	}

	private void loadDataCellColor (XSSFRow rowData, String value, int index, XSSFCellStyle styleCell, boolean isMedium){

		XSSFCell dataCell = rowData.createCell(index);
      	XSSFRichTextString cellValue = new XSSFRichTextString(value);
      	if (isMedium) {
      		styleCell.setBorderBottom(BorderStyle.MEDIUM);
      		styleCell.setBorderRight(BorderStyle.MEDIUM);
      		styleCell.setBorderTop(BorderStyle.MEDIUM);
      	}else {
      		styleCell.setBorderBottom(BorderStyle.THIN);
      		styleCell.setBorderRight(BorderStyle.THIN);
      		styleCell.setBorderTop(BorderStyle.THIN);
      	}

      	dataCell.setCellStyle(styleCell);
      	
		//dataCell.setCellType(XSSFCell.CELL_TYPE_STRING);
		dataCell.setCellValue(cellValue);
	}


	private void loadDataCell (XSSFRow rowData, String value, int index, XSSFCellStyle styleCellNormallyHeader){
		XSSFCell dataCell = rowData.createCell(index);
      	XSSFRichTextString cellValue = new XSSFRichTextString(value);
      	styleCellNormallyHeader.setBorderBottom(BorderStyle.MEDIUM);
      	styleCellNormallyHeader.setBorderRight(BorderStyle.MEDIUM);
      	styleCellNormallyHeader.setBorderTop(BorderStyle.MEDIUM);
      	styleCellNormallyHeader.setFillForegroundColor(new XSSFColor(new java.awt.Color(198, 224, 180)));
      	styleCellNormallyHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
      	dataCell.setCellStyle(styleCellNormallyHeader);
		//dataCell.setCellType(XSSFCell.CELL_TYPE_STRING);
		dataCell.setCellValue(cellValue);
	}

	private void loadDataGeneralInformation (XSSFRow rowData, String value, int index, XSSFCellStyle styleCellNormallyHeader){
		XSSFCell dataCell = rowData.createCell(index);
      	XSSFRichTextString cellValue = new XSSFRichTextString(value);
      	styleCellNormallyHeader.setFillForegroundColor(new XSSFColor(new java.awt.Color(142, 169, 219)));
      	styleCellNormallyHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
      	dataCell.setCellStyle(styleCellNormallyHeader);
		//dataCell.setCellType(XSSFCell.CELL_TYPE_STRING);
		dataCell.setCellValue(cellValue);
	}

	private void writeCellHeader(XSSFRow rowData, int sizeColumn, String strValue, XSSFCellStyle style){
		XSSFCell dataCell = rowData.createCell(sizeColumn);
      	XSSFRichTextString cellValue = new XSSFRichTextString(strValue);
   		dataCell.setCellStyle(style);
		//dataCell.setCellType(XSSFCell.CELL_TYPE_STRING);
		dataCell.setCellValue(cellValue);
	}


	private XSSFCellStyle styleCellHeader(XSSFWorkbook objLibro){
		XSSFFont sourceStyle = objLibro.createFont();
		sourceStyle.setFontHeightInPoints((short)11);
		sourceStyle.setBold(true);
		sourceStyle.setFontName(HSSFFont.FONT_ARIAL);
		//sourceStyle.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		XSSFCellStyle styleCell = objLibro.createCellStyle();
		styleCell.setWrapText(true);
		//styleCell.setAlignment(XSSFCellStyle. ALIGN_JUSTIFY);
		//styleCell.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);
		//styleCell.setBorderBottom(BorderStyle.MEDIUM);
		//styleCell.setBorderRight(BorderStyle.MEDIUM);
		//styleCell.setBorderTop(BorderStyle.MEDIUM);
		styleCell.setFillForegroundColor(new XSSFColor(new java.awt.Color(198, 224, 180)));
		 styleCell.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		styleCell.setFont(sourceStyle);
		return styleCell;
	}


	private void writeCell(XSSFRow rowData, int sizeColumn, String strValue, XSSFCellStyle style){
		XSSFCell dataCell = rowData.createCell(sizeColumn);
      	XSSFRichTextString cellValue = new XSSFRichTextString(strValue);
      	if (style != null) {
      		style.setBorderBottom(BorderStyle.THIN);
            style.setBorderRight(BorderStyle.THIN);
            style.setBorderTop(BorderStyle.THIN);
            style.setWrapText(true);
      		dataCell.setCellStyle(style);
      	}
		//dataCell.setCellType(XSSFCell.CELL_TYPE_STRING);
		dataCell.setCellValue(cellValue);
	}

}

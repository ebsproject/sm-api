package org.ebs.services.custom;

import java.util.List;

import org.ebs.services.to.custom.AssayTo;
import org.ebs.services.to.custom.MarkerPanelTo;
import org.ebs.services.to.custom.MarkerTo;
import org.ebs.services.to.custom.ServiceProviderTo;
import org.ebs.services.to.custom.TechnologyServiceProviderTo;
import org.ebs.services.to.custom.TechnologyTo;
import org.ebs.services.to.custom.TraitCategoryTo;
import org.ebs.services.to.custom.TraitCustomTo;
import org.ebs.services.to.custom.VendorExternalTo;

public interface MarkerService {
	
	public MarkerPanelTo findMarkerGroupById(int id, boolean withMarkers);
	
	public TraitCategoryTo findTraitCategory(int id) ;
	
	public TraitCustomTo findTraitById (int id );

	public List<TraitCustomTo> findTraitCustoms(String filter);
	
	public List<MarkerPanelTo> findMarkerGroupByName(String filter);
	
	public AssayTo findAssayById (int id);

	public MarkerTo findMarkerById (int id);
	
    public List<ServiceProviderTo> findServiceProviders(String filter);

    public List<String> findAssayNamesByGroupAndTechServiceProv(int markerGroupId, int techServProv);

    public List<String> findAssayNamesByMarkerIdsAndTechServiceProv(List<Integer> markerIds, int techServProvider);

	public TechnologyServiceProviderTo findTechnologyAndSPBTechSerProviderId(int id);
	
	public VendorExternalTo findVendorExternalTo(int id);
	
	public TechnologyTo findTechnologyTo(int id);
	
	public List <TechnologyServiceProviderTo> getTechnologyServiceProviderToList(String filter);
}

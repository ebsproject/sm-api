package org.ebs.services.custom;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.ebs.model.RequestStatusModel.REQ_STATUS_DESIGN_CREATED;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.ebs.model.BatchCodeModel;
import org.ebs.model.BatchModel;
import org.ebs.model.CollectionLayoutModel;
import org.ebs.model.ControlTypeModel;
import org.ebs.model.LoadTypeModel;
import org.ebs.model.RequestListMemberModel;
import org.ebs.model.RequestModel;
import org.ebs.model.RequestStatusModel;
import org.ebs.model.SampleDetailModel;
import org.ebs.model.StatusModel;
import org.ebs.model.repos.BatchCodeRepository;
import org.ebs.model.repos.BatchRepository;
import org.ebs.model.repos.ControlTypeRepository;
import org.ebs.model.repos.PurposeRepository;
import org.ebs.model.repos.RequestListMemberRepository;
import org.ebs.model.repos.RequestRepository;
import org.ebs.model.repos.RequestStatusRepository;
import org.ebs.model.repos.SampleDetailRepository;
import org.ebs.model.repos.ServiceRepository;
import org.ebs.model.repos.StatusRepository;
import org.ebs.services.BatchMarkerGroupCustomService;
import org.ebs.services.BatchMarkerGroupService;
import org.ebs.services.BatchService;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.BatchCodeTo;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.RequestListMemberTo;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.Input.BatchInput;
import org.ebs.services.to.Input.BatchMarkerGroupCustomInput;
import org.ebs.services.to.Input.BatchMarkerGroupInput;
import org.ebs.services.to.Input.ServiceProviderInput;
import org.ebs.services.to.custom.BatchDesignTo;
import org.ebs.services.to.custom.BatchMarkerGoupCustomTo;
import org.ebs.services.to.custom.BatchMarkerGroupTo;
import org.ebs.services.to.custom.ControlSampleTo;
import org.ebs.services.to.custom.ControlVendorTo;
import org.ebs.services.to.custom.PlateTo;
import org.ebs.services.to.custom.RequestIDTo;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.SortMod;
import org.ebs.util.client.CompositeBatchRef;
import org.ebs.util.client.RestCsClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.Synchronized;

@Service  
@RequiredArgsConstructor
public class BatchDesignServiceImpl implements BatchDesignService {
	
	private final BatchService batchService;
	private final RequestListMemberService requestListMemberService;
	private final BatchRepository batchRepository;
	private final SampleDetailRepository SampleDetailRepository;
	private final BatchCodeRepository batchCodeRepository; 
	private final RequestRepository requestRepository; 
	private final PurposeRepository purposeRepository;
	private final RequestListMemberRepository requestListMemberRepository;
	private final ControlTypeRepository controlTypeRepository;
	private final ServiceRepository serviceRepository;
	private final StatusRepository statusRepository;
	private final RequestStatusRepository requestStatusRepository;
	private final CSProgram csProgram;
	private final ConversionService converter;
	private final BatchMarkerGroupService batchMarkerGroupService;
	private final BatchMarkerGroupCustomService batchMarkerGroupCustomService;
	
	private final RestCsClient restCsClient; 
	
	private int totalOfControls = 0;
	private List<String> blankControlList = null;
	
	private String positiveControl ;
	
	private List<ControlVendorTo> listControlVendor = new ArrayList<ControlVendorTo>();
	
 
	//private static  Map<String, BatchCodeModel> mapModelFlush = new HashMap<String, BatchCodeModel>()  ;
	
    private final static Logger LOG = LoggerFactory.getLogger(BatchDesignServiceImpl.class);
    
    @PostConstruct
    public void init() {
    	LOG.info("Created by thread: " + Thread.currentThread().getId());
    	//LOG.info("CPU>>>>>>> "+ Runtime.getRuntime().availableProcessors());
    }
    
    @Synchronized
    @Override @Transactional(readOnly = false)
	public void createSamplesAsynchronous(BatchDesignTo bean, Authentication auth, BatchTo to) {
    	LOG.info("Init batch creation process step 1" );
        SecurityContextHolder.getContext().setAuthentication(auth);
        LOG.info("Init batch creation process" );
        saveSamplesAsynchronous(bean, to);
        LOG.info("Ended batch creation process");
	}

    @Override @Transactional(readOnly = false)
    public BatchTo saveBatch(BatchDesignTo bean) {
    	BatchInput model = new BatchInput();

    	model.setName(bean.getBatch().getName());
    	model.setObjective(bean.getBatch().getName());
    	model.setTenant(bean.getBatch().getTenantID());
    	model.setNumMembers(bean.getBatch().getTotal());
    	
    	if (bean.getBatch() != null) {
    		if (bean.getBatch().getServiceprovider()!= null 
        			&& bean.getBatch().getServiceprovider().intValue() > 0) {
        		ServiceProviderInput spInput = new ServiceProviderInput();  
        		spInput.setId(bean.getBatch().getServiceprovider().intValue());
        		model.setServiceprovider(spInput);
        	}
    		if (bean.getBatch().getTechnologyplataform()!= null 
        			&& bean.getBatch().getTechnologyplataform().intValue() > 0)
        	model.setTechnologyPlatformId(bean.getBatch().getTechnologyplataform().intValue());
    		
    		if (bean.getBatch().getVendorId() != null && bean.getBatch().getVendorId().intValue() > 0)
    			model.setVendorId(bean.getBatch().getVendorId().intValue());
 
    		if (bean.getBatch().getReportId() != null && bean.getBatch().getReportId().intValue() > 0)
    			model.setReportId(bean.getBatch().getReportId().intValue());
    	}

    	model.setIdCrop(bean.getBatch().getCropID());
    	model.setTenant(bean.getIdTenant());
  	 	
    	return batchService.createBatch(model);
    }
  
    private void saveSamplesAsynchronous(BatchDesignTo bean, BatchTo to) {
        this.totalOfControls = 0;
        listControlVendor = getListControlVendor(bean.getVendorControls());
        int totalPlates = getTotalPlate(bean, to, listControlVendor.size());
        LOG.info("batch to {}", to);

        List<Integer> reqIds = Arrays.stream(bean.getArrRequestID()).boxed().collect(toList());
        Map<String, String> args = extractRuleArguments(to.getId(), reqIds);

        CompositeBatchRef CompositeBatchRef = restCsClient.getBatchCodes(args, totalPlates, bean.getBatch().getTotal());
        blankControlList = new ArrayList<String>(
                Arrays.asList(restCsClient.getSequence(RestCsClient.BLANK_SAMPLE_RULE_ID, args, totalOfControls)));
        positiveControl = restCsClient.getSequence(RestCsClient.CONTROL_SAMPLE_RULE_ID, args);
        
        if (CompositeBatchRef == null | (CompositeBatchRef.getPlateCodeList() == null
                | CompositeBatchRef.getPlateCodeList().size() == 0 |
                CompositeBatchRef.getSampleCodeList() == null | CompositeBatchRef.getSampleCodeList().size() == 0))
            throw new RuntimeException("The SM API can't get codes from the Rule Managment");
        LOG.info("Samples created size : "+CompositeBatchRef.getSampleCodeList().size());
        LOG.info("Plates created size : "+CompositeBatchRef.getPlateCodeList().size());
        switch (bean.getIdPlateDirection()) {
            case 1:
                saveColumnDirectionsAsynchronous(totalPlates, bean, to, CompositeBatchRef);
                break;
            case 2:
                saveRowsDirectionsAsynchronous(totalPlates, bean, to, CompositeBatchRef);
                break;
            default: {
                throw new RuntimeException("This Plate fill direction Does not exist:" + bean.getIdPlateDirection());
            }
        }
    }

	@Override
	public Map<String, String> extractRuleArguments(int batchId) {
		List<Integer> reqIds = batchService.getRequesByBatchID(batchId).map(r -> r.getId()).getContent();
		return extractRuleArguments(batchId, reqIds);
	}

    Map<String, String> extractRuleArguments(int batchId, List<Integer> requestIds) {
        List<RequestModel> requests = requestRepository.findAllById(requestIds);
        String cropIds = flatMapRequestValues(requests, r -> r.getIdCrop());
        String programIds = flatMapRequestValues(requests, r -> r.getIdProgram());
        String purposeIds = flatMapRequestValues(requests, r -> r.getPurpose().getId());

        BatchModel batch = batchRepository.findById(batchId)
                .orElseThrow(() -> new RuntimeException("batch " + batchId + " not found"));

        Map<String, String> args = new HashMap<>();
        args.put("3", programIds); //program id
        args.put("6", purposeIds); //purpose id
        args.put("12", String.valueOf(batchId)); //batch id
        args.put("16", String.valueOf(batch.getVendorId())); //vendor (markerDB Serv Prov)
        args.put("17", String.valueOf(batchId)); //batch id
        args.put("18", cropIds); //crop id
        args.put("19", cropIds); //crop id
        String servProvId = String.valueOf(batch.getServiceprovider().getId());
        args.put("20", requestIds.stream().map(String::valueOf).collect(joining(","))); //request id
        args.put("21", servProvId); //SM service provider id
        args.put("22", servProvId); //SM service provider id
        args.put("39", String.valueOf(batchId)); //SM batch id
        return args;
    }
    
    <R>String flatMapRequestValues(List<R> entities, Function<? super R,?> entityFunction) {
        return entities.stream().map(entityFunction).map(v -> String.valueOf(v)).collect(joining(","));
    }

	private void saveColumnDirectionsAsynchronous (int totalPlates,  BatchDesignTo bean, BatchTo to , CompositeBatchRef compositeBatchRef ) {
		LOG.info("Saving samples in column direcction.." );
		
		BatchModel batchModel = null; 
		int indexSample = 1;
		try {		
            int indexLastPlate = 0;
            indexLastPlate++;
            batchModel = batchRepository.findById(to.getId()).get();
            boolean areTherePlatesWithControls = bean.getPlates() != null && !bean.getPlates().isEmpty()? true : false ;
            List<Integer> listRequestListMemberId = new ArrayList<Integer>();
            for(int requestID:  bean.getArrRequestID()) {
                listRequestListMemberId.addAll(batchService.getRequestListMemberByRequestId(requestID));
            }
            if(listRequestListMemberId.size() == 0)
                    throw new RuntimeException("This batch does not have any request to store");
            List<String> platList = new ArrayList<String> (compositeBatchRef.getPlateCodeList());
            List<String> sampleListArray  = new ArrayList<String> (compositeBatchRef.getSampleCodeList());
            for (int plate = 0 ; plate < totalPlates; ) {
                String plateName = platList.get(0);
                Integer plateNumberInt = getPlateNumber(platList.get(0));
                for (int col = 1 ; col <= getColSize(bean.getIdCollection()); col ++) {
                    for (int row = 1; row <= getRowSize(bean.getIdCollection()); row++) {
                        SampleDetailModel sampleDetail = new SampleDetailModel();
                        sampleDetail.setId(0);
                        sampleDetail.setRow(getRowLetter(row));
                        sampleDetail.setColumn(col);
                        sampleDetail.setPlateName(plateName);
                        sampleDetail.setPlateId(plateNumberInt);
                        sampleDetail.setIdCrop(batchModel.getIdCrop());
                        sampleDetail.setTenant(bean.getIdTenant());
                        sampleDetail.setBatch(batchModel);
                        RequestListMemberModel model= null;
                        boolean isThereSampleControl = false;
                    
                        if (isToVendorControl(row, col)) {
                            sampleDetail.setVendorControl(true);
                            sampleDetail.setSampleCode("");
                            isThereSampleControl = true;
                        }else if (areTherePlatesWithControls) {
                            for (PlateTo plates : bean.getPlates()) {
                                if (plates.getPlateId().intValue() == indexLastPlate) {
                                    if (plates.getControlSamples() != null && !plates.getControlSamples().isEmpty())
                                    for (ControlSampleTo control : plates.getControlSamples()) {
                                        if (control.getColumn().intValue() == col && control.getRow().intValue() == row){
                                            setControlsPerPlate(sampleDetail, control);
                                            isThereSampleControl = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (!isThereSampleControl) 
                            if( listRequestListMemberId.size() >0  && sampleListArray.size() > 0) {
                                model = requestListMemberRepository.findById(listRequestListMemberId.get(0)).get();
                                String sampleStrId = sampleListArray.get(0);
                                sampleDetail.setSampleNumber(indexSample);
                                sampleDetail.setSampleCode(sampleStrId);
                                indexSample++;
                                sampleDetail.setDataId(model.getDataId());
                                sampleDetail.setEntityId(model.getEntityId());
                                sampleDetail.setGermplasmId(model.getGermplasmId());
                                sampleDetail.setSampleItemId(model.getTotalEntities());
                                listRequestListMemberId.remove(0);
                            
                                if (sampleListArray.size() > 0)
                                    sampleListArray.remove(0);
                            }else {
                                setBlankWell(sampleDetail);
                            }
                            SampleDetailRepository.save(sampleDetail);
                            if (model != null) {
                                model.setSampleDetail(sampleDetail);
                                requestListMemberRepository.save(model);
                            }
                    }
                }
                if (platList.size() > 0)
                    platList.remove(0);
                indexLastPlate++;
                plate ++;
                LOG.info("Plate created # > "+ plate );

            }
            updteBatchModel(batchModel, bean, totalPlates);
            changeRequestStatus(bean.getArrRequestID(), bean.getIdTenant(), REQ_STATUS_DESIGN_CREATED, "");
            createRequestListMemberElements(to.getId(), bean.getArrRequestID());
            saveBatchMarherGroups( batchModel, bean); 
            LOG.info("Enging creation samples in column direcction.." );
		}catch (Exception ex) {
			ex.printStackTrace();
			LOG.error(ex.getMessage());
			throw ex;
		}
	}
	
	private void saveBatchMarherGroups(BatchModel batchModel,BatchDesignTo bean) {
		if (bean.getBatch().getListBatchMarkerGroup() != null && bean.getBatch().getListBatchMarkerGroup().size() > 0 ) {
			try {
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			LOG.info("SAVING Batch Marker Group # > " );
			for (BatchMarkerGroupTo tobatchMarGro:bean.getBatch().getListBatchMarkerGroup()) {
				BatchMarkerGroupInput modelBMG = new BatchMarkerGroupInput();
				BatchInput batchInput = new BatchInput();
				batchInput.setId(batchModel.getId()); 
				modelBMG.setBatch(batchInput);
				modelBMG.setFixed(tobatchMarGro.getFixed()==0 ? false: true);
				modelBMG.setMarkerGroupID(tobatchMarGro.getMarkerGroupID());
				modelBMG.setTechnologyServiceProviderId(tobatchMarGro.getTechnologyServiceProviderId());
				LOG.info("Batch Marker Group # > " +modelBMG.toString() );
				org.ebs.services.to.BatchMarkerGroupTo result = batchMarkerGroupService.createBatchMarkerGroup(modelBMG);
				modelBMG.setId(result.getId());
				LOG.info("Is fixed Batch Marker Group # > " +tobatchMarGro.getFixed());	
				if (tobatchMarGro.getFixed() == 0) {
					if (tobatchMarGro.getListBatchMarkerGroupCustom() != null && tobatchMarGro.getListBatchMarkerGroupCustom().size() > 0) {
						for (BatchMarkerGoupCustomTo custom : tobatchMarGro.getListBatchMarkerGroupCustom()) {
							BatchMarkerGroupCustomInput inputBMGC = new BatchMarkerGroupCustomInput();
							inputBMGC.setAssayId(custom.getAssayId());
							inputBMGC.setMarkerId(custom.getMarkerId());
							inputBMGC.setBatchMarkerGroup(modelBMG);
							batchMarkerGroupCustomService.createBatchMarkerGroupCustom(inputBMGC);
						}
					}
				}
			}
		
		}catch (Exception ex) {
					ex.printStackTrace();
		 }
		}
	}

	private Integer getPlateNumber(String string) {
		  Pattern pat = Pattern.compile("\\d+$");
		  Matcher mat = pat.matcher(string);
		  if (mat.find())
			  return Integer.valueOf(mat.group()); 
		  return 0;
		  
	}
	private void saveRowsDirectionsAsynchronous (int totalPlates,BatchDesignTo bean, BatchTo to, CompositeBatchRef compositeBatchRef) {
		LOG.info("Saving samples in row direcction.." );
		BatchModel batchModel = null; 
		try {
            int indexLastPlate = 0;
            indexLastPlate++;
            int indexSample = 1;
            batchModel = batchRepository.findById(to.getId()).get(); 
            boolean areTherePlatesWithControls = bean.getPlates() != null && !bean.getPlates().isEmpty()? true : false ;
            List<Integer> listRequestListMemberId = new ArrayList<Integer>();
            for(int requestID:  bean.getArrRequestID()) {
                listRequestListMemberId.addAll(batchService.getRequestListMemberByRequestId(requestID));
            }
            if(listRequestListMemberId.size() == 0)
                        throw new RuntimeException("This batch does not have any request to store");
            List<String> platList = new ArrayList<String> (compositeBatchRef.getPlateCodeList());
            List<String> sampleListArray  = new ArrayList<String> (compositeBatchRef.getSampleCodeList());
            for (int plate = 0 ; plate < totalPlates; ) {
                String plateName = platList.get(0);
                Integer plateNumberInt = getPlateNumber(platList.get(0));
                for (int row = 1 ; row <= getRowSize(bean.getIdCollection()); row ++) {
                    for (int col = 1; col <= getColSize(bean.getIdCollection()); col++) {
                        SampleDetailModel sampleDetail = new SampleDetailModel();
                        sampleDetail.setId(0);
                        sampleDetail.setRow(getRowLetter(row));
                        sampleDetail.setColumn(col);
                        sampleDetail.setPlateName(plateName);
                        sampleDetail.setPlateId(plateNumberInt);
                        sampleDetail.setIdCrop(batchModel.getIdCrop());
                        sampleDetail.setBatch(batchModel);
                        sampleDetail.setTenant(bean.getIdTenant());
                        RequestListMemberModel model = null;
                        boolean isTheSampleControl =false;
                        if (isToVendorControl( row, col)) {
                            sampleDetail.setVendorControl(true);
                            sampleDetail.setSampleCode("");
                            isTheSampleControl = true;
                        }else if (areTherePlatesWithControls) {
                            for (PlateTo plates : bean.getPlates()) {
                                if (plates.getPlateId().intValue() == indexLastPlate) {
                                    if (plates.getControlSamples() != null && !plates.getControlSamples().isEmpty())
                                    for (ControlSampleTo control : plates.getControlSamples()) {
                                        if (control.getColumn().intValue() == col && control.getRow().intValue() == row) {
                                            setControlsPerPlate(sampleDetail, control);
                                            isTheSampleControl = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (!isTheSampleControl) 
                            if( listRequestListMemberId.size() >0 &&  sampleListArray.size() > 0) {
                                String sampleStrId = sampleListArray.get(0);
                                sampleDetail.setSampleNumber(indexSample);
                                model = requestListMemberRepository.findById(listRequestListMemberId.get(0)).get();
                                indexSample++;
                                sampleDetail.setSampleCode(sampleStrId);
                                sampleDetail.setDataId(model.getDataId());
                                sampleDetail.setEntityId(model.getEntityId());
                                sampleDetail.setGermplasmId(model.getGermplasmId());
                                sampleDetail.setSampleItemId(model.getTotalEntities());
                                listRequestListMemberId.remove(0);
                                if (sampleListArray.size() > 0)
                                    sampleListArray.remove(0);
                            }else {
                                setBlankWell(sampleDetail);
                            }
                            SampleDetailRepository.save(sampleDetail);
                            if (model != null) {
                                model.setSampleDetail(sampleDetail);
                                requestListMemberRepository.save(model);
                            }
                    }
                }
                if (platList.size() > 0)
                    platList.remove(0);
                indexLastPlate++;
                plate ++;
                LOG.info("Plate created # > "+ plate );
            }
            updteBatchModel(batchModel, bean, totalPlates);
            changeRequestStatus(bean.getArrRequestID(), bean.getIdTenant(), REQ_STATUS_DESIGN_CREATED, "");
            createRequestListMemberElements(to.getId(), bean.getArrRequestID());
            saveBatchMarherGroups( batchModel, bean); 
            LOG.info("Enging creation samples in row direcction.." );
					
		}catch (Exception ex){
			ex.printStackTrace();
			LOG.error(ex.getMessage());
			throw ex;
		}
	}
    
	private void setBlankWell (SampleDetailModel sampleDetail) {
		if (this.blankControlList.size() > 0) {
			sampleDetail.setSampleCode( this.blankControlList.get(0) );
			this.blankControlList.remove(0);
		}
	}
	
	private void setControlsPerPlate (SampleDetailModel sampleDetail, ControlSampleTo control) {
		ControlTypeModel controlTypeMode = controlTypeRepository.findById(control.getControlId()).get();	
	
		sampleDetail.setControltype(controlTypeMode);
		switch (controlTypeMode.getId()){
			case 1 :{ 
					if (this.blankControlList.size() > 0) {
					sampleDetail.setSampleCode( this.blankControlList.get(0) );
					this.blankControlList.remove(0);
					}
					break;
			}
			case 3 :{
				sampleDetail.setSampleCode( this.positiveControl );
				break;
			}
			default :{
				if (this.blankControlList.size() > 0) {
					sampleDetail.setSampleCode( this.blankControlList.get(0) );
					this.blankControlList.remove(0);
					}
					break;
			}
		}
	}
	 	
	 @Synchronized
	private void updteBatchModel (BatchModel batchModel, BatchDesignTo bean, int totalPlates) {
		batchModel.setNumContainers(totalPlates);
		CollectionLayoutModel collectionlayout = new CollectionLayoutModel();
		collectionlayout.setId(bean.getIdCollection());
		batchModel.setCollectionlayout(collectionlayout);
		LoadTypeModel loadtype = new LoadTypeModel();
		loadtype.setId(bean.getIdPlateDirection());
		batchModel.setLoadtype(loadtype);
		
		batchModel.setInDesign(false);
		batchRepository.save(batchModel);
	}
	
	 @Synchronized
     private void updateBatchCodeGroup(BatchCodeTo toBatchCode, int indexSample, int totalPlates) {
         BatchCodeModel model = batchCodeRepository.findById(toBatchCode.getId()).get();
         //BatchCodeModel modelCodeBatch = mapModelFlush.get(String.valueOf(toBatchCode.getId()));
         model.setLastSampleNumber(toBatchCode.getLastSampleNumber() + indexSample);
         model.setLastPlateNumber(toBatchCode.getLastPlateNumber() + totalPlates);
         model.setLastBatchNumber(toBatchCode.getLastBatchNumber() + 1);
         BatchCodeModel modelFlush = batchCodeRepository.saveAndFlush(model);
         //modelFlush(modelFlush);

     }
	
     @Override
     public void changeRequestStatusByBatch(int batchId, int tenantId, int statusId, String statusMessage) {
         int[] ids = batchService.getRequesByBatchID(batchId).get()
                 .mapToInt(RequestIDTo::getId)
                 .toArray();
         changeRequestStatus(ids, tenantId, statusId, statusMessage);
     }

    /**
     * Changes provided requests to the specified status,
     * Performing proper actions in request status log.
     * @param arrRequestID array of request ids to modify
     * @param tenantID current tenant
     * @param statusId new status of requests
     */
    private void changeRequestStatus(int[] arrRequestID, int tenantID, int statusId, String statusMessage) {
        for (int id : arrRequestID) {
            changeRequestStatus(id, tenantID, statusId, statusMessage);
        }
    }
     
    /**
     * Changes a request to the specified status,
     * Performing proper actions in request status log.
     * @param requestId request to modify
     * @param tenantID current tenant
     * @param statusId new status of request
     */
    public void changeRequestStatus(int requestId, int tenantID, int statusId, String statusMessage){
		LOG.debug("changeRequestStatus: {}", statusId);
        RequestModel modelRequest = requestRepository.findById(requestId).get();
        StatusModel statusModel = statusRepository.findById(statusId).get();
        modelRequest.setStatus(statusModel);
        requestRepository.save(modelRequest);
        for (RequestStatusModel requestStatusModel : requestStatusRepository
                .findByRequestId(modelRequest.getId())) {
            if (requestStatusModel.getCompleted() == null) {
                requestStatusModel.setCompleted(new Date());
                requestStatusRepository.save(requestStatusModel);
            }
        }

        RequestStatusModel requestStatusModel = new RequestStatusModel();
        requestStatusModel.setId(0);
        requestStatusModel.setTenant(tenantID);
        requestStatusModel.setRequest(modelRequest);
        requestStatusModel.setCreatedOn(new Date());
        requestStatusModel.setStatus(statusModel);
		requestStatusModel.setComments(statusMessage);
        requestStatusRepository.save(requestStatusModel);
    }

    private List<ControlVendorTo> getListControlVendor(String vendorControls){
    	List<ControlVendorTo> listControlVendorTo = new ArrayList<ControlVendorTo>();
    	if (vendorControls != null && !vendorControls.isEmpty()) {
    	String [] arr =  vendorControls.split(",");
    	String[] arrWithoutDuplicates = Arrays.stream(arr).distinct().toArray(String[]::new);
    	for (String controlStr : arrWithoutDuplicates ){
    		ControlVendorTo controlVendor = new ControlVendorTo();
    		controlVendor.setValue(controlStr);
    		Pattern pattern = Pattern.compile("[0-9]+"); 
    		Matcher matcher = pattern.matcher(controlStr);
   		    String match = "";
    		  while (matcher.find()) { 
    		     match = matcher.group();
    		  }
    		String row = controlStr.substring(0,controlStr.indexOf(match) );
    		controlVendor.setRow(convertRowControl(row.charAt(0)));
    		controlVendor.setCol(Integer.valueOf(match));
    		listControlVendorTo.add(controlVendor);
    	 }
    	}
    	 return listControlVendorTo;
    }
    
    private int convertRowControl (char ch) {
    	return ch - 'A' + 1;
    }

	private boolean isToVendorControl ( int row, int col) {
		for (ControlVendorTo to :listControlVendor) {
			if (to.getCol() == col && to.getRow() == row)
				return true;
		}
		return false;
	}

	
	private String getRowLetter(int row ) {
		switch (row) {
		case 1:
			return "A";
		case 2:
			return "B";
		case 3:
			return "C";
		case 4:
			return "D";
		case 5:
			return "E";
		case 6:
			return "F";
		case 7:
			return "G";
		case 8:
			return "H";
		case 9:
			return "I";
		case 10:
			return "J";
		case 11:
			return "K";
		case 12:
			return "L";
		case 13:
			return "M";
		case 14:
			return "N";
		case 15:
			return "O";
		case 16:
			return "P";
		}
		return null;
		
	}

	private int getRowSize (int idcollection) {
		switch (idcollection) {
		case 1 :
			return 8;
		case 2 :
			return 16;
		}
		return 8;
	}

	private int getColSize (int idcollection) {
		switch (idcollection) {
		case 1 :
			return 12;
		case 2 :
			return 24;
		}
		return 12;
	}
	
	private int getTotalPlate(BatchDesignTo bean, BatchTo to, int sizeVendorControls) {
		int totalControls = 0;
		LOG.info("total members :::::: " + to.getNumMembers());
		for ( PlateTo plates : bean.getPlates()) {
			totalControls = totalControls + plates.getControlSamples().size();	
		}
		LOG.info("totalControls :::::: " + totalControls);
		int totalPlate= (to.getNumMembers()+totalControls) / getSizePlate(bean.getIdCollection());
		int mod =  (to.getNumMembers()+totalControls)% getSizePlate(bean.getIdCollection());
		if (mod > 0 )
			++totalPlate;

		int vendorControls = totalPlate * sizeVendorControls;
		totalPlate= (to.getNumMembers()+totalControls + vendorControls) / getSizePlate(bean.getIdCollection());
		 mod =  (to.getNumMembers()+totalControls + vendorControls) % getSizePlate(bean.getIdCollection());
		if (mod > 0 )
			++totalPlate;
		int totalWellsPerPlates = totalPlate * 96;
		
		totalOfControls = totalWellsPerPlates - to.getNumMembers();
		LOG.info("total plates with vendor controls  :::::: "+ totalOfControls );
		return totalPlate;
	}
	
	private int getSizePlate(int idcollection) {
		switch (idcollection) {
		case 1 :
			return 96;
		case 2 :
			return 384;
		}
		return 96;
		
	} 
	
	
	private  List<RequestListMemberTo> getListRequestListMember (List<RequestIDTo>  requestList) {
		List<RequestListMemberTo> requestListMember = new ArrayList<RequestListMemberTo>();
		for(RequestIDTo requestID:  requestList) {
			List<FilterInput> filters = new ArrayList<FilterInput>();
			FilterInput filter = new FilterInput();
			filter.setMod(FilterMod.EQ);
			filter.setVal(String.valueOf(requestID.getId()));
			filter.setCol("request.id");
			filters.add(filter);
			Page<RequestListMemberTo> page = requestListMemberService.findRequestListMembers(null, null, filters);
			 for(int total= 1 ; total<= page.getTotalPages() ;total++) {
			        final Pageable pageable = page.nextOrLastPageable();
			        PageInput pageInput = new PageInput();
			        pageInput.setNumber(total);
			        pageInput.setSize(pageable.getPageSize());
			        page = requestListMemberService.findRequestListMembers(pageInput, null, filters);
			        requestListMember.addAll((page.getContent())); 
			    }
		} 
		return requestListMember;
	}

	@Transactional
	public void createRequestListMemberElements(int idBatch, int [] idRequest) {
		BatchModel modelBatch = batchRepository.findById(idBatch).get();
		if (idRequest != null)
		for (int id : idRequest) {
		RequestModel modelRequest = requestRepository.findById(id).get();
		modelRequest.setRequestlistmembers(getRequestListMemberByIDRequest( modelRequest, modelBatch ));
		requestRepository.saveAndFlush(modelRequest);
		}
	}

	private Set<RequestListMemberModel> getRequestListMemberByIDRequest(RequestModel modelRequest, BatchModel modelBatch  ){
		Set<RequestListMemberModel> list = new HashSet<RequestListMemberModel>();
		requestListMemberRepository.findByRequestId(modelRequest.getId()).stream()
		.forEach(e-> {
			e.setBatch(modelBatch); 
			e.setRequest(modelRequest);
			list.add(e);	
		});
		return list;
		
		
	}

	@Override
	public String createBatchCode(org.ebs.services.to.custom.BatchCodeTo bean) {
		String strProgramCode ="";
		for (Integer idProgram : bean.getProgram()) {			
			strProgramCode = strProgramCode + csProgram.findProgramId(idProgram).getCode().replace(" ", "");
		}
		String strServiceCode="";
		for (Integer idService : bean.getServicecode())
			strServiceCode = strServiceCode +serviceRepository.findById(idService).get().getCode().replace(" ", "");
		
		return "B"+strProgramCode+purposeRepository.findById(bean.getPurpose()).get().getCode().replace(" ", "")+
				strServiceCode+"_"
				+(batchCodeRepository.findById(bean.getBatchID()).get().getLastBatchNumber()+1);
		
	}

}

package org.ebs.services.custom;

import java.util.List;

import org.ebs.services.to.custom.CropTo;

public interface CSCrop {

	public CropTo findCropId(int id);
	
	public List<CropTo> findCrops(int id);
	
	public List<CropTo> findCrops(String filter);
}

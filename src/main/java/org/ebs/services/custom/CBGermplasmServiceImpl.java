package org.ebs.services.custom;

import static java.util.Collections.emptySet;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.ebs.model.SampleDetailModel.PACKAGE_ENTITY_TYPE;
import static org.ebs.model.SampleDetailModel.PLOT_ENTITY_TYPE;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;

import org.ebs.model.RequestModel;
import org.ebs.model.repos.RequestRepository;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.GermplasmDetailTo;
import org.ebs.services.to.SampleDetailTo;
import org.ebs.services.to.Input.RequestInput;
import org.ebs.services.to.Input.RequestListMemberInput;
import org.ebs.services.to.custom.CBHeadListMemberPackageTo;
import org.ebs.services.to.custom.CBHeadListMemberPlotTo;
import org.ebs.services.to.custom.CBListTo;
import org.ebs.services.to.custom.CBMemberPackageTo;
import org.ebs.services.to.custom.CBMemberPlotTo;
import org.ebs.services.to.custom.GraphqlTypeTo;
import org.ebs.services.to.custom.MemberTo;
import org.ebs.services.to.custom.PlotTo;
import org.ebs.util.Connection;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrapiClient;
import org.ebs.util.client.CBGraphClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class CBGermplasmServiceImpl implements CBGermplasmService{
	
	private final BrapiClient brapiClient;
	private final CBGraphClient cbGraphClient;
	private final RequestListMemberService requestListMemberService;
    private final RequestRepository requestRepository;

    private final String GERMPLASM_DETAIL_FIELDS = "entryId entryCode entryNumber plotId plotCode plotNumber rep packageId packageCode generation seedName seedCode germplasmId germplasmCode parentage designation occurrenceName siteGeospatialObjectCode fieldGeospatialObjectCode occurrenceId";

    @Override
	public void saveGermplasListFromCB(int listID, int entityTypeId, int requestId) {
		requestListMemberService.createListRequestListMember(getListOfInputFromCBInformation(getListMemberFromCB(listID, entityTypeId), entityTypeId, requestId),requestId );
	}
	
	public Object getListMemberFromCB(int listID, int entityTypeId) {
		if (entityTypeId == 3) {
			  return   brapiClient.getList("/lists/"+listID+"/members", CBHeadListMemberPlotTo.class).getResult().getData().get(0);
		}else {
			   return brapiClient.getList("/lists/"+listID+"/members", CBHeadListMemberPackageTo.class).getResult().getData().get(0);
		}
	}

	private List<RequestListMemberInput> getListOfInputFromCBInformation(Object cbListGermplasm, int entityTypeId, int requestId){
		RequestModel modelRequest = requestRepository.findById(requestId).get();
		List<RequestListMemberInput> list = new ArrayList<>();
		if (entityTypeId == 3) {
			CBHeadListMemberPlotTo listPlot = (CBHeadListMemberPlotTo)cbListGermplasm;
			listPlot.getMembers().stream().forEach((p)->{
				if (modelRequest != null && modelRequest.getSamplesperItem()!= null && modelRequest.getSamplesperItem().intValue() > 0)
					for (int i = 1; i <= modelRequest.getSamplesperItem().intValue(); i++)
					list.add( setRequestListMemberInput(p, entityTypeId, requestId, i));
				else 
					list.add(setRequestListMemberInput(p, entityTypeId, requestId, 1));
			});
		}else {
			CBHeadListMemberPackageTo listPackage = (CBHeadListMemberPackageTo)cbListGermplasm;
			listPackage.getMembers().stream().forEach((p)->{
				if (modelRequest != null && modelRequest.getSamplesperItem()!= null && modelRequest.getSamplesperItem().intValue() > 0)
					for (int i = 1; i <= modelRequest.getSamplesperItem().intValue(); i++)
					list.add( setRequestListMemberInput(p, entityTypeId, requestId, i));
				else 
					list.add(setRequestListMemberInput(p, entityTypeId, requestId, 1));
			});
		}
		return list;
	}
	
	private RequestListMemberInput setRequestListMemberInput(Object p,int entityTypeId, int requestId, int idNumer) {
		RequestListMemberInput input = new RequestListMemberInput();
		input.setDataId(entityTypeId == 3 ? ((CBMemberPlotTo)p).getPlotDbId():((CBMemberPackageTo)p).getPackageDbId());
		input.setGermplasmId(entityTypeId == 3 ? ((CBMemberPlotTo)p).getGermplasmDbId():((CBMemberPackageTo)p).getGermplasmDbId());
		input.setEntityId(entityTypeId);
		RequestInput reqIn = new RequestInput();
		reqIn.setId(requestId);
		input.setRequest(reqIn);
		input.setTotalEntities(idNumer);
		return input;
	}
	
	public Page<CBListTo> getListFromCB (PageInput page, SortInput sort, List<FilterInput> filters, boolean disjunctionFilters){
		CBObje<CBListTo> obj = new CBObje<CBListTo>();
		String body = setFiltersToCBListTo(filters);
		return obj.doFindByCriteria(CBListTo.class, body, page, disjunctionFilters, sort);
		
	}
	
	private String setFiltersToCBListTo ( List<FilterInput> filters) {
		CBListTo to = new CBListTo();
		String filterStr = "";
		String start= "{";
		String end = "}";
		boolean isHasTypeList = false;
		if (filters != null && !filters.isEmpty())
		for (FilterInput input: filters) {
			
			if (input.getCol().equals("type") ) {
				isHasTypeList = true;
				if (input.getVal().toLowerCase().equals(PLOT_ENTITY_TYPE) || input.getVal().toLowerCase().equals(PACKAGE_ENTITY_TYPE)) {
					if (filterStr.isEmpty()) 
					filterStr = "\"type\":\""+input.getVal()+"\"";
					else 
						filterStr =filterStr+" "+","+" \""+ input.getCol()+"\":\""+input.getVal()+"\"";
				}
			}else {
				if (filterStr.isEmpty()) 
					filterStr ="\""+ input.getCol()+"\":\""+input.getVal()+"\"";
				else 
					filterStr =filterStr+" "+","+" \""+ input.getCol()+"\":\""+input.getVal()+"\"";
				
			}
			
			
		}
		String defaultFilter = "\"type\":\"package|plot\"";
		if (!isHasTypeList) {
			if (filterStr.isEmpty()) 
				filterStr = defaultFilter;
			else 
				filterStr = filterStr+" " +","+" "+ defaultFilter;
		}
		
		return start+filterStr+end ;
	}
	
	class CBObje<T> {
		private final int minPageNumber = 0;
		private final int maxPageSize = 50;
		private Pageable defaultPage = PageRequest.of(minPageNumber, 20);

		@SuppressWarnings("unchecked")
		public Connection<T> doFindByCriteria(Class<T> entityClass,  Object body, PageInput pageInput, boolean disjuntionFilters, SortInput sort)  {
			Pageable page = createPage(pageInput);
			BrPagedResponse<CBListTo> response = getList(page, body, sort);
	        return new Connection<T>((List<T>) response.getResult().getData(), page,response.getMetadata().getPagination().getTotalCount() );

	    	}
		
		private BrPagedResponse<CBListTo>  getList(Pageable page,  Object body , SortInput sort) {
			String sortStr="";
			if(sort!= null) 
				if(sort.getCol().equals("requestCode"))
					sortStr = "&sort=listDbId:desc";
				else
					sortStr = sort != null ? "&sort="+sort.getCol()+":"+ (sort.getMod().toString().equals("DES") ?  "desc":sort.getMod().toString().toLowerCase()):"&sort=listDbId:desc";
			else 
				sortStr = "&sort=listDbId:desc";
			return brapiClient.post("lists-search?limit="+page.getPageSize()+"&page="+(page.getPageNumber()+1)+ sortStr, body, CBListTo.class);
		}
		private Pageable createPage(PageInput pageInput) {
		        return pageInput == null ? defaultPage :
		            PageRequest.of(Math.max( minPageNumber, pageInput.getNumber()-1),
		                            Math.min(maxPageSize, pageInput.getSize()));
		   }
		
		}

	@Override
	public CBListTo getCBListToByListId(int listId) {
		CBObje<CBListTo> obj = new CBObje<CBListTo>();
		CBListTo to = new CBListTo();
		to.setListDbId(String.valueOf(listId));
		List<CBListTo> list = obj.doFindByCriteria(CBListTo.class, to, null, false, null).getContent();
		return list != null && !list.isEmpty() ? list.get(0) : null;
	
	}

	@Override
	public PlotTo getPlotCB(SampleDetailTo sampledetailTo) {
		//MemberTo to =getMemeberByIdCB(sampledetailTo);
		String query= "findPlot(id:"+sampledetailTo.getDataId() +"){"
				+ " id"
				+ " designX"
				+ " blockNumber"
				+ " designY"
				+ " fieldX"
				+ " fieldY"
				+ " harvestStatus"
				+ " notes"
				+ " paX"
				+ " paY"
				+ " plotCode"
				+ " plotNumber"
				+ " plotOrderNumber"
				+ " plotQcCode"
				+ " plotStatus"
				+ " plotType"
				+ " rep"
				+ " entry{"
				+ "      entryCode"
				+ "      entryName"
				+ "      entryNumber"
				+ "      entryList{"
				+ "            entryListCode"
				+ "            entryListName"
				+ "            crossCount"
				+ "            entryListStatus"
				+ "            id"
				+ "            experiment{"
				+ "                  experimentName"
				+ "                  experimentCode"
				+ "                  experimentYear"
				+ "            }"
				+ "      }"
				+ "      germplasm{"
				+ "           parentage"
				+ "           germplasmCode"
				+ "           germplasmNormalizedName"
				+ "           germplasmDbId"
				+ "           designation"
				+ "      }"
				+ "      seed{"
				+ "         seedCode"
				+ "         seedName"
				+ "      }"
				+ "    }"
				+ " occurrence{"
				+ "       id"
				+ "       occurrenceName"
				+ "       field{"
				+ "             id"
				+ "             geospatialObjectCode"
				+ "             geospatialObjectName"
				+ "}"
				+ "       site{"
				+ "            geospatialObjectCode"
				+ "            geospatialObjectName"
				+ "}"
				+ "    }"
				+ " plantingInstructions{"
				+ "   id"
				+ "   entryCode"
				+ "   germplasm{"
				+ "       designation"
				+ "       parentage"
				+ "   }"
				+ " }"
				+"}";
		
		
		return cbGraphClient.findOneEntity(query, GraphqlTypeTo.PLOT);
	}

	@Override
	public MemberTo getMemeberByIdCB(SampleDetailTo sampledetailTo) {
		String query= "findListMember(id:"+sampledetailTo.getDataId() +"){"
				+ " id"
				+ " dataId"
				+"}";
		return cbGraphClient.findOneEntity(query, GraphqlTypeTo.MEMBER);
		
	}

    @Override
    public List<GermplasmDetailTo> getGermplasmDetail(Collection<Integer> dataIds, String type) {
        List<GermplasmDetailTo> details = stringIds(dataIds).parallelStream()
            .map(subdataIds -> {
                String query = String.format("findSampleDataList(%s:[%s]) { %s }",
                    type.equals(PLOT_ENTITY_TYPE) ? "plotIds" : "packageIds",
                    subdataIds,
                    GERMPLASM_DETAIL_FIELDS);
                List<GermplasmDetailTo> page = cbGraphClient.findOneEntity(query, GraphqlTypeTo.SAMPLE_GERMPLASM_DETAIL_LIST);
                log.trace("getGermplasmDetail returning {} elements from page of size: {}",page.size(), subdataIds.split(",").length);
                return page;
            })
            .flatMap(List::stream)
            .collect(toList());
        return details;
    }
    
	/**
     * Returns sub lists of 4K plotIds for parallel queries
     * @param plotIds
     * @return
     */
    List<String> stringIds(Collection<Integer> plotIds) {
        int pageSize = 4000;
        return IntStream.range(0, (plotIds.size() + pageSize - 1) / pageSize)
        .mapToObj(i -> plotIds.stream()
            .skip(i * pageSize)
            .limit(pageSize)
            .map(id -> id.toString())
            .collect(joining(",")))
        .collect(toList());
    }

    @Override
    public List<GermplasmDetailTo> getGermplasmDetail(Set<Integer> plotIds, Set<Integer> packageIds,
            Set<Integer> germplasmIds) {
        plotIds = ofNullable(plotIds).orElse(emptySet()); 
        packageIds = ofNullable(packageIds).orElse(emptySet()); 
        germplasmIds = ofNullable(germplasmIds).orElse(emptySet());
        if (plotIds.isEmpty() && packageIds.isEmpty() && germplasmIds.isEmpty()) {
            return Collections.emptyList();
        }
        String query = String.format("findSampleDataList(plotIds:%s packageIds:%s germplasmIds:%s ) { %s }",
            plotIds,
            packageIds,
            germplasmIds,
            GERMPLASM_DETAIL_FIELDS);
        List<GermplasmDetailTo> page = cbGraphClient.findOneEntity(query, GraphqlTypeTo.SAMPLE_GERMPLASM_DETAIL_LIST);
        log.trace("getGermplasmDetail returning {} elements",page.size());
        return page;
    }

}

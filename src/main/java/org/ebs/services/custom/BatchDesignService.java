package org.ebs.services.custom;

import java.util.Map;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.custom.BatchCodeTo;
import org.ebs.services.to.custom.BatchDesignTo;
import org.springframework.security.core.Authentication;

public interface BatchDesignService {

	void createRequestListMemberElements(int idBatch, int [] idRequest);
	String createBatchCode (BatchCodeTo bean);
	
    void createSamplesAsynchronous(BatchDesignTo bean, Authentication auth, BatchTo to);
    
    void changeRequestStatusByBatch(int batchId, int tenantId, int statusId, String statusMessage);
    void changeRequestStatus(int requestId, int tenantId, int statusId, String statusMessage);
    
    BatchTo saveBatch(BatchDesignTo bean);

    Map<String,String> extractRuleArguments(int batchId);
}

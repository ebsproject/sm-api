package org.ebs.services.custom;

import java.util.List;

import org.ebs.services.to.custom.ProgramTo;

public interface CSProgram {
	public ProgramTo findProgramId(int id);
	//public List<ProgramTo> findPrograms(PageInput page, SortInput sort, List<FilterInput> filters);
	public List<ProgramTo> findPrograms();

	public List<ProgramTo> findPrograms(String filter);
	
	public String getProgramsRestByUserId (int id);
}

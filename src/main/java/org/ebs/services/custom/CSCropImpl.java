package org.ebs.services.custom;

import java.util.List;

import org.ebs.services.to.custom.CropTo;
import org.ebs.services.to.custom.GraphqlTypeTo;
import org.ebs.util.client.GraphQLResolverClient;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
	
@Service
@RequiredArgsConstructor
public class CSCropImpl implements CSCrop{

	private final GraphQLResolverClient client;

	@Override
	public CropTo findCropId(int id) {
		String query="findCrop(id: "+id+"){ "
				+ "      id "
				+ "      code "
				+ "      name "
				
				+ "  }";
		 return client.findOneEntity(query,GraphqlTypeTo.CROP);
	}

	@Override
	public List<CropTo> findCrops(int id) {
		String query="findCropList{ "
				+ "    content{ "
				+ "      id "
				+ "      code "
				+ "      name "
				+ "    }"
				+ "  }";
		
			 return client.findEntityList (query,GraphqlTypeTo.CROP_LIST);
	}

	@Override
	public List<CropTo> findCrops(String filter) {
		String query="findCropList"+filter+"{ "
				+ "    content{ "
				+ "      id "
				+ "      code "
				+ "      name "
				+ "    }"
				+ "  }";
		
			 return client.findEntityList (query,GraphqlTypeTo.CROP_LIST);
	}
}

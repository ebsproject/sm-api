package org.ebs.services.custom;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.ebs.services.to.GermplasmDetailTo;
import org.ebs.services.to.SampleDetailTo;
import org.ebs.services.to.custom.CBListTo;
import org.ebs.services.to.custom.MemberTo;
import org.ebs.services.to.custom.PlotTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface CBGermplasmService {

	public void saveGermplasListFromCB(int listID,  int entityTypeId, int requestId);
	public Object getListMemberFromCB(int listID, int entityTypeId);
	public Page<CBListTo> getListFromCB (PageInput page, SortInput sort, List<FilterInput> filters, boolean disjunctionFilters);
	public CBListTo getCBListToByListId(int listId);
	
	public PlotTo getPlotCB(SampleDetailTo sampledetailTo);
	
    public MemberTo getMemeberByIdCB(SampleDetailTo sampledetailTo);

    public List<GermplasmDetailTo> getGermplasmDetail(Collection<Integer> dataIds, String type);
	public List<GermplasmDetailTo> getGermplasmDetail(Set<Integer> plotIds, Set<Integer> packageIds, Set<Integer> germplasmIds);

}

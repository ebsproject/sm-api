package org.ebs.services.custom;

import java.util.List;
import java.util.UUID;

import org.ebs.services.to.FileObjectTo;

public interface CSFileObject {

    public FileObjectTo modify(FileObjectTo fileObject);
    
    /**
     * Finds files based on a processing status
     * having tag 'gigwa'
     * @return
     */
    public List<FileObjectTo> findByStatus(int status);

    /**
     * Finds files by UUID of the file a.k.a. key
     * having tag 'gigwa'
     * @return
     */
    public FileObjectTo findByKey(UUID id);
}

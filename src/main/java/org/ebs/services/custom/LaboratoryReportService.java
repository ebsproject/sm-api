package org.ebs.services.custom;

import org.ebs.services.to.custom.BatchDesignTo;

public interface LaboratoryReportService {

	public byte[] createReportLaboratory(BatchDesignTo bean);
}

package org.ebs.services.custom;

import java.util.Map;
import java.util.SortedMap;

import org.ebs.model.SampleDetailModel;



class StudyLabReportBean {

	// variable to management of name of rows
		private String [] nameRow;
		//variable to management of number of colums
		private int numberColumn = 0;
		//variable to management sample detail in map
		private SortedMap <Integer, Map<String , SampleDetailModel>> mapPlateSamples;
		//variable to management of prefix of samples
		private String prefix;
		// pattern to plate
		private String patternPlate;

		private boolean isPadded;

		public String[] getNameRow() {
			return nameRow;
		}
		public void setNameRow(String[] nameRow) {
			this.nameRow = nameRow;
		}
		public int getNumberColumn() {
			return numberColumn;
		}
		public void setNumberColumn(int numberColumn) {
			this.numberColumn = numberColumn;
		}
		public SortedMap<Integer, Map<String , SampleDetailModel>> getMapPlateSamples() {
			return mapPlateSamples;
		}
		public void setMapPlateSamples(SortedMap<Integer, Map<String , SampleDetailModel>> mapPlateSamples) {
			this.mapPlateSamples = mapPlateSamples;
		}
		public String getPrefix() {
			return prefix;
		}
		public void setPrefix(String prefix) {
			this.prefix = prefix;
		}
		public String getPatternPlate() {
			return patternPlate;
		}
		public void setPatternPlate(String patternPlate) {
			this.patternPlate = patternPlate;
		}
		public boolean isPadded() {
			return isPadded;
		}
		public void setPadded(boolean isPadded) {
			this.isPadded = isPadded;
		}
}

package org.ebs.services.custom;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.ebs.services.to.custom.AssayTo;
import org.ebs.services.to.custom.GraphqlTypeTo;
import org.ebs.services.to.custom.MarkerPanelTo;
import org.ebs.services.to.custom.MarkerTo;
import org.ebs.services.to.custom.ServiceProviderTo;
import org.ebs.services.to.custom.TechnologyServiceProviderTo;
import org.ebs.services.to.custom.TechnologyTo;
import org.ebs.services.to.custom.TraitCategoryTo;
import org.ebs.services.to.custom.TraitCustomTo;
import org.ebs.services.to.custom.VendorExternalTo;
import org.ebs.util.client.MarkerClient;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class MarkerGraphqlServiceImpl implements MarkerService{

	private final MarkerClient client;
	
    public MarkerPanelTo findMarkerGroupById(int id, boolean withMarkers) {
        String markers = "markers{ id name }";
        String query = "findMarkerGroup(id: "+id+"){"
				+ " id"
				+ " name"
                + " vendor{ id name }"
                + " crop{"
                + "   id"
                + "   name"
                + " }"
                + (withMarkers ? markers : "")
				+ "}";
		
		 return client.findOneEntity(query,GraphqlTypeTo.MARKER_GROUP);
	}

	public TraitCategoryTo findTraitCategory(int id) {
		String query ="findTraitCategory(id:"+id+"){"
				+ "              id"
				+ "              name"
				+ "    }";
		return client.findOneEntity(query,GraphqlTypeTo.TRAITCATEGORY);
	}
	
	public TraitCustomTo findTraitById (int id ) {
		String query ="findTrait(id:"+id+"){"
				+ "              id"
				+ "              name"
				+ "              traitCategory{"
				+ "                   id"
				+ "                   name"
				+ "                   }"
				+ "              markergroups{"
				+ "                  id"
				+ "                  name"
				+ "                  vendor{"
				+ "                         id"
				+ "                         name"
				+ "                        }"
				+ "               }"
				
				+ "    }";
		return client.findOneEntity(query,GraphqlTypeTo.TRAIT);
		
	}
	
	public List<TraitCustomTo> findTraitCustoms(String filter){
		String query ="findTraitList"+filter+"{"
				+"               content{"				 
				+ "              id"
				+ "              name"
				+ "              traitCategory{"
				+ "                   id"
				+ "                   name"
				+ "                   }"
				+ "              markergroups{"
				+ "                  id"
				+ "                  name"
				+ "                  vendor{"
				+ "                         id"
				+ "                         name"
				+ "                        }"
				+ "               }"
				+ "              }"
				
				+ "    }";
		return client.findEntityList(query, GraphqlTypeTo.TRAIT_LIST);
	}
	
	public List<MarkerPanelTo> findMarkerGroupByName(String filter) {
		String query="findMarkerGroupList"+filter+"{"
				+ "content{ "
				+ " id"
				+ " name"
				+ " vendor{"
				+ "        id"
				+ "        name"
				+ "    }"
				+ "}"
				+ "}";
		
		 return client.findEntityList(query,GraphqlTypeTo.MARKER_GROUP_LIST);
	}
	
	public AssayTo findAssayById (int id) {
		String query="findAssay(id: "+id+"){"
				+ " id"
				+ " name"
				+ "}";
		return client.findOneEntity(query,GraphqlTypeTo.ASSAY);
		
	}
	
	public MarkerTo findMarkerById (int id) {
		String query="findMarker(id: "+id+"){"
				+ " id"
				+ " name"
				+ "}";
		return client.findOneEntity(query,GraphqlTypeTo.MARKER);
		
	}

	public List<ServiceProviderTo> findServiceProviders(String filter) {
		String query="findServiceProviderList"+filter+"{"
				+ "content{ "
				+ " id"
				+ " name"
				+ " type"
				+ " code"
				+ "}"
				+ "}";
		
		 return client.findEntityList(query,GraphqlTypeTo.SERVICE_PROVIDER_LIST);
	}

    @Override
    public List<String> findAssayNamesByGroupAndTechServiceProv(int markerGroupId, int techServProvider) {
        return client.findAssaysByMarkerGroupAndTechnolyServiceProvider(markerGroupId, techServProvider).stream()
                .map(m -> m.getName())
                .collect(toList());
    }

    @Override
    public List<String> findAssayNamesByMarkerIdsAndTechServiceProv(List<Integer> markerIds, int techServProvider) {
        return client.findAssaysByMarkerIdsAndTechnolyServiceProvider(markerIds, techServProvider).stream()
            .map(m -> m.getName())
            .collect(toList());
    }

	
	public TechnologyServiceProviderTo findTechnologyAndSPBTechSerProviderId(int id) {
		String query ="findTechnologyServiceProvider(id:"+id+"){"
				+ " id"
			     +"  reportId"
			     + " controlPlate"
				+ " serviceProvider{"
				+ "   id"
				+ "   name"
				+ "   type"
				+ "   code"
				+ "   }"
				+ "  technology{"
				+ "     id"
				+ "     name"
				+ "     description"
				+ "    }"
				+ "  }";
				
		return client.findOneEntity(query,GraphqlTypeTo.TECHNOLOGY_SERVICE_PROVIDER);	
	} 
	
	public VendorExternalTo findVendorExternalTo(int id) {
		String query ="findServiceProvider(id:"+id+"){"
				+ " id"
				+ "   name"
				
				+ " }";
				
		return client.findOneEntity(query,GraphqlTypeTo.VENDOR_EXTERNAL);
	}
	
	public TechnologyTo findTechnologyTo(int id) {
		String query ="findTechnology(id:"+id+"){"
				+ " id"
				+ "   name"
				+ "   description"
				
				+ " }";
				
		return client.findOneEntity(query,GraphqlTypeTo.TECHNOLOGY_EXT);
	}
	
	public List <TechnologyServiceProviderTo> getTechnologyServiceProviderToList(String filter){
		String query ="findTechnologyServiceProvidersList("+filter+")"
		    //    filters:[{ col:"serviceprovider.id", mod: EQ, val:"10"  },
		      //  { col:"technology.id", mod: EQ, val:"14"  }
		        //]
		    +"{ "
		     +"   content{"
		     + " id"
		     +"  reportId"
		     + " controlPlate"
				+ " serviceProvider{"
				+ "   id"
				+ "   name"
				+ "   type"
				+ "   code"
				+ "   }"
				+ "  technology{"
				+ "     id"
				+ "     name"
				+ "     description"
				+ "    }"
				+"}"
				+"}";

			
	 return client.findEntityList(query,GraphqlTypeTo.TECHNOLOGY_SERVICE_PROVIDER_LIST);
		
	}
	
	
}

package org.ebs.services.custom;

import java.util.List;

import org.ebs.services.to.custom.ContactByIDTo;
import org.ebs.services.to.custom.UserTo;

public interface CSUser {

	public UserTo findUserIdExternal (String filter);
	public List<UserTo> findUsers(String filter);
	public ContactByIDTo findContactByID(int id);
}

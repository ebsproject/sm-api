package org.ebs.services.custom;

import java.util.List;

import org.ebs.services.to.custom.GraphqlTypeTo;
import org.ebs.services.to.custom.ProgramTo;
import org.ebs.util.client.GraphQLResolverClient;
import org.ebs.util.client.RestCsClient;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CSProgramImpl implements CSProgram {

	private final GraphQLResolverClient client;
	private final RestCsClient restCsClient; 
	
	public ProgramTo findProgramId(int id) {
		String query="findProgram(id: "+id+"){ "
				+ "      id "
				+ "      code "
				+ "      name "
				+ "      externalId "
				+ "  }";
		 return client.findOneEntity(query,GraphqlTypeTo.PROGRAM);
	}
	

	public List<ProgramTo> findPrograms(){
		String query="findProgramList{ "
				+ "    content{ "
				+ "      id "
				+ "      code"
				+ "    }"
				+ "  }";
		
			 return client.findEntityList (query,GraphqlTypeTo.PROGRAM_LIST);
	}
	
	public List<ProgramTo> findPrograms(String filter){
		String query="findProgramList"+filter+"{ "
				+ "    content{ "
				+ "      id "
				+ "      code"
				+ "      name"
				+ "    }"
				+ "  }";
		
			 return client.findEntityList (query,GraphqlTypeTo.PROGRAM_LIST);
	}

	public String getProgramsRestByUserId (int id) {
		return restCsClient.getPrograms(id);
		
	}
}

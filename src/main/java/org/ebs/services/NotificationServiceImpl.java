package org.ebs.services;

import static org.ebs.Application.REQUEST_TOKEN;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpMethod.POST;

import java.util.Map;

import org.ebs.util.brapi.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NotificationServiceImpl implements NotificationService {
    @Value("${ebs.cs.graphql.endpoint}")
    private String csGraphqlEndpoint;
    @Value("${ebs.gigwa.api.endpoint}")
    private String gigwaApiEndpoint;

    private static final int JOB_BATCH_CREATION = 4;
    private static final int JOB_GENERIC_NOTIFICATION = 10;
    private static final int JOB_GIGWA_FILE_UPLOAD = 11;

    private final RestTemplate client;
    protected final TokenGenerator tokenGenerator;

    @Autowired
    public NotificationServiceImpl(RestTemplateBuilder restBuilder, TokenGenerator tokenGenerator) {
        client = restBuilder.build();
        this.tokenGenerator = tokenGenerator;
    }

    boolean sendNotification(int jobWorkflowId, int submitter, int record, String message, String title, String urlResponse) {
        String path = csGraphqlEndpoint.replace("graphql", "notification/v2/send");

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(CONTENT_TYPE, "application/json");
        headers.add(AUTHORIZATION, getTokenHeader());

        NotificationBody body = new NotificationBody(jobWorkflowId, submitter, record,
                Map.of("title", title, "message", message, "url_response", urlResponse));
        log.debug("Sending notification message: {}", body);
        HttpEntity<Object> entity = new HttpEntity<>(body, headers);

        ResponseEntity<NotificationResponse> response = client.exchange( path,  POST, entity, NotificationResponse.class);
        log.debug("Notification's response ({}): {}", response.getStatusCode(),response.getBody());
        return response.getBody().isSuccess();
    }

    String getTokenHeader() {
        return "Bearer " + (REQUEST_TOKEN.get() == null ? tokenGenerator.getToken() : REQUEST_TOKEN.get());
    }

    @Override
    public boolean sendNotificationAsGigwa(int submitter, int record, String message) {
        log.debug("sending message: {}", message);
        return sendNotification(JOB_GIGWA_FILE_UPLOAD, submitter, record, message, "GIGWA Import Service", gigwaApiEndpoint.replace("/rest", ""));
    }

    @Override
    public boolean sendNotificationAsBatchManager(int submitter, int record, String message) {
        return sendNotification(JOB_BATCH_CREATION, submitter, record, message, "Batch Manager", "");
    }

    @Override
    public boolean sendNotificationGeneric(int submitter, int record, String message) {
        return sendNotification(JOB_GENERIC_NOTIFICATION, submitter, record, message, "Batch Manager", "");
    }

    

}



@Data
@NoArgsConstructor @AllArgsConstructor
class NotificationResponse {
    private boolean success;
    private String message;
}

@Data
@AllArgsConstructor
class NotificationBody {
    private int job_workflow_id;
    private int submitter_id;
    private int record_id;
    private Map<String, String> other_parameters;
}
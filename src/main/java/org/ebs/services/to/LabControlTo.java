///////////////////////////////////////////////////////////
//  LabControlTo.java
//  Macromedia ActionScript Implementation of the Class LabControlTo
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:09 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:09 AM
 */
@Getter @Setter
public class LabControlTo implements Serializable {

	private Set<ContainerSetTo> containersets;
	private String description;
	private int id;
	private String name;
	private static final long serialVersionUID = -85084360;
	private int tenant;

	@Override
	public String toString(){
		return "LabControlTo []";
	}

}
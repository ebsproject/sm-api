package org.ebs.services.to.custom;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonTo implements Serializable{
	private String familyName;
    private String fullName;
    private String givenName;

}

package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EntryListTo {

	private String entryListCode;
	private String entryListName;
	private String crossCount;
	private String entryListStatus;
    private int id;
    private ExperimentTo experiment;
}

package org.ebs.services.to.custom;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PlateTo {
	private Integer plateId;
	private List<ControlSampleTo> controlSamples;

}

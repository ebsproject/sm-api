package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatchCodeTo {

	private Integer [] servicecode;
	private Integer [] program;
	private Integer purpose;
	private Integer batchID;
	
	
	public String toString() {
		return "servicecode: "+ this.servicecode +" Program: ["+this.program.toString()+"]"
				+" Purpose :"+this.purpose;
	}
	
}

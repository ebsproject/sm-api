package org.ebs.services.to.custom;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserTo implements Serializable {
	
	private int id;
    private int externalId;
    private String userName;
    private ContactTo contact;
}

package org.ebs.services.to.custom;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarkerPanelTo implements Serializable {
	private int id;
	private String name;
	private VendorExternalTo vendor;
	private List<MarkerTo> markers;
	private CropMarkerTo crop;
	private static final long serialVersionUID = 14564L;

	public MarkerPanelTo(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public MarkerPanelTo(int id) {
		this.id = id;
	}
}

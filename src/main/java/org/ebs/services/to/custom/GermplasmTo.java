package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GermplasmTo {

	private String parentage;
	private String designation;
	private String germplasmCode;
	private String germplasmNormalizedName;
	private int germplasmDbId;
	
}

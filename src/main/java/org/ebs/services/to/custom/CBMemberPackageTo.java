package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CBMemberPackageTo {

	private Integer listMemberDbId;
	private Integer orderNumber;
	private String creationTimestamp;
	private String displayValue;
	private boolean isActive;
	private String remarks;
	private String experiment;
	private Integer experimentDbId;
	private Integer experimentYear;
	private String experimentType;
	private Integer experimentStageDbId;
	private String experimentStage;
	private String experimentStageCode;
	private String experimentOccurrence;
    private Integer sourceOccurrenceDbId;
    private String sourceOccurrenceName;
    private String seedManager;
    private Integer programDbId;
    private Integer seedDbId;
    private String seedName;
    private String GID;
    private String germplasmNormalizedName;
    private String germplasmOtherNames;
    private String designation;
    private String parentage;
    private Integer germplasmDbId;
    private Integer locationDbId;
    private String location;
    private String sourceStudyName;
    private Integer ourceStudySeasonDbId;
    private String sourceStudySeason;
    private String sourceEntryCode;
    private Integer seedSourceEntryNumber;
    private String seedSourcePlotCode;
    private Integer seedSourcePlotNumb;
    private Integer replication;
  
    private Integer packageDbId;
    private String packageCode;
    private String label;
    private Integer quantity;
    private String unit;
   
 
  
}

package org.ebs.services.to.custom;

import java.util.List;
import java.util.Map;

import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.GermplasmDetailTo;
import org.springframework.core.ParameterizedTypeReference;

public class GraphqlTypeTo {
	
	
	    public static final ParameterizedTypeReference<Map<String,Map<String,ProgramTo>>> PROGRAM =
	        new ParameterizedTypeReference<Map<String,Map<String,ProgramTo>>>(){};
	    
	    public static final ParameterizedTypeReference<Map<String,Map<String,MarkerPanelTo>>> MARKER_GROUP =
	    	        new ParameterizedTypeReference<Map<String,Map<String,MarkerPanelTo>>>(){};
	    	        
	    public static final ParameterizedTypeReference<Map<String,Map<String,TraitCategoryTo>>> TRAITCATEGORY =
	    	    	        new ParameterizedTypeReference<Map<String,Map<String,TraitCategoryTo>>>(){};	    	        
	    public static final ParameterizedTypeReference<Map<String,Map<String,TraitCustomTo>>> TRAIT =
	    	    	    	        new ParameterizedTypeReference<Map<String,Map<String,TraitCustomTo>>>(){};
	    public static final ParameterizedTypeReference<Map<String,Map<String,PlotTo>>> PLOT =
	    	    	        new ParameterizedTypeReference<Map<String,Map<String,PlotTo>>>(){};
	    	    	 
	    public static final ParameterizedTypeReference<Map<String,Map<String,MemberTo>>> MEMBER =
	    	    	    	        new ParameterizedTypeReference<Map<String,Map<String,MemberTo>>>(){};
	        
	    public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<ProgramTo>>>>> PROGRAM_LIST =
	    	        new ParameterizedTypeReference<Map<String,Map<String,Map<String,List<ProgramTo>>>>>(){};
	   
	    public static final ParameterizedTypeReference<Map<String,Map<String,CropTo>>> CROP =
	       	        new ParameterizedTypeReference<Map<String,Map<String,CropTo>>>(){};
	    
	     public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<CropTo>>>>> CROP_LIST =
	       	        new ParameterizedTypeReference<Map<String,Map<String,Map<String,List<CropTo>>>>>(){};
	    
	     public static final ParameterizedTypeReference<Map<String,Map<String,UserTo>>> USER =
	       	        new ParameterizedTypeReference<Map<String,Map<String,UserTo>>>(){};
	    	    	    	    	        
	    public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<UserTo>>>>> USER_LIST =
	       	        new ParameterizedTypeReference<Map<String,Map<String,Map<String,List<UserTo>>>>>(){};

        public static final ParameterizedTypeReference<Map<String,Map<String,FileObjectTo>>> FILE_OBJECT =
            new ParameterizedTypeReference<Map<String, Map<String, FileObjectTo>>>() {};

        public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<FileObjectTo>>>>> FILE_OBJECT_LIST =
            new ParameterizedTypeReference<Map<String,Map<String,Map<String,List<FileObjectTo>>>>>(){};

            
            public static final ParameterizedTypeReference<String> STRING =
	    	        new ParameterizedTypeReference<String>(){};

	   public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<TraitCustomTo>>>>> TRAIT_LIST =
	      new ParameterizedTypeReference<Map<String,Map<String,Map<String,List<TraitCustomTo>>>>>(){};

	   public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<MarkerPanelTo>>>>> MARKER_GROUP_LIST =
	       new ParameterizedTypeReference<Map<String,Map<String,Map<String,List<MarkerPanelTo>>>>>(){};
	       
	    public static final ParameterizedTypeReference<Map<String,Map<String,AssayTo>>> ASSAY =
	    	new ParameterizedTypeReference<Map<String,Map<String,AssayTo>>>(){};
	    public static final ParameterizedTypeReference<Map<String,Map<String,MarkerTo>>> MARKER =
	      	new ParameterizedTypeReference<Map<String,Map<String,MarkerTo>>>(){};
   
	    public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<ServiceProviderTo>>>>> SERVICE_PROVIDER_LIST =
          new ParameterizedTypeReference<Map<String, Map<String, Map<String, List<ServiceProviderTo>>>>>() {};
        public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<MarkerTo>>>>> MARKER_LIST =
          new ParameterizedTypeReference<Map<String, Map<String, Map<String, List<MarkerTo>>>>>() {};
	   
	    public static final ParameterizedTypeReference<Map<String,Map<String,TechnologyServiceProviderTo>>> TECHNOLOGY_SERVICE_PROVIDER =
		      	        new ParameterizedTypeReference<Map<String,Map<String,TechnologyServiceProviderTo>>>(){};
		      	        
		 public static final ParameterizedTypeReference<Map<String,Map<String,VendorExternalTo>>> VENDOR_EXTERNAL =
				      	        new ParameterizedTypeReference<Map<String,Map<String,VendorExternalTo>>>(){};		      	        
	     public static final ParameterizedTypeReference<Map<String,Map<String,TechnologyTo>>> TECHNOLOGY_EXT =
		     	        new ParameterizedTypeReference<Map<String,Map<String,TechnologyTo>>>(){};	
		     	        
		     	        
		public static final ParameterizedTypeReference<Map<String,Map<String,Map<String,List<TechnologyServiceProviderTo>>>>> TECHNOLOGY_SERVICE_PROVIDER_LIST =
		     new ParameterizedTypeReference<Map<String, Map<String, Map<String, List<TechnologyServiceProviderTo>>>>>() {};
		      	      
        public static final ParameterizedTypeReference<Map<String,Map<String, List<GermplasmDetailTo>>>> SAMPLE_GERMPLASM_DETAIL_LIST =
        new ParameterizedTypeReference<Map<String, Map<String, List<GermplasmDetailTo>>>>(){};
                                 
        public static final ParameterizedTypeReference<Map<String,Map<String,ContactByIDTo>>> CONTACT_BY_ID =
     	        new ParameterizedTypeReference<Map<String,Map<String,ContactByIDTo>>>(){};
}

package org.ebs.services.to.custom;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatchTo {

	private Integer cropID;

	private String description;

	private String name;
	
	private Integer serviceprovider;
	
	private Integer technologyplataform;
	
	private Integer tenantID;
	
	private Integer total;
	
	private List<BatchMarkerGroupTo> listBatchMarkerGroup;
	
	private Integer vendorId;

	private Integer reportId;
	
}

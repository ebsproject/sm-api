package org.ebs.services.to.custom;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ControlVendorTo {
	private String value;
	private int row;
	private int col;

}

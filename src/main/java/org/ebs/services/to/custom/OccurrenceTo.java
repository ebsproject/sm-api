package org.ebs.services.to.custom;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OccurrenceTo implements Serializable{
	
	private int id;
	private String occurrenceName;
	private SiteTo site;
	private FieldTo field;
}

package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CBMemberPlotTo {

	private int listMemberDbId;
	private int orderNumber;
	private String creationTimestamp;
	private String displayValue;
	private boolean isActive;
	private String remarks;
	private int plotDbId;
	private int locationDbId;
	private int occurrenceDbId;
	private String occurrenceName;
	private int entryDbId;
	private String entryCode;
	private int entryNumber;
	private String plotCode;
	private int plotNumber;
	private String plotType;
	private int rep;
	private int designX;
	private int designY;
	private int paX;
	private int paY;
	private String blockNumber;
	private String harvestStatus;
	private int experimentDbId;
	private String entryName;
	private int germplasmDbId;
	private String designation;
	private String parentage;
	private String state;
	private int cropDbId;
	private String cropCode;
	
}

package org.ebs.services.to.custom;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BatchDesignTo {
	private BatchTo batch;
	private int[] arrRequestID;
	private Integer idBatch;
	private Integer idTenant;
	
	private Integer idPlateDirection;
	private Integer idCollection;
	private List<PlateTo> plates;
	private String vendorControls;
	
	@Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BatchDesign{");
      //  sb.append("idBatch=='").append(idBatch).append('\'');
        
        //sb.append("idControl=='").append(idVendorControl).append('\'');
        sb.append("idPlateDirection=='").append(idPlateDirection).append('\'');
        sb.append("idCollection=='").append(idCollection).append('\'');
        return sb.toString();
	}
}

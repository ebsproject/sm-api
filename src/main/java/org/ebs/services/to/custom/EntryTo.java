package org.ebs.services.to.custom;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EntryTo implements Serializable{

	private String entryCode;
	private String entryName;
	private int entryNumber;
	private EntryListTo entryList;
	private GermplasmTo germplasm;
	private SeedTo seed;
}

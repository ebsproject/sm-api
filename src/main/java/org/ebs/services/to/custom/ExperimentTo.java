package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExperimentTo {
	
	private String experimentName;
	private String experimentCode;
	private String experimentYear;

}

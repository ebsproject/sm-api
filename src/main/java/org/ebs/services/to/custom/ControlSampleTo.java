package org.ebs.services.to.custom;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ControlSampleTo {

	private Integer controlId;
	private Integer row;
	private Integer column;
}

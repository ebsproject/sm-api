package org.ebs.services.to.custom;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProgramTo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4028331114006347194L;
	private int id;
	private String code;
	private String name;
	private int externalId;
	

}

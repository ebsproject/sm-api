package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TraitCustomTo {

	private int id;
	private String name;
	private MarkerPanelTo [] markergroups;
	private TraitCategoryTo traitCategory;
}

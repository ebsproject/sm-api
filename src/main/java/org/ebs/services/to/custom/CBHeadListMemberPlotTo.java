package org.ebs.services.to.custom;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CBHeadListMemberPlotTo  {

	private int listDbId;
	private String abbrev;
	private String name;
	private String displayName;
	private String type;
	private int entityDbId;
	private String entity;
	private String entityAbbrev;
	private String description;
	private String remarks;
	private String creationTimestamp;
	private int creatorDbId;
	private String creator;
	private String modificationTimestamp;
	private String modifierDbId;
	private String modifier;
	private List<CBMemberPlotTo> members;
	
	}

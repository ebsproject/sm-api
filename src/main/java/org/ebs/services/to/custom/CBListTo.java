package org.ebs.services.to.custom;

import java.util.List;

import org.ebs.services.to.RequestTo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CBListTo {
	private String listDbId;
    private String abbrev;
    private String name;
    private String displayName;
    private String type;
    private String subType;
    private String entityId;
    private String description;
    private String remarksTest;
    private String notes;
    private String isActive;
    private String status;
    private String listUsage;
    private String creationTimestamp;
    private String creatorDbId;
    private String creator;
    private String modificationTimestamp;
    private String modifierDbId;
    private String modifier;
    private String memberCount;
    private String shareCount;
    private String permission;
    
    private List<RequestTo> requests ;
}

package org.ebs.services.to.custom;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlotTo implements Serializable{

	private int id;
	private int designX;
	private int blockNumber;
	private int designY;
	private int fieldX;
	private int fieldY;
	private String harvestStatus;
	private String notes;
	private int paX;
	private int paY;
	private String plotCode;
	private int plotNumber;
	private String plotStatus;
	private String plotType;
	private int rep;
	private EntryTo entry;
	private OccurrenceTo occurrence;
	private PlantingInstructions [] plantingInstructions;
}

package org.ebs.services.to.custom;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatchMarkerGroupTo {
	
	private Integer markerGroupID;
	private Integer technologyServiceProviderId;
	private int fixed = 0;
	List<BatchMarkerGoupCustomTo> listBatchMarkerGroupCustom;
	

}

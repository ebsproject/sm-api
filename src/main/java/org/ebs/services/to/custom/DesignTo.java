package org.ebs.services.to.custom;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DesignTo {
	
	private Integer id;
	private String name;
	private Integer nummembers;
	private String objective;
	private Integer numcontainers;
	private String fistPlate;
	private String lastPlate;
	private String updateDate;
	private String updateBy;
	
}

package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldTo {
	private int id;
	private String geospatialObjectCode;
	private String geospatialObjectName;
	
}

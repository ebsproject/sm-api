package org.ebs.services.to.custom;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServiceProviderTo implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String type;
	private String code;
	
	@Override
	public String toString(){
		return "ServiceProviderTo [ id="+id+" name="+name+" type="+type+" ]";
	}

}

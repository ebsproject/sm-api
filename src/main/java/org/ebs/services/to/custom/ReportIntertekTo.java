package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportIntertekTo {

	private Integer batchId = 0;
	private Integer shipmentId = 0;
	private String fullName;
	private String email;
	private String organization;
	private String crop;
	private Integer vendorId = 0;
	
}

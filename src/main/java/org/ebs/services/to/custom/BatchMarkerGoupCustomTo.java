package org.ebs.services.to.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatchMarkerGoupCustomTo {
	
	private Integer markerId = 0;
	private Integer assayId = 0;
}

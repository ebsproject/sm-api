///////////////////////////////////////////////////////////
//  FormTypeTo.java
//  Macromedia ActionScript Implementation of the Class FormTypeTo
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:18:59 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:18:59 PM
 */
@Getter @Setter @ToString
public class FormTypeTo implements Serializable {

	//private Set<EbsFormTo> ebsforms;
	//private Set<FormTo> forms;
	private int id;
	private String name;
	private static final long serialVersionUID = 358761174;
	private int tenant;



}
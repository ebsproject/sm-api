///////////////////////////////////////////////////////////
//  MixtureMethodTo.java
//  Macromedia ActionScript Implementation of the Class MixtureMethodTo
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:15 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:15 AM
 */
@Getter @Setter
public class MixtureMethodTo implements Serializable {

	private String code;
	private int id;
	private String name;
	private Set<SampleMixtureDetailTo> samplemixturedetails;
	private static final long serialVersionUID = 151547035;
	private int tenant;

	@Override
	public String toString(){
		return "MixtureMethodTo []";
	}

}
package org.ebs.services.to.Input;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestMarkerGroupInput {

	private int id;
	private int markergroup_id;
	private RequestInput request;
	private static final long serialVersionUID = 100111468;
	private int tenant;
}

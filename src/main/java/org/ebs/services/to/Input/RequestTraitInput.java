package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestTraitInput implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;

	private RequestInput request;

	private int traitId;
	
	private int markerGroupId;
}

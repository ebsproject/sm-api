package org.ebs.services.to.Input;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MarkerGroupPurposeInput {

	private int id;
	private int markerGroupId;
	private PurposeInput purpose;
	private static final long serialVersionUID = -44917065;

}

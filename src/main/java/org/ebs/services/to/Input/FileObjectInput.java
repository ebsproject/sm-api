package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FileObjectInput implements Serializable {

    private UUID id;

    private String name;

    private int ownerId;

    private long size;
    
    private String tags;

    private String description;

    private int status;

    private int tenantId;
   
}

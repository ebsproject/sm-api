package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BatchMarkerGroupInput implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	
	
	private Integer markerGroupID = 0;
	
	
	private Integer technologyServiceProviderId = 0;
	
	private  boolean isFixed;
	
	private BatchInput batch;
	@Override
	public String toString(){
		return "BatchMarkerGroupTo [markerGroupID=" + markerGroupID + ",technologyServiceProviderId=" + technologyServiceProviderId +
				"+batch = "+batch +",]";
	}
}

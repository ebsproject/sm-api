///////////////////////////////////////////////////////////
//  FieldInput.java
//  Macromedia ActionScript Implementation of the Class FieldInput
//  Generated by Enterprise Architect
//  Created on:      11-Aug-2021 1:18:55 PM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 11-Aug-2021 1:18:55 PM
 */
@Getter @Setter
public class FieldInput implements Serializable {

	private ComponentInput component;
	private DataTypeInput datatype;
	private String defaultValue;
	private FormInput form;
	private GroupInput group;
	private int id;
	private Boolean isBase;
	private Boolean isRequired;
	private String label;
	private String name;
	private Integer order;
	private static final long serialVersionUID = -430216339;
	private TabInput tab;
	private int tenant;
	private String tooltip;
	private ValidationRegexInput validationregex;

}
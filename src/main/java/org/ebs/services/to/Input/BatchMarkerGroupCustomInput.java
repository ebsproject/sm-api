package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BatchMarkerGroupCustomInput implements Serializable{
	private static final long serialVersionUID = 7976742196386706226L;
	private int id;	
	private Integer markerId = 0;
	private Integer assayId = 0;
	
	private BatchMarkerGroupInput batchMarkerGroup;
	
	@Override
	public String toString(){
		return "BatchMarkerGroupCustomTo [batchMarkerGroup=" + batchMarkerGroup + ",MarkerId=" + markerId + ",]";
	}

}

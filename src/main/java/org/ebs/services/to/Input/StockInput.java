///////////////////////////////////////////////////////////
//  StockInput.java
//  Macromedia ActionScript Implementation of the Class StockInput
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:39 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.to.Input;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:39 AM
 */
@Getter @Setter
public class StockInput implements Serializable {

	private ContainerSetInput containerset;
	private int id;
	private String name;
	private static final long serialVersionUID = -231437818;
	private String source;
	private int tenant;

}
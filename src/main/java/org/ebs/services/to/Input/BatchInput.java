///////////////////////////////////////////////////////////
//  BatchInput.java
//  Macromedia ActionScript Implementation of the Class BatchInput
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:00 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.to.Input;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:00 AM
 */
@Getter @Setter
public class BatchInput implements Serializable {

	
	
	private CollectionLayoutInput collectionlayout;
	private Integer  idCrop;
	private String endDate;
	private int id;
	private Boolean inDesign;
	private Integer markerGroupId;
	private LoadTypeInput loadtype;
	private String name;
	private Integer numContainers;
	private Integer numControls;
	private Integer numMembers;
	private String objective;
	private static final long serialVersionUID = 266969777;
	private ServiceProviderInput serviceprovider;
	private String startDate;
	private Integer tenant;
	private TissueTypeInput tissuetype;
	private Integer technologyPlatformId;
    private UUID resultFileId;
    private Integer vendorId;
    private Integer reportId;

	@Override
	public String toString(){
		return "BatchTo("+id+") [numContainers=" + numContainers + ",numMembers=" + numMembers + ",numControls=" + numControls 
				+  ", resultFileId="+resultFileId+"]";
	}
}
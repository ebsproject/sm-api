///////////////////////////////////////////////////////////
//  ServiceTo.java
//  Macromedia ActionScript Implementation of the Class ServiceTo
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:33 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:33 AM
 */
@Getter @Setter
public class ServiceTo implements Serializable {

	private String code;
	private String description;
	private int id;
	private String name;
	private PurposeTo purpose;
	private Set<RequestTo> requests;
	private static final long serialVersionUID = 240663275;
	private int tenant;

	@Override
	public String toString(){
		return "ServiceTo []";
	}

}
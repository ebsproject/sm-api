package org.ebs.services.to;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:42 AM
 */
@Getter @Setter
public class RequestMarkerGroupTo {

	private static final long serialVersionUID = 133632843;
	private Integer MarkerGroupID;
	private RequestTo request;
	private int id;
	
	private int tenant;

}

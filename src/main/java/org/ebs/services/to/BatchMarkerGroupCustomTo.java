package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class BatchMarkerGroupCustomTo implements Serializable{
	private static final long serialVersionUID = 7976742196386706226L;
	private int id;	
	private Integer markerId = 0;
	private Integer assayId = 0;
	
	private BatchMarkerGroupTo batchMarkerGroup;
	
	@Override
	public String toString(){
		return "BatchMarkerGroupCustomTo [batchMarkerGroup=" + batchMarkerGroup + ",MarkerId=" + markerId + ",]";
	}
}

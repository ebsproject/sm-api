package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class RequestTraitTo implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;

	private int traitId;
	
	private Integer markerGroupId;

	private RequestTo request;
	
	@Override
	public String toString(){
		return "RequestTraitTo [traitId=" + traitId + ",request=" + request + ",]";
	}
}

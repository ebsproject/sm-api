package org.ebs.services.to;

import java.io.Serializable;
import java.util.UUID;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FileObjectTo implements Serializable {
    
    private UUID key;
    //needed because cs-graphql api returns id
    private String id;

    private String name;

    private int ownerId;

    private long size;

    private String[] tags;

    private String description;

    private int status;

    private int tenant;

    private Date createdOn;
    
    private Date updatedOn;

}

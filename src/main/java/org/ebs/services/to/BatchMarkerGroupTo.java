package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class BatchMarkerGroupTo implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	
	
	private Integer markerGroupID;
	
	
	private Integer technologyServiceProviderId;
	
	private  boolean isFixed;
	private BatchTo batch;
	

	
	@Override
	public String toString(){
		return "BatchMarkerGroupTo [markerGroupID=" + markerGroupID + ",technologyServiceProviderId=" + technologyServiceProviderId + ",]";
	}
}

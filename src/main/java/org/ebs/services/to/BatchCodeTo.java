///////////////////////////////////////////////////////////
//  BatchCodeTo.java
//  Macromedia ActionScript Implementation of the Class BatchCodeTo
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:00 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services.to;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:00 AM
 */
@Getter @Setter
public class BatchCodeTo implements Serializable {

	private Set<BatchTo> batchs;
	private String code;
	private int id;
	private int lastBatchNumber;
	private int lastPlateNumber;
	private int lastSampleNumber;
	private String name;
	private Set<SampleTo> samples;
	private static final long serialVersionUID = -123807545;
	private ServiceProviderTo serviceprovider;
	private int tenant;

	@Override
	public String toString(){
		return "BatchCodeTo [lastBatchNumber=" + lastBatchNumber + ",lastPlateNumber=" + lastPlateNumber + ",lastSampleNumber=" + lastSampleNumber + ",]";
	}

}
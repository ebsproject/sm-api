package org.ebs.services.to;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class MarkerGroupPurposeTo implements Serializable {

	private int id;
	private int markerGroupId;
	private PurposeTo purpose;
	private static final long serialVersionUID = 525215659;
	

	@Override
	public String toString(){
		return "MarkerGroupPurposeTo [markerGroupId=" + markerGroupId + ",]";
	}
}

package org.ebs.services.to;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class GermplasmDetailTo implements Serializable{
    private int entryId;
    private String entryCode;
    private String entryNumber;
    private int plotId;
    private String plotCode;
    private Integer plotNumber;
    private Integer rep;
    private int packageId;
    private String packageCode;
    private String generation;
    private String seedName;
    private String seedCode;
    private Integer germplasmId;
    private String germplasmCode;
    private String parentage;
    private String designation;
    private String occurrenceName;
    private String siteGeospatialObjectCode;
    private String fieldGeospatialObjectCode;
    private Integer occurrenceId;

    public GermplasmDetailTo(int entryId, int plotId, int packageId, int germplasmId) {
        this.entryId = entryId;
        this.plotId = plotId;
        this.packageId = packageId;
        this.germplasmId = germplasmId;
    }
}
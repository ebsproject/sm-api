package org.ebs.services.to;

import java.util.List;

import org.ebs.services.to.custom.MarkerTo;

import lombok.Data;

@Data
public class MarkerGroupTo {

	private int id;
	private List<MarkerTo> markers;
}

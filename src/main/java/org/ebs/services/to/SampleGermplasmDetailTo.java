package org.ebs.services.to;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper=true)
public class SampleGermplasmDetailTo extends GermplasmDetailTo{
    private String sampleCode;
    private String plateCode;
    private String well;
    private String row;
    private String col;
    private int batchId;
    private Integer sampleNumber;
    private Integer id;
    private boolean isVendorControl;
    private Integer plateId;
    private Integer plantNumber;
    
    public SampleGermplasmDetailTo(String sampleCode, String plateCode, String well, String row, String col, int batchId) {
        this.sampleCode = sampleCode;
        this.plateCode = plateCode;
        this.well = well;
        this.row = row;
        this.col = col;
        this.batchId = batchId;
    }

    public SampleGermplasmDetailTo(String sampleCode, Integer germplasmId, String germplasmCode, String designation) {
        this.sampleCode = sampleCode;
        this.setGermplasmId(germplasmId);
        this.setGermplasmCode(germplasmCode);
        this.setDesignation(designation);

    }
}
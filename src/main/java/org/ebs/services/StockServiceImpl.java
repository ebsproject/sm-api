///////////////////////////////////////////////////////////
//  StockServiceImpl.java
//  Macromedia ActionScript Implementation of the Class StockServiceImpl
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:39 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.StockModel;
import org.ebs.model.repos.ContainerSetRepository;
import org.ebs.model.repos.SampleDetailRepository;
import org.ebs.model.repos.StockRepository;
import org.ebs.services.to.ContainerSetTo;
import org.ebs.services.to.SampleDetailTo;
import org.ebs.services.to.StockTo;
import org.ebs.services.to.Input.StockInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:39 AM
 */
@Service @Transactional(readOnly = true)
  class StockServiceImpl implements StockService {

	private ContainerSetRepository containersetRepository;
	private ConversionService converter;
	private SampleDetailRepository sampledetailRepository;
	private StockRepository stockRepository;

	/**
	 *
	 * @param Stock
	 */
	@Override @Transactional(readOnly = false)
	public StockTo createStock(StockInput Stock){
		StockModel model = converter.convert(Stock,StockModel.class);
		 model.setId(0);

		 model= stockRepository.save(model);
		 return converter.convert(model, StockTo.class);
	}

	/**
	 *
	 * @param stockId
	 */
	@Override @Transactional(readOnly = false)
	public int deleteStock(int stockId){
		StockModel stock = stockRepository.findById(stockId).orElseThrow(() -> new RuntimeException("Stock not found"));
		 stock.setDeleted(true);
		  stockRepository.save(stock);
		 return stockId;
	}

	/**
	 *
	 * @param stockId
	 */
	public Optional<ContainerSetTo> findContainerSet(int stockId){
		return stockRepository.findById(stockId).map(r -> converter.convert(r.getContainerset(),ContainerSetTo.class));
	}

	/**
	 *
	 * @param stockId
	 */
	public Set<SampleDetailTo> findSampleDetails(int stockId){
		return sampledetailRepository.findByStockId(stockId).stream().map(e -> converter.convert(e,SampleDetailTo.class)).collect(Collectors.toSet());
	}

	/**
	 *
	 * @param stockId
	 */
	@Override
	public Optional<StockTo> findStock(int stockId){
		if(stockId <1)
		 {return Optional.empty();}
		 return stockRepository.findById(stockId).filter(r -> !r.isDeleted()).map(r -> converter.convert(r,StockTo.class));
	}

	/**
	 *
	 * @param page
	 * @param sort
	 * @param filters
	 */
	@Override
	public Page<StockTo> findStocks(PageInput page, SortInput sort, List<FilterInput> filters){
		return stockRepository.findByCriteria(StockModel.class,filters,sort,page).map(r -> converter.convert(r,StockTo.class));
	}

	/**
	 *
	 * @param stock
	 */
	@Override @Transactional(readOnly = false)
	public StockTo modifyStock(StockInput stock){
		StockModel target= stockRepository.findById(stock.getId()).orElseThrow(() -> new RuntimeException("Stock not found"));
		 StockModel source= converter.convert(stock,StockModel.class);
		 Utils.copyNotNulls(source,target);
		 return converter.convert(stockRepository.save(target), StockTo.class);
	}

	/**
	 *
	 * @param containersetRepository
	 * @param sampledetailRepository
	 * @param converter
	 * @param stockRepository
	 */
	@Autowired
	public StockServiceImpl(ContainerSetRepository containersetRepository, SampleDetailRepository sampledetailRepository, ConversionService converter, StockRepository stockRepository){
		this.stockRepository =stockRepository;
		 this.converter = converter;
		 this.sampledetailRepository = sampledetailRepository;
		 this.containersetRepository = containersetRepository;
	}

}
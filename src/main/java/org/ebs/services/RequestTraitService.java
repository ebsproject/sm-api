package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.RequestTo;
import org.ebs.services.to.RequestTraitTo;
import org.ebs.services.to.Input.RequestTraitInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface RequestTraitService {

	public RequestTraitTo createRequestTrait(RequestTraitInput RequestTraitTInput);

	public int deleteRequestTrait(int id);

	public Optional <RequestTraitTo> findRequestTrait(int id);

	public Optional<RequestTo> findRequest(int requestlistmemberId);
	
	public Page<RequestTraitTo> findRequestTraits(PageInput page, SortInput sort, List<FilterInput> filters);

	public RequestTraitTo modifyRequestTrait(RequestTraitInput requestListMember);
}

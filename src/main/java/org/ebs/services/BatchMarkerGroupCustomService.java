package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.BatchMarkerGroupCustomTo;
import org.ebs.services.to.BatchMarkerGroupTo;
import org.ebs.services.to.Input.BatchMarkerGroupCustomInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface BatchMarkerGroupCustomService {

	public BatchMarkerGroupCustomTo createBatchMarkerGroupCustom(BatchMarkerGroupCustomInput input);

	public int deleteBatchMarkerGroupCustom(int id);

	public Optional <BatchMarkerGroupCustomTo> findBatchMarkerGroupCustom(int id);
	
	public Page<BatchMarkerGroupCustomTo> findBatchMarkerGroupCustoms(PageInput page, SortInput sort, List<FilterInput> filters);

	public BatchMarkerGroupCustomTo modifyBatchMarkerGroupCustom(BatchMarkerGroupCustomInput input);
	
	public Optional<BatchMarkerGroupTo> findBatchMarkerGroup(int id); 
}

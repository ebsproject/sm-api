package org.ebs.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.services.to.BatchMarkerGroupCustomTo;
import org.ebs.services.to.BatchMarkerGroupTo;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.Input.BatchMarkerGroupInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface BatchMarkerGroupService {

	public BatchMarkerGroupTo createBatchMarkerGroup(BatchMarkerGroupInput input);
	
	public BatchMarkerGroupTo modifyBatchMarkerGroup(BatchMarkerGroupInput input);
	
	public int deleteBatchMarkerGroup(int id);
	
	public Optional <BatchMarkerGroupTo> findBatchMarkerGroup(int id);
	
	public Page<BatchMarkerGroupTo> findBatchMarkerGroups(PageInput page, SortInput sort, List<FilterInput> filters);
	
	public Optional <BatchTo> findBatch(int id);
	
	public Set <BatchMarkerGroupCustomTo> findBatchMarkerGroupCustoms(int id);
	
	
}

package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.services.to.RequestMarkerGroupTo;
import org.ebs.services.to.RequestTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.springframework.data.domain.Page;

public interface RequestMarkerGroupService {

	Optional <RequestTo> findRequest(int requestMarkerGroupId);
	public Page<RequestMarkerGroupTo> findRequestMarkerGroups(PageInput page, SortInput sort, List<FilterInput> filters);
}

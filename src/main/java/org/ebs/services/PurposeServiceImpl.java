package org.ebs.services;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.model.MarkerGroupPurposeModel;
import org.ebs.model.PurposeModel;
import org.ebs.model.ServiceTypeModel;
import org.ebs.model.TriatCategoryPurposeModel;
import org.ebs.model.repos.MarkerGroupPurposeRepository;
import org.ebs.model.repos.PurposeRepository;
import org.ebs.model.repos.RequestRepository;
import org.ebs.model.repos.RequestTraitRepository;
import org.ebs.model.repos.ServiceRepository;
import org.ebs.model.repos.ServiceTypeRepository;
import org.ebs.model.repos.TriatCategoryPurposeRepository;
import org.ebs.services.custom.MarkerService;
import org.ebs.services.to.PurposeTo;
import org.ebs.services.to.RequestMarkerGroupTo;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.ServiceTo;
import org.ebs.services.to.ServiceTypeTo;
import org.ebs.services.to.Input.PurposeInput;
import org.ebs.services.to.custom.MarkerPanelTo;
import org.ebs.services.to.custom.TraitCategoryTo;
import org.ebs.services.to.custom.TraitCustomTo;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Juan Carlos Moreno Sanchez <j.m.sanchez@cgiar.org>
 * @version 1.0
 * @created 12-Mar-2021 11:46:19 AM
 */
@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
@Slf4j
class PurposeServiceImpl implements PurposeService {

	private final ConversionService converter;
	private final PurposeRepository purposeRepository;
	private final RequestRepository requestRepository;
	private final ServiceRepository serviceRepository;
	private final ServiceTypeRepository serviceTypeRepository;
	private final MarkerGroupPurposeRepository markerGroupPurposeRepository;
	private final TriatCategoryPurposeRepository triatCategoryPurposeRepository;
	private final RequestTraitRepository requestTraitRepository;
	private final TriatCategoryPurposeRepository traitCatRepository;
	private final MarkerGroupPurposeRepository markerPanelRepo;

	private final MarkerService markerService;

	/**
	 *
	 * @param Purpose
	 */
	@Override
	@Transactional(readOnly = false)
	public PurposeTo createPurpose(PurposeInput Purpose) {
		PurposeModel model = converter.convert(Purpose, PurposeModel.class);
		model.setId(0);
		ServiceTypeModel serviceTypeModel = serviceTypeRepository.findById(Purpose.getServicetype().getId()).get();
		model.setServicetype(serviceTypeModel);
		model = purposeRepository.save(model);
		return converter.convert(model, PurposeTo.class);
	}

	/**
	 *
	 * @param purposeId
	 */
	@Override
	@Transactional(readOnly = false)
	public int deletePurpose(int purposeId) {
		PurposeModel purpose = purposeRepository.findById(purposeId)
				.orElseThrow(() -> new RuntimeException("Purpose not found"));
		purpose.setDeleted(true);
		purposeRepository.save(purpose);
		return purposeId;
	}

	/**
	 *
	 * @param purposeId
	 */
	@Override
	public Optional<PurposeTo> findPurpose(int purposeId) {
		if (purposeId < 1) {
			return Optional.empty();
		}
		return purposeRepository.findById(purposeId).filter(r -> !r.isDeleted())
				.map(r -> converter.convert(r, PurposeTo.class));
	}

	/**
	 *
	 * @param page
	 * @param sort
	 * @param filters
	 */
	@Override
	public Page<PurposeTo> findPurposes(PageInput page, SortInput sort, List<FilterInput> filters,
			boolean disjunctionFilters) {
		return purposeRepository.findByCriteria(PurposeModel.class, filters, sort, page, disjunctionFilters)
				.map(r -> converter.convert(r, PurposeTo.class));
	}

	/**
	 *
	 * @param purposeId
	 */
	public Set<RequestTo> findRequests(int purposeId) {
		return requestRepository.findByPurposeId(purposeId).stream().map(e -> converter.convert(e, RequestTo.class))
				.collect(Collectors.toSet());
	}

	/**
	 *
	 * @param purposeId
	 */
	public Set<ServiceTo> findServices(int purposeId) {
		return serviceRepository.findByPurposeId(purposeId).stream().map(e -> converter.convert(e, ServiceTo.class))
				.collect(Collectors.toSet());
	}

	/**
	 *
	 * @param purposeId
	 */
	public Optional<ServiceTypeTo> findServiceType(int purposeId) {
		return purposeRepository.findById(purposeId)
				.map(r -> converter.convert(r.getServicetype(), ServiceTypeTo.class));
	}

	/**
	 *
	 * @param purpose
	 */
	@Override
	@Transactional(readOnly = false)
	public PurposeTo modifyPurpose(PurposeInput purpose) {
		PurposeModel target = purposeRepository.findById(purpose.getId())
				.orElseThrow(() -> new RuntimeException("Purpose not found"));
		PurposeModel source = converter.convert(purpose, PurposeModel.class);
		if (purpose.getServicetype() != null ){
			source.setServicetype(serviceTypeRepository.findById(purpose.getServicetype().getId()).get());
		}
		Utils.copyNotNulls(source, target);
		return converter.convert(purposeRepository.save(target), PurposeTo.class);
	}

	public Set<MarkerPanelTo> findMarkerGroupPurposes(int purposeId) {
		Set<MarkerPanelTo> setMarkerPanel = new HashSet<MarkerPanelTo>();
		List<MarkerGroupPurposeModel> markerGroupPurposeModels = markerGroupPurposeRepository
				.findByPurposeId(purposeId);
		try {
			markerGroupPurposeModels.stream()
					.forEach(markerPanel -> setMarkerPanel
							.add(markerService.findMarkerGroupById(markerPanel.getMarkerGroupId(), false)));
		} catch (Exception e) {
			log.error("Error to get the marker from purpose -> {}", e);
			setMarkerPanel.clear();
			markerGroupPurposeModels.stream()
					.forEach(markerPanel -> setMarkerPanel.add(new MarkerPanelTo(markerPanel.getId())));
		}
		// If some marker panel can't get return clean all and return only ids
		if (setMarkerPanel.contains(null)) {
			setMarkerPanel.clear();
			markerGroupPurposeModels.stream()
					.forEach(markerPanel -> setMarkerPanel.add(new MarkerPanelTo(markerPanel.getId())));
		}
		return setMarkerPanel;
	}

	public Set<MarkerPanelTo> findMarkerGroupPurpose(Set<RequestMarkerGroupTo> set) {
		Set<MarkerPanelTo> setMarkerPanelTos = new HashSet<MarkerPanelTo>();
		if (set != null && set.size() > 0) {
			for (RequestMarkerGroupTo to : set) {
				setMarkerPanelTos.add(markerService.findMarkerGroupById(to.getMarkerGroupID(), false));
			}
		} else
			return new HashSet<MarkerPanelTo>();
		return setMarkerPanelTos;
	}

	public Set<TraitCustomTo> findTraitList(int id) {
		Set<TraitCustomTo> set = new HashSet<TraitCustomTo>();
		try {
			requestTraitRepository.findByRequestId(id).stream().forEach(e -> {
				if (!e.isDeleted()) {
					TraitCustomTo traitCustomTo = markerService.findTraitById(e.getTraitId());
					MarkerPanelTo[] arr = new MarkerPanelTo[1];
					for (MarkerPanelTo to : traitCustomTo.getMarkergroups()) {
						if (e.getMarkerGroupId() != null && e.getMarkerGroupId().intValue() == to.getId()) {
							arr[0] = to;
							break;
						}
					}
					traitCustomTo.setMarkergroups(arr);
					set.add(traitCustomTo);
				}
			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return set;
	}

	public Set<TraitCategoryTo> findTriatCategoryPurposes(int purposeId) {
		Set<TraitCategoryTo> setTraitCategoryTo = new HashSet<TraitCategoryTo>();
		triatCategoryPurposeRepository.findByPurposeId(purposeId).stream()
				.forEach(e -> setTraitCategoryTo.add(markerService.findTraitCategory(e.getTraitCategoryId())));
		return setTraitCategoryTo;
	}

	@Override
	@Transactional(readOnly = false)
	public PurposeTo addTraitCategories(Integer purposeId, Integer[] traitCategories) {
		final PurposeModel model = purposeRepository.findById(purposeId)
				.orElseThrow(() -> new RuntimeException("Purpose not found"));
		List<Integer> traitCategoryIds = new ArrayList<>(asList(traitCategories));
		List<Integer> currentTraitCategories = model.getTriatCategoryPurposes().stream()
				.map(t -> t.getTraitCategoryId()).collect(toList());
		traitCategoryIds.removeAll(currentTraitCategories);
		traitCategoryIds.forEach(id -> {
			if (markerService.findTraitCategory(id) == null)
				throw new RuntimeException("Trait Category with id " + id + " does not exist");

			TriatCategoryPurposeModel traitCat = new TriatCategoryPurposeModel(0, id, model);
			traitCat.setPurpose(model);
			model.getTriatCategoryPurposes().add(traitCat);
		});
		PurposeModel purpose = purposeRepository.save(model);
		return converter.convert(purpose, PurposeTo.class);
	}

	@Override
	@Transactional(readOnly = false)
	public PurposeTo removeTraitCategories(Integer purposeId, Integer[] traitCategories) {
		PurposeModel model = purposeRepository.findById(purposeId)
				.orElseThrow(() -> new RuntimeException("Purpose not found"));

		log.trace("Trait Categories before remove: {}", model.getTriatCategoryPurposes().size());
		List<Integer> traitCategoryIds = new ArrayList<>(asList(traitCategories));

		List<TriatCategoryPurposeModel> traitCatToRemove = model.getTriatCategoryPurposes().stream()
				.filter(t -> traitCategoryIds.contains(t.getTraitCategoryId())).collect(toList());
		List<Integer> traitCatIdsToRemove = traitCatToRemove.stream().map(t -> t.getTraitCategoryId())
				.collect(toList());

		traitCategoryIds.removeAll(traitCatIdsToRemove);
		if (!traitCategoryIds.isEmpty()) {
			throw new RuntimeException(
					"Trait Category with id " + traitCategoryIds.get(0) + " does not exist in this purpose");
		}

		traitCatRepository.deleteAllInBatch(traitCatToRemove);

		model.getTriatCategoryPurposes().removeAll(traitCatToRemove);
		model = purposeRepository.save(model);
		log.trace("Trait Categories after remove: {}", model.getTriatCategoryPurposes().size());

		return converter.convert(model, PurposeTo.class);
	}

	@Override
	@Transactional(readOnly = false)
	public PurposeTo addMarkerPanels(Integer purposeId, Integer[] markerPanels) {
		log.trace("Service purpose ::: -> Add new marker : {} to purpose: {}Marker Panels before add: {}",markerPanels, purposeId);
		final PurposeModel model = purposeRepository.findById(purposeId)
				.orElseThrow(() -> new RuntimeException("Purpose not found"));
		List<Integer> markerPanelIds = new ArrayList<>(asList(markerPanels));
		List<Integer> currentMarkerPanels = model.getMarkergrouppurposes().stream().map(t -> t.getMarkerGroupId())
				.collect(toList());
		markerPanelIds.removeAll(currentMarkerPanels);
		markerPanelIds.forEach(id -> {
			if (markerService.findMarkerGroupById(id, false) == null)
				throw new RuntimeException("Marker Panel with id " + id + " does not exist");
			MarkerGroupPurposeModel markerPanel = new MarkerGroupPurposeModel(0, id, model);
			markerPanel.setPurpose(model);
			model.getMarkergrouppurposes().add(markerPanel);
		});
		PurposeModel purpose = purposeRepository.save(model);
		return converter.convert(purpose, PurposeTo.class);
	}

	@Override
	@Transactional(readOnly = false)
	public PurposeTo removeMarkerPanels(Integer purposeId, Integer[] markerPanels) {
		PurposeModel model = purposeRepository.findById(purposeId)
				.orElseThrow(() -> new RuntimeException("Purpose not found"));
		List<Integer> markerPanelIds = new ArrayList<>(asList(markerPanels));
		List<MarkerGroupPurposeModel> markerPanelsToRemove = model.getMarkergrouppurposes().stream()
				.filter(t -> markerPanelIds.contains(t.getMarkerGroupId())).collect(toList());
		List<Integer> markerPanelIdsToRemove = markerPanelsToRemove.stream().map(m -> m.getMarkerGroupId())
				.collect(toList());
		markerPanelIds.removeAll(markerPanelIdsToRemove);
		if (!markerPanelIds.isEmpty()) {
			throw new RuntimeException(
					"Marker Panel with id " + markerPanelIds.get(0) + " does not exist in this purpose");
		}
		markerPanelRepo.deleteAllInBatch(markerPanelsToRemove);
		model.getMarkergrouppurposes().removeAll(markerPanelsToRemove);
		model = purposeRepository.save(model);
		return converter.convert(model, PurposeTo.class);
	}

}
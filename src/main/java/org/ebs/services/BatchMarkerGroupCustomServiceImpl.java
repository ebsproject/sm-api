package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.BatchMarkerGroupCustomModel;
import org.ebs.model.repos.BatchMarkerGroupCustomRepository;
import org.ebs.model.repos.BatchMarkerGroupRepository;
import org.ebs.services.to.BatchMarkerGroupCustomTo;
import org.ebs.services.to.BatchMarkerGroupTo;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.Input.BatchMarkerGroupCustomInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@Service @Transactional(readOnly = true)

public class BatchMarkerGroupCustomServiceImpl implements BatchMarkerGroupCustomService{
	
	private final ConversionService converter;
	private final BatchMarkerGroupCustomRepository batchMarkerGroupCustomRepository;
	private final BatchMarkerGroupRepository batchMarkerGroupRepository; 
	
	
	@Override @Transactional(readOnly = false)
	public BatchMarkerGroupCustomTo createBatchMarkerGroupCustom(BatchMarkerGroupCustomInput input) {
		BatchMarkerGroupCustomModel model = converter.convert(input, BatchMarkerGroupCustomModel.class);	
		model.setId(0);
		System.out.println("input "+input.getBatchMarkerGroup().getId());
		
		if (input.getBatchMarkerGroup() != null && input.getBatchMarkerGroup().getId() > 0) {
			model.setBatchMarkerGroup(batchMarkerGroupRepository.findById(input.getBatchMarkerGroup().getId()).get());
		}
		model = batchMarkerGroupCustomRepository.save(model);
		return converter.convert(model,BatchMarkerGroupCustomTo.class );
	}

	@Override @Transactional(readOnly = false)
	public int deleteBatchMarkerGroupCustom(int id) {
		BatchMarkerGroupCustomModel model = batchMarkerGroupCustomRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("BatchMarkerGroupCustomModel not found"));
		model.setDeleted(true);
		batchMarkerGroupCustomRepository.save(model);
		return id;
	}

	@Override
	public Optional<BatchMarkerGroupCustomTo> findBatchMarkerGroupCustom(int id) {
		if(id <1)
		 {return Optional.empty();}
		
		return batchMarkerGroupCustomRepository.findById(id).filter(r -> !r.isDeleted()).map(r -> converter.convert(r,BatchMarkerGroupCustomTo.class));

	}

	@Override
	public Page<BatchMarkerGroupCustomTo> findBatchMarkerGroupCustoms(PageInput page, SortInput sort,
			List<FilterInput> filters) {
		return batchMarkerGroupCustomRepository.findByCriteria(BatchMarkerGroupCustomModel.class, filters, sort, page).map(r -> converter.convert(r,BatchMarkerGroupCustomTo.class));
	}

	@Override @Transactional(readOnly = false)
	public BatchMarkerGroupCustomTo modifyBatchMarkerGroupCustom(BatchMarkerGroupCustomInput input) {
		BatchMarkerGroupCustomModel target = batchMarkerGroupCustomRepository.findById(input.getId())
				.orElseThrow(() -> new RuntimeException("RequestTrait not found"));
		BatchMarkerGroupCustomModel source =  converter.convert(input, BatchMarkerGroupCustomModel.class);
		Utils.copyNotNulls(source,target);
		return converter.convert(batchMarkerGroupCustomRepository.save(target), BatchMarkerGroupCustomTo.class);

	}

	@Override
	public Optional<BatchMarkerGroupTo> findBatchMarkerGroup(int id) {
		return batchMarkerGroupCustomRepository.findById(id).map(r -> converter.convert(r.getBatchMarkerGroup(),BatchMarkerGroupTo.class));
	}

}

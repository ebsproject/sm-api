package org.ebs.services;

import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static org.ebs.model.RequestStatusModel.REQ_STATUS_FILE_PROCESSED;
import static org.ebs.model.RequestStatusModel.REQ_STATUS_FILE_PROCESSING_FAILED;
import static org.ebs.model.RequestStatusModel.REQ_STATUS_FILE_PROCESSING_ONGOING;
import static org.ebs.services.GigwaLoader.getBatchIdFromFileObject;
import static org.ebs.util.client.FileClient.STATUS_GIGWA_LOADED_ERROR;
import static org.ebs.util.client.FileClient.STATUS_GIGWA_SPLIT;
import static org.ebs.util.client.FileClient.STATUS_GIGWA_SPLIT_ERROR;
import static org.ebs.util.client.FileClient.STATUS_UPLOADED;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.ebs.model.BatchModel;
import org.ebs.model.repos.BatchRepository;
import org.ebs.rest.to.FileObject;
import org.ebs.rest.to.GenotypeSearchParams;
import org.ebs.rest.validation.Formats;
import org.ebs.services.custom.BatchDesignService;
import org.ebs.services.custom.CSFileObject;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.Input.BatchInput;
import org.ebs.services.to.custom.RequestIDTo;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.ebs.util.client.FileClient;
import org.ebs.util.client.FileUploadForm;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;



@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class GigwaLoaderImpl implements GigwaLoader{

    private final CSFileObject csFileObjectService;
    private final FileClient fileClient;
    private final BatchService batchService;
    private final GigwaService gigwaService;
    private final NotificationService notificationService;
    private final BatchRepository batchRepo;
    private final BatchDesignService batchDesignService;
    private final Formats formats;

    private static final int MAX_PROCESS_MINUTES = 45;

    /**
     * Finds genotype result files in the system storage and tries to upload them to gigwa
     */
    @Override
    @Transactional(readOnly = false)
    public boolean loadUnprocessedFiles(UUID id) {
        log.debug("loading file with id {}", id.toString());
        FileObjectTo f = csFileObjectService.findByKey(id);

        if (f == null)
            return false;
        int batchId = 0;
        log.info("filesToLoad: {}", f);

        int creatorId = -1;
        try{
            log.debug("fileObject to process {}", f);
            batchId = getBatchIdFromFileObject(f);
            Optional<BatchModel> model = batchRepo.findById(batchId);
            
            if(isFileTimedOut(f)) {
                setFileFailed(f, " File was not processed after " + MAX_PROCESS_MINUTES + " minutes");
                return false;
            }

            if(!model.isPresent()) {
                setFileFailed(f," Batch with id (" +batchId+ ") does not exist");
                return false;
            }

            creatorId = model.get().getCreatedBy();
            int vendorId = model.get().getVendorId();

            Path tmpPath = null;
            log.trace("Preparing resource for vendorId {}", vendorId);
            FileSystemResource fsr = prepareResource(f, vendorId, tmpPath);
            tmpPath = Path.of(fsr.getPath());
            String response = gigwaService.sendImport(fsr);
            log.info("gigwaService.sendImport() response: {}", response);
            if (response != null) {
                try {
                    Files.deleteIfExists(tmpPath);
                } catch (IOException e) {
                    log.error("cannot delete temporary file {}", tmpPath);
                }
                //log.trace("before updating file: {}", f);
                //f.setStatus(STATUS_GIGWA_LOADED);
                //FileObjectTo fo = csFileObjectService.modify(f);
                //log.trace("after updating file: {}", fo);
                String importProgressStatus = gigwaService.getProcessStatus(response);
                while (importProgressStatus.equals("complete:false"))
                {
                    if (isFileTimedOut(f) ) {
                        throw new RuntimeException("Could not find study for file " + f.getName() + " and batch id" +batchId+ " in gigwa since " + f.getCreatedOn()+ ". Marked as failed");
                    } else {
                        log.info("File {} is still being processed in Gigwa since {}", f.getName(), f.getCreatedOn());
                        Thread.sleep(7 * 1000);
                        importProgressStatus = gigwaService.getProcessStatus(response);
                    }
                }
                if (importProgressStatus.contains("error")) {
                    throw new RuntimeException(importProgressStatus);
                } else if (importProgressStatus.equals("No content")) {
                    throw new RuntimeException("Could not assertain progress status of file " + f.getName() + " in Gigwa, received [HTTP 204 No Content] response");
                }
                updateBatch(f, batchId, REQ_STATUS_FILE_PROCESSING_ONGOING, null);
                log.trace("File Object updated: {}", f);
                log.info("File {} for batch({}) was sent to gigwa and set to REQ_STATUS_FILE_PROCESSING_ONGOING", f.getName(), batchId);
            } else {
                log.info("Could not get response from Gigwa when importing genotyping data");
            }
        } catch (Exception e) {
            e.printStackTrace();
            updateBatch(f, batchId, REQ_STATUS_FILE_PROCESSING_FAILED, e.getMessage());
            String error = String.format("[Could not process file %s: %s]", f.getName(), e.getMessage());
            log.error(error);

            setFileFailed(f, error);

            notificationService.sendNotificationGeneric(creatorId, batchId, error);
            return false;
        }
        System.gc();
        return true;
    }

    private void setFileFailed(FileObjectTo file, String error) {
        file.setDescription(file.getDescription() + error);
        file.setStatus(STATUS_GIGWA_LOADED_ERROR);
        csFileObjectService.modify(file);
    }

    private boolean isFileTimedOut(FileObjectTo file) {
        return file.getCreatedOn().toInstant().plus(MAX_PROCESS_MINUTES, MINUTES).isBefore(now());
    }

    /**
     * Creates a Resource that can be used as file attachment in a request for upload.
     * @param fileObject with the information of the file to be prepared
     * @param vendorId defining the expected format of the result file
     * @tmpPath where th efile to be imported will be temporarily living
     * @return a {@link FileSystemResource} that can be sent in a Multipart Form Data request
     */
    private FileSystemResource prepareResource(FileObjectTo fileObject, int vendorId, Path tmpPath) {
        FileSystemResource fileResource = null;

        if (vendorId == formats.getVENDOR_INTERTEK_ID()) {
            tmpPath = Path.of("tmp",fileObject.getName().replace(".csv", ".intertek"));
        } else if(vendorId == formats.getVENDOR_DARTAG()){
            tmpPath = Path.of("tmp",fileObject.getName().replace(".csv", ".hapmap"));
        } else if(vendorId == formats.getVENDOR_AGRIPLEX_ID()){
            tmpPath = Path.of("tmp",fileObject.getName().replace(".xlsx", ".hapmap"));
        }  else {
            tmpPath = Path.of("tmp",fileObject.getName());
        }

        try {
            log.trace("preparing File for {}", tmpPath);
            gigwaService.prepareFile(fileObject, vendorId, tmpPath);
            log.trace("preparing FileSystemResource for {}", tmpPath);
            fileResource = new FileSystemResource(tmpPath);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Cannot prepare file system resource: " + e.getMessage());
        }
        log.trace("FileResource ready {}", fileResource.getFilename());
        return fileResource;
    }

    private void updateBatch(FileObjectTo fileObject, int batchId, int requestStatus, String statusMessage) {
        log.trace("Updating batch(id={}) with file object: {}", batchId, fileObject);
        if (batchId != 0) {
            BatchInput batch = new BatchInput();
            batch.setId(batchId);
            batch.setResultFileId(fileObject.getKey());
            log.trace("BatchInput: {}", batch);
            try {
            BatchTo b =  batchService.modifyBatch(batch);
            log.trace("After updating batch(id={}): {}", batchId, b);
            batchDesignService.changeRequestStatusByBatch(batchId, fileObject.getTenant(), requestStatus, statusMessage);
            }catch (Exception ex){
            	log.error(ex.getMessage());
            	throw new Error("Error to save the batch........");
            }
            
        }
    }

    /**
     * Looks for result files that have been loaded into gigwa
     * and splits the data by request. The file is available
     * via file-api being the name of the file the same as the request
     * with .csv extension
     */
    @Override
    @Transactional(readOnly = false)
    public BrResponse<String> generateRequestResultFiles(UUID id) {
        FileObjectTo fileToSplit = csFileObjectService.findByKey(id);

        if (fileToSplit == null) {
            log.trace("No file to split");
            return BrapiResponseBuilder.forData("File not found").withStatusSuccess().build();
        }
        log.debug("processing file {}", fileToSplit);
        int batchId = getBatchIdFromFileObject(fileToSplit);
        Optional<BatchModel> batch = batchRepo.findById(batchId);

        //int vendorId = batch.get().getVendorId();

        List<RequestIDTo> requests = batchService.getRequesByBatchID(batchId).getContent();
        requests.forEach(req -> {
            try {
                log.debug("extracting samples for batch/request: {}/{}", batchId, req.getId());

                int tenantId = fileToSplit.getTenant();
                /* BrStudy study = gigwaService.getStudy(fileToSplit.getName());
                while (study == null) {
                    if ( isFileTimedOut(fileToSplit) ) {
                        throw new RuntimeException("Could not find study for file " + fileToSplit.getName() + " and batch " +batch.get().getName()+ " in gigwa since " + fileToSplit.getCreatedOn()+ ". Marked as failed");
                    } else {
                        log.warn("Cannot find study for file {} and batch {} since {}", fileToSplit.getName(), batch.get().getName(), fileToSplit.getCreatedOn());
                        Thread.sleep(7 * 1000);
                        study = gigwaService.getStudy(fileToSplit.getName());
                    }
                } 
                log.debug("Found gigwa stufy: {}", study); */

                FileUploadForm form = new FileUploadForm(tenantId,
                        generateRequestFileResource(req.getId()),
                        List.of("requestId:" + req.getId()),
                        "Autogenerated Request Result File",
                        STATUS_UPLOADED);
                FileObject fileObject = fileClient.uploadFile(form);
                log.debug("File Uploaded: {}", fileObject.getName());
                System.gc();
                
                removeTmpFiles();

                fileToSplit.setStatus(STATUS_GIGWA_SPLIT);
                csFileObjectService.modify(fileToSplit);
                
                batchDesignService.changeRequestStatus(req.getId(), batch.get().getTenant(), REQ_STATUS_FILE_PROCESSED, "");
            
            } catch (Exception e) {
                String error = String.format(" [Could not process file for %s: %s]", req,
                        e.getMessage());
                log.error(error);
                fileToSplit.setStatus(STATUS_GIGWA_SPLIT_ERROR);
                fileToSplit.setDescription(fileToSplit.getDescription() + error);
                csFileObjectService.modify(fileToSplit);

                batchDesignService.changeRequestStatus(req.getId(), batch.get().getTenant(), REQ_STATUS_FILE_PROCESSING_FAILED, error);
                notificationService.sendNotificationGeneric(batch.get().getCreatedBy(), batchId, error);

            }
        });
        System.gc();
        notificationService.sendNotificationAsGigwa(batch.get().getCreatedBy(), batchId,
            "Result Data for request(s) in Batch "+ batch.map(b -> b.getName()).orElse("") + " ready for download");

        return BrapiResponseBuilder.forData("File uploaded")
                .withStatusSuccess()
                .build();

    }

    void removeTmpFiles() {
        try {
            Files.list(Path.of("tmp")).forEach(t -> {
                try {
                    Files.deleteIfExists(t);
                } catch (IOException e) {
                    log.error("Could not remove temporal file: {}", e.getMessage());
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            log.error("Cannot read tmp directory: {}", e.getMessage());
        }
    }
    /**
     * Generates a csv containing the result data from a request
     * @return
     */
    FileSystemResource generateRequestFileResource(Integer requestId) {
        Path tmpFile = null;
        GenotypeSearchParams params = new GenotypeSearchParams(requestId);
        try {
            Files.createDirectories(Path.of("tmp"));
            Path tmpPath = Path.of("tmp", gigwaService.createFileName(params));
            Files.deleteIfExists(tmpPath);
            tmpFile = Files.createFile(tmpPath);

            BufferedWriter writer = new BufferedWriter(new FileWriter(tmpFile.toFile()));
            gigwaService.generateGenotypeMatrix(params, writer);
            writer.flush();
            writer.close();
        
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(
                    "Cannot generate genotype Request Result Data: " + e.getMessage());
        }
        return new FileSystemResource(tmpFile);
    }

}

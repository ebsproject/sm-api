package org.ebs.services;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.ebs.rest.to.FileObject;
import org.ebs.services.to.FileObjectTo;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrResponse;
import org.springframework.web.multipart.MultipartFile;

public interface GigwaLoader {

    public static int getBatchIdFromFileObject(FileObjectTo fileObject) {
        return getBatchIdFromTags( Stream.of(fileObject.getTags()));
    }

    public static int getBatchIdFromTags(Stream<String> tags) {
        return tags
            .filter(t -> t.startsWith("batchId:")).map(t -> t.replace("batchId:", ""))
            .mapToInt(Integer::valueOf).findFirst().orElse(0);
    }

    /**
     * Finds genotype result files in the system storage and tries to upload them to gigwa, 
     * then marks them as processed
     */
    public boolean loadUnprocessedFiles(UUID id);

    /**
     * Finds processed genotype result files in the system storage and use them as
     * starting point to generate result files. One file is generated for each request,
     * so one result file can generate multiple output files.
     */
    public BrResponse<String> generateRequestResultFiles(UUID id);

}

package org.ebs.services;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.ebs.services.GigwaLoader.getBatchIdFromFileObject;
import static org.ebs.util.client.FileClient.STATUS_UPLOADED;
import static org.ebs.util.client.FileClient.STATUS_UPLOADED_NEEDS_CONFIRMATION;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.ebs.rest.to.FileObject;
import org.ebs.rest.to.GenotypeSearchParams;
import org.ebs.rest.validation.Formats;
import org.ebs.rest.validation.ResultFileValidator;
import org.ebs.services.converter.AgriplexToHapMapConverter;
import org.ebs.services.converter.DarTagToHapMapConverter;
import org.ebs.services.custom.CSFileObject;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.SampleGermplasmDetailTo;
import org.ebs.util.brapi.BrCall;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrSample;
import org.ebs.util.brapi.BrStudy;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.ebs.util.client.FileClient;
import org.ebs.util.client.FileUploadForm;
import org.ebs.util.client.GigwaClient;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class GigwaServiceImpl implements GigwaService {


    private final FileClient fileClient;
    private final AgriplexToHapMapConverter agriplexConverter;
    private final DarTagToHapMapConverter darTagConverter;
    private final RequestListMemberService listService;
    private final GigwaClient gigwaClient;
    private final Formats formats;
    private final BatchService batchService;
    private final CSFileObject csFileObjectService;

    private static final String SEPARATOR = "§";
    private static final String CALLSET_REPLACING = DEFAULT_MODULE_NAME + SEPARATOR + "\\d*" + SEPARATOR;
    private static final String CALLSET_REPLACEMENT = DEFAULT_MODULE_NAME + SEPARATOR;

    @Override
    public void prepareFile(FileObjectTo fileObject, int vendorId, Path tmpPath) throws IllegalStateException, IOException, RuntimeException {
        Files.createDirectories(Path.of("tmp"));
        Files.deleteIfExists(tmpPath);
        tmpPath = Files.createFile(tmpPath);

        byte[] fileData = fileClient.download(fileObject.getTenant(), fileObject.getKey());

        int batchId = getBatchIdFromFileObject(fileObject);
        if(fileObject.getName().toLowerCase().endsWith("hapmap")) {
            log.trace("No conversion for hapmap file: {}", tmpPath);
            Files.write(tmpPath, fileData);
        }else if(vendorId == Formats.AGRIPLEX.getVendorId()){
                log.trace("converting agriplex to hapmap for: {}", tmpPath);
                agriplexConverter.convert(batchId, tmpPath, fileData);
                log.trace("uploading hapmap: {}", tmpPath);
                fileClient.uploadFile(
                    new FileUploadForm(fileObject.getTenant(), new FileSystemResource(tmpPath),
                            List.of("agriplex-converted"), "hapmap converted from agriplex source", 1));
        }else if (vendorId == Formats.DARTAG.getVendorId()){
                log.trace("converting darTag to hapmap for: {}", tmpPath);
                darTagConverter.convert(batchId, tmpPath, fileData);
                log.trace("uploading hapmap: {}", tmpPath);
                fileClient.uploadFile(
                    new FileUploadForm(fileObject.getTenant(), new FileSystemResource(tmpPath),
                            List.of("darTag-converted"), "hapmap converted from darTag source", 1));
        } else if(vendorId == Formats.INTERTEK.getVendorId()){
                log.trace("No conversion for intertek file: {}", tmpPath);
                Files.write(tmpPath, fileData);
        }else{
            throw new RuntimeException("Vendor not recognized for " + fileObject);
        }
    }

    @Override
    public List<BrCall> searchCalls(String studyDbId, int batchId, int requestId) {

        List<String> smSampleIds = listService.findAllSampleNames(batchId, requestId);
        log.trace("SM samples: {}", smSampleIds.size());

        List<String> callsetIds = new ArrayList<>(1500);

        gigwaClient.getSamples(studyDbId).forEach(s -> {
            if (smSampleIds.contains(smSampleName(s)))
                callsetIds.add(callsetFromSampleDbId(s.getSampleDbId()));
        });

        log.trace("callsets from filtered  samples: {}", callsetIds.size());
        if (callsetIds.size() == 0)
            throw new RuntimeException("No samples matching between SM and gigwa");

        return gigwaClient.getCalls(callsetIds);
    }

    @Override
    public List<BrCall> searchCalls(List<String> germplasmIds) {
        List<String> callsetIds = new ArrayList<>(500);
        
        gigwaClient.getSamplesByGermplasm(germplasmIds).forEach(s -> callsetIds.add(callsetFromSampleDbId(s.getSampleDbId())));
        log.trace("Searching for {} callsets: {}",callsetIds.size(), callsetIds);
        return gigwaClient.getCalls(callsetIds);
    }

    @Override
    public String smSampleName(BrSample sample) {
        String s = sample.getSampleDbId().replaceAll("\\w+" + SEPARATOR + "\\d+" + SEPARATOR, "")
                .replaceAll(SEPARATOR + "\\d+", "");
        log.trace("sm sample for {}: {}", sample, s);
        return s;
    }

    @Override
    public String smSampleName(BrCall call) {
        return call.getCallSetDbId().replaceFirst("\\w+" + SEPARATOR, "").replaceFirst(SEPARATOR + "\\d+", "");
    }
    
    @Override
    public String smMarkerName(BrCall call) {
        return call.getVariantDbId().replace(DEFAULT_MODULE_NAME + SEPARATOR, "");
    }

    @Override
    public String genotypeOf(BrCall call) {
        return call.getGenotype().getValues()[0];
    }

    @Override
    public BrStudy getStudy(String fileName) {   
        String studyName = "project-" + fileName.substring(0, fileName.lastIndexOf("."));
        return gigwaClient.getStudy(studyName).orElse(null);
    }

    @Override
    public String sendImport(FileSystemResource resource) {
        return gigwaClient.sendImport(resource);
    }

    @Override
    public String getProcessStatus(String progressId) {
        return gigwaClient.getProcessStatus(progressId);
    }

    @Override
    public String gemplasmDbId(String studyDbId, String smSampleId) {
        if (studyDbId == null || smSampleId == null) {
            log.warn("cannot generate gwGermplasmDbId with study {} and smSampleId {}", studyDbId, smSampleId);
            return null;
        }
        return String.format("%s%s%s", studyDbId, SEPARATOR, smSampleId);
    }

    String callsetFromSampleDbId(String sampleDbId) {
        return sampleDbId.replaceFirst(CALLSET_REPLACING, CALLSET_REPLACEMENT);
    }

    @Override
    public BrPagedResponse<FileObject> uploadFile(int tenantId, MultipartFile[] files,
            List<String> tags, String description, boolean hasValidationErrors) {

        log.trace("Has Validation Errors ({}) while uploading file {}", hasValidationErrors,
                files[0].getOriginalFilename());
        if (hasValidationErrors) {
            tags.add(ResultFileValidator.CONFIRM_WARNINGS_TAG);
        }

        try {
            FileObject fo = fileClient
                .uploadFile(new FileUploadForm(tenantId, generateFileResource(files), tags, description,
                        hasValidationErrors ? STATUS_UPLOADED_NEEDS_CONFIRMATION : STATUS_UPLOADED));
            log.trace("File Client response: {}", fo);
            return BrapiResponseBuilder.forData(new PageImpl<>(List.of(fo))).withStatusSuccess().build();
        } catch (Exception e) {
            int fileExistsMessageStart = e.getMessage().indexOf("A file named");
            if (fileExistsMessageStart != -1) {
                int fileExistsMessageEnd = e.getMessage().indexOf("for tenant");
                if (fileExistsMessageEnd != -1) {
                    String fileExistsMessage = e.getMessage().substring(fileExistsMessageStart, fileExistsMessageEnd).trim();
                    return BrapiResponseBuilder.forData(new PageImpl<>(new ArrayList<FileObject>())).withStatusError(fileExistsMessage).build();
                }
            }
            return BrapiResponseBuilder.forData(new PageImpl<>(new ArrayList<FileObject>())).withStatusError(e.getMessage()).build();
        }
    }

    FileSystemResource generateFileResource(MultipartFile[] files) {
        try {
            Files.createDirectories(Path.of("tmp"));
            Path tmpPath = Path.of("tmp", files[0].getOriginalFilename());
            Files.deleteIfExists(tmpPath);
            Path tmpFile = Files.createFile(tmpPath);
            Files.write(tmpFile, files[0].getInputStream().readAllBytes(), new OpenOption[] {});
            log.trace("FileSystemResource generated at tmp path: {}", tmpFile);
            return new FileSystemResource(tmpFile);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(
                    "Cannot generate temporal file for file: " + files[0].getName() + ": " + e.getMessage());
        }
    }
    
    @Override
    public void generateGenotypeMatrix(GenotypeSearchParams params, BufferedWriter writer) throws IOException {
        
        // search sample germplasm detail(CB + SM)
        Map<Integer, List<SampleGermplasmDetailTo>> sgd = batchService.findSampleGermplasmDetails(params);
        log.trace("{} SGD elements found with params {} ==> {}", sgd.size(), params, sgd);

        for (int requestId : sgd.keySet()) {
            List<SampleGermplasmDetailTo> sampleGermDetList = sgd.get(requestId);
            if (sampleGermDetList == null || sampleGermDetList.isEmpty()) {
                log.warn("Discarding search of calls for request {} with no SampleGermplasmDetails", requestId);
            } else {
                List<BrCall> callList = findCalls(sampleGermDetList);
                if (callList == null || callList.isEmpty()) {
                    log.warn("No calls found for SGDs of request {}", requestId);
                } else {
                    Map<String, Map<String, String>> matrix = new HashMap<>();
                    Set<String> samples = new HashSet<>(100);
                    for (BrCall call : callList) {
                        String sample = smSampleName(call);
                        String marker = smMarkerName(call);
                        String genotype = genotypeOf(call);

                        matrix.putIfAbsent(marker, new HashMap<>());
                        matrix.get(marker).putIfAbsent(sample, genotype);
                        samples.add(sample);
                    }

                    writer.write("sampleId,germplasmId,germplasmCode,germplasmName,");
                    if (params.hasPlotFilter()) {
                        writer.write("plotId,");
                    }
                    if (params.hasPackageFilter()) {
                        writer.write("packageId,");
                    }

                    log.info(">>>>>>>>>>>>>> markers found: " + matrix.keySet().size());
                    log.info(">>>>>>>>>>>>>> samples matching: " + samples.size());

                    String markerNames = matrix.keySet().stream().collect(Collectors.joining(","));
                    writer.write(markerNames);
                    writer.newLine();

                    Map<String, SampleGermplasmDetailTo> sgdMap = sampleGermDetList.stream()
                            .filter(d -> d.getSampleCode() != null && !d.getSampleCode().isEmpty())
                            .collect(toMap(s -> s.getSampleCode(), s -> s, (a, b) -> a));

                    // merge sgd with matrix of calls(markers x samples)
                    StringBuilder sb;
                    //one line by sample
                    for (String sample : samples) {
                        sb = new StringBuilder();
                        SampleGermplasmDetailTo det = sgdMap.get(sample);
                        sb.append(sample);

                        if (det != null) {
                            sb.append(String.format(",%s,%s,%s", det.getGermplasmId(), det.getGermplasmCode(),
                                    det.getDesignation()));
                            if (params.hasPlotFilter()) {
                                sb.append(",").append(det.getPlotId());
                            }
                            if (params.hasPackageFilter()) {
                                sb.append(",").append(det.getPackageId());
                            }
                        } else {
                            sb.append(",,,");
                            if (params.hasPlotFilter()) {
                                sb.append(",");
                            }
                            if (params.hasPackageFilter()) {
                                sb.append(",");
                            }
                        }

                        for (String marker : matrix.keySet()) {
                            String g = matrix.get(marker).get(sample);
                            sb.append(",");
                            if (g != null)
                                sb.append(g);
                        }
                        
                        writer.write(sb.toString());
                        writer.newLine();
                        writer.flush();
                    }
                }
            }
        }

    }
    
    List<BrCall> findCalls(List<SampleGermplasmDetailTo> sgd) {
        //get batches -> getFileNames -> get study names -> get GW studyDbId
        Map<Integer, String> batchStudyDbIdMap = new HashMap<>();
        sgd.stream().map(s -> s.getBatchId())
            .distinct()
            .map(bid -> batchService.findBatch(bid).orElse(null))
                .filter(b -> b != null)
            .forEach(b -> {
                String studyDbId = getStudyDbId(b);
                if(studyDbId != null)
                    batchStudyDbIdMap.put(b.getId(), studyDbId);
            });
        log.trace("{} elements in batch to studyDbId map: {}", batchStudyDbIdMap.size(), batchStudyDbIdMap);

        if(batchStudyDbIdMap.isEmpty()) 
            return Collections.emptyList();

        //Generate Gigwa gids :: studyDbId + sampleDbId = germplasmDbId
        List<String> gigwaGermplasmList = sgd.stream()
                .map(s -> gemplasmDbId(batchStudyDbIdMap.get(s.getBatchId()), s.getSampleCode()))
                .filter(s -> s != null)
                .collect(toList());
        
        log.trace("searching genotypes for germplasmDbIds: {}", gigwaGermplasmList);
        List<BrCall> calls = searchCalls(gigwaGermplasmList);
        log.trace("calls found for {} samples: {}", gigwaGermplasmList.size(), calls.size());
        return calls;
        
    }

    String getStudyDbId(BatchTo batch) {
        log.debug("getStudyDbId with File ID {}", batch.getResultFileId());
        String id = Optional.ofNullable(batch.getResultFileId())
                .map(fk -> csFileObjectService.findByKey(fk))
                .map(f -> getStudy(f.getName()))
                .map(s -> s.getStudyDbId())
                .orElseGet(() -> {
                    log.warn("Cannot find gigwa study for batch ({}) with fileKey {}", batch.getId(), batch.getResultFileId());
                    return null;
                });
        log.debug("studyDbId returned: {}", id);
        return id;
    }

    @Override
    public String createFileName(GenotypeSearchParams params) {
        return (params.hasRequestFilter() ? "request-" + params.getRequestId() : "search-" + params.hashCode()) + ".csv";
    }
}

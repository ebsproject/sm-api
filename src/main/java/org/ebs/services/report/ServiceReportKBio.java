package org.ebs.services.report;

import org.ebs.services.to.custom.ReportIntertekTo;

public interface ServiceReportKBio {

	public byte [] getBytesReportEIBDARTAGIntertekOrderExtraction(ReportIntertekTo to);
}

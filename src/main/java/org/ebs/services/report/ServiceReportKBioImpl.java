/*
Copyright 2013 International Maize and Wheat Improvement Center
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
       http://www.apache.org/licenses/LICENSE-2.0
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package org.ebs.services.report;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.ebs.graphql.resolvers.SampleDetailResolver;
import org.ebs.services.BatchService;
import org.ebs.services.SampleDetailService;
import org.ebs.services.custom.CSUser;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.SampleDetailTo;
import org.ebs.services.to.ServiceProviderTo;
import org.ebs.services.to.custom.ContactByIDTo;
import org.ebs.services.to.custom.ReportIntertekTo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import lombok.RequiredArgsConstructor;



@Service @Transactional(readOnly = true)
@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
public class ServiceReportKBioImpl implements ServiceReportKBio{

	@Value("${sm.report.loadExcelEIBLDGSDARTAGListIntertekOrderExtraction}")
	private int loadExcelEIBLDGSDARTAGListIntertekOrderExtraction;
	@Value("${sm.report.loadExcelIntertekLDGSOrderExtraction}")
	private int loadExcelIntertekLDGSOrderExtraction;
	@Value("${sm.report.loadExcelEIBDARTAGListIntertekOrderExtraction}")
	private int loadExcelEIBDARTAGListIntertekOrderExtraction;
	@Value("${sm.report.loadExcelAgriplex}")
	private int loadExcelAgriplex;
	
	
	private HSSFWorkbook listBook = null;

	private int rowsplate;
	private int colsplate;
	private short cellBlankForegroundColor = 48;
	private short cellKBiocontrolRandom = 52;
	private short cellControlRandom = 10;
	private short cellControlDART = 23;
	private short cellControl = 17;
	private	HSSFCellStyle styleCellNormally;
	private	HSSFCellStyle styleCellAgriplexNormal;
	private HSSFCellStyle styleCellControl;
	private HSSFCellStyle styleCellControlDART; 
	private HSSFCellStyle styleCellControlRandom; 
	private HSSFCellStyle styleCellControlKBIo;
	private HSSFCellStyle styleCellBlank;
	private HSSFCellStyle styleCellNormallyHeader;
	private TreeMap<String, String> mapPlate = new TreeMap<String, String>();
	
	private Map<Integer, String> mapBlank = new HashMap<Integer, String>();
	private int NCIBMiaze = 4577;
	private int NCIBWeath = 4564;
	private int SIZE_PLATE_96 =1;
	
	BatchTo batchTo ;
	List<SampleDetailTo> list;
	private final BatchService batchService;
	private ReportIntertekTo reportIntertekTo;
	private final CSUser cSUser;  
	
	private final SampleDetailService sampleDetailService;
	private final SampleDetailResolver sampleDetailResolver;
	private boolean isCimmyt= true;
	private String organitation = "International Rice Research Institute (IRRI)";
	private String addres = "College, Los Banos, Laguna";
	private String zipCode = "4031";
	private String city = "Los Baños, Laguna";
	private String country = "Philippines";
	private String mainContact = "Annalhea Jarana, Maria Ymber Reveche";
	private String email = "gsl@irri.org";
	private String phone = "+63 49 536 2701";
	private String emailConfirm = "cimmyt-eib-genotyping@cgiar.org; gsl@irri.org";
	private String emailRepor = "cimmyt-eib-genotyping@cgiar.org; gsl@irri.org";
	private List<String> markerNames = new ArrayList<>();
	
	private final static Logger LOG = LoggerFactory.getLogger(ServiceReportKBioImpl.class);
	
	private void loadStyleCells(){
		styleCellNormally = styleCellNormally(listBook, false);
		styleCellControl = getStyleCeldSolidForeground(listBook,cellControl );
		styleCellControlDART = getStyleCeldSolidForeground(listBook,cellControlDART );
		styleCellControlRandom = getStyleCeldSolidForeground(listBook,cellControlRandom );
		styleCellControlKBIo = getStyleCeldSolidForeground(listBook,cellKBiocontrolRandom );
		styleCellBlank = getStyleCeldSolidForeground(listBook,cellBlankForegroundColor );
	}

	private byte[] getArryByte (HSSFWorkbook objWB){
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try{
			objWB.write(baos);
			baos.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
		InputStream is = bais;	
		byte[] b = null;
		try {
			b = this.inputStreamToBytes(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

	private byte[] inputStreamToBytes(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
		byte[] buffer = new byte[1024];
		int len;
		while((len = in.read(buffer)) >= 0)
		out.write(buffer, 0, len);
		in.close();
		out.close();
		return out.toByteArray();
	}

	public byte [] getBytesReportEIBDARTAGIntertekOrderExtraction(ReportIntertekTo to){

		try {
			
		this.mapPlate = new TreeMap<String, String>();
		this.mapBlank = new HashMap<Integer, String>();
		this.batchTo = batchService.findBatch(to.getBatchId()).get();
		this.reportIntertekTo = to;
		LOG.info("EBS batch to:: "+ batchTo + " batchTo >> "+ batchTo.getId());
		ServiceProviderTo serviceProviderTo = batchService.findServiceProvider(batchTo.getId()).get();
		LOG.info("EBS serviceProviderTo to:: "+ serviceProviderTo);
		
		ContactByIDTo contactByIDTo = cSUser.findContactByID(serviceProviderTo.getId());
		LOG.info("contactByIDTo: "+ contactByIDTo.toString());
		if (contactByIDTo != null && contactByIDTo.getParents() != null && contactByIDTo.getParents().length > 0) {
			if (contactByIDTo.getParents()[0].getInstitution() != null && contactByIDTo.getParents()[0].getInstitution().getInstitution()!= null) {
				String commonName = contactByIDTo.getParents()[0].getInstitution().getInstitution().getCommonName().toUpperCase();
				if (commonName.contains("IRRI"))
					isCimmyt = false;
			}
		}
		Set<SampleDetailTo> listSample = batchService.findSampleDetails(to.getBatchId());
		this.list = Lists.newArrayList(listSample);
		Collections.sort(list, new SampleDetailTo()); 
		listBook = null;
		if (batchTo.getReportId() == loadExcelEIBLDGSDARTAGListIntertekOrderExtraction)
			loadExcelEIBLDGSDARTAGListIntertekOrderExtraction(to.getBatchId());
		else if (batchTo.getReportId() == loadExcelIntertekLDGSOrderExtraction )
			loadExcelIntertekLDGSOrderExtraction(to.getBatchId());
		else if (batchTo.getReportId() == loadExcelEIBDARTAGListIntertekOrderExtraction )
			loadExcelEIBDARTAGListIntertekOrderExtraction( to.getBatchId());
		else if (batchTo.getReportId() == loadExcelAgriplex )
			loadExcelAgriplex(to.getBatchId());
		else
			loadExcelAgriplex(to.getBatchId());
		return getArryByte(listBook);
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	 
	 private void loadExcelAgriplex(int batchId){
		 String rutaResumen= "/org/ebs/report/src/AgriPlexGenomicsSamplelist Form.xls";
			InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
			try {
				
				listBook = new HSSFWorkbook(inputStream);
				loadStyleCells();
				HSSFSheet sheetList = listBook.getSheetAt(1);
				loadAgriplexistSampleIDGenotypingService(sheetList, 12, false, batchId);
				HSSFSheet sheetDetails = listBook.getSheetAt(0);
				loadCustomerDetailAgriplexExtraction(sheetDetails);
				loadDartAGListIntertekExtraction(2,1,1, true);
			} catch (IOException e) {
				e.printStackTrace();
			}
	 }
	 
	 private void loadAgriplexistSampleIDGenotypingService(HSSFSheet sheetList, int inidatsheetList, boolean reportType , int idBatch){
			
			String wellkbio;
			int i=0;
			int indexBlank = 1;
			int indexWell = 1;
			styleCellAgriplexNormal = sheetList.getRow(i+inidatsheetList).getCell(2).getCellStyle();
			for (SampleDetailTo sampledet : list ) {
				HSSFRow actrowsheetList;
				//****************    sheetList  **************************
				//
				if (sheetList.getRow(i+inidatsheetList)==null)
					sheetList.createRow(i+inidatsheetList);
				actrowsheetList=sheetList.getRow(i+inidatsheetList);
				if (actrowsheetList.getCell(4)==null) actrowsheetList.createCell(4);
				if(sampledet.getDataId() > 0) {
					//actrowsheetList.getCell(4).setCellValue(sampledet.getPlateName()+"_"+
						//	(getPaddingCeros(sampledet.getSampleNumber())));
					actrowsheetList.getCell(4).setCellValue(sampledet.getSampleCode());
					actrowsheetList.getCell(4).setCellStyle(styleCellAgriplexNormal);
				}
				else {
						getPathEmpty(indexBlank, sampledet.getId());
						actrowsheetList.getCell(4).setCellValue(sampledet.getSampleCode());
						actrowsheetList.getCell(4).setCellStyle(styleCellAgriplexNormal);
						indexBlank++;
				}
				
				if (actrowsheetList.getCell(3)==null) actrowsheetList.createCell(3);
				actrowsheetList.getCell(3).setCellValue(getPlate(sampledet.getPlateName(),batchTo.getName()+"_"+ sampledet.getPlateName()));
				actrowsheetList.getCell(3).setCellStyle(styleCellAgriplexNormal);
				
				if (actrowsheetList.getCell(2)==null) actrowsheetList.createCell(2);
				wellkbio=sampledet.getRow()+sampledet.getColumn();
				if (wellkbio.length()==2)
					wellkbio=wellkbio.substring(0,1)+"0"+wellkbio.substring(1);
				actrowsheetList.getCell(2).setCellValue(wellkbio);
				actrowsheetList.getCell(2).setCellStyle(styleCellAgriplexNormal);
				
				if (actrowsheetList.getCell(1)==null) actrowsheetList.createCell(1);
				actrowsheetList.getCell(1).setCellValue(indexWell);
				actrowsheetList.getCell(1).setCellStyle(styleCellAgriplexNormal);
				indexWell ++;
				if (indexWell >= 97) indexWell = 1; 
				
				if (actrowsheetList.getCell(5)==null) actrowsheetList.createCell(5);
				if (actrowsheetList.getCell(6)==null) actrowsheetList.createCell(6);
				if (actrowsheetList.getCell(7)==null) actrowsheetList.createCell(7);
				actrowsheetList.getCell(5).setCellStyle(styleCellAgriplexNormal);
				actrowsheetList.getCell(6).setCellStyle(styleCellAgriplexNormal);
				actrowsheetList.getCell(7).setCellStyle(styleCellAgriplexNormal);
				
			
				i=i+1;		
			}
		}
	 
		private void loadCustomerDetailAgriplexExtraction(HSSFSheet sheetDetails){
			sheetDetails.getRow(31).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getOrganization():organitation);
			sheetDetails.getRow(32).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getFullName(): mainContact);
			sheetDetails.getRow(33).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail() : email);
			sheetDetails.getRow(34).getCell(2).setCellValue(isCimmyt ? "" : phone);
			
			sheetDetails.getRow(37).getCell(2).setCellValue("96 Wells");
			
			sheetDetails.getRow(40).getCell(2).setCellValue(mapPlate.size());
			sheetDetails.getRow(41).getCell(2).setCellValue(batchTo.getNumMembers());
			
		}
	 
	private void loadExcelIntertekLDGSOrderExtraction(int batchId){
			 String rutaResumen= "/org/ebs/report/src/gsl_form_data.xls";
				InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
				try {
					listBook = new HSSFWorkbook(inputStream);
					HSSFSheet sheetList = listBook.getSheetAt(0);
					loadLDSGListSampleIDGenotypingService(sheetList, 1, false, batchId);
					//HSSFSheet sheetDetails = listBook.getSheetAt(1);
					
					//loadCustomerDetailLDSGIntertekExtraction(sheetDetails);
					//sheetDetails.getRow(1).getCell(2).setCellValue(this.reportIntertekTo.getCrop());
					//HSSFSheet sheetListPlate = listBook.getSheetAt(2);
					//sheetListPlate.getRow(1).getCell(2).setCellValue(this.reportIntertekTo.getCrop());
					//sheetListPlate.getRow(3).getCell(2).setCellValue(this.batchTo.getName());
					//loadPlateID(sheetListPlate, 14);
					loadStyleCells();
					//loadDartAGListIntertekExtraction(4,11, 11);
				} catch (IOException e) {
					e.printStackTrace();
				}
	 }
		
	 private void loadExcelEIBLDGSDARTAGListIntertekOrderExtraction(int batchId){
		 String rutaResumen= "/org/ebs/report/src/EIB_LDSG_SNP_v_2.xls";
			InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
			try {
				listBook = new HSSFWorkbook(inputStream);
				HSSFSheet sheetList = listBook.getSheetAt(3);
				loadDARTAGListSampleIDGenotypingService(sheetList, 17, false, batchId);
				HSSFSheet sheetDetails = listBook.getSheetAt(1);
				
				loadCustomerDetailLDSGIntertekExtraction(sheetDetails);
				sheetDetails.getRow(1).getCell(2).setCellValue(this.reportIntertekTo.getCrop());
				HSSFSheet sheetListPlate = listBook.getSheetAt(2);
				sheetListPlate.getRow(1).getCell(2).setCellValue(this.reportIntertekTo.getCrop());
				sheetListPlate.getRow(3).getCell(2).setCellValue(this.batchTo.getName());
				loadPlateID(sheetListPlate, 14);
				loadStyleCells();
				loadDartAGListIntertekExtraction(4,11, 11, false);
				loadLDSGSNPS(5,11,11, 1);
			} catch (IOException e) {
				e.printStackTrace();
			}
	 }

	private void loadLDSGSNPS(int sheed, int row , int idRowStyle, int col) {
		LOG.info("EBS Before of get assays");
		try {
		markerNames = batchService.findMarkers(batchTo.getId());
		}catch(Exception ex) {
			LOG.info("Warning the system can't retrieve the markers name ");
		}
		
		LOG.info("assays list "+markerNames);
		HSSFSheet sheetgrid = listBook.getSheetAt(sheed);
		HSSFCell cellStyle =sheetgrid.getRow(idRowStyle).getCell(0);
		HSSFCellStyle style = cellStyle.getCellStyle();
		HSSFCell cellStyleSNP =sheetgrid.getRow(idRowStyle).getCell(1);
		HSSFCellStyle styleSNP = cellStyleSNP.getCellStyle();
		int index = 1; 
		int indexRow = 0;
		if (markerNames != null && !markerNames.isEmpty())
		for (String snp : markerNames) {
			
			if (sheetgrid.getRow(row+indexRow).getCell(0) == null)
				sheetgrid.getRow(row+indexRow).createCell(0);
			HSSFCell cell = sheetgrid.getRow(row+indexRow).getCell(0);
			cell.setCellValue(index);
			cell.setCellStyle(style);
			
			if (sheetgrid.getRow(row+indexRow).getCell(1) == null)
				sheetgrid.getRow(row+indexRow).createCell(1);
			
			HSSFCell cellSNP = sheetgrid.getRow(row+indexRow).getCell(1);
			cellSNP.setCellValue(snp);
			cellSNP.setCellStyle(styleSNP);
			
			if (sheetgrid.getRow(row+indexRow).getCell(2) == null)
				sheetgrid.getRow(row+indexRow).createCell(2).setCellStyle(styleSNP);
			if (sheetgrid.getRow(row+indexRow).getCell(3) == null)
				sheetgrid.getRow(row+indexRow).createCell(3).setCellStyle(styleSNP);
			
			index++;
			indexRow++;
			
		}
				
		
		
	} 

	private void loadExcelEIBDARTAGListIntertekOrderExtraction(int batchId){
		String rutaResumen= "/org/ebs/report/src/EIB_DArTag_v.4.xls";
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet sheetList = listBook.getSheetAt(2);
			loadDARTAGListSampleIDGenotypingService(sheetList, 15, false, batchId);
			HSSFSheet sheetDetails = listBook.getSheetAt(0);
			
			loadDartagCustomerDetailLDSGIntertekExtraction(sheetDetails);
			sheetDetails.getRow(1).getCell(2).setCellValue(this.reportIntertekTo.getCrop());
			HSSFSheet sheetListPlate = listBook.getSheetAt(1);
			sheetListPlate.getRow(1).getCell(2).setCellValue(this.reportIntertekTo.getCrop());
			sheetListPlate.getRow(3).getCell(2).setCellValue(this.batchTo.getName());
			loadPlateID(sheetListPlate, 12);
			loadStyleCells();
			loadDartAGListIntertekExtraction(3,11,11, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void loadCustomerDetailLDSGIntertekExtraction(HSSFSheet sheetDetails){
		sheetDetails.getRow(13).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getOrganization() :organitation);
		sheetDetails.getRow(13).getCell(4).setCellValue(isCimmyt ? this.reportIntertekTo.getFullName() : mainContact);
		sheetDetails.getRow(14).getCell(2).setCellValue(isCimmyt ? "" : addres);
		sheetDetails.getRow(14).getCell(4).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail() : email);
		
		sheetDetails.getRow(15).getCell(2).setCellValue(isCimmyt ? "" : zipCode);
		sheetDetails.getRow(15).getCell(4).setCellValue(isCimmyt ? "" : phone);
		sheetDetails.getRow(16).getCell(2).setCellValue(isCimmyt ? "" : city);
		sheetDetails.getRow(17).getCell(2).setCellValue(isCimmyt ? "" : country);
		
		sheetDetails.getRow(16).getCell(4).setCellValue(this.batchTo.getName());
		sheetDetails.getRow(18).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail()+"; "+"cimmyt-eib-genotyping@cgiar.org" : emailConfirm);
		sheetDetails.getRow(20).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail()+";"+"cimmyt-eib-genotyping@cgiar.org; CIMMYT-DMU@cgiar.org" : emailRepor);
		sheetDetails.getRow(25).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getOrganization() : "");
		sheetDetails.getRow(25).getCell(4).setCellValue(isCimmyt ? this.reportIntertekTo.getFullName() : "");
		sheetDetails.getRow(26).getCell(4).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail() : "");
		
	}
	
	private void loadDartagCustomerDetailLDSGIntertekExtraction(HSSFSheet sheetDetails){
		sheetDetails.getRow(13).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getOrganization() :organitation);
		sheetDetails.getRow(13).getCell(4).setCellValue(isCimmyt ? this.reportIntertekTo.getFullName() : mainContact);
		
		sheetDetails.getRow(14).getCell(2).setCellValue(isCimmyt ? "" : addres);
		
		sheetDetails.getRow(15).getCell(2).setCellValue(isCimmyt ? "" : zipCode);
		sheetDetails.getRow(16).getCell(2).setCellValue(isCimmyt ? "" : city);
		sheetDetails.getRow(16).getCell(4).setCellValue(isCimmyt ? "" : phone);
		sheetDetails.getRow(17).getCell(2).setCellValue(isCimmyt ? "" : country);
		
		sheetDetails.getRow(15).getCell(4).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail() : email);
		
		sheetDetails.getRow(17).getCell(4).setCellValue(this.batchTo.getName());
		sheetDetails.getRow(18).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail()+"; "+"cimmyt-eib-genotyping@cgiar.org" : emailConfirm);
		sheetDetails.getRow(20).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail()+";"+"cimmyt-eib-genotyping@cgiar.org; CIMMYT-DMU@cgiar.org" : emailRepor);
		sheetDetails.getRow(25).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getCrop() : "RICE");
		sheetDetails.getRow(26).getCell(4).setCellValue(mapPlate.size());
		sheetDetails.getRow(34).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getOrganization() : "");
		
		sheetDetails.getRow(34).getCell(4).setCellValue(isCimmyt ? this.reportIntertekTo.getFullName(): "");
		sheetDetails.getRow(35).getCell(4).setCellValue(isCimmyt ? this.reportIntertekTo.getEmail() : "");
		
		
		sheetDetails.getRow(46).getCell(2).setCellValue("");
		sheetDetails.getRow(50).getCell(2).setCellValue(isCimmyt ? this.reportIntertekTo.getCrop():"RICE");
		
	}
	
private void loadLDSGListSampleIDGenotypingService(HSSFSheet sheetList, int inidatsheetList, boolean reportType , int idBatch){
		
		String wellkbio;
		int i=0;
		int indexBlank = 1;
		
		
		for (SampleDetailTo sampledet : list ) {
			HSSFRow actrowsheetList;
			//****************    sheetList  **************************
			//
			if (sheetList.getRow(i+inidatsheetList)==null)
				sheetList.createRow(i+inidatsheetList);
			actrowsheetList=sheetList.getRow(i+inidatsheetList);
			if (actrowsheetList.getCell(0)==null) actrowsheetList.createCell(0);
			wellkbio=sampledet.getRow()+sampledet.getColumn();
			if (wellkbio.length()==2)
				wellkbio=wellkbio.substring(0,1)+"0"+wellkbio.substring(1);
			actrowsheetList.getCell(0).setCellValue(wellkbio);
			if (actrowsheetList.getCell(1)==null) actrowsheetList.createCell(1);
			actrowsheetList.getCell(1).setCellValue(getPlate(sampledet.getPlateName(),batchTo.getName()+"_"+ sampledet.getPlateName()));
			if (actrowsheetList.getCell(2)==null) actrowsheetList.createCell(2);
			if(sampledet.getDataId() > 0)
				//actrowsheetList.getCell(2).setCellValue(sampledet.getPlateName()+
					//	"_"+ (getPaddingCeros(sampledet.getSampleNumber())));
				actrowsheetList.getCell(2).setCellValue(sampledet.getSampleCode());
			else {
					getPathEmpty(indexBlank, sampledet.getId());
					actrowsheetList.getCell(2).setCellValue(sampledet.getSampleCode());
					indexBlank++;
			}
		
				if (actrowsheetList.getCell(3)==null) actrowsheetList.createCell(3);
				if (sampledet.getDataId()>0)
					actrowsheetList.getCell(3).setCellValue(sampledet.getDataId());
			
			
				if (actrowsheetList.getCell(5)==null) actrowsheetList.createCell(5);
				if (sampledet.getDataId()>0)
				actrowsheetList.getCell(5).setCellValue(sampledet.getDataId());
			/*
			if(sampledet.getDataId() > 0)
				actrowsheetList.getCell(1).setCellValue(batchTo.getName()+"_"+ sampledet.getPlateName()+
						"_"+ (getPaddingCeros(sampledet.getSampleNumber())));
			else {
				actrowsheetList.getCell(1).setCellValue(getPathEmpty(indexBlank, sampledet.getId()));
				indexBlank++;
			}
			
			if (actrowsheetList.getCell(2)==null) actrowsheetList.createCell(2);
			actrowsheetList.getCell(2).setCellValue(getPlate(batchTo.getName()+"_"+sampledet.getPlateName(),batchTo.getName()+"_"+ sampledet.getPlateName()));
			if (actrowsheetList.getCell(3)==null) actrowsheetList.createCell(3);
			wellkbio=sampledet.getRow()+sampledet.getColumn();
			if (wellkbio.length()==2)
				wellkbio=wellkbio.substring(0,1)+"0"+wellkbio.substring(1);
			actrowsheetList.getCell(3).setCellValue(wellkbio);
			
			if (actrowsheetList.getCell(7)==null) actrowsheetList.createCell(7);
				actrowsheetList.getCell(7).setCellValue(sampledet.getDataId() != 0 ? String.valueOf(sampledet.getDataId()):"");
			if (actrowsheetList.getCell(8)==null) actrowsheetList.createCell(8);
				actrowsheetList.getCell(8).setCellValue("1");*/
			i=i+1;		
		}
	}
	
	private void loadDARTAGListSampleIDGenotypingService(HSSFSheet sheetList, int inidatsheetList, boolean reportType , int idBatch){
		
		String wellkbio;
		int i=0;
		int indexBlank = 1;
		for (SampleDetailTo sampledet : list ) {
			HSSFRow actrowsheetList;
			boolean isValuePresent =false;
			if (sheetList.getRow(i+inidatsheetList)==null)
				sheetList.createRow(i+inidatsheetList);
			actrowsheetList=sheetList.getRow(i+inidatsheetList);
			if (actrowsheetList.getCell(1)==null) actrowsheetList.createCell(1);
			if(sampledet.getDataId() > 0)
				/*actrowsheetList.getCell(1).setCellValue(sampledet.getPlateName()+
						"_"+ (getPaddingCeros(sampledet.getSampleNumber())));*/
			actrowsheetList.getCell(1).setCellValue(sampledet.getSampleCode());
			else {	
				if (sampledet.isVendorControl())
					isValuePresent = true;
						
				if (!isValuePresent)
					{	
						getPathEmpty(indexBlank, sampledet.getId());
						actrowsheetList.getCell(1).setCellValue(sampledet.getSampleCode());
						indexBlank++;
					}
				
			}
			
			if (actrowsheetList.getCell(2)==null) actrowsheetList.createCell(2);
			actrowsheetList.getCell(2).setCellValue(getPlate(sampledet.getPlateName(),batchTo.getName()+"_"+ sampledet.getPlateName()));
			if (actrowsheetList.getCell(3)==null) actrowsheetList.createCell(3);
			wellkbio=sampledet.getRow()+sampledet.getColumn();
			if (wellkbio.length()==2)
				wellkbio=wellkbio.substring(0,1)+"0"+wellkbio.substring(1);
			actrowsheetList.getCell(3).setCellValue(wellkbio);
			
			if (actrowsheetList.getCell(8)==null) actrowsheetList.createCell(8);
				/*if(sampledet.getEntityId()> 0 && sampledet.getEntityId() == 3 ) {
					PlotTo toPlot = sampleDetailResolver.getPlot(sampledet);
						actrowsheetList.getCell(8).setCellValue(toPlot!= null ? toPlot.getEntry().getSeed().getSeedCode(): "");
				}
				else*/
				actrowsheetList.getCell(8).setCellValue(sampledet.getDataId() != 0 ? String.valueOf(sampledet.getDataId()):"");
		
			/*if (actrowsheetList.getCell(8)==null) actrowsheetList.createCell(8);
			if (!isValuePresent)
				actrowsheetList.getCell(8).setCellValue("1");*/
			i=i+1;		
		}
	}

	public static String getPaddingCeros(Integer value){
		final int Totalsize = 6;
		if (value == null || value.intValue() == 0)
			return "";
		int size = value.toString().length();
		StringBuffer strBuffer = new StringBuffer();
		for (int index = size; index < Totalsize; index++){
			strBuffer.append("0");
		}
		strBuffer.append(value.intValue());
		return strBuffer.toString();
	}
	
	private void loadDartAGListIntertekExtraction(int sheed, int row , int idRowStyle, boolean isAgriplex){
	
		int size = 1;
		HSSFSheet sheetgrid = listBook.getSheetAt(sheed);
		HSSFCell cellStyle =sheetgrid.getRow(idRowStyle).getCell(0);
		HSSFCellStyle style = cellStyle.getCellStyle();
		HSSFCell cellStyleIntertek =sheetgrid.getRow(19).getCell(11);
		HSSFCellStyle styleIntertek = cellStyleIntertek.getCellStyle();
		if (size == SIZE_PLATE_96){
			rowsplate=8;
			colsplate=12;
		}
		else{
			rowsplate=16;
			colsplate=24;
		}
		
		LinkedHashMap<String, Map<String, SampleDetailTo>> mapPlates = new LinkedHashMap<String, Map<String,SampleDetailTo>>();
		for(SampleDetailTo detail : list){ 
			if (mapPlates.containsKey(detail.getPlateName())){
				Map<String,SampleDetailTo> map = mapPlates.get(detail.getPlateName());
					String letter = detail.getRow();
					String number = ""+detail.getColumn();
					map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
			}else{
				Map<String, SampleDetailTo> map = new HashMap<String, SampleDetailTo>();
				String letter = detail.getRow();
				String number = ""+detail.getColumn();
				map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
				mapPlates.put(detail.getPlateName(), map);
			}
		}

		
		Iterator<Entry<String, Map<String, SampleDetailTo>>> it = mapPlates.entrySet().iterator();
		 
		int column = 0 ;
		while (it.hasNext()){
			Map.Entry<String, Map<String,SampleDetailTo>> entry = (Entry<String, Map<String, SampleDetailTo>>) it.next();
			if (entry.getKey() != null){
				Map<String,SampleDetailTo> map = entry.getValue();
				validateRow(sheetgrid, row);
				HSSFCell cellPlateName =validateCell(sheetgrid, row, 0);
				cellPlateName.setCellValue("Plate");
				cellPlateName.setCellStyle(style);
				HSSFCell cellPlate =validateCell(sheetgrid, row, 1);
				cellPlate.setCellValue(entry.getKey());
				row = row + 1;
				drawingFormatPlate(style, sheetgrid, row);
					for (int indexRow = 1; indexRow <= rowsplate ; indexRow++){
						for (int indexCol = 1 ; indexCol<= colsplate; indexCol++){
							
							SampleDetailTo detail = map.get(indexRow+"|"+indexCol);
							validateRow(sheetgrid, row+indexRow);
							HSSFCellStyle styleCell = null;
						
							if (sheetgrid.getRow(row+indexRow).getCell(column+indexCol) == null)
								sheetgrid.getRow(row+indexRow).createCell(column+indexCol);
								HSSFCell cell = sheetgrid.getRow(row+indexRow).getCell(column+indexCol);
							if(detail == null || detail.getDataId() == 0 ){
								styleCell = styleCellBlank;
								//cell.setCellValue(mapBlank.get(detail!= null ?detail.getId():0));
								cell.setCellValue(detail.getSampleCode());
								cell.setCellStyle(styleCell);
								
							}
							else{
								styleCell = styleCellNormally;
								cell.setCellValue( detail.getSampleCode());
							}
							if(detail != null) {
									String plateLoc = detail.getRow()+detail.getColumn();
									if (plateLoc.equals("H11") || plateLoc.equals("H12")) {
										if (detail.getDataId() > 0)
											cell.setCellValue(detail.getSampleCode());
											/*cell.setCellValue(detail.getPlateName()+"_"+
													getPaddingCeros(detail.getSampleNumber()));*/
									}
							}
						}
					}
					row = row +rowsplate;
					row = row+2;
				}
				
			}
	}
	
	private HSSFCell validateCell (HSSFSheet sheetgrid, int indexRow, int indexCell){
		if (sheetgrid.getRow(indexRow).getCell(indexCell) == null){
			sheetgrid.getRow(indexRow).createCell(indexCell);
			return sheetgrid.getRow(indexRow).getCell(indexCell);
		}else {
			return sheetgrid.getRow(indexRow).getCell(indexCell);
		}
	}
	/*	
	public byte [] getBytesReportEIBLDSGIntertekOrderExtraction (	
			List<ShipmentDetail> setShipmentDetail, PropertyHelper property,
			UserBean userBean, Shipment shipment) {
		this.property =  property;
		this.setShipmentDetail = setShipmentDetail;
		this.userBean = userBean;
		this.shipment = shipment;
		this.mapPlate = new TreeMap<String, String>();
		this.mapBlank = new HashMap<Integer, String>();
		listBook = null;
		loadExcelEIBLDSGListIntertekOrderExtraction();
		return getArryByte(listBook);
	}


	private void loadExcelEIBLDSGListIntertekOrderExtraction(){
		String rutaResumen= property.getKey(Constants.REPORT_PATH_EIB_LDSG_INTERTEK_ORDER_EXTRA, Bundle.conf);
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet sheetList = listBook.getSheetAt(3);
			loadLDSGListSampleIDGenotypingService(sheetList, 17, false);
			HSSFSheet sheetDetails = listBook.getSheetAt(1);
			loadCustomerDetailLDSGIntertekExtraction(sheetDetails);
			sheetDetails.getRow(1).getCell(2).setCellValue(StrUtils.getCrop(userBean.getTypeCorp(), property));
			HSSFSheet sheetListPlate = listBook.getSheetAt(2);
			sheetListPlate.getRow(1).getCell(2).setCellValue(StrUtils.getCrop(userBean.getTypeCorp(), property));
			sheetListPlate.getRow(3).getCell(2).setCellValue(shipment.getComment());
			loadPlateID(sheetListPlate, 14);
			loadStyleCells();
			loadListIntertekExtraction(4,11);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
	
	private void loadLDSGListSampleIDGenotypingService(HSSFSheet sheetList, int inidatsheetList, boolean reportType){
		
		String wellkbio;
		int i=0;
		int indexBlank = 1;
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
				SampleDetail sampledet = shipmentDetail.getStSampleDetail();
			HSSFRow actrowsheetList;
			//****************    sheetList  **************************
			//
			if (sheetList.getRow(i+inidatsheetList)==null)
				sheetList.createRow(i+inidatsheetList);
			actrowsheetList=sheetList.getRow(i+inidatsheetList);
			if (actrowsheetList.getCell(1)==null) actrowsheetList.createCell(1);
			if(sampledet.getSamplegid()!=null)
			actrowsheetList.getCell(1).setCellValue(sampledet.getLabstudyid().getPrefix()+
					(sampledet.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(sampledet.getSamplegid()) :
						String.valueOf(sampledet.getSamplegid()))+ "_" + sampledet.getStudysampleid());
			if (actrowsheetList.getCell(2)==null) actrowsheetList.createCell(2);
			actrowsheetList.getCell(2).setCellValue(getPlate( sampledet.getPlatename(), sampledet.getLabstudyid().getPrefix()));
			if (actrowsheetList.getCell(3)==null) actrowsheetList.createCell(3);
			wellkbio=sampledet.getPlateloc();
			if (wellkbio.length()==2)
				wellkbio=wellkbio.substring(0,1)+"0"+wellkbio.substring(1);
			actrowsheetList.getCell(3).setCellValue(wellkbio);
			
			if (actrowsheetList.getCell(8)==null) actrowsheetList.createCell(8);
				actrowsheetList.getCell(8).setCellValue(sampledet.getBreedergid() != null ? String.valueOf(sampledet.getBreedergid()):"");
			if (actrowsheetList.getCell(9)==null) actrowsheetList.createCell(9);
				actrowsheetList.getCell(9).setCellValue(sampledet.getNplanta() != null ? String.valueOf(sampledet.getNplanta()):"");
				
			if(sampledet.getControltype()!=null && 
					(sampledet.getControltype().equals("B") 
					|| sampledet.getControltype().equals("R")
					|| sampledet.getControltype().equals("C")
					|| sampledet.getControltype().equals("K")
					|| sampledet.getControltype().equals("D")
					)
				){
				switch (sampledet.getPlateloc()){
				case "H11":
				case "H12":
					break;
				default:{	
					actrowsheetList.getCell(1).setCellValue(getPathEmpty(indexBlank, sampledet.getStudysampleid()));
					indexBlank++;
					}
				}
			}	
				
			i=i+1;
		}
	}

	public byte [] getBytesReportIntertekOrderExtraction (	
			List<ShipmentDetail> setShipmentDetail, PropertyHelper property,
			UserBean userBean, Shipment shipment) {
		this.property =  property;
		this.setShipmentDetail = setShipmentDetail;
		this.userBean = userBean;
		this.shipment = shipment;
		this.mapPlate = new TreeMap<String, String>();
		this.mapBlank = new HashMap<Integer, String>();
		listBook = null;
		loadExcelListIntertekOrderExtraction();
		return getArryByte(listBook);
	}


	private void loadExcelListIntertekGenotyping(){
		String rutaResumen= property.getKey(Constants.REPORT_PATH_GENOTYPING_KSU_TEMPLATE, Bundle.conf);
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet sheetDetails = listBook.getSheetAt(1);
			loadCustomerDetailIntertek(sheetDetails);
			HSSFSheet sheetList = listBook.getSheetAt(5);
			loadListSampleIDGenotypingService(sheetList, 17, true);
			loadPlateID(listBook.getSheetAt(4), 14);
			loadStyleCells();
			loadListIntertekExtraction(6,11);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}


	private void loadExcelListIntertekOrderExtraction(){
		String rutaResumen= property.getKey(Constants.REPORT_PATH_GENOTYPING_INTERTEK_ORDER_EXTRA, Bundle.conf);
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet sheetDetails = listBook.getSheetAt(1);
			loadCustomerDetailIntertekExtraction(sheetDetails);
			HSSFSheet sheetList = listBook.getSheetAt(4);
			sheetDetails.getRow(1).getCell(2).setCellValue(StrUtils.getCrop(userBean.getTypeCorp(), property));
			loadListSampleIDGenotypingService(sheetList, 5, false);
			HSSFSheet sheetListPlate = listBook.getSheetAt(3);
			sheetListPlate.getRow(1).getCell(2).setCellValue(StrUtils.getCrop(userBean.getTypeCorp(), property));
			sheetListPlate.getRow(3).getCell(2).setCellValue(shipment.getComment());
			loadPlateID(sheetListPlate, 12);
			loadStyleCells();
			loadListIntertekExtraction(5,6);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
	
	public byte [] getBytesReportGnotypingServices (	
			List<ShipmentDetail> setShipmentDetail, PropertyHelper property,
			UserBean userBean, Shipment shipment) {
		this.property =  property;
		this.setShipmentDetail = setShipmentDetail;
		this.userBean = userBean;
		this.shipment = shipment;
		this.mapPlate = new TreeMap<String, String>();
		this.mapBlank = new HashMap<Integer, String>();
		listBook = null;
		loadExcelListGenotypingService();
		return getArryByte(listBook);
	}
 
	public byte [] getBytesReportLGCServices (	
			List<ShipmentDetail> setShipmentDetail, PropertyHelper property,
			UserBean userBean, Shipment shipment) {
		this.property =  property;
		this.setShipmentDetail = setShipmentDetail;
		this.userBean = userBean;
		this.shipment = shipment;
		this.mapPlate = new TreeMap<String, String>();
		this.mapBlank = new HashMap<Integer, String>();
		listBook = null;
		loadExcelListGenotypingLGCService();
		return getArryByte(listBook);
	}
	
	public byte [] getBytesReportGDFServices (	
			List<ShipmentDetail> setShipmentDetail, PropertyHelper property,
			UserBean userBean, Shipment shipment) {
		this.property =  property;
		this.setShipmentDetail = setShipmentDetail;
		this.userBean = userBean;
		this.shipment = shipment;
		this.mapPlate = new TreeMap<String, String>();
		this.mapBlank = new HashMap<Integer, String>();
		listBook = null;
		loadExcelListGenotypingGDFService();
		return getArryByte(listBook);
	}

	private void loadExcelListGenotypingGDFService(){
		String rutaResumen= property.getKey(Constants.REPORT_PATH_LGC_CORNELL_TEMPLATE, Bundle.conf);
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet sheetDetails = listBook.getSheetAt(0);
			//loadCustomerDetailSheetLGC(sheetDetails);
			loadStyleCells();
			loadListGenotypingGDFSewrvices(sheetDetails);
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	private void loadListGenotypingGDFSewrvices(HSSFSheet sheetList){
		int inidatsheetList=2;
		
		int i=0;
		int indexBlank = 1;
		boolean isMaize = false;
		if (userBean.getOrganism().getOrganismid().intValue() == 1) {
			isMaize = true;
		}
			
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
				SampleDetail sampledet = shipmentDetail.getStSampleDetail();
			HSSFRow actrowsheetList;
			//****************    sheetList  **************************
			//
			if (sheetList.getRow(i+inidatsheetList)==null)
				sheetList.createRow(i+inidatsheetList);
			actrowsheetList=sheetList.getRow(i+inidatsheetList);
			//column sample id
			if (actrowsheetList.getCell(0)==null) actrowsheetList.createCell(0);
			if(sampledet.getSamplegid()!=null)
			actrowsheetList.getCell(0).setCellValue(sampledet.getLabstudyid().getPrefix()+
					(sampledet.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(sampledet.getSamplegid()) :
						//String.valueOf(sampledet.getSamplegid()))+ "-" + sampledet.getStudysampleid());
						String.valueOf(sampledet.getSamplegid())));
			else {
				actrowsheetList.getCell(0).setCellValue("BLANK");
				indexBlank++;
			}
			
			if (sampledet.getSamplegid()!=null)
			if (actrowsheetList.getCell(1)==null) { 
				actrowsheetList.createCell(1);
					if (isMaize) 
						actrowsheetList.getCell(1).setCellValue(NCIBMiaze);
					else 
						actrowsheetList.getCell(1).setCellValue(NCIBWeath);
					}

			// column plate name
			if (actrowsheetList.getCell(4)==null) actrowsheetList.createCell(4);
			actrowsheetList.getCell(4).setCellValue(getPlate( sampledet.getPlatename(), sampledet.getLabstudyid().getPrefix()));

			if (actrowsheetList.getCell(5)==null) actrowsheetList.createCell(5);
			actrowsheetList.getCell(5).setCellValue(sampledet.getPlateloc());

			i=i+1;
		}
	}
	
	private void loadExcelListGenotypingLGCService(){
		String rutaResumen= property.getKey(Constants.REPORT_PATH_GENOTYPING_LGC_TEMPLATE, Bundle.conf);
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet sheetDetails = listBook.getSheetAt(0);
			loadCustomerDetailSheetLGC(sheetDetails);
			loadStyleCells();
			loadListGenotypingLGCSewrvices();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}


	private void loadCustomerDetailSheetLGC(HSSFSheet sheetDetails){
		sheetDetails.getRow(2).getCell(1).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(3).getCell(1).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(4).getCell(1).setCellValue(userBean.getResearcherEMail()+";"+Constants.EMAIL_ACCOUNT_RECEIVER_WHEAT);
		sheetDetails.getRow(8).getCell(1).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
	}

	private void loadListGenotypingLGCSewrvices(){
		int size = setShipmentDetail.iterator().next().getStSampleDetail().getLabstudyid().getPlatesize();
		HSSFSheet sheetgrid = listBook.getSheetAt(1);
		HSSFCell cellStyle =sheetgrid.getRow(2).getCell(0);
		HSSFCellStyle style = cellStyle.getCellStyle();
		
		
		
		if (size == SIZE_PLATE_96){
			rowsplate=8;
			colsplate=12;
		}
		else{
			rowsplate=16;
			colsplate=24;
		}
		
		LinkedHashMap<String, Map<String, SampleDetail>> mapPlates = new LinkedHashMap<String, Map<String,SampleDetail>>();
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
			
			SampleDetail detail = shipmentDetail.getStSampleDetail();
			if (mapPlates.containsKey(detail.getPlatename())){
				Map<String,SampleDetail> map = mapPlates.get(detail.getPlatename());
					String letter = detail.getPlateloc().substring(0, 1);
					String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
					map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
			}else{
				Map<String, SampleDetail> map = new HashMap<String, SampleDetail>();
				String letter = detail.getPlateloc().substring(0, 1);
				String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
				map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
				mapPlates.put(detail.getPlatename(), map);
			}
		}
	
		Iterator<Entry<String, Map<String, SampleDetail>>> it = mapPlates.entrySet().iterator();
		int row = 1;
		int column = 0 ;
		int indexBlank = 1;
		while (it.hasNext()){
			Map.Entry<String, Map<String,SampleDetail>> entry = (Entry<String, Map<String, SampleDetail>>) it.next();
			if (entry.getKey() != null){
				Map<String,SampleDetail> map = entry.getValue();
				validateRow(sheetgrid, row);
				HSSFCell cellPlateName =validateCell(sheetgrid, row, 0);
				cellPlateName.setCellValue("Plate");
				cellPlateName.setCellStyle(style);
				HSSFCell cellPlate =validateCell(sheetgrid, row, 1);
				cellPlate.setCellValue(entry.getKey());
				row = row + 1;
				drawingFormatPlate(style, sheetgrid, row);
					for (int indexRow = 1; indexRow <= rowsplate ; indexRow++){
						for (int indexCol = 1 ; indexCol<= colsplate; indexCol++){
							
							SampleDetail detail = map.get(indexRow+"|"+indexCol);
							validateRow(sheetgrid, row+indexRow);
							HSSFCellStyle styleCell = null;
							
							if (detail.getControltype() != null && !detail.getControltype().equals("")) {
								styleCell = validateStatusSample(detail.getControltype());
								
							}
							else {
								styleCell = styleCellNormally;
								}
							if (sheetgrid.getRow(row+indexRow).getCell(column+indexCol) == null)
									sheetgrid.getRow(row+indexRow).createCell(column+indexCol);
							HSSFCell cell = sheetgrid.getRow(row+indexRow).getCell(column+indexCol);
							if (  !detail.getPlateloc().equals("H12"))
								if(detail.getControltype() != null && !detail.getControltype().equals("")){
									cell.setCellValue(getPathEmpty(indexBlank, detail.getStudysampleid()));
									cell.setCellStyle(styleCellBlank);
									indexBlank++;
								} else{
									styleCell = styleCellNormally;
									cell.setCellValue(detail.getLabstudyid().getPrefix()+
											(detail.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(detail.getSamplegid()):
												String.valueOf(detail.getSamplegid())));
								}
							
							if (detail.getPlateloc().equals("H12")) {
								cell.setCellStyle(styleCellControlDART);
								cell.setCellValue("");
							}
						}
					}
					row = row +rowsplate;
					row = row+2;
				}
				
			}
	}
	
	public byte [] getBytesReportGnotypingServicesKSU (	
			List<ShipmentDetail> setShipmentDetail, PropertyHelper property,
			UserBean userBean, Shipment shipment) {
		this.property =  property;
		this.setShipmentDetail = setShipmentDetail;
		this.userBean = userBean;
		this.shipment = shipment;
		this.mapPlate = new TreeMap<String, String>();
		this.mapBlank = new HashMap<Integer, String>();
		listBook = null;
		loadExcelListIntertekGenotyping();
		//loadExcelListGenotypingServiceKSU();
		return getArryByte(listBook);
		
		
	}

	private void loadExcelListGenotypingServiceKSU(){
		String rutaResumen= property.getKey(Constants.REPORT_PATH_GENOTYPING_KSU_TEMPLATE, Bundle.conf);
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet input = listBook.getSheetAt(1);

			loadStyleCells();
			loadListSampleIDGenotypingServiceKSU(input);
			loadListGenotypingKSUSewrvices();
			
			
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	
	private void loadListGenotypingKSUSewrvices(){
		int size = setShipmentDetail.iterator().next().getStSampleDetail().getLabstudyid().getPlatesize();
		HSSFSheet sheetgrid = listBook.getSheetAt(0);
		HSSFCell cellStyle =sheetgrid.getRow(3).getCell(1);
		HSSFCellStyle style = cellStyle.getCellStyle();
		
		
		
		if (size == SIZE_PLATE_96){
			rowsplate=8;
			colsplate=12;
		}
		else{
			rowsplate=16;
			colsplate=24;
		}
		
		LinkedHashMap<String, Map<String, SampleDetail>> mapPlates = new LinkedHashMap<String, Map<String,SampleDetail>>();
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
			
			SampleDetail detail = shipmentDetail.getStSampleDetail();
			if (mapPlates.containsKey(detail.getPlatename())){
				Map<String,SampleDetail> map = mapPlates.get(detail.getPlatename());
					String letter = detail.getPlateloc().substring(0, 1);
					String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
					map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
			}else{
				Map<String, SampleDetail> map = new HashMap<String, SampleDetail>();
				String letter = detail.getPlateloc().substring(0, 1);
				String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
				map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
				mapPlates.put(detail.getPlatename(), map);
			}
		}
		
		
		Iterator<Entry<String, Map<String, SampleDetail>>> it = mapPlates.entrySet().iterator();
		int row = 2;
		int column = 1 ;
		int indexPlate = 1;
		int indexSample = 1;
		while (it.hasNext()){
			Map.Entry<String, Map<String,SampleDetail>> entry = (Entry<String, Map<String, SampleDetail>>) it.next();
			if (entry.getKey() != null){
				Map<String,SampleDetail> map = entry.getValue();
				validateRow(sheetgrid, row);
				
				int rowIndexPlate = row ;
				validateRow(sheetgrid, rowIndexPlate);
				HSSFCell cellPlateIndex =validateCell(sheetgrid, rowIndexPlate, 0);
				cellPlateIndex.setCellValue(indexPlate);
				
				
				int rowIndexDNAPlate = row + 1;
				validateRow(sheetgrid, rowIndexDNAPlate);
				HSSFCell cellDNAPlateIndex =validateCell(sheetgrid, rowIndexDNAPlate, 0);
				
				cellDNAPlateIndex.setCellValue("DNA"+StrUtils.getDateFormatSimple(setShipmentDetail.get(0).getStShipmentSet().getDatCreated())+"P0"+indexPlate);
				indexPlate++;
				
				int rowPlateName = row + 2;
				validateRow(sheetgrid, rowPlateName);
				HSSFCell cellPlate =validateCell(sheetgrid, rowPlateName, 0);
				
				cellPlate.setCellValue(entry.getKey());
				row = row + 1;
				drawingFormatPlateKSU(style, sheetgrid, row);
					for (int indexRow = 1; indexRow <= rowsplate ; indexRow++){
						for (int indexCol = 1 ; indexCol<= colsplate; indexCol++){
							
							SampleDetail detail = map.get(indexRow+"|"+indexCol);
							validateRow(sheetgrid, row+indexRow);
							HSSFCellStyle styleCell = null;
							
							if (sheetgrid.getRow(row+indexRow).getCell(column+indexCol) == null)
								sheetgrid.getRow(row+indexRow).createCell(column+indexCol);
								HSSFCell cell = sheetgrid.getRow(row+indexRow).getCell(column+indexCol);
						
								styleCell = styleCellNormally;
								if (detail.getSamplegid() != null && detail.getSamplegid().intValue() > 0) {
									cell.setCellValue(detail.getEntryNo());
									indexSample ++;
								}else 
									cell.setCellValue("B");
							}
						}
					row = row +rowsplate;
					row = row+2;
				
				
			}
		}
	}
	
	private void loadListSampleIDGenotypingServiceKSU(HSSFSheet sheetList){
		int inidatsheetList=1;
		
		int i=0;
		int indexBlank = 1;
		int indexplate= 1;
		String plateName = "";
		String wellkbio;
		int indeWell =1;
	
		HSSFCell cellStyle =sheetList.getRow(2).getCell(0);
		HSSFCellStyle style = cellStyle.getCellStyle();
		
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
				SampleDetail sampledet = shipmentDetail.getStSampleDetail();
			HSSFRow actrowsheetList;
			//****************    sheetList  **************************
			//
			if (sheetList.getRow(i+inidatsheetList)==null)
				sheetList.createRow(i+inidatsheetList);
			actrowsheetList=sheetList.getRow(i+inidatsheetList);
			//column sample id
			if (actrowsheetList.getCell(4)==null) actrowsheetList.createCell(4);
			if(sampledet.getSamplegid()!=null)
			actrowsheetList.getCell(4).setCellValue(sampledet.getLabstudyid().getPrefix()+
					(sampledet.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(sampledet.getSamplegid()) :
						//String.valueOf(sampledet.getSamplegid()))+ "-" + sampledet.getStudysampleid());
						String.valueOf(sampledet.getSamplegid())));
			else {
				actrowsheetList.getCell(4).setCellValue((getPathEmpty(indexBlank, sampledet.getSamplegid())));
				indexBlank++;
			}
			

			
			// column date
			if (actrowsheetList.getCell(0)==null) actrowsheetList.createCell(0);{
			actrowsheetList.getCell(0).setCellValue(StrUtils.getDateFormatSimpleKSU(shipmentDetail.getStShipmentSet().getDatCreated()));
			actrowsheetList.getCell(0).setCellStyle(style);
			}
			
			if (actrowsheetList.getCell(1)==null) actrowsheetList.createCell(1);
				if (plateName.equals("")) {
					plateName = sampledet.getPlatename();
					actrowsheetList.getCell(1).setCellValue(indexplate);
				} else
					if (!plateName.equals(sampledet.getPlatename())) {
						plateName = sampledet.getPlatename();
						actrowsheetList.getCell(1).setCellValue(++indexplate);
					}else
						actrowsheetList.getCell(1).setCellValue(indexplate);
				
				actrowsheetList.getCell(1).setCellStyle(style);

			if (actrowsheetList.getCell(2)==null) actrowsheetList.createCell(2);
			actrowsheetList.getCell(2).setCellValue("DNA"+StrUtils.getDateFormatSimple(shipmentDetail.getStShipmentSet().getDatCreated())+"P0"+indexplate);
			
			actrowsheetList.getCell(2).setCellStyle(style);
			
			if (actrowsheetList.getCell(5)==null) actrowsheetList.createCell(5);
			wellkbio=sampledet.getPlateloc();
			if (wellkbio.length()==2)
				wellkbio=wellkbio.substring(0,1)+"0"+wellkbio.substring(1);
			actrowsheetList.getCell(5).setCellValue(wellkbio);
			
			actrowsheetList.getCell(5).setCellStyle(style);
			
			if (actrowsheetList.getCell(6)==null) actrowsheetList.createCell(6);
			wellkbio=sampledet.getPlateloc();
			if (wellkbio.length()==2) {
				wellkbio="0"+wellkbio.substring(1)+wellkbio.substring(0,1);
			actrowsheetList.getCell(6).setCellValue(wellkbio);
			}else {
				wellkbio=wellkbio.substring(1,3)+wellkbio.substring(0,1);
				actrowsheetList.getCell(6).setCellValue(wellkbio);
			}
			actrowsheetList.getCell(6).setCellStyle(style);
			
			// column plate name
			if (actrowsheetList.getCell(3)==null) actrowsheetList.createCell(3);
			actrowsheetList.getCell(3).setCellValue(getPlate( sampledet.getPlatename(), sampledet.getLabstudyid().getPrefix()));
			
			//DNA  creating shipment
			if (actrowsheetList.getCell(7)==null) actrowsheetList.createCell(7);
			actrowsheetList.getCell(7).setCellValue(StrUtils.getDateFormatSimple(shipmentDetail.getStShipmentSet().getDatCreated()));
			
			//project 
			if (actrowsheetList.getCell(8)==null) actrowsheetList.createCell(8);
			actrowsheetList.getCell(8).setCellValue(shipmentDetail.getStShipmentSet().getComments());
			
			// Entry number
			if (actrowsheetList.getCell(9)==null) actrowsheetList.createCell(9);
			if (sampledet.getEntryNo() != null && sampledet.getEntryNo().intValue() > 0) {
				sampledet.setEntryNo(indeWell);
				actrowsheetList.getCell(9).setCellValue(indeWell);
				indeWell++;
			}
			else {
			actrowsheetList.getCell(9).setCellValue("B");
			
			}
			
			
			// Tissue id
			if (actrowsheetList.getCell(10)==null) actrowsheetList.createCell(10);
				if (sampledet.getEntryNo()== null)
					actrowsheetList.getCell(10).setCellValue("B");
				else 
					actrowsheetList.getCell(10).setCellValue(sampledet.getEntryNo());

				// sample name
				if (actrowsheetList.getCell(11)==null) actrowsheetList.createCell(11);
					if (sampledet.getSamplegid() != null )
						actrowsheetList.getCell(11).setCellValue(sampledet.getBreedergid());
					else 
						actrowsheetList.getCell(11).setCellValue("B");	
				// external id
				if (actrowsheetList.getCell(12)==null) actrowsheetList.createCell(12);
					if (sampledet.getSamplegid() == null )
						actrowsheetList.getCell(12).setCellValue("B");
					else 
						actrowsheetList.getCell(12).setCellValue(sampledet.getStudysampleid());
						
				// tissue type
				if (actrowsheetList.getCell(13)==null) actrowsheetList.createCell(13);
					if (sampledet.getSamplegid() != null)
						actrowsheetList.getCell(13).setCellValue(sampledet.getLabstudyid().getTissue().getTissueName());
				
					//dna person
				if (actrowsheetList.getCell(15)==null) actrowsheetList.createCell(15);
				actrowsheetList.getCell(15).setCellValue(sampledet.getLabstudyid().getInvestigatorid().getInvest_name());
				// species
				if (actrowsheetList.getCell(16)==null) actrowsheetList.createCell(16);
				actrowsheetList.getCell(16).setCellValue(sampledet.getLabstudyid().getOrganismid().getOrganismname());

			
			i=i+1;
		}
	}
	

	private void loadExcelListGenotypingService(){
		String rutaResumen= property.getKey(Constants.REPORT_PATH_GENOTYPING_SERVICES_96, Bundle.conf);
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet sheetDetails = listBook.getSheetAt(1);
			loadCustomerDetailSheetGenotyping(sheetDetails);
			HSSFSheet sheetList = listBook.getSheetAt(4);
			loadListSampleIDGenotypingService(sheetList,17, true);
			loadPlateID(listBook.getSheetAt(3),12);
			loadStyleCells();
			loadListGenotypingSewrvices();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

	private void loadListIntertekExtraction(int sheed, int row){
		int size = setShipmentDetail.iterator().next().getStSampleDetail().getLabstudyid().getPlatesize();
		HSSFSheet sheetgrid = listBook.getSheetAt(sheed);
		HSSFCell cellStyle =sheetgrid.getRow(12).getCell(0);
		HSSFCellStyle style = cellStyle.getCellStyle();
		HSSFCell cellStyleIntertek =sheetgrid.getRow(19).getCell(11);
		HSSFCellStyle styleIntertek = cellStyleIntertek.getCellStyle();
		if (size == SIZE_PLATE_96){
			rowsplate=8;
			colsplate=12;
		}
		else{
			rowsplate=16;
			colsplate=24;
		}
		
		LinkedHashMap<String, Map<String, SampleDetail>> mapPlates = new LinkedHashMap<String, Map<String,SampleDetail>>();
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
			
			SampleDetail detail = shipmentDetail.getStSampleDetail();
			if (mapPlates.containsKey(detail.getPlatename())){
				Map<String,SampleDetail> map = mapPlates.get(detail.getPlatename());
					String letter = detail.getPlateloc().substring(0, 1);
					String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
					map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
			}else{
				Map<String, SampleDetail> map = new HashMap<String, SampleDetail>();
				String letter = detail.getPlateloc().substring(0, 1);
				String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
				map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
				mapPlates.put(detail.getPlatename(), map);
			}
		}

		
		Iterator<Entry<String, Map<String, SampleDetail>>> it = mapPlates.entrySet().iterator();
		 
		int column = 0 ;
		while (it.hasNext()){
			Map.Entry<String, Map<String,SampleDetail>> entry = (Entry<String, Map<String, SampleDetail>>) it.next();
			if (entry.getKey() != null){
				Map<String,SampleDetail> map = entry.getValue();
				validateRow(sheetgrid, row);
				HSSFCell cellPlateName =validateCell(sheetgrid, row, 0);
				cellPlateName.setCellValue("Plate");
				cellPlateName.setCellStyle(style);
				HSSFCell cellPlate =validateCell(sheetgrid, row, 1);
				cellPlate.setCellValue(entry.getKey());
				row = row + 1;
				drawingFormatPlate(style, sheetgrid, row);
					for (int indexRow = 1; indexRow <= rowsplate ; indexRow++){
						for (int indexCol = 1 ; indexCol<= colsplate; indexCol++){
							
							SampleDetail detail = map.get(indexRow+"|"+indexCol);
							validateRow(sheetgrid, row+indexRow);
							HSSFCellStyle styleCell = null;
							if (detail.getControltype() != null && !detail.getControltype().equals("")){
								if (!detail.getPlateloc().equals("H11") || !detail.getPlateloc().equals("H12"))
								styleCell = validateStatusSample(detail.getControltype());
							}
							else {
								if (!detail.getPlateloc().equals("H11") || !detail.getPlateloc().equals("H12"))
								styleCell = styleCellNormally;
							}
							if (sheetgrid.getRow(row+indexRow).getCell(column+indexCol) == null)
								sheetgrid.getRow(row+indexRow).createCell(column+indexCol);
								HSSFCell cell = sheetgrid.getRow(row+indexRow).getCell(column+indexCol);
							if(detail.getSamplegid()==null || (detail.getControltype()!=null && !detail.getControltype().equals(""))){
								switch (detail.getPlateloc()){
								case "H11":
								case "H12":
									break;
								default:{
									styleCell = styleCellBlank;
									cell.setCellValue(mapBlank.get(detail.getStudysampleid()));
									cell.setCellStyle(styleCell);	
									}
								}
								
							}
							else{
								styleCell = styleCellNormally;
								cell.setCellValue(detail.getLabstudyid().getPrefix()+
										(detail.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(detail.getSamplegid()):
											String.valueOf(detail.getSamplegid()))+"_"+detail.getStudysampleid());
							}
							
							if (detail.getPlateloc().equals("H11") || detail.getPlateloc().equals("H12")) {
								if (detail.getSamplegid()!=null)
									cell.setCellValue(detail.getLabstudyid().getPrefix()+
											(detail.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(detail.getSamplegid()):
												String.valueOf(detail.getSamplegid()))+"_"+detail.getStudysampleid());
								//cell.setCellStyle(styleIntertek);
							}
						}
					}
					row = row +rowsplate;
					row = row+2;
				}
				
			}
	}
	
	private void loadListGenotypingSewrvices(){
		int size = setShipmentDetail.iterator().next().getStSampleDetail().getLabstudyid().getPlatesize();
		HSSFSheet sheetgrid = listBook.getSheetAt(5);
		HSSFCell cellStyle =sheetgrid.getRow(11).getCell(0);
		HSSFCellStyle style = cellStyle.getCellStyle();
		HSSFCell cellStyleIntertek =sheetgrid.getRow(19).getCell(11);
		HSSFCellStyle styleIntertek = cellStyleIntertek.getCellStyle();
		if (size == SIZE_PLATE_96){
			rowsplate=8;
			colsplate=12;
		}
		else{
			rowsplate=16;
			colsplate=24;
		}
		
		LinkedHashMap<String, Map<String, SampleDetail>> mapPlates = new LinkedHashMap<String, Map<String,SampleDetail>>();
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
			
			SampleDetail detail = shipmentDetail.getStSampleDetail();
			if (mapPlates.containsKey(detail.getPlatename())){
				Map<String,SampleDetail> map = mapPlates.get(detail.getPlatename());
					String letter = detail.getPlateloc().substring(0, 1);
					String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
					map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
			}else{
				Map<String, SampleDetail> map = new HashMap<String, SampleDetail>();
				String letter = detail.getPlateloc().substring(0, 1);
				String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
				map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
				mapPlates.put(detail.getPlatename(), map);
			}
		}

		
		Iterator<Entry<String, Map<String, SampleDetail>>> it = mapPlates.entrySet().iterator();
		int row = 10;
		int column = 0 ;
		while (it.hasNext()){
			Map.Entry<String, Map<String,SampleDetail>> entry = (Entry<String, Map<String, SampleDetail>>) it.next();
			if (entry.getKey() != null){
				Map<String,SampleDetail> map = entry.getValue();
				validateRow(sheetgrid, row);
				HSSFCell cellPlateName =validateCell(sheetgrid, row, 0);
				cellPlateName.setCellValue("Plate");
				cellPlateName.setCellStyle(style);
				HSSFCell cellPlate =validateCell(sheetgrid, row, 1);
				cellPlate.setCellValue(entry.getKey());
				row = row + 1;
				drawingFormatPlate(style, sheetgrid, row);
					for (int indexRow = 1; indexRow <= rowsplate ; indexRow++){
						for (int indexCol = 1 ; indexCol<= colsplate; indexCol++){
							
							SampleDetail detail = map.get(indexRow+"|"+indexCol);
							validateRow(sheetgrid, row+indexRow);
							HSSFCellStyle styleCell = null;
							if (detail.getControltype() != null && !detail.getControltype().equals(""))
							styleCell = validateStatusSample(detail.getControltype());
							else {
								styleCell = styleCellNormally;
							}
							if (sheetgrid.getRow(row+indexRow).getCell(column+indexCol) == null)
								sheetgrid.getRow(row+indexRow).createCell(column+indexCol);
								HSSFCell cell = sheetgrid.getRow(row+indexRow).getCell(column+indexCol);
							if(detail.getSamplegid()==null || (detail.getControltype()!=null && !detail.getControltype().equals(""))){
								cell.setCellValue(mapBlank.get(detail.getStudysampleid()));
								cell.setCellStyle(styleCell);
							}
							else{
								styleCell = styleCellNormally;
								cell.setCellValue(detail.getLabstudyid().getPrefix()+
										(detail.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(detail.getSamplegid()):
											String.valueOf(detail.getSamplegid()))+"-"+detail.getStudysampleid());
							}
							detail.getPlateloc();
							if (detail.getPlateloc().equals("H11") || detail.getPlateloc().equals("H12")) {
								cell.setCellStyle(styleIntertek);
							}
						}
					}
					row = row +rowsplate;
					row = row+2;
				}
				
			}
	}
*/
	private void loadPlateID (HSSFSheet sheetList, int inidatsheetList) {
		if (mapPlate !=  null && !mapPlate.isEmpty()) {
			Iterator<Map.Entry<String,String>> it = mapPlate.entrySet().iterator();
			int indexRow =  0;
			while (it.hasNext()) {
				HSSFRow actrowsheetList;
				if (sheetList.getRow(indexRow+inidatsheetList)==null)
					sheetList.createRow(indexRow+inidatsheetList);
				actrowsheetList=sheetList.getRow(indexRow+inidatsheetList);
				if (actrowsheetList.getCell(1)==null) 
					actrowsheetList.createCell(1);
				Map.Entry<String,String> pair = it.next();
				actrowsheetList.getCell(1).setCellValue(pair.getValue());
				indexRow ++;
			}
		}
	}
/*	
	private void loadListSampleIDGenotypingService(HSSFSheet sheetList, int inidatsheetList, boolean reportType){
		
		String wellkbio;
		int i=0;
		int indexBlank = 1;
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
				SampleDetail sampledet = shipmentDetail.getStSampleDetail();
			HSSFRow actrowsheetList;
			//****************    sheetList  **************************
			//
			if (sheetList.getRow(i+inidatsheetList)==null)
				sheetList.createRow(i+inidatsheetList);
			actrowsheetList=sheetList.getRow(i+inidatsheetList);
			if (actrowsheetList.getCell(1)==null) actrowsheetList.createCell(1);
			if(sampledet.getSamplegid()!=null)
			actrowsheetList.getCell(1).setCellValue(sampledet.getLabstudyid().getPrefix()+
					(sampledet.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(sampledet.getSamplegid()) :
						String.valueOf(sampledet.getSamplegid()))+ "-" + sampledet.getStudysampleid());
			if (actrowsheetList.getCell(2)==null) actrowsheetList.createCell(2);
			actrowsheetList.getCell(2).setCellValue(getPlate( sampledet.getPlatename(), sampledet.getLabstudyid().getPrefix()));
			if (actrowsheetList.getCell(3)==null) actrowsheetList.createCell(3);
			wellkbio=sampledet.getPlateloc();
			if (wellkbio.length()==2)
				wellkbio=wellkbio.substring(0,1)+"0"+wellkbio.substring(1);
			actrowsheetList.getCell(3).setCellValue(wellkbio);
			
			if (sampledet.getPlateloc().equals("H11") || sampledet.getPlateloc().equals("H12")) {
				actrowsheetList.getCell(1).setCellValue("");
			}else
				{
					//Por especificaciones de KBIo se pone C si es un control y nada si es ADN normal
					if(sampledet.getControltype()!=null) {
							if (!sampledet.getControltype().equals("")) {
									actrowsheetList.getCell(1).setCellValue(getPathEmpty(indexBlank, sampledet.getStudysampleid()));
									indexBlank++;
							}
					}else
						if (sampledet.getSamplegid()==null) {
							actrowsheetList.getCell(1).setCellValue(getPathEmpty(indexBlank, sampledet.getStudysampleid()));
							indexBlank++;
						}
					
					
				}
			if (reportType) {
					if (actrowsheetList.getCell(8)==null) actrowsheetList.createCell(8);
					actrowsheetList.getCell(8).setCellValue(sampledet.getBreedergid() != null ? String.valueOf(sampledet.getBreedergid()):"");
					if (actrowsheetList.getCell(9)==null) actrowsheetList.createCell(9);
					actrowsheetList.getCell(9).setCellValue(sampledet.getNplanta() != null ? String.valueOf(sampledet.getNplanta()):"");
				}else {
					if (actrowsheetList.getCell(7)==null) actrowsheetList.createCell(7);
					actrowsheetList.getCell(7).setCellValue(sampledet.getBreedergid() != null ? String.valueOf(sampledet.getBreedergid()):"");
					if (actrowsheetList.getCell(8)==null) actrowsheetList.createCell(8);
					actrowsheetList.getCell(8).setCellValue(sampledet.getNplanta() != null ? String.valueOf(sampledet.getNplanta()):"");
				}
			i=i+1;
		}
	}
*/
	private String getPathEmpty(int index, Integer idSample) {
		String strBlank = "ZZ_Blank_";

		mapBlank.put(idSample, strBlank + idSample);
		return strBlank+idSample;
	}
	
	private String getPlate (String plate, String prefixPlate) {
		
		if (mapPlate.size() == 0) {
			mapPlate.put(plate, plate);
			return plate;
		}
		if (!mapPlate.containsKey(plate))
			mapPlate.put(plate, plate);
			return plate;
	}
/*	

	private void loadCustomerDetailIntertekExtraction(HSSFSheet sheetDetails){
		sheetDetails.getRow(13).getCell(2).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(13).getCell(4).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(14).getCell(4).setCellValue(userBean.getResearcherEMail());
		sheetDetails.getRow(16).getCell(4).setCellValue(shipment.getComment());
		sheetDetails.getRow(18).getCell(2).setCellValue(userBean.getResearcherEMail()+"; "+property.getKey(Constants.REPORT_PATH_GENOTYPING_SERVICES_EMAIL_INTERTEK_EXTRACTION, Bundle.conf));
		
		sheetDetails.getRow(26).getCell(4).setCellValue(shipment.getComment());
		sheetDetails.getRow(28).getCell(2).setCellValue(userBean.getResearcherEMail());
		
		sheetDetails.getRow(35).getCell(2).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(35).getCell(4).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(36).getCell(4).setCellValue(userBean.getResearcherEMail()+";"
				+property.getKey(Constants.REPORT_PATH_GENOTYPING_SERVICES_EMAIL_INTERTEK_EXTRACTION, Bundle.conf));
		
		sheetDetails.getRow(46).getCell(2).setCellValue(shipment.getTrackingNumberLocal());
		sheetDetails.getRow(49).getCell(2).setCellValue(StrUtils.getCrop(userBean.getTypeCorp(), property));
	}
	

	private void loadCustomerDetailLDSGIntertekExtraction(HSSFSheet sheetDetails){
		sheetDetails.getRow(13).getCell(2).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(13).getCell(4).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(14).getCell(4).setCellValue(userBean.getResearcherEMail());
		sheetDetails.getRow(16).getCell(4).setCellValue(shipment.getComment());
		sheetDetails.getRow(18).getCell(2).setCellValue(userBean.getResearcherEMail()+"; "+"cimmyt-eib-genotyping@cgiar.org");
		
		sheetDetails.getRow(25).getCell(4).setCellValue(shipment.getComment());
		sheetDetails.getRow(26).getCell(4).setCellValue(userBean.getResearcherEMail());
		
		sheetDetails.getRow(25).getCell(2).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(25).getCell(4).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(20).getCell(2).setCellValue(userBean.getResearcherEMail()+";"+"cimmyt-eib-genotyping@cgiar.org; CIMMYT-DMU@cgiar.org");
		
		sheetDetails.getRow(37).getCell(2).setCellValue(shipment.getTrackingNumberLocal());
		sheetDetails.getRow(41).getCell(2).setCellValue(StrUtils.getCrop(userBean.getTypeCorp(), property));
		sheetDetails.getRow(42).getCell(2).setCellValue(mapPlate.size());
	}
	

	private void loadCustomerDetailIntertek(HSSFSheet sheetDetails){
		sheetDetails.getRow(13).getCell(2).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(13).getCell(4).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(14).getCell(4).setCellValue(userBean.getResearcherEMail());
		sheetDetails.getRow(16).getCell(4).setCellValue(shipment.getComment());
		sheetDetails.getRow(18).getCell(2).setCellValue(userBean.getResearcherEMail()+"; "+property.getKey(Constants.REPORT_PATH_GENOTYPING_SERVICES_EMAIL_INTERTEK_EXTRACTION, Bundle.conf));
		
		sheetDetails.getRow(20).getCell(2).setCellValue(userBean.getResearcherEMail()+"; "+ Constants.EMAIL_ACCOUNT_RECEIVER_MAIZE+property.getKey(Constants.REPORT_PATH_GENOTYPING_SERVICES_EMAIL_INTERTEK_EXTRACTION, Bundle.conf));
		
		sheetDetails.getRow(25).getCell(2).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(25).getCell(4).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(26).getCell(4).setCellValue(userBean.getResearcherEMail()+";"
				+property.getKey(Constants.REPORT_PATH_GENOTYPING_SERVICES_EMAIL_INTERTEK_EXTRACTION, Bundle.conf));
		
		sheetDetails.getRow(37).getCell(2).setCellValue(shipment.getTrackingNumberLocal());
		sheetDetails.getRow(41).getCell(2).setCellValue(StrUtils.getCrop(userBean.getTypeCorp(), property));
	}
	

	private void loadCustomerDetailSheetGenotyping(HSSFSheet sheetDetails){
		sheetDetails.getRow(13).getCell(2).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(13).getCell(4).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(14).getCell(4).setCellValue(userBean.getResearcherEMail());
		sheetDetails.getRow(16).getCell(4).setCellValue(shipment.getComment());
		sheetDetails.getRow(18).getCell(2).setCellValue(userBean.getResearcherEMail()+";"+property.getKey(Constants.REPORT_PATH_GENOTYPING_SERVICES_EMAIL_INTERTEK, Bundle.conf));
		sheetDetails.getRow(20).getCell(2).setCellValue(userBean.getResearcherEMail()+";"+Constants.EMAIL_ACCOUNT_RECEIVER_MAIZE+property.getKey(Constants.REPORT_PATH_GENOTYPING_SERVICES_EMAIL_INTERTEK, Bundle.conf));
		sheetDetails.getRow(25).getCell(2).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheetDetails.getRow(25).getCell(4).setCellValue(shipment.getStShipmentSet().getStInvestigator().getInvest_name());
		sheetDetails.getRow(37).getCell(2).setCellValue(shipment.getTrackingNumberLocal());
		sheetDetails.getRow(41).getCell(2).setCellValue(StrUtils.getCrop(userBean.getTypeCorp(), property));
	}

	@Override
	public byte[] getBytesReportIntertekGrid(
			List<ShipmentDetail> setShipmentDetail, PropertyHelper property,
			UserBean userBean, Shipment shipment) {
		this.property =  property;
		this.setShipmentDetail = setShipmentDetail;
		this.userBean = userBean;
		this.shipment = shipment;
		listBook = null;
		loadExcelListIntertek();
		return getArryByte(listBook);
	}

	private void loadExcelListIntertek(){
		String rutaResumen= property.getKey(Constants.REPORT_PATH_INTERTEK_G96, Bundle.conf);
		InputStream inputStream =getClass().getResourceAsStream(rutaResumen);
		try {
			listBook = new HSSFWorkbook(inputStream);
			HSSFSheet sheetDetails = listBook.getSheetAt(0);
			loadCustomerDetailSheet(sheetDetails);
			HSSFSheet sheeorderForm = listBook.getSheetAt(1);
			loadOrderForm(sheeorderForm);
			HSSFSheet sheetList = listBook.getSheetAt(3);
			loadListSampleID(sheetList);
			loadStyleCells();
			loadListGridIntertek();
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

*/
	private int getEquivalenceInInteger (String letter){
		
		switch (letter ){
		case "A" :
			return 1;
		case "B" :
			return 2;
		case "C" :
			return 3;
		case "D" :
			return 4;
		case "E" :
			return 5;
		case "F" :
			return 6;
		case "G" :
			return 7;
		case "H" :
			return 8;
		}
		return 0;
	}

/*	
	private void drawingFormatPlateKSU (HSSFCellStyle style, HSSFSheet sheetgrid, int _indexRow){
		int indexRow=0;
		indexRow = indexRow + _indexRow;
		validateRow(sheetgrid, indexRow);
		validateCell(sheetgrid, indexRow, 1).setCellStyle(style);
		//sheetgrid.getRow(indexRow).getCell(2).setCellStyle(style);
		
		
		loadStyleandText(validateCell(sheetgrid, indexRow, 2), style, 1);
		loadStyleandText(validateCell(sheetgrid, indexRow, 3), style, 2);
		loadStyleandText(validateCell(sheetgrid, indexRow, 4), style, 3);
		loadStyleandText(validateCell(sheetgrid, indexRow, 5), style, 4);
		loadStyleandText(validateCell(sheetgrid, indexRow, 6), style, 5);
		loadStyleandText(validateCell(sheetgrid, indexRow, 7), style, 6);
		loadStyleandText(validateCell(sheetgrid, indexRow, 8), style, 7);
		loadStyleandText(validateCell(sheetgrid, indexRow, 9), style, 8);
		loadStyleandText(validateCell(sheetgrid, indexRow, 10), style, 9);
		loadStyleandText(validateCell(sheetgrid, indexRow, 11), style, 10);
		loadStyleandText(validateCell(sheetgrid, indexRow, 12), style, 11);
		loadStyleandText(validateCell(sheetgrid, indexRow, 13), style, 12);
		for (int index =1 ; index <= 8 ; index ++){
			validateRow(sheetgrid, indexRow+index);
		}
		loadStyleandText(validateCell(sheetgrid,indexRow+1, 1), style, "A");
		loadStyleandText(validateCell(sheetgrid,indexRow+2, 1), style, "B");
		loadStyleandText(validateCell(sheetgrid,indexRow+3, 1), style, "C");
		loadStyleandText(validateCell(sheetgrid,indexRow+4, 1), style, "D");
		loadStyleandText(validateCell(sheetgrid,indexRow+5, 1), style, "E");
		loadStyleandText(validateCell(sheetgrid,indexRow+6, 1), style, "F");
		loadStyleandText(validateCell(sheetgrid,indexRow+7, 1), style, "G");
		loadStyleandText(validateCell(sheetgrid,indexRow+8, 1), style, "H");
	
	

}
	
*/	
	private void drawingFormatPlate (HSSFCellStyle style, HSSFSheet sheetgrid, int indexRow){
				validateRow(sheetgrid, indexRow);
				validateCell(sheetgrid, indexRow, 0).setCellStyle(style);
				//sheetgrid.getRow(indexRow).getCell(2).setCellStyle(style);
				
				
				loadStyleandText(validateCell(sheetgrid, indexRow, 1), style, 1);
				loadStyleandText(validateCell(sheetgrid, indexRow, 2), style, 2);
				loadStyleandText(validateCell(sheetgrid, indexRow, 3), style, 3);
				loadStyleandText(validateCell(sheetgrid, indexRow, 4), style, 4);
				loadStyleandText(validateCell(sheetgrid, indexRow, 5), style, 5);
				loadStyleandText(validateCell(sheetgrid, indexRow, 6), style, 6);
				loadStyleandText(validateCell(sheetgrid, indexRow, 7), style, 7);
				loadStyleandText(validateCell(sheetgrid, indexRow, 8), style, 8);
				loadStyleandText(validateCell(sheetgrid, indexRow, 9), style, 9);
				loadStyleandText(validateCell(sheetgrid, indexRow, 10), style, 10);
				loadStyleandText(validateCell(sheetgrid, indexRow, 11), style, 11);
				loadStyleandText(validateCell(sheetgrid, indexRow, 12), style, 12);
				for (int index =1 ; index <= 8 ; index ++){
					validateRow(sheetgrid, indexRow+index);
				}
				loadStyleandText(validateCell(sheetgrid,indexRow+1, 0), style, "A");
				loadStyleandText(validateCell(sheetgrid,indexRow+2, 0), style, "B");
				loadStyleandText(validateCell(sheetgrid,indexRow+3, 0), style, "C");
				loadStyleandText(validateCell(sheetgrid,indexRow+4, 0), style, "D");
				loadStyleandText(validateCell(sheetgrid,indexRow+5, 0), style, "E");
				loadStyleandText(validateCell(sheetgrid,indexRow+6, 0), style, "F");
				loadStyleandText(validateCell(sheetgrid,indexRow+7, 0), style, "G");
				loadStyleandText(validateCell(sheetgrid,indexRow+8, 0), style, "H");
			
			
		
	}


	private void loadStyleandText(HSSFCell cell,HSSFCellStyle style, String value ){
		cell.setCellStyle(style);
		cell.setCellValue(value);
	}
	private void loadStyleandText(HSSFCell cell,HSSFCellStyle style, int value ){
		cell.setCellStyle(style);
		cell.setCellValue(value);
	}

	private void validateRow(HSSFSheet sheetgrid, int indexRow){
		if(sheetgrid.getRow(indexRow)==null)
			sheetgrid.createRow(indexRow);
	}
/*
	private void loadListGridIntertek(){
		int size = setShipmentDetail.iterator().next().getStSampleDetail().getLabstudyid().getPlatesize();
		HSSFSheet sheetgrid = listBook.getSheetAt(4);
		HSSFCell cellStyle =sheetgrid.getRow(5).getCell(2);
		HSSFCellStyle style = cellStyle.getCellStyle();
		if (size == SIZE_PLATE_96){
			rowsplate=8;
			colsplate=12;
		}
		else{
			rowsplate=16;
			colsplate=24;
		}
		
		LinkedHashMap<String, Map<String, SampleDetail>> mapPlates = new LinkedHashMap<String, Map<String,SampleDetail>>();
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
			SampleDetail detail = shipmentDetail.getStSampleDetail();
			if (mapPlates.containsKey(detail.getPlatename())){
				Map<String,SampleDetail> map = mapPlates.get(detail.getPlatename());
					String letter = detail.getPlateloc().substring(0, 1);
					String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
					map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
			}else{
				Map<String, SampleDetail> map = new HashMap<String, SampleDetail>();
				String letter = detail.getPlateloc().substring(0, 1);
				String number = detail.getPlateloc().substring(1, detail.getPlateloc().length());
				map.put(getEquivalenceInInteger(letter)+"|" +number, detail);
				mapPlates.put(detail.getPlatename(), map);
			}
		}

		Iterator<Entry<String, Map<String, SampleDetail>>> it = mapPlates.entrySet().iterator();
		int row = 4;
		int column = 2;
		while (it.hasNext()){
			Map.Entry<String, Map<String,SampleDetail>> entry = (Entry<String, Map<String, SampleDetail>>) it.next();
			if (entry.getKey() != null){
				
				Map<String,SampleDetail> map = entry.getValue();
				validateRow(sheetgrid, row);
				HSSFCell cellPlate =validateCell(sheetgrid, row, 1);
				cellPlate.setCellValue(entry.getKey());
				row = row + 1;
				drawingFormatPlate(style, sheetgrid, row);
					for (int indexRow = 1; indexRow <= rowsplate ; indexRow++){
						for (int indexCol = 1 ; indexCol<= colsplate; indexCol++){
							SampleDetail detail = map.get(indexRow+"|"+indexCol);
							validateRow(sheetgrid, row+indexRow);
							HSSFCellStyle styleCell = null;
							if (detail.getControltype() != null && !detail.getControltype().equals(""))
							styleCell = validateStatusSample(detail.getControltype());
							else {
								styleCell = styleCellNormally;
							}
							if (sheetgrid.getRow(row+indexRow).getCell(column+indexCol) == null)
								sheetgrid.getRow(row+indexRow).createCell(column+indexCol);
								HSSFCell cell = sheetgrid.getRow(row+indexRow).getCell(column+indexCol);
							if(detail.getSamplegid()==null || (detail.getControltype()!=null && !detail.getControltype().equals(""))){	
								cell.setCellValue("BLANK");
								cell.setCellStyle(styleCell);
							}
							else{
								styleCell = styleCellNormally;
								cell.setCellValue(detail.getLabstudyid().getPrefix()+
										(detail.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(detail.getSamplegid()):
											String.valueOf(detail.getSamplegid())));
							}
						}
					}
					row = row +rowsplate;
					row = row+2;
				}
				
			}
	}
	private void loadListSampleID(HSSFSheet sheetList){
		
		int inidatsheetList=2;
		String wellkbio;
		int mod = setShipmentDetail.iterator().next().getStSampleDetail().getLabstudyid().getPlatesize();
		int plates = setShipmentDetail.size() / mod;

		int i=0;
		for(ShipmentDetail shipmentDetail : setShipmentDetail){ 
				SampleDetail sampledet = shipmentDetail.getStSampleDetail();
			HSSFRow actrowsheetList;
			//****************    sheetList  **************************
			//
			if (sheetList.getRow(i+inidatsheetList)==null)
				sheetList.createRow(i+inidatsheetList);
			actrowsheetList=sheetList.getRow(i+inidatsheetList);
			if (actrowsheetList.getCell(0)==null) actrowsheetList.createCell(0);
			if(sampledet.getSamplegid()!=null)
			actrowsheetList.getCell(0).setCellValue(sampledet.getLabstudyid().getPrefix()+
					(sampledet.getLabstudyid().isUsePadded() ? StrUtils.getPaddingCeros(sampledet.getSamplegid()) :
						String.valueOf(sampledet.getSamplegid())));
			if (actrowsheetList.getCell(1)==null) actrowsheetList.createCell(1);
			actrowsheetList.getCell(1).setCellValue(sampledet.getPlatename());
			if (actrowsheetList.getCell(2)==null) actrowsheetList.createCell(2);
			wellkbio=sampledet.getPlateloc();
			if (wellkbio.length()==2)
				wellkbio=wellkbio.substring(0,1)+"0"+wellkbio.substring(1);
			actrowsheetList.getCell(2).setCellValue(wellkbio);
			if (actrowsheetList.getCell(3)==null) actrowsheetList.createCell(3);
			actrowsheetList.getCell(3).setCellValue("");
			//Por especificaciones de KBIo se pone C si es un control y nada si es ADN normal
			if(sampledet.getControltype()!=null)
				if (sampledet.getControltype().equals("K")||sampledet.getControltype().equals("B"))
						actrowsheetList.getCell(3).setCellValue("C");
			
			if (sampledet.getSamplegid()==null)
				actrowsheetList.getCell(3).setCellValue("C");

			if (actrowsheetList.getCell(4)==null) actrowsheetList.createCell(4);
			actrowsheetList.getCell(4).setCellValue(sampledet.getLabstudyid().getPlatesize());
			
			
			i=i+1;
		}
	}
	
	private void loadOrderForm(HSSFSheet sheet){
		sheet.getRow(3).getCell(4).setCellValue(property.getKey(NAME_COMPANY_SEQUENCE, Bundle.conf));
		sheet.getRow(6).getCell(4).setCellValue(userBean.getCorp());
		
	}
*/

	private HSSFCellStyle styleCellNormally(HSSFWorkbook objLibro, boolean isHeader){
		HSSFFont sourceStyle = objLibro.createFont();
		sourceStyle.setFontHeightInPoints((short)11);
		//sourceStyle.setBoldweight((short)11);
		sourceStyle.setFontName(HSSFFont.FONT_ARIAL);
		if (isHeader){
			//sourceStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		}
		HSSFCellStyle styleCell = objLibro.createCellStyle();
		styleCell.setWrapText(true);
		//styleCell.setAlignment(HSSFCellStyle. ALIGN_JUSTIFY);
		
		//styleCell.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
		styleCell.setFont(sourceStyle);
		return styleCell;
	}

	private HSSFCellStyle getStyleCeldSolidForeground (HSSFWorkbook objBook, short foregroundColor){
		HSSFFont sourceStyle = objBook.createFont();
		sourceStyle.setFontHeightInPoints((short)11);
		sourceStyle.setFontName(HSSFFont.FONT_ARIAL);
		HSSFCellStyle stileCell = objBook.createCellStyle();
		stileCell.setWrapText(true);
		//stileCell.setAlignment(HSSFCellStyle. ALIGN_JUSTIFY);
		//stileCell.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
		stileCell.setFont(sourceStyle);
		stileCell.setFillForegroundColor(foregroundColor);
		//stileCell.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		return stileCell;
	}
/*
	private HSSFCellStyle validateStatusSample(String status){
		char c=status.toCharArray()[0];
		HSSFCellStyle style = styleCellNormally;
		switch (c){
		case ConstantsDNA.INDIVIDUAL_CONTROL :
			style = styleCellControl;
			break;
		case ConstantsDNA.RANDOM_CONTROL :
			style = styleCellControlRandom;	
			break;
		case ConstantsDNA.DART_CONTROL :
			style = styleCellControlDART;
			break;
		case ConstantsDNA.KBIOSCIENCIES_CONTROL :
			style = styleCellControlKBIo; 
			break;
		case ConstantsDNA.NOT_CONTROL :
			style = styleCellNormally;
			break;
		case ConstantsDNA.BANK_CONTROL:
			style = styleCellBlank;
			break;
			default  :
				style = styleCellNormally;
			break;
		}
		return style;
	}
	*/
}

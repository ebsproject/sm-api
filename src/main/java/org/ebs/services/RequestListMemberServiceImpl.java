///////////////////////////////////////////////////////////
//  RequestListMemberServiceImpl.java
//  Macromedia ActionScript Implementation of the Class RequestListMemberServiceImpl
//  Generated by Enterprise Architect
//  Created on:      12-Mar-2021 11:46:21 AM
//  Original author: CIMMYT
///////////////////////////////////////////////////////////

package org.ebs.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.ebs.model.RequestListMemberModel;
import org.ebs.model.RequestModel;
import org.ebs.model.SampleDetailModel;
import org.ebs.model.repos.BatchRepository;
import org.ebs.model.repos.RequestListMemberRepository;
import org.ebs.model.repos.RequestRepository;
import org.ebs.model.repos.SampleDetailRepository;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.RequestListMemberTo;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.Input.RequestListMemberInput;
import org.ebs.util.Connection;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author CIMMYT
 * @version 1.0
 * @created 12-Mar-2021 11:46:21 AM
 */
@RequiredArgsConstructor
@Service
@Slf4j
@Transactional(readOnly = true)
  class RequestListMemberServiceImpl implements RequestListMemberService {

	private final BatchRepository batchRepository;
	private final ConversionService converter;
	private final RequestListMemberRepository requestlistmemberRepository;
	private final RequestRepository requestRepository;
	private final SampleDetailRepository sampleDetailRepository; 

	/**
	 *
	 * @param RequestListMember
	 */
	@Override @Transactional(readOnly = false)
	public RequestListMemberTo createRequestListMember(RequestListMemberInput RequestListMember){
		RequestListMemberModel model = converter.convert(RequestListMember,RequestListMemberModel.class);
		 model.setId(0);
		 if (RequestListMember.getRequest() != null && RequestListMember.getRequest().getId()>0)
			 model.setRequest(requestRepository.findById(RequestListMember.getRequest().getId()).get());
		 if(RequestListMember.getBatch() != null && RequestListMember.getBatch().getId()>0)
			model.setBatch(batchRepository.findById(RequestListMember.getBatch().getId()).get());
		 	model= requestlistmemberRepository.save(model);
		 return converter.convert(model, RequestListMemberTo.class);
	}

	/**
	 *
	 * @param requestListMemberId
	 */
	@Override @Transactional(readOnly = false)
	public int deleteRequestListMember(int requestListMemberId){
		RequestListMemberModel requestlistmember = requestlistmemberRepository.findById(requestListMemberId).orElseThrow(() -> new RuntimeException("RequestListMember not found"));
		 requestlistmember.setDeleted(true);
		  requestlistmemberRepository.save(requestlistmember);
		 return requestListMemberId;
	}

	/**
	 *
	 * @param requestlistmemberId
	 */
	public Optional<BatchTo> findBatch(int requestlistmemberId){
		return requestlistmemberRepository.findById(requestlistmemberId).map(r -> converter.convert(r.getBatch(),BatchTo.class));
	}

	/**
	 *
	 * @param requestlistmemberId
	 */
	public Optional<RequestTo> findRequest(int requestlistmemberId){
		return requestlistmemberRepository.findById(requestlistmemberId).map(r -> converter.convert(r.getRequest(),RequestTo.class));
	}

	/**
	 *
	 * @param requestListMemberId
	 */
	@Override
	public Optional<RequestListMemberTo> findRequestListMember(int requestListMemberId){
		if(requestListMemberId <1)
		 {return Optional.empty();}
		 return requestlistmemberRepository.findById(requestListMemberId).filter(r -> !r.isDeleted()).map(r -> converter.convert(r,RequestListMemberTo.class));
	}

	/**
	 *
	 * @param page
	 * @param sort
	 * @param filters
	 */
	@Override
	public Page<RequestListMemberTo> findRequestListMembers(PageInput page, SortInput sort, List<FilterInput> filters){
		return requestlistmemberRepository.findByCriteria(RequestListMemberModel.class,filters,sort,page).map((r) ->{
			RequestListMemberTo to = converter.convert(r,RequestListMemberTo.class);
			return to;
		  }
		);
	}

	/**
	 *
	 * @param requestListMember
	 */
	@Override @Transactional(readOnly = false)
	public RequestListMemberTo modifyRequestListMember(RequestListMemberInput requestListMember){
		RequestListMemberModel target= requestlistmemberRepository.findById(requestListMember.getId()).orElseThrow(() -> new RuntimeException("RequestListMember not found"));
		 RequestListMemberModel source= converter.convert(requestListMember,RequestListMemberModel.class);
		 Utils.copyNotNulls(source,target);
		 return converter.convert(requestlistmemberRepository.save(target), RequestListMemberTo.class);
	}


	@Override @Transactional(readOnly = false)
    public void createListRequestListMember(List<RequestListMemberInput> requestListMember, int requestId) {
        RequestModel modelRequest = requestRepository.findById(requestId).get();
        List<RequestListMemberModel> list = new ArrayList<RequestListMemberModel>();
        requestListMember.stream().forEach((p) -> {
            RequestListMemberModel requestListModel = converter.convert(p, RequestListMemberModel.class);
            requestListModel.setRequest(modelRequest);
            list.add(requestListModel);
        });
        requestlistmemberRepository.saveAll(list);
    }
 
    @Override
    public List<String> findAllSampleNames(int batchId, int requestId) {
        int page = 1;
        List<String> samples = new ArrayList<>(5000);
        List<String> tmp = null;

        List<FilterInput> filters = List.of(new FilterInput("batch.id", ""+batchId,FilterMod.EQ, null),
                new FilterInput("request.id", ""+requestId,FilterMod.EQ, null));

        while (page == 1 || !tmp.isEmpty()) {
            PageInput pageInput = new PageInput(page, 1000);
            tmp = findRequestListMembers(pageInput, null, filters)
                    .map(s -> s.getSampleName())
                    .getContent();
            log.trace(">> retrieved lsit members page {} with {} elements:", page, tmp.size());
            samples.addAll(tmp);
            page++;
        }
        log.trace("Total samples found: {}", samples.size());
        return samples;
    }

}

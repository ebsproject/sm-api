package org.ebs.services;

public interface NotificationService {
    boolean sendNotificationGeneric(int submitter, int record, String message);
    boolean sendNotificationAsGigwa(int submitter, int record, String message);
    boolean sendNotificationAsBatchManager(int submitter, int record, String message);
}

package org.ebs.services;

import java.util.List;
import java.util.Optional;

import org.ebs.model.RequestTraitModel;
import org.ebs.model.repos.RequestRepository;
import org.ebs.model.repos.RequestTraitRepository;
import org.ebs.services.to.RequestTo;
import org.ebs.services.to.RequestTraitTo;
import org.ebs.services.to.Input.RequestTraitInput;
import org.ebs.util.FilterInput;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.ebs.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(onConstructor = @__({@Autowired}))
@Service @Transactional(readOnly = true)
public class RequestTraitServiceImpl implements RequestTraitService{

	private final ConversionService converter;
	private final  RequestRepository requestRepository;
	private final RequestTraitRepository RequestTraitRepository; 

	@Override @Transactional(readOnly = false)
	public RequestTraitTo createRequestTrait(RequestTraitInput RequestTraitTInput) {
		RequestTraitModel model = converter.convert(RequestTraitTInput, RequestTraitModel.class);
		model.setId(0);
		if (RequestTraitTInput.getRequest() != null && RequestTraitTInput.getRequest().getId()>0)
			 model.setRequest(requestRepository.findById(RequestTraitTInput.getRequest().getId()).get());
		 model= RequestTraitRepository.save(model);
		return  converter.convert(model, RequestTraitTo.class);
	}

	@Override  @Transactional(readOnly = false)
	public int deleteRequestTrait(int id) {
		RequestTraitModel model = RequestTraitRepository.findById(id).orElseThrow(() -> new RuntimeException("RequestTrait not found"));
		model.setDeleted(true);
		RequestTraitRepository.save(model);
		return id;
	}

	@Override
	public Optional<RequestTraitTo> findRequestTrait(int id) {
		if(id <1)
		 {return Optional.empty();}
		return RequestTraitRepository.findById(id).filter(r -> !r.isDeleted()).map(r -> converter.convert(r,RequestTraitTo.class));
	}

	@Override
	public Optional<RequestTo> findRequest(int requestlistmemberId) {
		return RequestTraitRepository.findById(requestlistmemberId).map(r -> converter.convert(r.getRequest(),RequestTo.class));
	}

	@Override
	public Page<RequestTraitTo> findRequestTraits(PageInput page, SortInput sort, List<FilterInput> filters) {
		return RequestTraitRepository.findByCriteria(RequestTraitModel.class, filters, sort, page).map(r -> converter.convert(r,RequestTraitTo.class));
	}

	@Override @Transactional(readOnly = false)
	public RequestTraitTo modifyRequestTrait(RequestTraitInput intput) {
		RequestTraitModel target = RequestTraitRepository.findById(intput.getId()).orElseThrow(() -> new RuntimeException("RequestTrait not found"));
		RequestTraitModel source =  converter.convert(intput, RequestTraitModel.class);
		Utils.copyNotNulls(source,target);
		return converter.convert(RequestTraitRepository.save(target), RequestTraitTo.class);
	}
	
}

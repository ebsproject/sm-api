package org.ebs;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Configuration
@EnableScheduling
@EnableAsync
@RequiredArgsConstructor
@Slf4j

public class AsyncConfig implements AsyncConfigurer {

	 @Value("${ebs.asyncTask.corePoolSize}")
	 private int corePoolSize;
	 @Value("${ebs.asyncTask.maxPoolSize}")
	 private int maxPoolSize;
	 
	 @Value("${ebs.asyncTask.queueCapacity}")
	 private int queueCapacity;
	
    @Override
    @Bean(name = "taskExecutorPool")
    public AsyncTaskExecutor getAsyncExecutor() {
    	log.info("Creating Task Exceutor Pool");
    	log.info("config :: "+ corePoolSize +" " + maxPoolSize +" "+queueCapacity);
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(corePoolSize);
        executor.setMaxPoolSize(maxPoolSize);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setQueueCapacity(queueCapacity);
       
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new SimpleAsyncUncaughtExceptionHandler();
    }
}

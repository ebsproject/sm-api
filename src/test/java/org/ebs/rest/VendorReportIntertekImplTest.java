package org.ebs.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.ebs.services.report.ServiceReportKBio;
import org.ebs.services.to.custom.ReportIntertekTo;
import org.ebs.util.client.RestCsClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class VendorReportIntertekImplTest {
    
    private MockMvc mockMvc;

    @Mock
    private ServiceReportKBio serviceReportKBio;
    @Mock
    private RestCsClient csClient;

    private ObjectMapper om = new ObjectMapper();

    @InjectMocks
    private VendorReportIntertekImpl subject;
    
    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(subject).build();
    }   

    @Test
    public void callingCreateRequestCode() throws Exception {
        when(serviceReportKBio.getBytesReportEIBDARTAGIntertekOrderExtraction(any()))
                .thenReturn("binaryFile".getBytes());
        when(csClient.getSequence(anyInt(), any()))
                .thenReturn("generatedFileName");
        
        mockMvc.perform(post("/vendor-report/createIntertekReport")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(new ReportIntertekTo(1, 2, "aName", null, null, "aCrop", 7))))
            .andExpect(status().isOk())
            .andExpect(content().string("binaryFile"))
            .andExpect(header().string("Content-Type", MediaType.APPLICATION_OCTET_STREAM_VALUE))
            .andExpect(header().string(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION))
            .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"generatedFileName.xls\""));

    }

}
 
package org.ebs.rest.validation;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.BatchService;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.custom.RequestIDTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageImpl;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class IntertekFormatValidatorTest {
    @Mock
    private BatchService batchServiceMock;
    @Mock
    private RequestListMemberService listServiceMock;

    IntertekFormatValidator subject = null;
    
    @BeforeEach
    public void init() {
        subject = new IntertekFormatValidator(batchServiceMock, listServiceMock);
    }

    //TODO
    //@Test
    public void givenTestFileHasNoErrors_whenValidate_thenReturnNoErrorMeggages() {

        when(batchServiceMock.findBatch(anyInt())).thenReturn(Optional.of(new BatchTo(111)));
        PageImpl<RequestIDTo> page = new PageImpl<>(List.of(new RequestIDTo(111)));
        when(batchServiceMock.getRequesByBatchID(anyInt())).thenReturn(page);
        /*MarkerTo m1 = new MarkerTo("snpZM00170");
        MarkerTo m2 = new MarkerTo("snpZM00171");
        MarkerTo m3 = new MarkerTo("snpZM00172");
        MarkerTo m4 = new MarkerTo("snpZM00173");
        MarkerTo m5 = new MarkerTo("snpZM00174");
        when(batchServiceMock.findMarkers(anyInt())).thenReturn(Set.of(m1, m2, m3, m4, m5));*/
        when(listServiceMock.findAllSampleNames(anyInt(), anyInt())).thenReturn(new ArrayList<>(
                List.of("MX19MVEM000718-329161", "MX19MVEM000730-329173", "MX19MVEM000742-329185",
                        "MX19MVEM000001-328430", "MX19MVEM000013-328442", "MX19MVEM000025-328454")));
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
            .thenReturn(new ArrayList<>(of("SWE-20806-03_MX19MVEMMEM3", "SWE-20806-01_MX19MVEMMEM1", "SWE-20806-02_MX19MVEMMEM2")));
        
        ClassPathResource r = new ClassPathResource("intertekSample.csv");

        MockMultipartFile mockFile;
        try {
            mockFile = new MockMultipartFile(r.getFilename(), r.getInputStream());
            UploadResultFileRequest dataStub = new UploadResultFileRequest();
            dataStub.setFile(new MultipartFile[] { mockFile });
            dataStub.setTags(Arrays.asList("gigwa", "batchId:123"));
            String errors = subject.validate(dataStub);
            assertThat(errors).isEmpty();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void givenAllPlatesExistWhenValidateExistingPlatesThenReturnNoErrors() {
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
                .thenReturn(new ArrayList<>(of("SWE-20806-03_MX19MVEMMEM3", "SWE-20806-01_MX19MVEMMEM1", "SWE-20806-02_MX19MVEMMEM2")));
        String actual = subject.validateExistingPlates(Set.of("SWE-20806-01_MX19MVEMMEM1", "SWE-20806-02_MX19MVEMMEM2", "SWE-20806-03_MX19MVEMMEM3", "extraPlate"), 123);
        assertEquals("", actual);
    }

    @Test
    public void givenPlatesMissingWhenValidateExistingPlatesThenReturnErrorMessage() {
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
            .thenReturn(new ArrayList<>(of("SWE-20806-03_MX19MVEMMEM3", "SWE-20806-01_MX19MVEMMEM1", "SWE-20806-02_MX19MVEMMEM2")));
        String actual = subject.validateExistingPlates(Set.of("SWE-20806-02_MX19MVEMMEM2", "extraPlate"), 123);
        assertEquals("There are missing plates in the file: SWE-20806-01_MX19MVEMMEM1,SWE-20806-03_MX19MVEMMEM3", actual);
    }

}

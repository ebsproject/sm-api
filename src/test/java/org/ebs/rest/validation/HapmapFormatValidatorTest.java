package org.ebs.rest.validation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.BatchService;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.custom.RequestIDTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageImpl;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class HapmapFormatValidatorTest {

    @Mock
    private BatchService batchServiceMock;
    @Mock
    private RequestListMemberService listServiceMock;

    HapMapFormatValidator subject = null;
    
    @BeforeEach
    public void init() {
        subject = new HapMapFormatValidator(batchServiceMock, listServiceMock);
    }

    //TODO
    //@Test
    public void givenTestFileHasNoErrors_whenValidate_thenReturnNoErrorMeggages() {
        when(batchServiceMock.findBatch(anyInt())).thenReturn(Optional.of(new BatchTo(111)));
        PageImpl<RequestIDTo> page = new PageImpl<>(List.of(new RequestIDTo(111)));
        when(batchServiceMock.getRequesByBatchID(anyInt())).thenReturn(page);
      /*  MarkerTo m1 = new MarkerTo("IRRI_SNP0001_CHR01_194844");
        MarkerTo m2 = new MarkerTo("IRRI_SNP0002_CHR01_375814");
        MarkerTo m3 = new MarkerTo("IRRI_SNP0003_CHR01_717702");
        MarkerTo m4 = new MarkerTo("IRRI_SNP0005_CHR01_1044946");
        MarkerTo m5 = new MarkerTo("IRRI_SNP0006_CHR01_1175585");
        when(batchServiceMock.findMarkers(anyInt())).thenReturn(Set.of(m1,m2,m3,m4,m5));*/
        when(listServiceMock.findAllSampleNames(anyInt(), anyInt()))
            .thenReturn(new ArrayList<>(List.of("BIRSEAGS_102_P8155_792166", "BIRSEAGS_102_P8155_792167",
                    "BIRSEAGS_102_P8155_792168", "BIRSEAGS_102_P8155_792169", "BIRSEAGS_102_P8155_792170",
                    "BIRSEAGS_102_P8155_792171","BIRSEAGS_102_P8155_792172")));

        ClassPathResource r = new ClassPathResource("sample_intertek.hapmap");
        
        MockMultipartFile mockFile;
        try {
            mockFile = new MockMultipartFile(r.getFilename(), r.getInputStream());
            UploadResultFileRequest dataStub = new UploadResultFileRequest();
            dataStub.setFile(new MultipartFile[] { mockFile });
            dataStub.setTags(Arrays.asList("gigwa", "batchId:123"));
            String errors = subject.validate(dataStub);
            assertThat(errors).isEmpty();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package org.ebs.rest.validation;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.BatchService;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.custom.RequestIDTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageImpl;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class AgriplexFormatValidatorTest {

    @Mock
    private BatchService batchServiceMock;
    @Mock
    private RequestListMemberService listServiceMock;

    AgriplexFormatValidator subject = null;
    
    @BeforeEach
    public void init() {
        subject = new AgriplexFormatValidator(batchServiceMock, listServiceMock);
    }

    @Test
    public void givenTestFileHasNoErrors_whenValidate_thenReturnNoErrorMeggages() {
        when(batchServiceMock.findBatch(anyInt())).thenReturn(Optional.of(new BatchTo(111)));
        PageImpl<RequestIDTo> page = new PageImpl<>(List.of(new RequestIDTo(111)));
        when(batchServiceMock.getRequesByBatchID(anyInt())).thenReturn(page);
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
            .thenReturn(new ArrayList<>(of("2021-3_AGR_P03", "2021-3_AGR_P01", "2021-3_AGR_P02")));
        when(listServiceMock.findAllSampleNames(anyInt(), anyInt()))
            .thenReturn(new ArrayList<>(of("RCRS-1-1-1",
                "RCRS-1-2-1", "RCRS-1-3-1", "RCRS-1-4-1", "RCRS-1-5-1", "RCRS-1-6-1", "RCRS-1-7-1")));

        ClassPathResource r = new ClassPathResource("agriplexSample.xlsx");

        MockMultipartFile mockFile;
        try {
            mockFile = new MockMultipartFile(r.getFilename(), r.getInputStream());
            UploadResultFileRequest dataStub = new UploadResultFileRequest();
            dataStub.setFile(new MultipartFile[] { mockFile });
            dataStub.setTags(Arrays.asList("gigwa", "batchId:123"));
            String errors = subject.validate(dataStub);
            assertThat(errors).isEmpty();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void givenAllPlatesExistWhenValidateExistingPlatesThenReturnNoErrors() {
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
                .thenReturn(new ArrayList<>(of("2021-3_AGR_P03", "2021-3_AGR_P01", "2021-3_AGR_P02")));
        String actual = subject.validateExistingPlates(Set.of("2021-3_AGR_P01", "2021-3_AGR_P02", "2021-3_AGR_P03", "extraPlate"), 123);
        assertEquals("", actual);
    }

    @Test
    public void givenPlatesMissingWhenValidateExistingPlatesThenReturnErrorMessage() {
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
                .thenReturn(new ArrayList<>(of("2021-3_AGR_P03", "2021-3_AGR_P01", "2021-3_AGR_P02")));
        String actual = subject.validateExistingPlates(Set.of("2021-3_AGR_P02", "extraPlate"), 123);
        assertEquals("There are missing plates in the file: 2021-3_AGR_P01,2021-3_AGR_P03", actual);
    }

    @Test
    public void givenTestFileHasNumericSamples_whenValidate_thenReturnNoErrorMeggages() {
        when(batchServiceMock.findBatch(anyInt())).thenReturn(Optional.of(new BatchTo(123)));
        PageImpl<RequestIDTo> page = new PageImpl<>(List.of(new RequestIDTo(456)));
        when(batchServiceMock.getRequesByBatchID(anyInt())).thenReturn(page);
        when(batchServiceMock.findMarkers(anyInt())).thenReturn(List.of("IRRI_SNP0001_CHR01_194844","IRRI_SNP0002_CHR01_375814","IRRI_SNP0003_CHR01_717702",
            "IRRI_SNP0005_CHR01_1044946","IRRI_SNP0006_CHR01_1175585"));
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
            .thenReturn(new ArrayList<>(of("2021-3_AGR_P03", "2021-3_AGR_P01", "2021-3_AGR_P02")));
        when(listServiceMock.findAllSampleNames(anyInt(), anyInt()))
            .thenReturn(new ArrayList<>(of("2",
            "4",
            "6",
            "8",
            "10",
            "12",
            "14"
            )));

        ClassPathResource r = new ClassPathResource("agriplexSample2.xlsx");

        MockMultipartFile mockFile;
        try {
            mockFile = new MockMultipartFile(r.getFilename(), r.getInputStream());
            UploadResultFileRequest dataStub = new UploadResultFileRequest();
            dataStub.setFile(new MultipartFile[] { mockFile });
            dataStub.setTags(Arrays.asList("gigwa", "batchId:123"));
            String errors = subject.validate(dataStub);
            assertThat(errors).isEmpty();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
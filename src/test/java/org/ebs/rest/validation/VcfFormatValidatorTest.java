package org.ebs.rest.validation;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.Arrays;

import org.ebs.rest.to.UploadResultFileRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class VcfFormatValidatorTest {
    VcfFormatValidator subject = null;
    
    @BeforeEach
    public void init() {
        subject = new VcfFormatValidator();
    }

    @Test
    public void givenTestFileHasNoErrors_whenValidate_thenReturnNoErrorMeggages() {
        ClassPathResource r = new ClassPathResource("vcfSample.vcf");
        
        MockMultipartFile mockFile;
        try {
            mockFile = new MockMultipartFile(r.getFilename(), r.getInputStream());
            UploadResultFileRequest dataStub = new UploadResultFileRequest();
            dataStub.setFile(new MultipartFile[] { mockFile });
            dataStub.setTags(Arrays.asList("gigwa", "batchId:123"));
            String errors = subject.validate(dataStub);
            assertThat(errors).isEmpty();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

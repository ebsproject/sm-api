package org.ebs.rest.validation;

import static java.util.List.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.ebs.rest.to.UploadResultFileRequest;
import org.ebs.services.BatchService;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.custom.RequestIDTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageImpl;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class DarTagFormatValidatorTest {
    @Mock
    private BatchService batchServiceMock;
    @Mock
    private RequestListMemberService listServiceMock;

    DarTagFormatValidator subject = null;
    
    @BeforeEach
    public void init() {
        subject = new DarTagFormatValidator(batchServiceMock, listServiceMock);
    }

    //TODO
   // @Test
    public void givenTestFileHasNoErrors_whenValidate_thenReturnNoErrorMeggages() {
        when(batchServiceMock.findBatch(anyInt())).thenReturn(Optional.of(new BatchTo(111)));
        PageImpl<RequestIDTo> page = new PageImpl<>(List.of(new RequestIDTo(111)));
        when(batchServiceMock.getRequesByBatchID(anyInt())).thenReturn(page);
        //TODO
        /*MarkerTo m1 = new MarkerTo("MDTM00001");
        MarkerTo m2 = new MarkerTo("MDTM00002");
        MarkerTo m3 = new MarkerTo("MDTM00003");
        MarkerTo m4 = new MarkerTo("MDTM00004");
        MarkerTo m5 = new MarkerTo("MDTM00005");
        when(batchServiceMock.findMarkers(anyInt())).thenReturn(Set.of(m1, m2, m3, m4, m5));*/
        when(listServiceMock.findAllSampleNames(anyInt(), anyInt())).thenReturn(
            new ArrayList<>(List.of("MDTS220000001", "MDTS220000002", "MDTS220000003", "MDTS220000004")));
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
            .thenReturn(new ArrayList<>(of("MDTP3", "MDTP1", "MDTP2")));

        ClassPathResource r = new ClassPathResource("darTagSample.csv");

        MockMultipartFile mockFile;
        try {
            mockFile = new MockMultipartFile(r.getFilename(), r.getInputStream());
            UploadResultFileRequest dataStub = new UploadResultFileRequest();
            dataStub.setFile(new MultipartFile[] { mockFile });
            dataStub.setTags(Arrays.asList("gigwa", "batchId:123"));
            String errors = subject.validate(dataStub);
            assertThat(errors).isEmpty();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void givenAllPlatesExistWhenValidateExistingPlatesThenReturnNoErrors() {
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
                .thenReturn(new ArrayList<>(of("MDTP3", "MDTP1", "MDTP2")));
        String actual = subject.validateExistingPlates(Set.of("MDTP1", "MDTP2", "MDTP3", "extraPlate"), 123);
        assertEquals("", actual);
    }

    @Test
    public void givenPlatesMissingWhenValidateExistingPlatesThenReturnErrorMessage() {
        when(batchServiceMock.getPlateListByBatchId(anyInt()))
                .thenReturn(new ArrayList<>(of("MDTP3", "MDTP1", "MDTP2")));
        String actual = subject.validateExistingPlates(Set.of("MDTP2", "extraPlate"), 123);
        assertEquals("There are missing plates in the file: MDTP1,MDTP3", actual);
    }

}

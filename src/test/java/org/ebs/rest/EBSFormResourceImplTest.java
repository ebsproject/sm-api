package org.ebs.rest;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.ebs.services.EbsFormService;
import org.ebs.services.to.EbsFormTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
public class EBSFormResourceImplTest {
    
    private MockMvc mockMvc;

    @Mock
    private EbsFormService ebsformService;

    @InjectMocks
    private EBSFormResourceImpl subject;
    
    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(subject).build();
    }

    @Test
    public void givenExistingFormWhenGetListEBSFormThenReturnEBSForm() throws Exception {
        when(ebsformService.findEbsFormById(anyInt())).thenReturn(new EbsFormTo());

        mockMvc.perform(get("/EbsForm/list-EBSForm?id=123"))
                .andDo(print())
                .andExpect(status().isOk());
    
        verify(ebsformService, times(1))
                .findEbsFormById(eq(123));
    }
}

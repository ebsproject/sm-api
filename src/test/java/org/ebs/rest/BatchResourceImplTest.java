package org.ebs.rest;

import static org.ebs.model.RequestStatusModel.REQ_STATUS_FILE_UPLOADED;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.UUID;

import org.ebs.rest.to.FileObject;
import org.ebs.rest.validation.ResultFileValidator;
import org.ebs.services.BatchService;
import org.ebs.services.GigwaServiceImpl;
import org.ebs.services.NotificationService;
import org.ebs.services.custom.BatchDesignService;
import org.ebs.services.custom.CSFileObject;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.FileObjectTo;
import org.ebs.services.to.custom.BatchCodeTo;
import org.ebs.services.to.custom.BatchDesignTo;
import org.ebs.services.to.custom.DesignTo;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.ebs.util.client.FileClient;
import org.ebs.util.client.RestCsClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.task.AsyncTaskExecutor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class BatchResourceImplTest {
    
    private MockMvc mockMvc;

    @Mock private BatchService batchService;
	@Mock private BatchDesignService batchDesignService;
	@Mock private NotificationService notificationService;
    @Mock private AsyncTaskExecutor taskExecutorPool;
    @Mock private ResultFileValidator fileValidator;
    @Mock private CSFileObject csFileObjectService;
    @Mock private FileClient fileClient;
    //we point to GigwaServiceImpl rather than GigwaService to use thenCallRealMethod() for simple funcions
    @Mock private GigwaServiceImpl gigwaService; 
    @Mock private RestCsClient csClient;
    
    private ObjectMapper om = new ObjectMapper();

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new BatchResourceImpl(batchService
        ,batchDesignService
        ,notificationService
        ,taskExecutorPool
        ,fileValidator
        ,csFileObjectService
        ,fileClient
        ,gigwaService
        ,csClient))
            .build();
    }

    @Test
    public void givenValidRequestWhenGetListRequestByBatchThenReturnMatchingRequests() throws Exception {
        when(batchService.getRequesByBatchID(anyInt())).thenReturn(Page.empty());

        mockMvc.perform(get("/batch/list-request-byBatchID?batchId=123"))
                .andDo(print())
                .andExpect(status().isOk());
    
        verify(batchService, atLeastOnce())
                .getRequesByBatchID(eq(123));
    }

    @Test
    public void givenValidInputWhenCreateSamplesTaskThenSuccess() throws Exception {
        when(batchDesignService.saveBatch(any())).thenReturn(new BatchTo(123));
        when(batchService.getListBatchForDesign(anyInt()))
                .thenReturn(new PageImpl<>(List.of(new DesignTo())));

        mockMvc.perform(post("/batch/createSamplesTask")
                .contentType(APPLICATION_JSON)
                .content(om.writeValueAsString(new BatchDesignTo())))
                .andDo(print())
                .andExpect(status().isOk());

        verify(taskExecutorPool, atLeastOnce())
            .execute(any());
            
    }

    @Test
    public void givenBatchExistsWhenGetListBatchForDesignThenReturnDesign() throws Exception {
        when(batchService.getListBatchForDesign(anyInt()))
                .thenReturn(new PageImpl<>(List.of(new DesignTo(), new DesignTo())));

        mockMvc.perform(get("/batch/list-byBatchIDToDesign?batchId=123"))
                .andDo(print())
                .andExpect(status().isOk());

        verify(batchService, atLeastOnce()).getListBatchForDesign(eq(123));
    }

    @Test
    public void givenBatchExistsWhencreateRequestListMemeberThenSuccess() throws Exception {

        mockMvc.perform(get("/batch/createRequestListMemberById?batchId=123&requestId=11&requestId=22"))
                .andDo(print())
                .andExpect(status().isOk());

        verify(batchDesignService, atLeastOnce())
                .createRequestListMemberElements(eq(123), eq(new int[] { 11, 22 }));
    }

    @Test
    public void givenValidInputWhenCreateBatchCodeThenReturnCode() throws Exception {

        when(batchDesignService.createBatchCode(any()))
                .thenReturn("aCode");

        mockMvc.perform(post("/batch/createBatchCode")
                .contentType(APPLICATION_JSON)
                .content(om.writeValueAsString(new BatchCodeTo())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result").value("aCode"));

        verify(batchDesignService, atLeastOnce())
                .createBatchCode(any());
    }
    
    @Test
    public void givenValidResultFileWhenPostObjectThenSuccess() throws Exception {
        FileObject fileResp = new FileObject("aKey", "aName", 1, 1122, List.of("tag1", "tag2"));
        when(fileValidator.findErrors(any())).thenReturn("");
        when(gigwaService.uploadFile(eq(1), any(), any(), anyString(), eq(false)))
                .thenReturn(BrapiResponseBuilder.forData(
                        new PageImpl<>(List.of(fileResp))).withStatusSuccess().build());
        when(csClient.callGigwaLoader(any()))
                .thenReturn("success");

        mockMvc.perform(multipart("/batch/upload-results/1")
                .file(new MockMultipartFile("aFileName", "file content".getBytes()))
                .param("tags", "batchId:123", "tag2")
                .param("description", "some desc"))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.result.data[0].key").value("aKey"));

        verify(batchDesignService, atLeastOnce())
                .changeRequestStatusByBatch(eq(123), eq(1), eq(REQ_STATUS_FILE_UPLOADED), eq(""));
        verify(csClient, atLeastOnce())
                .callGigwaLoader(any());
    }
    
    @Test
    public void givenResultFileWithErrorsWhenPostConfirmationAndAcceptedThenSuccess() throws Exception {
        UUID key = UUID.randomUUID();
        FileObjectTo fileObject = new FileObjectTo(key, "1", "aFile", 11, 1122, new String[] {"tag1","tag2"}, "aDesc",
                REQ_STATUS_FILE_UPLOADED, 1, null, null);
        when(csFileObjectService.findByKey(any())).thenReturn(fileObject);

        mockMvc.perform(put("/batch/upload-results/622ceaed-d315-4439-a24f-a794c2d7bcad?accepted=true"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.result.key").value(key.toString()));
        
        verify(csFileObjectService, atLeastOnce()).modify(any());
        verify(csClient, atLeastOnce()).callGigwaLoader(any());
    }

    @Test
    public void givenResultFileWithErrorsWhenPostConfirmationAndRejectedThenSuccess() throws Exception {
        UUID key = UUID.randomUUID();
        FileObjectTo fileObject = new FileObjectTo(key, "1", "aFile", 11, 1122, new String[] {"tag1","tag2"}, "aDesc",
                REQ_STATUS_FILE_UPLOADED, 1, null, null);
        when(csFileObjectService.findByKey(any())).thenReturn(fileObject);

        mockMvc.perform(put("/batch/upload-results/622ceaed-d315-4439-a24f-a794c2d7bcad?accepted=false"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.result.key").value(key.toString()));
        
        verify(fileClient, atLeastOnce()).deleteFile(any());
    }

    @Test
    public void givenGenotypeDataExistsWhenWhenGetGenotypesThenReturnGenotypes() throws Exception {
        when(gigwaService.createFileName(any())).thenReturn("aFileName.txt");
        
        //StreamingResponseBody requires async handling
        MvcResult mvcResult = mockMvc.perform(post("/batch/genotype")
                .contentType(APPLICATION_JSON)
                .content("{\"requestId\":9876}"))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(header().stringValues(HttpHeaders.CONTENT_TYPE, APPLICATION_OCTET_STREAM_VALUE))
                .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"aFileName.txt\""))
            .andExpect(request().asyncStarted())
            .andReturn();
            
        mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk());
        

        // Map<Integer, List<SampleGermplasmDetailTo>> sampleMap = new HashMap<>();
        // SampleGermplasmDetailTo sample1 = new SampleGermplasmDetailTo("sample1",10001, "germCode1", "designation1")
        //     ,sample2 = new SampleGermplasmDetailTo("sample2", 10002, "germCode2", "designation2")
        //     ,sample3 = new SampleGermplasmDetailTo("sample3", 10003, "germCode3", "designation3")
        //     ,sample4 = new SampleGermplasmDetailTo("sample4", 10004, "germCode4", "designation4");
        // sampleMap.put(11, List.of(sample1, sample2, sample3, sample4));

        // List<BrCall> calls = new ArrayList<>();
        // calls.add(new BrCall("ebs$sample1$1000", "ebs$variant1", null, new BrGenotype(new String[]{"A/T"})));
        // calls.add(new BrCall("ebs$sample1$1000", "ebs$variant2", null, new BrGenotype(new String[]{"A/A"})));
        // calls.add(new BrCall("ebs$sample2$1001", "ebs$variant1", null, new BrGenotype(new String[]{"T/T"})));
        // calls.add(new BrCall("ebs$sample2$1001", "ebs$variant2", null, new BrGenotype(new String[]{"T/A"})));
        // calls.add(new BrCall("ebs$sample3$1002", "ebs$variant1", null, new BrGenotype(new String[]{"G/T"})));
        // calls.add(new BrCall("ebs$sample3$1002", "ebs$variant2", null, new BrGenotype(new String[]{"G/A"})));
        // calls.add(new BrCall("ebs$sample4$1003", "ebs$variant1", null, new BrGenotype(new String[]{"C/T"})));
        // calls.add(new BrCall("ebs$sample4$1003", "ebs$variant2", null, new BrGenotype(new String[] { "C/A" })));
        // UUID uuid = UUID.randomUUID();
        // BatchTo batchMock = new BatchTo(12345);
        // batchMock.setResultFileId(uuid);

        // when((gigwaService.smSampleName(any(BrCall.class)))).thenCallRealMethod();
        // when((gigwaService.smMarkerName(any(BrCall.class)))).thenCallRealMethod();
        // when((gigwaService.genotypeOf(any(BrCall.class)))).thenCallRealMethod();
        // when(batchService.findSampleGermplasmDetails(any(GenotypeSearchParams.class)))
        //         .thenReturn(sampleMap);
        // when(batchService.findBatch(anyInt())).thenReturn(
        //     Optional.of(batchMock));
        // when(csFileObjectService.findByKey(any())).thenReturn(
        //     new FileObjectTo(uuid,"7","dumyName",1,112233,null,null,0,0,null,null));
        // when(gigwaService.getStudy(anyString())).thenReturn(
        //     new BrStudy("aStudyName", "aType", "aTrialId", "aStudyDbId"));
        // when(gigwaService.searchCalls(anyList())).thenReturn(calls);
        
    }
}

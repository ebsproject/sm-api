package org.ebs.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.ebs.rest.to.GigwaLoadTO;
import org.ebs.services.GigwaLoader;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class GigwaResourceImplTest {
    
    private MockMvc mockMvc;

    private ObjectMapper om = new ObjectMapper();

    @Mock
    private GigwaLoader gigwaLoader;

    @InjectMocks
    private GigwaResourceImpl subject;
    
    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(subject).build();
    }

    @Test
    public void givenExistingFormWhenGetListEBSFormThenReturnEBSForm() throws Exception {
        String uuid = "5db3067d-336f-4052-b787-6c0851930a53";
        
        when(gigwaLoader.loadUnprocessedFiles(any())).thenReturn(true);
        when(gigwaLoader.generateRequestResultFiles(any()))
            .thenReturn(BrapiResponseBuilder.forData("success").build());

        mockMvc.perform(post("/gigwa/load")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(new GigwaLoadTO(uuid))))
            .andDo(print())
                .andExpect(status().isOk())
            .andExpect(content().string("5db3067d-336f-4052-b787-6c0851930a53 - success"));
    
    }
}

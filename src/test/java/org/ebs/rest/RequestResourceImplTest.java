package org.ebs.rest;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.ebs.services.RequestService;
import org.ebs.services.custom.CBGermplasmService;
import org.ebs.services.to.RequestTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class RequestResourceImplTest {
    
    private MockMvc mockMvc;

    @Mock
	private RequestService requestService;
    @Mock
	private CBGermplasmService CBGermplasmService;  

    private ObjectMapper om = new ObjectMapper();

    @InjectMocks
    private RequestResourceImpl subject;
    
    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(subject).build();
    }   

    @Test
    public void callingCreateRequestCode() throws Exception {
        when(requestService.findRequests(eq(null), eq(null), eq(null), eq(false)))
                .thenReturn(new PageImpl<>(List.of(new RequestTo())));

        mockMvc.perform(get("/request/createRequestCode"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result")
                        .value("RLB-" + new SimpleDateFormat("YYYYMMdd").format(new Date()) + "-2"));

    }

    @Test
    public void callingCreateRequestListMembers() throws Exception {
        
        mockMvc.perform(get("/request/createRequestListMemberById?listID=123&entityTypeId=3&requestId=112233"))
            .andExpect(status().isOk());
            
        verify(CBGermplasmService, times(1)).saveGermplasListFromCB(eq(123), eq(3), eq(112233));
    }
}
 
package org.ebs.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.ebs.services.custom.LaboratoryReportService;
import org.ebs.services.to.custom.BatchDesignTo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
public class LaboratoryReportImplTest {
    
    private MockMvc mockMvc;

    @Mock
    private LaboratoryReportService laboratoryReportService;

    private ObjectMapper om = new ObjectMapper();

    @InjectMocks
    private LaboratoryReportImpl subject;
    
    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(subject).build();
    }

    @Test
    public void givenExistingFormWhenGetListEBSFormThenReturnEBSForm() throws Exception {
        
        mockMvc.perform(post("/laboratory-report/createLaboratoryReport")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(new BatchDesignTo())))
            .andDo(print())
                .andExpect(status().isOk());

        verify(laboratoryReportService, times(1))
            .createReportLaboratory(any());

    }
}

package org.ebs.util.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.COOKIE;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Collections;
import java.util.Optional;

import org.ebs.util.brapi.BrCall;
import org.ebs.util.brapi.BrMetadata;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrPagination;
import org.ebs.util.brapi.BrResult;
import org.ebs.util.brapi.BrSample;
import org.ebs.util.brapi.BrStudy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class GigwaClientTest {
    @Mock
    RestTemplate restClientMock;
    @Mock
    GigwaTokenGenerator tokenGeneratorMock;

    @Mock
    FileSystemResource fileResourceMock;

    URI brEndpointMock = URI.create("some/uri");

    private GigwaClient subject;

    @BeforeEach
    public void init() {
        subject = new GigwaClient(restClientMock, brEndpointMock , tokenGeneratorMock);
    }

    @Test
    public void givenConfigurationIsCorrectWhenAuthorizedEntityThenReturnAuthorizedHttpEntity() {
        when(tokenGeneratorMock.getSessionID()).thenReturn("someID");

        HttpEntity<String> result = subject.authorizedEntity();

        assertThat(result.getHeaders().get(ACCEPT)).element(0)
                .isEqualTo("application/json");
        assertThat(result.getHeaders().get(COOKIE)).element(0)
                .isEqualTo("someID");
    }
    
    @Test
    public void givenExsiingFileResourceWhenSendImportThenSendFileToGigwa() {
        when(restClientMock.postForEntity(anyString(), any(), eq(String.class)))
                .thenReturn(ResponseEntity.of(Optional.of("aBody")));
        when(fileResourceMock.getFilename()).thenReturn("aFileName.hapmap");
        
        String result = subject.sendImport(fileResourceMock);

        assertThat(result).isEqualTo("aBody");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenStudyExistsInGigwaWhenGetStudyThenReturnBrStudy() {
        BrPagedResponse<BrStudy> resp1 = new BrPagedResponse<>(),
            resp2 = new BrPagedResponse<>();
        resp1.setMetadata(new BrMetadata(new BrPagination(2, 1, 3, 2), null, null));
        resp2.setMetadata(new BrMetadata(new BrPagination(2, 2, 3, 2), null, null));
        
        resp1.setResult(new BrResult<>(List.of(new BrStudy("studyA"), new BrStudy("studyB"))));
        resp2.setResult(new BrResult<>(List.of(new BrStudy("studyC"))));
        ResponseEntity<BrPagedResponse<BrStudy>> responseMock1 = ResponseEntity.of(Optional.of(resp1))
        , responseMock2 = ResponseEntity.of(Optional.of(resp2));


        when(restClientMock.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(responseMock1, responseMock2);

        Optional<BrStudy> result = subject.getStudy("studyB");
        assertThat(result).get().extracting("studyName").isEqualTo("studyB");
    }
    
    @Test
    public void givenStudyNotExistsInGigwaWhenGetStudyThenReturnEmpty() {

        Optional<BrStudy> result = subject.getStudy("studyB");
        assertThat(result).isEmpty();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenSamplesExistInGigwaWhenGetSamplesByGermplasmThenReturnSampleList() {
        List<ResponseEntity<BrPagedResponse<BrSample>>> responseMocks = dummySampleBrResponse();

        when(restClientMock.exchange(anyString(), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(responseMocks.get(0), responseMocks.get(1));

        List<BrSample> result = subject.getSamplesByGermplasm(List.of("11","22","33"));
        assertThat(result).extracting("sampleDbId").contains("sampleB","sampleC","sampleA");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenSamplesExistInGigwaWhenGetSamplesThenReturnSampleList() {
        List<ResponseEntity<BrPagedResponse<BrSample>>> responseMocks = dummySampleBrResponse();

        when(restClientMock.exchange(anyString(), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(responseMocks.get(0), responseMocks.get(1));

        List<BrSample> result = subject.getSamples("123");
        assertThat(result).extracting("sampleDbId").contains("sampleB","sampleC","sampleA");
    }

    private List<ResponseEntity<BrPagedResponse<BrSample>>> dummySampleBrResponse(){
        BrPagedResponse<BrSample> resp1 = new BrPagedResponse<>(),
            resp2 = new BrPagedResponse<>();
        resp1.setMetadata(new BrMetadata(new BrPagination(2, 1, 3, 2), null, null));
        resp2.setMetadata(new BrMetadata(new BrPagination(2, 2, 3, 2), null, null));
        
        resp1.setResult(new BrResult<>(List.of(new BrSample("sampleA"), new BrSample("sampleB"))));
        resp2.setResult(new BrResult<>(List.of(new BrSample("sampleC"))));
        ResponseEntity<BrPagedResponse<BrSample>> responseMock1 = ResponseEntity.of(Optional.of(resp1))
        , responseMock2 = ResponseEntity.of(Optional.of(resp2));

        return List.of(responseMock1, responseMock2);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenCallsExistInGigwaWhenGetCallsThenReturnCallList() {
        BrPagedResponse<BrCall> resp1 = new BrPagedResponse<>(),
            resp2 = new BrPagedResponse<>();
        resp1.setMetadata(new BrMetadata(new BrPagination(2, 1, 3, 2), null, null));
        resp2.setMetadata(new BrMetadata(new BrPagination(2, 2, 3, 2), null, null));
        
        resp1.setResult(new BrResult<>(List.of(new BrCall("callA"), new BrCall("callB"), new BrCall("callC"))));
        resp2.setResult(new BrResult<>(Collections.emptyList()));
        ResponseEntity<BrPagedResponse<BrCall>> responseMock1 = ResponseEntity.of(Optional.of(resp1))
        , responseMock2 = ResponseEntity.of(Optional.of(resp2));


        when(restClientMock.exchange(anyString(), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(responseMock1, responseMock2);

        List<BrCall> result = subject.getCalls(List.of("123","456","789"));
        assertThat(result).extracting("callSetDbId").contains("callB","callC","callA");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenModulesExistInGigwaWhenGetModulesThenReturnModuleMap() {
        GigwaModule gm1 = new GigwaModule(123, false, true, "aHost")
                , gm2 = new GigwaModule(123, false, true, "aHost");
        ResponseEntity<Map<String, GigwaModule>> resp = ResponseEntity.of(Optional.of(Map.of("ebs", gm1, "test", gm2)));

        when(restClientMock.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(resp);

        Map<String, GigwaModule> result = subject.listModules();

        assertThat(result).containsKeys("ebs", "test");

    }

    @Test
    public void givenEbsModuleNotExistInGigwaWhenCreateModuleThenReturnSuccess() {
        when(restClientMock.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class),
                eq(String.class)))
                .thenReturn(ResponseEntity.of(Optional.of("serverResponse")));

        String result = subject.createModule("testEbsModule");
        
        assertThat(result).isEqualTo("serverResponse");
    }

    @Test
    public void givenEbsModuletExistInGigwaWhenSetModuleVisibilityThenReturnSuccess() {
        when(restClientMock.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class),
                eq(String.class)))
                .thenReturn(ResponseEntity.of(Optional.of("serverResponse")));

        String result = subject.setModuleVisibility("testEbsModule", false, true);
        
        assertThat(result).isEqualTo("serverResponse");
    }

}

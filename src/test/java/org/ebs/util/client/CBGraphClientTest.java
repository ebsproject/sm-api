package org.ebs.util.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.ebs.services.to.custom.CropTo;
import org.ebs.services.to.custom.GraphqlTypeTo;
import org.ebs.services.to.custom.PlotTo;
import org.ebs.util.brapi.TokenGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class CBGraphClientTest {

    @Mock
    TokenGenerator tokenGeneratorMock;
    @Mock
    RestTemplate restClientMock;

    URI cbEndpointMock = URI.create("some/uri");

    private CBGraphClient subject;

    @BeforeEach
    public void init() {
        subject = new CBGraphClient(restClientMock, cbEndpointMock, tokenGeneratorMock);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenEntityExistsInCbWhenFindOneEntityThenReturnEntity() {
        PlotTo p = new PlotTo();
        p.setId(123);
        p.setPlotCode("aPlotCode");
        ResponseEntity<Map<String,Map<String,PlotTo>>> mappedResponse = 
        ResponseEntity.ok(Map.of("data", Map.of("findPlot", p)));

        when(restClientMock.exchange(any(URI.class), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(mappedResponse);

        PlotTo result = subject.findOneEntity("findPlot(id:123){id, name}", GraphqlTypeTo.PLOT);
        assertThat(result).extracting("id", "plotCode").contains(123, "aPlotCode");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenEntityExistsInMarkerDbWhenFindEntityListThenReturnEntityList() {

        ResponseEntity<Map<String,Map<String,Map<String,List<CropTo>>>>> mappedResponse = 
        ResponseEntity.ok(Map.of("data", Map.of("findCropList", Map.of("content", 
                List.of(new CropTo(1, "aRiceCode","rice"), new CropTo(2, "aMaizeCode", "maize"))))));

        when(restClientMock.exchange(any(URI.class), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(mappedResponse);

        List<CropTo> result = subject.findEntityList("findCropList{id name}", GraphqlTypeTo.CROP_LIST);
        assertThat(result).size().isEqualTo(2);
        assertThat(result).extracting("name").contains("rice", "maize");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenEntityExistsInMarkerDbWhenModifyEntityListThenSuccess() {

        ResponseEntity<Map<String,Map<String,CropTo>>> mappedResponse = 
        ResponseEntity.ok(Map.of("data", Map.of("modifyCrop", new CropTo(123, "aCode", "maize"))));

        when(restClientMock.exchange(any(URI.class), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(mappedResponse);

        CropTo result = subject.modifyEntity("modifyCrop(marker:{id:123 name:\"maize\"}){id name}", GraphqlTypeTo.CROP);
        assertThat(result).extracting("id","name").contains(123, "maize");
    }

}

package org.ebs.util.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

import java.io.IOException;
import java.util.Map;

import org.ebs.rest.to.GigwaLoadTO;
import org.ebs.util.brapi.TokenGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

@ExtendWith(MockitoExtension.class)
public class RestCsClientTest {

    @Mock
    private TokenGenerator tokenGeneratorMock;
    private static MockWebServer mockServer;
    private RestCsClient subject;

    @BeforeAll
    static void setUp() throws IOException {
        mockServer = new MockWebServer();
        mockServer.start();
    }
    @AfterAll
    static void shutdown() throws IOException {
        mockServer.shutdown();
    }

    @BeforeEach
    public void init() {
        subject = new RestCsClient(tokenGeneratorMock, String.format("http://%s:%s/graphql",
            mockServer.getHostName(), mockServer.getPort()));
    }

    @Test
    public void givenSequenceExistsWhenGetSequenceThenReturnStringCode(){
        mockServer.enqueue(new MockResponse().setBody("a-complex-code"));

        String result = subject.getSequence(2, Map.of("11", "aValue"));
        try {
            RecordedRequest recordedRequest = mockServer.takeRequest();
            assertEquals("POST", recordedRequest.getMethod());
            assertEquals("/sequence/next/2", recordedRequest.getPath());
            assertEquals("[text={\"11\":\"aValue\"}]",
                    recordedRequest.getBody().readByteString().toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals("a-complex-code", result);

    }

    @Test
    public void givenSequenceExistsWhenGetSequenceThenReturnStringArrayCode(){
        mockServer.enqueue(new MockResponse()
            .setBody("[\"code1\",\"code2\",\"code3\"]")
            .setHeader(CONTENT_TYPE, "application/json"));

        String[] result = subject.getSequence(2, Map.of("11", "aValue"), 3);
        try {
            RecordedRequest recordedRequest = mockServer.takeRequest();
            assertThat(recordedRequest.getMethod()).isEqualTo("POST");
            assertThat(recordedRequest.getPath()).isEqualTo("/sequence/next/2/batch/3");
            assertThat(recordedRequest.getBody().readByteString().toString())
                .isEqualTo("[text={\"11\":\"aValue\"}]");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertThat(result).contains("code1","code2","code3");

    }

    @Test
    public void givenSequenceNotExistsWhenGetSequenceThenReturnError(){
        mockServer.enqueue(new MockResponse().setResponseCode(200));

        Exception e = assertThrows(RuntimeException.class, () -> subject.getSequence(2, Map.of("11","aValue"), 3));
        assertThat(e.getMessage()).isEqualTo("Could not generate code with rule (2) and args {11=aValue}");

    }

    @Test
    public void givenBatchSequencesExistWhenGetBatchCodesThenReturnAllCodes(){
        mockServer.enqueue(new MockResponse()
            .setBody("[\"plate1\",\"plate2\"]")
            .setHeader(CONTENT_TYPE, "application/json"));
        mockServer.enqueue(new MockResponse()
            .setBody("[\"sample1\",\"sample2\",\"sample3\",\"sample4\",\"sample5\"]")
            .setHeader(CONTENT_TYPE, "application/json"));

        CompositeBatchRef result = subject.getBatchCodes(Map.of("11", "aValue"), 2, 5);

        assertThat(result.getPlateCodeList()).contains("plate1","plate2");
        assertThat(result.getSampleCodeList()).contains("sample1","sample2","sample3","sample4","sample5");

    }

    @Test
    public void givenProgramsByUserExistWhenGetProgramsThenReturnProgramList(){
        String body = "{\"programs\": [{\n" + //
                        "\"type\": \"Program\",\n" + //
                        "\"id\": 94,\n" + //
                        "\"externalDbId\": 103,\n" + //
                        "\"name\": \"KE Maize Breeding Program\",\n" + //
                        "\"code\": \"KE\"\n" + //
                        "},{\n" + //
                        "\"type\": \"Program\",\n" + //
                        "\"id\": 92,\n" + //
                        "\"externalDbId\": 101,\n" + //
                        "\"name\": \"Irrigated South-East Asia\",\n" + //
                        "\"code\": \"IRSEA\"\n" + //
                        "}]}";
        mockServer.enqueue(new MockResponse()
            .setBody(body)
            .setHeader(CONTENT_TYPE, "application/json"));
 
        String result = subject.getPrograms(999);

        try {
            RecordedRequest recordedRequest = mockServer.takeRequest();
            assertThat(recordedRequest.getMethod()).isEqualTo("GET");
            assertThat(recordedRequest.getPath()).isEqualTo("/user/999/programs");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertThat(result).isEqualTo(body);
    }

    @Test
    public void givenCsQueueIsUpWhenCallGigwaLoaderThenCallLoader(){
        String body = "{\"message\": \"Request received\"}";
        mockServer.enqueue(new MockResponse()
            .setBody(body)
            .setHeader(CONTENT_TYPE, "application/json"));

        String result = subject.callGigwaLoader(new GigwaLoadTO("asd-123"));

        try {
            RecordedRequest recordedRequest = mockServer.takeRequest();
            assertThat(recordedRequest.getMethod()).isEqualTo("POST");
            assertThat(recordedRequest.getPath()).isEqualTo("/queue/gigwa-loader");
            assertThat(recordedRequest.getBody().readByteString().toString())
                .isEqualTo("[text={\"id\":\"asd-123\"}]");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    
        assertThat(result).isEqualTo(body);

    }

}

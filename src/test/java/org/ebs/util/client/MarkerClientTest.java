package org.ebs.util.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.util.List;
import java.util.Collections;
import java.util.Map;

import org.ebs.services.to.custom.AssayTo;
import org.ebs.services.to.custom.GraphqlTypeTo;
import org.ebs.services.to.custom.MarkerPanelTo;
import org.ebs.services.to.custom.MarkerTo;
import org.ebs.util.brapi.TokenGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class MarkerClientTest {

    @Mock
    RestTemplate restClientMock;
    @Mock
    TokenGenerator tokenGeneratorMock;

    URI brEndpointMock = URI.create("some/uri");

    private MarkerClient subject;

    @BeforeEach
    public void init() {
        subject = new MarkerClient(restClientMock, brEndpointMock, tokenGeneratorMock);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenEntityExistsInMarkerDbWhenFindOneEntityThenReturnEntity() {

        ResponseEntity<Map<String,Map<String,MarkerPanelTo>>> mappedResponse = 
        ResponseEntity.ok(Map.of("data", Map.of("findMarkerGroup", new MarkerPanelTo(123,"aGroupName"))));

        when(restClientMock.exchange(any(URI.class), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(mappedResponse);

        MarkerPanelTo result = subject.findOneEntity("findMarkerGroup{id, name}", GraphqlTypeTo.MARKER_GROUP);
        assertThat(result).extracting("id", "name").contains(123, "aGroupName");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenEntityExistsInMarkerDbWhenFindEntityListThenReturnEntityList() {

        ResponseEntity<Map<String,Map<String,Map<String,List<MarkerTo>>>>> mappedResponse = 
        ResponseEntity.ok(Map.of("data", Map.of("findMarkerList", Map.of("content", 
                List.of(new MarkerTo(123,"markerA"), new MarkerTo(123,"markerB"))))));

        when(restClientMock.exchange(any(URI.class), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(mappedResponse);

        List<MarkerTo> result = subject.findEntityList("findMarkerList{id name}", GraphqlTypeTo.MARKER_LIST);
        assertThat(result).size().isEqualTo(2);
        assertThat(result).extracting("name").contains("markerA", "markerB");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenEntityExistsInMarkerDbWhenModifyEntityListThenSuccess() {

        ResponseEntity<Map<String,Map<String,MarkerTo>>> mappedResponse = 
        ResponseEntity.ok(Map.of("data", Map.of("modifyMarker", new MarkerTo(123,"aNewName"))));

        when(restClientMock.exchange(any(URI.class), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(mappedResponse);

        MarkerTo result = subject.modifyEntity("modifyMarker(marker:{id:123 name:\"aNewName\"}){id name}", GraphqlTypeTo.MARKER);
        assertThat(result).extracting("id","name").contains(123, "aNewName");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenAssayExistsInMarkerDbWhenSearchAssaysThenReturnAssayList() {
        AssaySearchResponse assayResp1 = new AssaySearchResponse();
        assayResp1.setNumberOfElements(2);
        assayResp1.setTotalPages(2);
        assayResp1.setContent(List.of(new AssayTo(123,"assay1"), new AssayTo(124,"assay2")));
        ResponseEntity<AssaySearchResponse> resp1 = ResponseEntity.ok(assayResp1);
        AssaySearchResponse assayResp2 = new AssaySearchResponse();
        assayResp2.setNumberOfElements(1);
        assayResp1.setTotalPages(2);
        assayResp2.setContent(List.of(new AssayTo(125,"assay3")));
        ResponseEntity<AssaySearchResponse> resp2 = ResponseEntity.ok(assayResp2);
        AssaySearchResponse assayResp3 = new AssaySearchResponse();
        assayResp3.setNumberOfElements(0);
        assayResp3.setContent(Collections.emptyList());
        ResponseEntity<AssaySearchResponse> resp3 = ResponseEntity.ok(assayResp3);

        when(restClientMock.exchange(anyString(), eq(HttpMethod.POST), any(HttpEntity.class),
                eq(AssaySearchResponse.class)))
                .thenReturn(resp1, resp2, resp3);

        List<AssayTo> result = subject.searchAssays("modifyMarker(marker:{id:123 name:\"aNewName\"}){id name}");
        assertThat(result).size().isEqualTo(3);
        assertThat(result).extracting("name").contains("assay1", "assay2", "assay3");
    }

}

package org.ebs.util.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.UUID;

import org.ebs.rest.to.FileObject;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrResult;
import org.ebs.util.brapi.TokenGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class FileClientTest {

    @Mock
    private RestTemplate restTemplateMock;
    @Mock
    private TokenGenerator tokenGeneratorMock;

    private FileClient subject;

    @BeforeEach
    public void init() {
        subject = new FileClient(restTemplateMock, URI.create("some/uri"), tokenGeneratorMock);
    }
    
    @Test
    public void givenFileExistsWhenDownloadThenReturnByteArray() {
        
        when(restTemplateMock.exchange(anyString(), eq(HttpMethod.GET), any(HttpEntity.class), eq(byte[].class)))
                .thenReturn(ResponseEntity.of(Optional.of("some-file-content".getBytes())));
        try {

            byte[] result = subject.download(1, UUID.randomUUID());

            assertThat(new String(result))
                    .isEqualTo("some-file-content");

        } catch (IllegalStateException | IOException e) {
            assertTrue("Exception not expected: " + e.getMessage(), false);
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenFileExistsWhenDownloadFileThenReturnTempFile() {
        File f;
        try{
            f = new ClassPathResource("darTagSample.csv").getFile();
            when(restTemplateMock.execute(anyString(), eq(HttpMethod.GET), any(RequestCallback.class), any(ResponseExtractor.class)))
                    .thenReturn(f);

            Scanner result = new Scanner(subject.downloadFile(1, UUID.randomUUID()));

    
            assertThat(result.nextLine())
                    .isEqualTo("PLATE_ID,WELL,SUBJECT_ID,MDTM00001,MDTM00002,MDTM00003,MDTM00004,MDTM00005");
            result.close();
        } catch (IllegalStateException | IOException e) {
            assertTrue("Exception not expected: " + e.getMessage(), false);
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenFileExistsWhenDeleteThenReturnSuccess() {
        BrResponse<String> brResp = new BrResponse<>();
        brResp.setResult("deleted-id");
        when(restTemplateMock.exchange(anyString(), eq(HttpMethod.DELETE), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(ResponseEntity.of(Optional.of(brResp)));

        String result = subject.deleteFile(UUID.randomUUID().toString());

        assertThat(result).isEqualTo("deleted-id");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void givenFileServiceUpWhenUploadFileThenSuccess() {
        BrPagedResponse<FileObject> brResp = new BrPagedResponse<>();
        brResp.setResult(new BrResult<>(List.of(
                new FileObject("aKey", "aName", 1, 1100, null))));
            
        when(restTemplateMock.exchange(anyString(), eq(HttpMethod.POST), any(HttpEntity.class),
                any(ParameterizedTypeReference.class)))
                .thenReturn(ResponseEntity.of(Optional.of(brResp)));

        FileUploadForm form = new FileUploadForm(1, new FileSystemResource("somePath"), List.of("aTag") , "aDescription", 1);
        
        FileObject result = subject.uploadFile(form);

        assertThat(result).extracting("key", "name", "size").contains("aKey", "aName", 1100L);
    }
}

package org.ebs.graphql;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.ebs.services.BatchCodeService;
import org.ebs.services.BatchMarkerGroupCustomService;
import org.ebs.services.BatchMarkerGroupService;
import org.ebs.services.BatchService;
import org.ebs.services.CheckPropService;
import org.ebs.services.CollectionContainerTypeService;
import org.ebs.services.CollectionLayoutService;
import org.ebs.services.ComponentService;
import org.ebs.services.ComponentTypeService;
import org.ebs.services.ContainerSetService;
import org.ebs.services.ControlTypeService;
import org.ebs.services.DataFormatService;
import org.ebs.services.DataTypeService;
import org.ebs.services.EbsFormService;
import org.ebs.services.FieldService;
import org.ebs.services.FormService;
import org.ebs.services.FormTypeService;
import org.ebs.services.GroupService;
import org.ebs.services.HelperService;
import org.ebs.services.HistoricalStorageService;
import org.ebs.services.InputPropService;
import org.ebs.services.LabControlService;
import org.ebs.services.LabLayoutControlService;
import org.ebs.services.LabLayoutService;
import org.ebs.services.LoadTypeService;
import org.ebs.services.MixtureMethodService;
import org.ebs.services.OptionService;
import org.ebs.services.PurposeService;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.RequestService;
import org.ebs.services.RequestStatusService;
import org.ebs.services.RequestTraitService;
import org.ebs.services.ResultPreferenceDetailService;
import org.ebs.services.ResultPreferenceService;
import org.ebs.services.SampleDetailService;
import org.ebs.services.SampleMixtureDetailService;
import org.ebs.services.SampleMixtureService;
import org.ebs.services.SampleService;
import org.ebs.services.ServiceProviderService;
import org.ebs.services.ServiceService;
import org.ebs.services.ServiceTypeService;
import org.ebs.services.ShipmentDetailService;
import org.ebs.services.ShipmentService;
import org.ebs.services.ShipmentSetService;
import org.ebs.services.StatusService;
import org.ebs.services.StockService;
import org.ebs.services.StorageLocationService;
import org.ebs.services.TabService;
import org.ebs.services.TissueTypeService;
import org.ebs.services.ValidationRegexService;
import org.ebs.services.ValueService;
import org.ebs.services.custom.CBGermplasmService;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class QueryResolverTest {

    @Mock private BatchCodeService batchCodeService;
	@Mock private BatchService batchService;
	@Mock private CollectionContainerTypeService collectionContainerTypeService;
	@Mock private CollectionLayoutService collectionLayoutService;
	@Mock private ContainerSetService containerSetService;
	@Mock private ControlTypeService controlTypeService;
	@Mock private DataFormatService dataFormatService;
	@Mock private HistoricalStorageService historicalStorageService;
	@Mock private LabControlService labControlService;
	@Mock private LabLayoutControlService labLayoutControlService;
	@Mock private LabLayoutService labLayoutService;
	@Mock private LoadTypeService loadTypeService;
	@Mock private MixtureMethodService mixtureMethodService;
	@Mock private PurposeService purposeService;
	@Mock private RequestListMemberService requestListMemberService;
	@Mock private RequestService requestService;
	@Mock private RequestStatusService requestStatusService;
	@Mock private ResultPreferenceDetailService resultPreferenceDetailService;
	@Mock private ResultPreferenceService resultPreferenceService;
	@Mock private SampleDetailService sampleDetailService;
	@Mock private SampleMixtureDetailService sampleMixtureDetailService;
	@Mock private SampleMixtureService sampleMixtureService;
	@Mock private SampleService sampleService;
	@Mock private ServiceProviderService serviceProviderService;
	@Mock private ServiceService serviceService;
	@Mock private ServiceTypeService serviceTypeService;
	@Mock private ShipmentDetailService shipmentDetailService;
	@Mock private ShipmentService shipmentService;
	@Mock private ShipmentSetService shipmentSetService;
	@Mock private StatusService statusService;
	@Mock private StockService stockService;
	@Mock private StorageLocationService storageLocationService;
	@Mock private TissueTypeService tissueTypeService;
	@Mock private CheckPropService checkPropService;
	@Mock private ComponentService componentService;
	@Mock private ComponentTypeService componentTypeService;
	@Mock private DataTypeService dataTypeService;
	@Mock private EbsFormService ebsFormService;
	@Mock private FieldService fieldService;
	@Mock private FormService formService;
	@Mock private FormTypeService formTypeService;
	@Mock private GroupService groupService;
	@Mock private HelperService helperService;
	@Mock private InputPropService inputPropService;
	@Mock private OptionService optionService;
	@Mock private TabService tabService;
	@Mock private ValidationRegexService validationRegexService;
	@Mock private ValueService valueService;
	@Mock private CBGermplasmService cBGermplasmService;
	@Mock private RequestTraitService requestTraitService; 
	@Mock private BatchMarkerGroupService batchMarkerGroupService;	
	@Mock private BatchMarkerGroupCustomService batchMarkerGroupCustomService;

    @InjectMocks
    private QueryResolver subject;

    @Test
	public void findBatchTest(){
		subject.findBatch(0);
		verify (batchService, times(1)).findBatch(anyInt());
	}

	@Test
	public void findBatchCodeTest(){
		subject.findBatchCode(0);
		verify (batchCodeService, times(1)).findBatchCode(anyInt());
	}

	@Test
	public void findBatchCodeListTest(){
		subject.findBatchCodeList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (batchCodeService, times(1)).findBatchCodes(any(), any(), any());
	}

	@Test
	public void findBatchListTest(){
		subject.findBatchList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify(batchService,times(1)).findBatchs(any(), any(), any(), eq(true));
	}

	@Test
	public void findPlateListTest(){
		subject.findPlateList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (batchService, times(1)).findPlateList(any(), any(), any(), eq(true));
	}

	@Test
	public void findCollectionContainerTypeTest(){
		subject.findCollectionContainerType(0);
		verify (collectionContainerTypeService, times(1)).findCollectionContainerType(anyInt());
	}

	@Test
	public void findCollectionContainerTypeListTest(){
		subject.findCollectionContainerTypeList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (collectionContainerTypeService, times(1)).findCollectionContainerTypes(any(), any(), any());
	}

	@Test
	public void findCollectionLayoutTest(){
		subject.findCollectionLayout(0);
		verify (collectionLayoutService, times(1)).findCollectionLayout(anyInt());
	}

	@Test
	public void findCollectionLayoutListTest(){
		subject.findCollectionLayoutList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (collectionLayoutService, times(1)).findCollectionLayouts(any(), any(), any());
	}

	@Test
	public void findContainerSetTest(){
		subject.findContainerSet(0);
		verify (containerSetService, times(1)).findContainerSet(anyInt());
	}

	@Test
	public void findContainerSetListTest(){
		subject.findContainerSetList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (containerSetService, times(1)).findContainerSets(any(), any(), any());
	}

	@Test
	public void findControlTypeTest(){
		subject.findControlType(0);
		verify (controlTypeService, times(1)).findControlType(anyInt());
	}

	@Test
	public void findControlTypeListTest(){
		subject.findControlTypeList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (controlTypeService, times(1)).findControlTypes(any(), any(), any());
	}

	@Test
	public void findDataFormatTest(){
		subject.findDataFormat(0);
		verify (dataFormatService, times(1)).findDataFormat(anyInt());
	}

	@Test
	public void findDataFormatListTest(){
		subject.findDataFormatList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (dataFormatService, times(1)).findDataFormats(any(), any(), any());
	}

	@Test
	public void findHistoricalStorageTest(){
		subject.findHistoricalStorage(0);
		verify (historicalStorageService, times(1)).findHistoricalStorage(anyInt());
	}

	@Test
	public void findHistoricalStorageListTest(){
		subject.findHistoricalStorageList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (historicalStorageService, times(1)).findHistoricalStorages(any(), any(), any());
	}

	@Test
	public void findLabControlTest(){
		subject.findLabControl(0);
		verify (labControlService, times(1)).findLabControl(anyInt());
	}

	@Test
	public void findLabControlListTest(){
		subject.findLabControlList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (labControlService, times(1)).findLabControls(any(), any(), any());
	}

	@Test
	public void findLabLayoutTest(){
		subject.findLabLayout(0);
		verify (labLayoutService, times(1)).findLabLayout(anyInt());
	}

	@Test
	public void findLabLayoutControlTest(){
		subject.findLabLayoutControl(0);
		verify (labLayoutControlService, times(1)).findLabLayoutControl(anyInt());
	}

	@Test
	public void findLabLayoutControlListTest(){
		subject.findLabLayoutControlList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (labLayoutControlService, times(1)).findLabLayoutControls(any(), any(), any());
	}

	@Test
	public void findLabLayoutListTest(){
		subject.findLabLayoutList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (labLayoutService, times(1)).findLabLayouts(any(), any(), any());
	}

	@Test
	public void findLoadTypeTest(){
		subject.findLoadType(0);
		verify (loadTypeService, times(1)).findLoadType(anyInt());
	}

	@Test
	public void findLoadTypeListTest(){
		subject.findLoadTypeList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (loadTypeService, times(1)).findLoadTypes(any(), any(), any());
	}

	@Test
	public void findMixtureMethodTest(){
		subject.findMixtureMethod(0);
		verify (mixtureMethodService, times(1)).findMixtureMethod(anyInt());
	}

	@Test
	public void findMixtureMethodListTest(){
		subject.findMixtureMethodList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (mixtureMethodService, times(1)).findMixtureMethods(any(), any(), any());
	}

	@Test
	public void findPurposeTest(){
		subject.findPurpose(0);
		verify (purposeService, times(1)).findPurpose(anyInt());
	}

	@Test
	public void findPurposeListTest(){
		subject.findPurposeList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (purposeService, times(1)).findPurposes(any(), any(), any(), eq(true));
	}

	@Test
	public void findRequestTest(){
		subject.findRequest(0);
		verify (requestService, times(1)).findRequest(anyInt());
	}

	@Test
	public void findRequestListTest(){
		subject.findRequestList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (requestService, times(1)).findRequests(any(), any(), any(), eq(true));
	}

	@Test
	public void findListListTest() {
		subject.findListList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (cBGermplasmService, times(1)).getListFromCB(any(), any(), any(), eq(true));
	}
	@Test
	public void findRequestListMemberTest(){
		subject.findRequestListMember(0);
		verify (requestListMemberService, times(1)).findRequestListMember(anyInt());
	}

	@Test
	public void findRequestTraitTest(){
		subject.findRequestTrait(0);
		verify (requestTraitService, times(1)).findRequestTrait(anyInt());
	}
	@Test
	public void findRequestListMemberListTest(){
		subject.findRequestListMemberList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (requestListMemberService, times(1)).findRequestListMembers(any(), any(), any());
	}

	@Test
	public void findRequestTraitListTest(){
		subject.findRequestTraitList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (requestTraitService, times(1)).findRequestTraits(any(), any(), any());
	}

	@Test
	public void findRequestStatusTest(){
		subject.findRequestStatus(0);
		verify (requestStatusService, times(1)).findRequestStatus(anyInt());
	}

	@Test
	public void findRequestStatusListTest(){
		subject.findRequestStatusList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (requestStatusService, times(1)).findRequestStatuss(any(), any(), any(), eq(true));
	}

	@Test
	public void findResultPreferenceTest(){
		subject.findResultPreference(0);
		verify (resultPreferenceService, times(1)).findResultPreference(anyInt());
	}

	@Test
	public void findResultPreferenceDetailTest(){
		subject.findResultPreferenceDetail(0);
		verify (resultPreferenceDetailService, times(1)).findResultPreferenceDetail(anyInt());
	}

	@Test
	public void findResultPreferenceDetailListTest(){
		subject.findResultPreferenceDetailList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (resultPreferenceDetailService, times(1)).findResultPreferenceDetails(any(), any(), any());
	}

	@Test
	public void findResultPreferenceListTest(){
		subject.findResultPreferenceList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (resultPreferenceService, times(1)).findResultPreferences(any(), any(), any());
	}

	@Test
	public void findSampleTest(){
		subject.findSample(0);
		verify (sampleService, times(1)).findSample(anyInt());
	}

	@Test
	public void findSampleDetailTest(){
		subject.findSampleDetail(0);
		verify (sampleDetailService, times(1)).findSampleDetail(anyInt());
	}

	@Test
	public void findSampleDetailListTest(){
		subject.findSampleDetailList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (sampleDetailService, times(1)).findSampleDetails(any(), any(), any(), eq(true));
	}

	@Test
	public void findSampleListTest(){
		subject.findSampleList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (sampleService, times(1)).findSamples(any(), any(), any());
	}

	@Test
	public void findSampleMixtureTest(){
		subject.findSampleMixture(0);
		verify (sampleMixtureService, times(1)).findSampleMixture(anyInt());
	}

	@Test
	public void findSampleMixtureDetailTest(){
		subject.findSampleMixtureDetail(0);
		verify (sampleMixtureDetailService, times(1)).findSampleMixtureDetail(anyInt());
	}

	@Test
	public void findSampleMixtureDetailListTest(){
		subject.findSampleMixtureDetailList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (sampleMixtureDetailService, times(1)).findSampleMixtureDetails(any(), any(), any());
	}

	@Test
	public void findSampleMixtureListTest(){
		subject.findSampleMixtureList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (sampleMixtureService, times(1)).findSampleMixtures(any(), any(), any());
	}

	@Test
	public void findServiceTest(){
		subject.findService(0);
		verify (serviceService, times(1)).findService(anyInt());
	}

	@Test
	public void findServiceListTest(){
		subject.findServiceList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (serviceService, times(1)).findServices(any(), any(), any(), eq(true));
	}

	@Test
	public void findServiceProviderTest(){
		subject.findServiceProvider(0);
		verify (serviceProviderService, times(1)).findServiceProvider(anyInt());
	}

	@Test
	public void findServiceProviderListTest(){
		subject.findServiceProviderList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (serviceProviderService, times(1)).findServiceProviders(any(), any(), any(), eq(true));
	}

	@Test
	public void findServiceTypeTest(){
		subject.findServiceType(0);
		verify (serviceTypeService, times(1)).findServiceType(anyInt());
	}

	@Test
	public void findServiceTypeListTest(){
		subject.findServiceTypeList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (serviceTypeService, times(1)).findServiceTypes(any(), any(), any(), eq(true));
	}

	@Test
	public void findShipmentTest(){
		subject.findShipment(0);
		verify (shipmentService, times(1)).findShipment(anyInt());
	}

	@Test
	public void findShipmentDetailTest(){
		subject.findShipmentDetail(0);
		verify (shipmentDetailService, times(1)).findShipmentDetail(anyInt());
	}

	@Test
	public void findShipmentDetailListTest(){
		subject.findShipmentDetailList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (shipmentDetailService, times(1)).findShipmentDetails(any(), any(), any());
	}

	@Test
	public void findShipmentListTest(){
		subject.findShipmentList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (shipmentService, times(1)).findShipments(any(), any(), any());
	}

	@Test
	public void findShipmentSetTest(){
		subject.findShipmentSet(0);
		verify (shipmentSetService, times(1)).findShipmentSet(anyInt());
	}

	@Test
	public void findShipmentSetListTest(){
		subject.findShipmentSetList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (shipmentSetService, times(1)).findShipmentSets(any(), any(), any());
	}

	@Test
	public void findStatusTest(){
		subject.findStatus(0);
		verify (statusService, times(1)).findStatus(anyInt());
	}

	@Test
	public void findStatusListTest(){
		subject.findStatusList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (statusService, times(1)).findStatuss(any(), any(), any());
	}

	@Test
	public void findStockTest(){
		subject.findStock(0);
		verify (stockService, times(1)).findStock(anyInt());
	}

	@Test
	public void findStockListTest(){
		subject.findStockList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (stockService, times(1)).findStocks(any(), any(), any());
	}

	@Test
	public void findStorageLocationTest(){
		subject.findStorageLocation(0);
		verify (storageLocationService, times(1)).findStorageLocation(anyInt());
	}

	@Test
	public void findStorageLocationListTest(){
		subject.findStorageLocationList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (storageLocationService, times(1)).findStorageLocations(any(), any(), any());
	}


	@Test
	public void findTissueTypeTest(){
		subject.findTissueType(0);
		verify (tissueTypeService, times(1)).findTissueType(anyInt());
	}

	@Test
	public void findTissueTypeListTest(){
		subject.findTissueTypeList(new PageInput(), new SortInput(), List.of(new FilterInput()), true);
		verify (tissueTypeService, times(1)).findTissueTypes(any(), any(), any(), eq(true));
	}


	
	@Test
	public void findCheckPropTest(){
		subject.findCheckProp(0);
		verify (checkPropService, times(1)).findCheckProp(anyInt());
	}

	@Test
	public void findCheckPropListTest(){
		subject.findCheckPropList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (checkPropService, times(1)).findCheckProps(any(), any(), any());
	}

	@Test
	public void findComponentTest(){
		subject.findComponent(0);
		verify (componentService, times(1)).findComponent(anyInt());
	}

	@Test
	public void findComponentListTest(){
		subject.findComponentList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (componentService, times(1)).findComponents(any(), any(), any());
	}

	@Test
	public void findComponentTypeTest(){
		subject.findComponentType(0);
		verify (componentTypeService, times(1)).findComponentType(anyInt());
	}

	@Test
	public void findComponentTypeListTest(){
		subject.findComponentTypeList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (componentTypeService, times(1)).findComponentTypes(any(), any(), any());
	}

	@Test
	public void findDataTypeTest(){
		subject.findDataType(0);
		verify (dataTypeService, times(1)).findDataType(anyInt());
	}

	@Test
	public void findDataTypeListTest(){
		subject.findDataTypeList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (dataTypeService, times(1)).findDataTypes(any(), any(), any());
	}

	@Test
	public void findEbsFormTest(){
		subject.findEbsForm(0);
		verify (ebsFormService, times(1)).findEbsForm(anyInt());
	}

	@Test
	public void findEbsFormListTest(){
		subject.findEbsFormList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (ebsFormService, times(1)).findEbsForms(any(), any(), any());
	}

	@Test
	public void findFieldTest(){
		subject.findField(0);
		verify (fieldService, times(1)).findField(anyInt());
	}

	@Test
	public void findFieldListTest(){
		subject.findFieldList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (fieldService, times(1)).findFields(any(), any(), any());
	}

	@Test
	public void findFormTest(){
		subject.findForm(0);
		verify (formService, times(1)).findForm(anyInt());
	}

	@Test
	public void findFormListTest(){
		subject.findFormList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (formService, times(1)).findForms(any(), any(), any());
	}

	@Test
	public void findFormTypeTest(){
		subject.findFormType(0);
		verify (formTypeService, times(1)).findFormType(anyInt());
	}

	@Test
	public void findFormTypeListTest(){
		subject.findFormTypeList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (formTypeService, times(1)).findFormTypes(any(), any(), any());
	}

	@Test
	public void findGroupTest(){
		subject.findGroup(0);
		verify (groupService, times(1)).findGroup(anyInt());
	}

	@Test
	public void findGroupListTest(){
		subject.findGroupList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (groupService, times(1)).findGroups(any(), any(), any());
	}

	@Test
	public void findHelperTest(){
		subject.findHelper(0);
		verify (helperService, times(1)).findHelper(anyInt());
	}

	@Test
	public void findHelperListTest(){
		subject.findHelperList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (helperService, times(1)).findHelpers(any(), any(), any());
	}

	@Test
	public void findInputPropTest(){
		subject.findInputProp(0);
		verify (inputPropService, times(1)).findInputProp(anyInt());
	}

	@Test
	public void findInputPropListTest(){
		subject.findInputPropList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (inputPropService, times(1)).findInputProps(any(), any(), any());
	}

	@Test
	public void findOptionTest(){
		subject.findOption(0);
		verify (optionService, times(1)).findOption(anyInt());
	}

	@Test
	public void findOptionListTest(){
		subject.findOptionList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (optionService, times(1)).findOptions(any(), any(), any());
	}

	@Test
	public void findTabTest(){
		subject.findTab(0);
		verify (tabService, times(1)).findTab(anyInt());
	}

	@Test
	public void findTabListTest(){
		subject.findTabList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (tabService, times(1)).findTabs(any(), any(), any());
	}
	
	@Test
	public void findValidationRegexTest(){
		subject.findValidationRegex(0);
		verify (validationRegexService, times(1)).findValidationRegex(anyInt());
	}

	@Test
	public void findValidationRegexListTest(){
		subject.findValidationRegexList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (validationRegexService, times(1)).findValidationRegexs(any(), any(), any());
	}

	@Test
	public void findValueTest(){
		subject.findValue(0);
		verify (valueService, times(1)).findValue(anyInt());
	}

	@Test
	public void findValueListTest(){
		subject.findValueList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (valueService, times(1)).findValues(any(), any(), any());
	}

	@Test
	public void findBatchMarkerGroupTest(){
		subject.findBatchMarkerGroup(0);
		verify (batchMarkerGroupService, times(1)).findBatchMarkerGroup(anyInt());
	}

	@Test
	public void findBatchMarkerGroupListTest(){
		subject.findBatchMarkerGroupList(new PageInput(), new SortInput(), List.of(new FilterInput()));
		verify (batchMarkerGroupService, times(1)).findBatchMarkerGroups(any(), any(), any());
	}

	@Test
	public void findBatchMarkerGroupCustomTest(){
		subject.findBatchMarkerGroupCustom(0);
		verify (batchMarkerGroupCustomService, times(1)).findBatchMarkerGroupCustom(anyInt());
	}

    @Test
	public void findBatchMarkerGroupCustomListTest(){
        subject.findBatchMarkerGroupCustomList(new PageInput(), new SortInput(), List.of(new FilterInput()));
        verify (batchMarkerGroupCustomService, times(1)).findBatchMarkerGroupCustoms(any(), any(), any());
    }
    
    @Test
    public void findPlateSampleDetailListTest() {
        FilterInput f = new FilterInput("plateCode", "ABC123", FilterMod.EQ, null);
        subject.findPlateSampleDetailList(new PageInput(), new SortInput(), List.of(f), true);
        verify(batchService, times(1)).findSampleGermplasmDetails(eq("ABC123"));
    }

    @Test
    public void findPlateSampleDetailListNoFilterTest() {
        FilterInput f = new FilterInput("noPlateCodeFilter", "ABC123", FilterMod.EQ, null);
        
        Exception e = assertThrows(RuntimeException.class, () -> subject.findPlateSampleDetailList(new PageInput(), new SortInput(), List.of(f), true));
        assertThat(e.getMessage()).isEqualTo("Cannot find a filter for plateCode");
    }

}
package org.ebs.graphql.mutation;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.ebs.services.BatchCodeService;
import org.ebs.services.BatchMarkerGroupCustomService;
import org.ebs.services.BatchMarkerGroupService;
import org.ebs.services.BatchService;
import org.ebs.services.CheckPropService;
import org.ebs.services.CollectionContainerTypeService;
import org.ebs.services.CollectionLayoutService;
import org.ebs.services.ComponentService;
import org.ebs.services.ComponentTypeService;
import org.ebs.services.ContainerSetService;
import org.ebs.services.ControlTypeService;
import org.ebs.services.DataFormatService;
import org.ebs.services.DataTypeService;
import org.ebs.services.EbsFormService;
import org.ebs.services.FieldService;
import org.ebs.services.FormService;
import org.ebs.services.FormTypeService;
import org.ebs.services.GroupService;
import org.ebs.services.HelperService;
import org.ebs.services.HistoricalStorageService;
import org.ebs.services.InputPropService;
import org.ebs.services.LabControlService;
import org.ebs.services.LabLayoutControlService;
import org.ebs.services.LabLayoutService;
import org.ebs.services.LoadTypeService;
import org.ebs.services.MixtureMethodService;
import org.ebs.services.OptionService;
import org.ebs.services.PurposeService;
import org.ebs.services.RequestListMemberService;
import org.ebs.services.RequestService;
import org.ebs.services.RequestStatusService;
import org.ebs.services.RequestTraitService;
import org.ebs.services.ResultPreferenceDetailService;
import org.ebs.services.ResultPreferenceService;
import org.ebs.services.SampleDetailService;
import org.ebs.services.SampleMixtureDetailService;
import org.ebs.services.SampleMixtureService;
import org.ebs.services.SampleService;
import org.ebs.services.ServiceProviderService;
import org.ebs.services.ServiceService;
import org.ebs.services.ServiceTypeService;
import org.ebs.services.ShipmentDetailService;
import org.ebs.services.ShipmentService;
import org.ebs.services.ShipmentSetService;
import org.ebs.services.StatusService;
import org.ebs.services.StockService;
import org.ebs.services.StorageLocationService;
import org.ebs.services.TabService;
import org.ebs.services.TissueTypeService;
import org.ebs.services.ValidationRegexService;
import org.ebs.services.ValueService;
import org.ebs.services.to.Input.BatchCodeInput;
import org.ebs.services.to.Input.BatchInput;
import org.ebs.services.to.Input.BatchMarkerGroupCustomInput;
import org.ebs.services.to.Input.BatchMarkerGroupInput;
import org.ebs.services.to.Input.CheckPropInput;
import org.ebs.services.to.Input.CollectionContainerTypeInput;
import org.ebs.services.to.Input.CollectionLayoutInput;
import org.ebs.services.to.Input.ComponentInput;
import org.ebs.services.to.Input.ComponentTypeInput;
import org.ebs.services.to.Input.ContainerSetInput;
import org.ebs.services.to.Input.ControlTypeInput;
import org.ebs.services.to.Input.DataFormatInput;
import org.ebs.services.to.Input.DataTypeInput;
import org.ebs.services.to.Input.EbsFormInput;
import org.ebs.services.to.Input.FieldInput;
import org.ebs.services.to.Input.FormInput;
import org.ebs.services.to.Input.FormTypeInput;
import org.ebs.services.to.Input.GroupInput;
import org.ebs.services.to.Input.HelperInput;
import org.ebs.services.to.Input.HistoricalStorageInput;
import org.ebs.services.to.Input.InputPropInput;
import org.ebs.services.to.Input.LabControlInput;
import org.ebs.services.to.Input.LabLayoutControlInput;
import org.ebs.services.to.Input.LabLayoutInput;
import org.ebs.services.to.Input.LoadTypeInput;
import org.ebs.services.to.Input.MixtureMethodInput;
import org.ebs.services.to.Input.OptionInput;
import org.ebs.services.to.Input.PurposeInput;
import org.ebs.services.to.Input.RequestInput;
import org.ebs.services.to.Input.RequestListMemberInput;
import org.ebs.services.to.Input.RequestStatusInput;
import org.ebs.services.to.Input.RequestTraitInput;
import org.ebs.services.to.Input.ResultPreferenceDetailInput;
import org.ebs.services.to.Input.ResultPreferenceInput;
import org.ebs.services.to.Input.SampleDetailInput;
import org.ebs.services.to.Input.SampleInput;
import org.ebs.services.to.Input.SampleMixtureDetailInput;
import org.ebs.services.to.Input.SampleMixtureInput;
import org.ebs.services.to.Input.ServiceInput;
import org.ebs.services.to.Input.ServiceProviderInput;
import org.ebs.services.to.Input.ServiceTypeInput;
import org.ebs.services.to.Input.ShipmentDetailInput;
import org.ebs.services.to.Input.ShipmentInput;
import org.ebs.services.to.Input.ShipmentSetInput;
import org.ebs.services.to.Input.StatusInput;
import org.ebs.services.to.Input.StockInput;
import org.ebs.services.to.Input.StorageLocationInput;
import org.ebs.services.to.Input.TabInput;
import org.ebs.services.to.Input.TissueTypeInput;
import org.ebs.services.to.Input.ValidationRegexInput;
import org.ebs.services.to.Input.ValueInput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class MutationResolverTest {
    
    @Mock private BatchCodeService batchCodeServiceMock;
	@Mock private BatchService batchServiceMock;
	@Mock private CollectionContainerTypeService collectionContainerTypeServiceMock;
	@Mock private CollectionLayoutService collectionLayoutServiceMock;
	@Mock private ContainerSetService containerSetServiceMock;
	@Mock private ControlTypeService controlTypeServiceMock;
	@Mock private DataFormatService dataFormatServiceMock;
	@Mock private HistoricalStorageService historicalStorageServiceMock;
	@Mock private LabControlService labControlServiceMock;
	@Mock private LabLayoutControlService labLayoutControlServiceMock;
	@Mock private LabLayoutService labLayoutServiceMock;
	@Mock private LoadTypeService loadTypeServiceMock;
	@Mock private MixtureMethodService mixtureMethodServiceMock;
	@Mock private PurposeService purposeServiceMock;
	@Mock private RequestListMemberService requestListMemberServiceMock;
	@Mock private RequestService requestServiceMock;
	@Mock private RequestStatusService requestStatusServiceMock;
	@Mock private ResultPreferenceDetailService resultPreferenceDetailServiceMock;
	@Mock private ResultPreferenceService resultPreferenceServiceMock;
	@Mock private SampleDetailService sampleDetailServiceMock;
	@Mock private SampleMixtureDetailService sampleMixtureDetailServiceMock;
	@Mock private SampleMixtureService sampleMixtureServiceMock;
	@Mock private SampleService sampleServiceMock;
	@Mock private ServiceProviderService serviceProviderServiceMock;
	@Mock private ServiceService serviceServiceMock;
	@Mock private ServiceTypeService serviceTypeServiceMock;
	@Mock private ShipmentDetailService shipmentDetailServiceMock;
	@Mock private ShipmentService shipmentServiceMock;
	@Mock private ShipmentSetService shipmentSetServiceMock;
	@Mock private StatusService statusServiceMock;
	@Mock private StockService stockServiceMock;
	@Mock private StorageLocationService storageLocationServiceMock;
	@Mock private TissueTypeService tissueTypeServiceMock;
	@Mock private CheckPropService checkPropServiceMock;
	@Mock private ComponentService componentServiceMock;
	@Mock private ComponentTypeService componentTypeServiceMock;
	@Mock private DataTypeService dataTypeServiceMock;
	@Mock private EbsFormService ebsFormServiceMock;
	@Mock private FieldService fieldServiceMock;
	@Mock private FormService formServiceMock;
	@Mock private FormTypeService formTypeServiceMock;
	@Mock private GroupService groupServiceMock;
	@Mock private HelperService helperServiceMock;
	@Mock private InputPropService inputPropServiceMock;
	@Mock private OptionService optionServiceMock;
	@Mock private TabService tabServiceMock;
	@Mock private ValidationRegexService validationRegexServiceMock;
	@Mock private ValueService valueServiceMock;
    @Mock private RequestTraitService requestTraitServiceMock;
    @Mock private BatchMarkerGroupService batchMarkerGroupServiceMock;
	@Mock private BatchMarkerGroupCustomService batchMarkerGroupCustomServiceMock;

    @InjectMocks
    MutationResolver subject;

    @Test
    public void createBatch() {
        subject.createBatch(new BatchInput());
        verify(batchServiceMock, times(1)).createBatch(any(BatchInput.class));
	}
    
    
	@Test
    public void createBatchCodeTest(){
        subject.createBatchCode(new BatchCodeInput());
		verify(batchCodeServiceMock, times(1)).createBatchCode(any(BatchCodeInput.class));
	}

	@Test
    public void createCollectionContainerTypeTest(){
        subject.createCollectionContainerType(new CollectionContainerTypeInput());
		verify(collectionContainerTypeServiceMock, times(1)).createCollectionContainerType(any(CollectionContainerTypeInput.class));
	}

	@Test
    public void createCollectionLayoutTest(){
        subject.createCollectionLayout(new CollectionLayoutInput());
		verify(collectionLayoutServiceMock, times(1)).createCollectionLayout(any(CollectionLayoutInput.class));
	}
	@Test
    public void createContainerSetTest(){
        subject.createContainerSet(new ContainerSetInput());
		verify(containerSetServiceMock, times(1)).createContainerSet(any());
	}

    
	@Test
    public void createControlTypeTest(){
        subject.createControlType(new ControlTypeInput());
		verify(controlTypeServiceMock, times(1)).createControlType(any());
	}


	@Test
    public void createDataFormatTest(){
        subject.createDataFormat(new DataFormatInput());
		verify(dataFormatServiceMock, times(1)).createDataFormat(any());
	}

	@Test
    public void createHistoricalStorageTest(){
        subject.createHistoricalStorage(new HistoricalStorageInput());
		verify(historicalStorageServiceMock, times(1)).createHistoricalStorage(any());
	}

	@Test
    public void createLabControlTest(){
        subject.createLabControl(new LabControlInput());
		verify(labControlServiceMock, times(1)).createLabControl(any());
	}

	@Test
    public void createLabLayoutTest(){
        subject.createLabLayout(new LabLayoutInput());
		verify(labLayoutServiceMock, times(1)).createLabLayout(any());
	}

	@Test
    public void createLabLayoutControlTest(){
        subject.createLabLayoutControl(new LabLayoutControlInput());
		verify(labLayoutControlServiceMock, times(1)).createLabLayoutControl(any());
	}

	@Test
    public void createLoadTypeTest(){
        subject.createLoadType(new LoadTypeInput());
		verify(loadTypeServiceMock, times(1)).createLoadType(any());
	}

	@Test
    public void createMixtureMethodTest(){
        subject.createMixtureMethod(new MixtureMethodInput());
		verify(mixtureMethodServiceMock, times(1)).createMixtureMethod(any());
	}

	@Test
    public void createPurposeTest(){
        subject.createPurpose(new PurposeInput());
		verify(purposeServiceMock, times(1)).createPurpose(any());
	}

	@Test
    public void createRequestTest(){
        subject.createRequest(new RequestInput());
		verify(requestServiceMock, times(1)).createRequest(any());
	}

	@Test
    public void createRequestListMemberTest(){
        subject.createRequestListMember(new RequestListMemberInput());
		verify(requestListMemberServiceMock, times(1)).createRequestListMember(any());
	}
	
	@Test
    public void createRequestTraitTest (){
        subject.createRequestTrait(new RequestTraitInput());
		verify(requestTraitServiceMock, times(1)).createRequestTrait(any());
	}

	@Test
    public void createRequestStatusTest(){
        subject.createRequestStatus(new RequestStatusInput());
		verify(requestStatusServiceMock, times(1)).createRequestStatus(any());
	}

	@Test
    public void createResultPreferenceTest(){
        subject.createResultPreference(new ResultPreferenceInput());
		verify(resultPreferenceServiceMock, times(1)).createResultPreference(any());
	}

	@Test
    public void createResultPreferenceDetailTest(){
        subject.createResultPreferenceDetail(new ResultPreferenceDetailInput());
		verify(resultPreferenceDetailServiceMock, times(1)).createResultPreferenceDetail(any());
	}

	@Test
    public void createSampleTest(){
        subject.createSample(new SampleInput());
		verify(sampleServiceMock, times(1)).createSample(any());
	}

	@Test
    public void createSampleDetailTest(){
        subject.createSampleDetail(new SampleDetailInput());
		verify(sampleDetailServiceMock, times(1)).createSampleDetail(any());
	}

	@Test
    public void createSampleMixtureTest(){
        subject.createSampleMixture(new SampleMixtureInput());
		verify(sampleMixtureServiceMock, times(1)).createSampleMixture(any());
	}

	@Test
    public void createSampleMixtureDetailTest(){
        subject.createSampleMixtureDetail(new SampleMixtureDetailInput());
		verify(sampleMixtureDetailServiceMock, times(1)).createSampleMixtureDetail(any());
	}

	@Test
    public void createServiceTest(){
        subject.createService(new ServiceInput());
		verify(serviceServiceMock, times(1)).createService(any());
	}

	@Test
    public void createServiceProviderTest(){
        subject.createServiceProvider(new ServiceProviderInput());
		verify(serviceProviderServiceMock, times(1)).createServiceProvider(any());
	}

	@Test
    public void createServiceTypeTest(){
        subject.createServiceType(new ServiceTypeInput());
		verify(serviceTypeServiceMock, times(1)).createServiceType(any());
	}

	@Test
    public void createShipmentTest(){
        subject.createShipment(new ShipmentInput());
		verify(shipmentServiceMock, times(1)).createShipment(any());
	}

	@Test
    public void createShipmentDetailTest(){
        subject.createShipmentDetail(new ShipmentDetailInput());
		verify(shipmentDetailServiceMock, times(1)).createShipmentDetail(any());
	}

	@Test
    public void createShipmentSetTest(){
        subject.createShipmentSet(new ShipmentSetInput());
		verify(shipmentSetServiceMock, times(1)).createShipmentSet(any());
	}

	@Test
    public void createStatusTest(){
        subject.createStatus(new StatusInput());
		verify(statusServiceMock, times(1)).createStatus(any());
	}

	@Test
    public void createStockTest(){
        subject.createStock(new StockInput());
		verify(stockServiceMock, times(1)).createStock(any());
	}

	@Test
    public void createStorageLocationTest(){
        subject.createStorageLocation(new StorageLocationInput());
		verify(storageLocationServiceMock, times(1)).createStorageLocation(any());
	}

	@Test
    public void createTissueTypeTest(){
        subject.createTissueType(new TissueTypeInput());
		verify(tissueTypeServiceMock, times(1)).createTissueType(any());
	}

	@Test
    public void deleteBatchTest(){
        subject.deleteBatch(0);
		verify(batchServiceMock, times(1)).deleteBatch(anyInt());
	}

	@Test
    public void deleteBatchCodeTest(){
        subject.deleteBatchCode(0);
		verify(batchCodeServiceMock, times(1)).deleteBatchCode(anyInt());
	}

	@Test
    public void deleteCollectionContainerTypeTest(){
        subject.deleteCollectionContainerType(0);
		verify(collectionContainerTypeServiceMock, times(1)).deleteCollectionContainerType(anyInt());
	}

	@Test
    public void deleteCollectionLayoutTest(){
        subject.deleteCollectionLayout(0);
		verify(collectionLayoutServiceMock, times(1)).deleteCollectionLayout(anyInt());
	}

	@Test
    public void deleteContainerSetTest(){
        subject.deleteContainerSet(0);
		verify(containerSetServiceMock, times(1)).deleteContainerSet(anyInt());
	}

	@Test
    public void deleteControlTypeTest(){
        subject.deleteControlType(0);
		verify(controlTypeServiceMock, times(1)).deleteControlType(anyInt());
	}

	@Test
    public void deleteDataFormatTest(){
        subject.deleteDataFormat(0);
		verify(dataFormatServiceMock, times(1)).deleteDataFormat(anyInt());
	}

	@Test
    public void deleteHistoricalStorageTest(){
        subject.deleteHistoricalStorage(0);
		verify(historicalStorageServiceMock, times(1)).deleteHistoricalStorage(anyInt());
	}

	@Test
    public void deleteLabControlTest(){
        subject.deleteLabControl(0);
		verify(labControlServiceMock, times(1)).deleteLabControl(anyInt());
	}

	@Test
    public void deleteLabLayoutTest(){
        subject.deleteLabLayout(0);
		verify(labLayoutServiceMock, times(1)).deleteLabLayout(anyInt());
	}

	@Test
    public void deleteLabLayoutControlTest(){
        subject.deleteLabLayoutControl(0);
		verify(labLayoutControlServiceMock, times(1)).deleteLabLayoutControl(anyInt());
	}

	@Test
    public void deleteLoadTypeTest(){
        subject.deleteLoadType(0);
		verify(loadTypeServiceMock, times(1)).deleteLoadType(anyInt());
	}

	@Test
    public void deleteMixtureMethodTest(){
        subject.deleteMixtureMethod(0);
		verify(mixtureMethodServiceMock, times(1)).deleteMixtureMethod(anyInt());
	}

	@Test
    public void deletePurposeTest(){
        subject.deletePurpose(0);
		verify(purposeServiceMock, times(1)).deletePurpose(anyInt());
	}

	@Test
    public void deleteRequestTest(){
        subject.deleteRequest(0);
		verify(requestServiceMock, times(1)).deleteRequest(anyInt());
	}

	@Test
    public void deleteRequestListMemberTest(){
        subject.deleteRequestListMember(0);
		verify(requestListMemberServiceMock, times(1)).deleteRequestListMember(anyInt());
	}

	@Test
    public void deleteRequestTraitTest(){
        subject.deleteRequestTrait(0);
		verify(requestTraitServiceMock, times(1)).deleteRequestTrait(anyInt());
		
	}
	@Test
    public void deleteRequestStatusTest(){
        subject.deleteRequestStatus(0);
		verify(requestStatusServiceMock, times(1)).deleteRequestStatus(anyInt());
	}

	@Test
    public void deleteResultPreferenceTest(){
        subject.deleteResultPreference(0);
		verify(resultPreferenceServiceMock, times(1)).deleteResultPreference(anyInt());
	}

	@Test
    public void deleteResultPreferenceDetailTest(){
        subject.deleteResultPreferenceDetail(0);
		verify(resultPreferenceDetailServiceMock, times(1)).deleteResultPreferenceDetail(anyInt());
	}

	@Test
    public void deleteSampleTest(){
        subject.deleteSample(0);
		verify(sampleServiceMock, times(1)).deleteSample(anyInt());
	}

	@Test
    public void deleteSampleDetailTest(){
        subject.deleteSampleDetail(0);
		verify(sampleDetailServiceMock, times(1)).deleteSampleDetail(anyInt());
	}

	@Test
    public void deleteSampleMixtureTest(){
        subject.deleteSampleMixture(0);
		verify(sampleMixtureServiceMock, times(1)).deleteSampleMixture(anyInt());
	}

	@Test
    public void deleteSampleMixtureDetailTest(){
        subject.deleteSampleMixtureDetail(0);
		verify(sampleMixtureDetailServiceMock, times(1)).deleteSampleMixtureDetail(anyInt());
	}

	@Test
    public void deleteServiceTest(){
        subject.deleteService(0);
		verify(serviceServiceMock, times(1)).deleteService(anyInt());
	}

	@Test
    public void deleteServiceProviderTest(){
        subject.deleteServiceProvider(0);
		verify(serviceProviderServiceMock, times(1)).deleteServiceProvider(anyInt());
	}

	@Test
    public void deleteServiceTypeTest(){
        subject.deleteServiceType(0);
		verify(serviceTypeServiceMock, times(1)).deleteServiceType(anyInt());
	}

	@Test
    public void deleteShipmentTest(){
        subject.deleteShipment(0);
		verify(shipmentServiceMock, times(1)).deleteShipment(anyInt());
	}

	@Test
    public void deleteShipmentDetailTest(){
        subject.deleteShipmentDetail(0);
		verify(shipmentDetailServiceMock, times(1)).deleteShipmentDetail(anyInt());
	}

	@Test
    public void deleteShipmentSetTest(){
        subject.deleteShipmentSet(0);
		verify(shipmentSetServiceMock, times(1)).deleteShipmentSet(anyInt());
	}

	@Test
    public void deleteStatusTest(){
        subject.deleteStatus(0);
		verify(statusServiceMock, times(1)).deleteStatus(anyInt());
	}

	@Test
    public void deleteStockTest(){
        subject.deleteStock(0);
		verify(stockServiceMock, times(1)).deleteStock(anyInt());
	}

	@Test
    public void deleteStorageLocationTest(){
        subject.deleteStorageLocation(0);
		verify(storageLocationServiceMock, times(1)).deleteStorageLocation(anyInt());
	}


	@Test
    public void deleteTissueTypeTest(){
        subject.deleteTissueType(0);
		verify(tissueTypeServiceMock, times(1)).deleteTissueType(anyInt());
	}

	@Test
    public void modifyBatchTest(){
        subject.modifyBatch(new BatchInput());
		verify(batchServiceMock, times(1)).modifyBatch(any(BatchInput.class));
	}

	@Test
    public void modifyBatchCodeTest(){
        subject.modifyBatchCode(new BatchCodeInput());
		verify(batchCodeServiceMock, times(1)).modifyBatchCode(any(BatchCodeInput.class));
	}

	@Test
    public void modifyCollectionContainerTypeTest(){
        subject.modifyCollectionContainerType(new CollectionContainerTypeInput());
		verify(collectionContainerTypeServiceMock, times(1)).modifyCollectionContainerType(any(CollectionContainerTypeInput.class));
	}

	@Test
    public void modifyCollectionLayoutTest(){
        subject.modifyCollectionLayout(new CollectionLayoutInput());
		verify(collectionLayoutServiceMock, times(1)).modifyCollectionLayout(any(CollectionLayoutInput.class));
	}

	@Test
    public void modifyContainerSetTest(){
        subject.modifyContainerSet(new ContainerSetInput());
		verify(containerSetServiceMock, times(1)).modifyContainerSet(any(ContainerSetInput.class));
	}

	@Test
    public void modifyControlTypeTest(){
        subject.modifyControlType(new ControlTypeInput());
		verify(controlTypeServiceMock, times(1)).modifyControlType(any(ControlTypeInput.class));
	}

	@Test
    public void modifyDataFormatTest(){
        subject.modifyDataFormat(new DataFormatInput());
		verify(dataFormatServiceMock, times(1)).modifyDataFormat(any(DataFormatInput.class));
	}

	@Test
    public void modifyHistoricalStorageTest(){
        subject.modifyHistoricalStorage(new HistoricalStorageInput());
		verify(historicalStorageServiceMock, times(1)).modifyHistoricalStorage(any(HistoricalStorageInput.class));
	}

	@Test
    public void modifyLabControlTest(){
        subject.modifyLabControl(new LabControlInput());
		verify(labControlServiceMock, times(1)).modifyLabControl(any(LabControlInput.class));
	}

	@Test
    public void modifyLabLayoutTest(){
        subject.modifyLabLayout(new LabLayoutInput());
		verify(labLayoutServiceMock, times(1)).modifyLabLayout(any(LabLayoutInput.class));
	}

	@Test
    public void modifyLabLayoutControlTest(){
        subject.modifyLabLayoutControl(new LabLayoutControlInput());
		verify(labLayoutControlServiceMock, times(1)).modifyLabLayoutControl(any(LabLayoutControlInput.class));
	}

	@Test
    public void modifyLoadTypeTest(){
        subject.modifyLoadType(new LoadTypeInput());
		verify(loadTypeServiceMock, times(1)).modifyLoadType(any(LoadTypeInput.class));
	}

	@Test
    public void modifyMixtureMethodTest(){
        subject.modifyMixtureMethod(new MixtureMethodInput());
		verify(mixtureMethodServiceMock, times(1)).modifyMixtureMethod(any(MixtureMethodInput.class));
	}

	@Test
    public void modifyPurposeTest(){
        subject.modifyPurpose(new PurposeInput());
		verify(purposeServiceMock, times(1)).modifyPurpose(any(PurposeInput.class));
	}

	@Test
    public void modifyRequestTest(){
        subject.modifyRequest(new RequestInput());
		verify(requestServiceMock, times(1)).modifyRequest(any(RequestInput.class));
	}

	@Test
    public void modifyRequestListMemberTest(){
        subject.modifyRequestListMember(new RequestListMemberInput());
		verify(requestListMemberServiceMock, times(1)).modifyRequestListMember(any(RequestListMemberInput.class));
	}

    @Test
    public void modifyRequestTraitTest() {
        subject.modifyRequestTrait(new RequestTraitInput());
        verify(requestTraitServiceMock, times(1)).modifyRequestTrait(any(RequestTraitInput.class));
    }

	@Test
    public void modifyRequestStatusTest(){
        subject.modifyRequestStatus(new RequestStatusInput());
		verify(requestStatusServiceMock, times(1)).modifyRequestStatus(any(RequestStatusInput.class));
	}

	@Test
    public void modifyResultPreferenceTest(){
        subject.modifyResultPreference(new ResultPreferenceInput());
		verify(resultPreferenceServiceMock, times(1)).modifyResultPreference(any(ResultPreferenceInput.class));
	}

	@Test
    public void modifyResultPreferenceDetailTest(){
        subject.modifyResultPreferenceDetail(new ResultPreferenceDetailInput());
		verify(resultPreferenceDetailServiceMock, times(1)).modifyResultPreferenceDetail(any(ResultPreferenceDetailInput.class));
	}

	@Test
    public void modifySampleTest(){
        subject.modifySample(new SampleInput());
		verify(sampleServiceMock, times(1)).modifySample(any(SampleInput.class));
	}

	@Test
    public void modifySampleDetailTest(){
        subject.modifySampleDetail(new SampleDetailInput());
		verify(sampleDetailServiceMock, times(1)).modifySampleDetail(any(SampleDetailInput.class));
	}

	@Test
    public void modifySampleMixtureTest(){
        subject.modifySampleMixture(new SampleMixtureInput());
		verify(sampleMixtureServiceMock, times(1)).modifySampleMixture(any(SampleMixtureInput.class));
	}

	@Test
    public void modifySampleMixtureDetailTest(){
        subject.modifySampleMixtureDetail(new SampleMixtureDetailInput());
		verify(sampleMixtureDetailServiceMock, times(1)).modifySampleMixtureDetail(any(SampleMixtureDetailInput.class));
	}

	@Test
    public void modifyServiceTest(){
        subject.modifyService(new ServiceInput());
		verify(serviceServiceMock, times(1)).modifyService(any(ServiceInput.class));
	}

	@Test
    public void modifyServiceProviderTest(){
        subject.modifyServiceProvider(new ServiceProviderInput());
		verify(serviceProviderServiceMock, times(1)).modifyServiceProvider(any(ServiceProviderInput.class));
	}

	@Test
    public void modifyServiceTypeTest(){
        subject.modifyServiceType(new ServiceTypeInput());
		verify(serviceTypeServiceMock, times(1)).modifyServiceType(any(ServiceTypeInput.class));
	}

	@Test
    public void modifyShipmentTest(){
        subject.modifyShipment(new ShipmentInput());
		verify(shipmentServiceMock, times(1)).modifyShipment(any(ShipmentInput.class));
	}

	@Test
    public void modifyShipmentDetailTest(){
        subject.modifyShipmentDetail(new ShipmentDetailInput());
		verify(shipmentDetailServiceMock, times(1)).modifyShipmentDetail(any(ShipmentDetailInput.class));
	}

	@Test
    public void modifyShipmentSetTest(){
        subject.modifyShipmentSet(new ShipmentSetInput());
		verify(shipmentSetServiceMock, times(1)).modifyShipmentSet(any(ShipmentSetInput.class));
	}

	@Test
    public void modifyStatusTest(){
        subject.modifyStatus(new StatusInput());
		verify(statusServiceMock, times(1)).modifyStatus(any(StatusInput.class));
	}

	@Test
    public void modifyStockTest(){
        subject.modifyStock(new StockInput());
		verify(stockServiceMock, times(1)).modifyStock(any(StockInput.class));
	}

	@Test
    public void modifyStorageLocationTest(){
        subject.modifyStorageLocation(new StorageLocationInput());
		verify(storageLocationServiceMock, times(1)).modifyStorageLocation(any(StorageLocationInput.class));
	}

	@Test
    public void modifyTissueTypeTest(){
        subject.modifyTissueType(new TissueTypeInput());
		verify(tissueTypeServiceMock, times(1)).modifyTissueType(any(TissueTypeInput.class));
	}

	@Test
    public void createCheckPropTest(){
        subject.createCheckProp(new CheckPropInput());
		verify(checkPropServiceMock, times(1)).createCheckProp(any(CheckPropInput.class));
	}

	@Test
    public void createComponentTest(){
        subject.createComponent(new ComponentInput());
		verify(componentServiceMock, times(1)).createComponent(any(ComponentInput.class));
	}

	@Test
    public void createComponentTypeTest(){
        subject.createComponentType(new ComponentTypeInput());
		verify(componentTypeServiceMock, times(1)).createComponentType(any(ComponentTypeInput.class));
	}

	@Test
    public void createDataTypeTest(){
        subject.createDataType(new DataTypeInput());
		verify(dataTypeServiceMock, times(1)).createDataType(any(DataTypeInput.class));
	}

	@Test
    public void createEbsFormTest(){
        subject.createEbsForm(new EbsFormInput());
		verify(ebsFormServiceMock, times(1)).createEbsForm(any(EbsFormInput.class));
	}

	@Test
    public void createFieldTest(){
        subject.createField(new FieldInput());
		verify(fieldServiceMock, times(1)).createField(any(FieldInput.class));
	}

	@Test
    public void createFormTest(){
        subject.createForm(new FormInput());
		verify(formServiceMock, times(1)).createForm(any(FormInput.class));
	}

	@Test
    public void createFormTypeTest(){
        subject.createFormType(new FormTypeInput());
		verify(formTypeServiceMock, times(1)).createFormType(any(FormTypeInput.class));
	}

	@Test
    public void createGroupTest(){
        subject.createGroup(new GroupInput());
		verify(groupServiceMock, times(1)).createGroup(any(GroupInput.class));
	}

	@Test
    public void createHelperTest(){
        subject.createHelper(new HelperInput());
		verify(helperServiceMock, times(1)).createHelper(any(HelperInput.class));
	}

	@Test
    public void createInputPropTest(){
        subject.createInputProp(new InputPropInput());
		verify(inputPropServiceMock, times(1)).createInputProp(any(InputPropInput.class));
	}

	@Test
    public void createOptionTest(){
        subject.createOption(new OptionInput());
		verify(optionServiceMock, times(1)).createOption(any(OptionInput.class));
	}
	@Test
    public void createTabTest(){
        subject.createTab(new TabInput());
		verify(tabServiceMock, times(1)).createTab(any(TabInput.class));
	}
	@Test
    public void createValidationRegexTest(){
        subject.createValidationRegex(new ValidationRegexInput());
		verify(validationRegexServiceMock, times(1)).createValidationRegex(any(ValidationRegexInput.class));
	}

	@Test
    public void createValueTest(){
        subject.createValue(new ValueInput());
		verify(valueServiceMock, times(1)).createValue(any(ValueInput.class));
	}
	@Test
    public void deleteCheckPropTest(){
        subject.deleteCheckProp(0);
		verify(checkPropServiceMock, times(1)).deleteCheckProp(anyInt());
	}

	@Test
    public void deleteComponentTest(){
        subject.deleteComponent(0);
		verify(componentServiceMock, times(1)).deleteComponent(anyInt());
	}

	@Test
    public void deleteComponentTypeTest(){
        subject.deleteComponentType(0);
		verify(componentTypeServiceMock, times(1)).deleteComponentType(anyInt());
	}

	@Test
    public void deleteDataTypeTest(){
        subject.deleteDataType(0);
		verify(dataTypeServiceMock, times(1)).deleteDataType(anyInt());
	}

	@Test
    public void deleteEbsFormTest(){
        subject.deleteEbsForm(0);
		verify(ebsFormServiceMock, times(1)).deleteEbsForm(anyInt());
	}

	@Test
    public void deleteFieldTest(){
        subject.deleteField(0);
		verify(fieldServiceMock, times(1)).deleteField(anyInt());
	}

	@Test
    public void deleteFormTest(){
        subject.deleteForm(0);
		verify(formServiceMock, times(1)).deleteForm(anyInt());
	}

	@Test
    public void deleteFormTypeTest(){
        subject.deleteFormType(0);
		verify(formTypeServiceMock, times(1)).deleteFormType(anyInt());
	}

	@Test
    public void deleteGroupTest(){
        subject.deleteGroup(0);
		verify(groupServiceMock, times(1)).deleteGroup(anyInt());
	}

	@Test
    public void deleteHelperTest(){
        subject.deleteHelper(0);
		verify(helperServiceMock, times(1)).deleteHelper(anyInt());
	}

	@Test
    public void deleteInputPropTest(){
        subject.deleteInputProp(0);
		verify(inputPropServiceMock, times(1)).deleteInputProp(anyInt());
	}

	@Test
    public void deleteOptionTest(){
        subject.deleteOption(0);
		verify(optionServiceMock, times(1)).deleteOption(anyInt());
	}
	
	@Test
    public void deleteTabTest(){
        subject.deleteTab(0);
		verify(tabServiceMock, times(1)).deleteTab(anyInt());
	}
	@Test
    public void deleteValidationRegexTest(){
        subject.deleteValidationRegex(0);
		verify(validationRegexServiceMock, times(1)).deleteValidationRegex(anyInt());
	}

	@Test
    public void deleteValueTest(){
        subject.deleteValue(0);
		verify(valueServiceMock, times(1)).deleteValue(anyInt());
	}

	@Test
    public void modifyCheckPropTest(){
        subject.modifyCheckProp(new CheckPropInput());
		verify(checkPropServiceMock, times(1)).modifyCheckProp(any(CheckPropInput.class));
	}

	@Test
    public void modifyComponentTest(){
        subject.modifyComponent(new ComponentInput());
		verify(componentServiceMock, times(1)).modifyComponent(any(ComponentInput.class));
	}

	@Test
    public void modifyComponentTypeTest(){
        subject.modifyComponentType(new ComponentTypeInput());
		verify(componentTypeServiceMock, times(1)).modifyComponentType(any(ComponentTypeInput.class));
	}

	@Test
    public void modifyDataTypeTest(){
        subject.modifyDataType(new DataTypeInput());
		verify(dataTypeServiceMock, times(1)).modifyDataType(any(DataTypeInput.class));
	}

	@Test
    public void modifyEbsFormTest(){
        subject.modifyEbsForm(new EbsFormInput());
		verify(ebsFormServiceMock, times(1)).modifyEbsForm(any(EbsFormInput.class));
	}

	@Test
    public void modifyFieldTest(){
        subject.modifyField(new FieldInput());
		verify(fieldServiceMock, times(1)).modifyField(any(FieldInput.class));
	}

	@Test
    public void modifyFormTest(){
        subject.modifyForm(new FormInput());
		verify(formServiceMock, times(1)).modifyForm(any(FormInput.class));
	}

	@Test
    public void modifyFormTypeTest(){
        subject.modifyFormType(new FormTypeInput());
		verify(formTypeServiceMock, times(1)).modifyFormType(any(FormTypeInput.class));
	}

	@Test
    public void modifyGroupTest(){
        subject.modifyGroup(new GroupInput());
		verify(groupServiceMock, times(1)).modifyGroup(any(GroupInput.class));
	}

	@Test
    public void modifyHelperTest(){
        subject.modifyHelper(new HelperInput());
		verify(helperServiceMock, times(1)).modifyHelper(any(HelperInput.class));
	}

	@Test
    public void modifyInputPropTest(){
        subject.modifyInputProp(new InputPropInput());
		verify(inputPropServiceMock, times(1)).modifyInputProp(any(InputPropInput.class));
	}

	@Test
    public void modifyOptionTest(){
        subject.modifyOption(new OptionInput());
		verify(optionServiceMock, times(1)).modifyOption(any(OptionInput.class));
	}
	@Test
    public void modifyTabTest(){
        subject.modifyTab(new TabInput());
		verify(tabServiceMock, times(1)).modifyTab(any(TabInput.class));
	}
	@Test
    public void modifyValidationRegexTest(){
        subject.modifyValidationRegex(new ValidationRegexInput());
		verify(validationRegexServiceMock, times(1)).modifyValidationRegex(any(ValidationRegexInput.class));
	}

    @Test
    public void modifyValueTest(){
        subject.modifyValue(new ValueInput());
        verify(valueServiceMock, times(1)).modifyValue(any(ValueInput.class));
    }

    @Test
    public void addTraitCategoriesToPurposeTest(){
        subject.addTraitCategoriesToPurpose(0, new Integer[] {});
        verify(purposeServiceMock, times(1)).addTraitCategories(anyInt(), any());
    }

    @Test
    public void removeTraitCategoriesFromPurposeTest(){
        subject.removeTraitCategoriesFromPurpose(0, new Integer[] {});
        verify(purposeServiceMock, times(1)).removeTraitCategories(anyInt(), any());
    }

    @Test
    public void addMarkerPanelsToPurposeTest(){
        subject.addMarkerPanelsToPurpose(0, new Integer[] {});
        verify(purposeServiceMock, times(1)).addMarkerPanels(anyInt(), any());
    }

    @Test
    public void removeMarkerPanelsFromPurposeTest(){
        subject.removeMarkerPanelsFromPurpose(0, new Integer[] {});
        verify(purposeServiceMock, times(1)).removeMarkerPanels(anyInt(), any());
    }

    @Test
    public void addServiceTypesToServiceProviderTest(){
        subject.addServiceTypesToServiceProvider(0, new Integer[] {});
        verify(serviceProviderServiceMock, times(1)).addServiceTypes(anyInt(), any());
    }

    @Test
    public void removeServiceTypesFromServiceProviderTest(){
        subject.removeServiceTypesFromServiceProvider(0, new Integer[] {});
            verify(serviceProviderServiceMock, times(1)).removeServiceTypes(anyInt(), any());
    }

    @Test
    public void addServiceProvidersToServiceTypeTest(){
        subject.addServiceProvidersToServiceType(0, new Integer[] {});
        verify(serviceTypeServiceMock, times(1)).addServiceProviders(anyInt(), any());
    }

    @Test
    public void removeServiceProvidersFromServiceTypeTest(){
        subject.removeServiceProvidersFromServiceType(0, new Integer[] {});
        verify(serviceTypeServiceMock, times(1)).removeServiceProviders(anyInt(), any());
    }
    
    
	@Test
    public void createBatchMarkerGroupTest (){
        subject.createBatchMarkerGroup(new BatchMarkerGroupInput());
		verify(batchMarkerGroupServiceMock, times(1)).createBatchMarkerGroup(any());
	}

	@Test
    public void modifyBatchMarkerGroupTest(){
        subject.modifyBatchMarkerGroup(new BatchMarkerGroupInput());
		verify(batchMarkerGroupServiceMock, times(1)).modifyBatchMarkerGroup(any());
	}

	@Test
    public void deleteBatchMarkerGroupTest(){
        subject.deleteBatchMarkerGroup(0);
        verify(batchMarkerGroupServiceMock, times(1)).deleteBatchMarkerGroup(anyInt());
	}
	  
	@Test
    public void createBatchMarkerGroupCustomTest(){
        subject.createBatchMarkerGroupCustom(new BatchMarkerGroupCustomInput());
		verify(batchMarkerGroupCustomServiceMock, times(1)).createBatchMarkerGroupCustom(any());
	}

	@Test
    public void modifyBatchMarkerGroupCustomTest(){
        subject.modifyBatchMarkerGroupCustom(new BatchMarkerGroupCustomInput());
		verify(batchMarkerGroupCustomServiceMock, times(1)).modifyBatchMarkerGroupCustom(any());
	}

	@Test
    public void deleteBatchMarkerGroupCustomTest(){
        subject.deleteBatchMarkerGroupCustom(0);
		verify(batchMarkerGroupCustomServiceMock, times(1)).deleteBatchMarkerGroupCustom(anyInt());	
	}

}

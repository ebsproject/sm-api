package org.ebs.services;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.ebs.model.SampleDetailModel.PACKAGE_ENTITY_ID;
import static org.ebs.model.SampleDetailModel.PLOT_ENTITY_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;

import org.assertj.core.groups.Tuple;
import org.ebs.model.BatchMarkerGroupCustomModel;
import org.ebs.model.BatchMarkerGroupModel;
import org.ebs.model.BatchModel;
import org.ebs.model.CollectionLayoutModel;
import org.ebs.model.ContainerSetModel;
import org.ebs.model.LoadTypeModel;
import org.ebs.model.RequestListMemberModel;
import org.ebs.model.RequestModel;
import org.ebs.model.SampleDetailModel;
import org.ebs.model.ServiceProviderModel;
import org.ebs.model.TissueTypeModel;
import org.ebs.model.repos.BatchCodeRepository;
import org.ebs.model.repos.BatchMarkerGroupRepository;
import org.ebs.model.repos.BatchRepository;
import org.ebs.model.repos.ContainerSetRepository;
import org.ebs.model.repos.LoadTypeRepository;
import org.ebs.model.repos.RequestListMemberRepository;
import org.ebs.model.repos.SampleDetailRepository;
import org.ebs.model.repos.ServiceProviderRepository;
import org.ebs.model.repos.TissueTypeRepository;
import org.ebs.rest.to.GenotypeSearchParams;
import org.ebs.security.EbsAuthorizationService;
import org.ebs.security.to.EbsUserProfile;
import org.ebs.security.to.ServiceProvider;
import org.ebs.services.custom.CBGermplasmService;
import org.ebs.services.custom.CSUser;
import org.ebs.services.custom.MarkerService;
import org.ebs.services.to.BatchMarkerGroupTo;
import org.ebs.services.to.BatchTo;
import org.ebs.services.to.CollectionLayoutTo;
import org.ebs.services.to.ContainerSetTo;
import org.ebs.services.to.GermplasmDetailTo;
import org.ebs.services.to.LoadTypeTo;
import org.ebs.services.to.RequestListMemberTo;
import org.ebs.services.to.SampleDetailTo;
import org.ebs.services.to.SampleGermplasmDetailTo;
import org.ebs.services.to.ServiceProviderTo;
import org.ebs.services.to.TissueTypeTo;
import org.ebs.services.to.Input.BatchInput;
import org.ebs.services.to.Input.LoadTypeInput;
import org.ebs.services.to.Input.ServiceProviderInput;
import org.ebs.services.to.Input.TissueTypeInput;
import org.ebs.services.to.custom.DesignTo;
import org.ebs.services.to.custom.RequestIDTo;
import org.ebs.services.to.custom.UserTo;
import org.ebs.util.Connection;
import org.ebs.util.FilterInput;
import org.ebs.util.FilterMod;
import org.ebs.util.PageInput;
import org.ebs.util.SortInput;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
public class BatchServiceImplTest {

    @Mock private BatchCodeRepository batchcodeRepository;
	@Mock private BatchRepository batchRepository;
	@Mock private ContainerSetRepository containersetRepository;
	@Mock private ConversionService converter;
	@Mock private LoadTypeRepository loadtypeRepository;
	@Mock private RequestListMemberRepository requestlistmemberRepository;
	@Mock private SampleDetailRepository sampledetailRepository;
	@Mock private ServiceProviderRepository serviceproviderRepository;
	@Mock private TissueTypeRepository tissuetypeRepository;
	@Mock private EbsAuthorizationService authorizationService;
	@Mock private CSUser csUser;
    @Mock private BatchMarkerGroupRepository batchMarkerGroupRepository;
    @Mock private MarkerService markerService;
    @Mock private CBGermplasmService cbGermplasmService;
    @Mock private RequestService requestService; 

    @Mock private StoredProcedureQuery spq;
    @Mock private EntityManager em;

    @InjectMocks
    private BatchServiceImpl subject;

    @Test
    public void givenValidBatchWhenCreateBatchThenSuccess() {

        when(converter.convert(any(), eq(BatchModel.class))).thenReturn(new BatchModel());
        when(serviceproviderRepository.findById(anyInt())).thenReturn(Optional.of(new ServiceProviderModel()));
        when(loadtypeRepository.findById(anyInt())).thenReturn(Optional.of(new LoadTypeModel()));
        when(tissuetypeRepository.findById(anyInt())).thenReturn(Optional.of(new TissueTypeModel()));

        BatchInput input = new BatchInput();
        input.setServiceprovider(new ServiceProviderInput(1));
        input.setLoadtype(new LoadTypeInput(2));
        input.setTissuetype(new TissueTypeInput(3));
        input.setTechnologyPlatformId(4);
        input.setVendorId(5);
        
        subject.createBatch(input);

        verify(serviceproviderRepository,times(1)).findById(eq(1));
        verify(loadtypeRepository,times(1)).findById(eq(2));
        verify(tissuetypeRepository,times(1)).findById(eq(3));
        verify(batchRepository,times(1)).saveAndFlush(any());
        verify(converter,times(1)).convert(any(), eq(BatchTo.class));
    }

    @Test
    public void givenExistingBatchWhenDeleteBatchThenSuccess() {

        when(batchRepository.getEntityManager()).thenReturn(em);
        when(em.createStoredProcedureQuery(anyString())).thenReturn(spq);
        when(spq.registerStoredProcedureParameter(anyInt(), any(), any())).thenReturn(spq);
        when(spq.getOutputParameterValue(anyInt())).thenReturn(Boolean.valueOf(true));
        when(spq.setParameter(anyInt(), any())).thenReturn(spq);
                
        int result = subject.deleteBatch(123);

        assertEquals(123, result);
    }

    @Test
    public void givenInvalidIdWhenFindBatchThenReturnEmpty() {
        
        Optional<BatchTo> result = subject.findBatch(123);
        assertTrue(result.isEmpty());
    }

    @Test
    public void givenExistingBatchWhenFindBatchThenReturnBatch() {
        
        BatchModel model = new BatchModel();
        BatchTo to = new BatchTo(123);

        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(model));
        when(converter.convert(any(BatchModel.class), eq(BatchTo.class))).thenReturn(to);

        Optional<BatchTo> result = subject.findBatch(123);
        assertEquals(123, result.get().getId());
    }

    @Test
    public void givenDeletedBatchWhenFindBatchThenReturnEmpty() {
        
        BatchModel model = new BatchModel();
        model.setDeleted(true);
        
        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(model));

        Optional<BatchTo> result = subject.findBatch(123);
        assertTrue(result.isEmpty());
    }

    @Test
    public void givenActiveBaseFiltersWhenFindBatchsThenReturnList() {
        List<FilterInput> filters = new ArrayList<>();
        filters.add(new FilterInput("activeBaseFilters", "true", FilterMod.EQ, null));

        EbsUserProfile profile = new EbsUserProfile();
        profile.setServiceProviders(List.of(new ServiceProvider(11,"sp1"), new ServiceProvider(22, "sp2")));

        when(authorizationService.getUserProfile()).thenReturn(profile);
        when(batchRepository.findByCriteria(eq(BatchModel.class), anyList(), any(SortInput.class), any(PageInput.class), anyBoolean()))
            .thenReturn(new Connection<>( List.of(new BatchModel(), new BatchModel()), Pageable.ofSize(2), 2));
        when(converter.convert(any(BatchModel.class), eq(BatchTo.class)))
            .thenReturn(new BatchTo());

        Page<BatchTo> batches = subject.findBatchs(new PageInput(), new SortInput(), filters, false);

        assertEquals(2, batches.getContent().size());

    }

    @Test
    public void givenMultipleFiltersWhenFindBatchsThenReturnList() {
        List<FilterInput> filters = new ArrayList<>();
        filters.add(new FilterInput("numMembers", "true", FilterMod.EQ, null));
        filters.add(new FilterInput("numContainers", "true", FilterMod.EQ, null));
//        filters.add(new FilterInput("createByUser", "true", FilterMod.EQ, null));
//        filters.add(new FilterInput("updatedByUser", "true", FilterMod.EQ, null));
        filters.add(new FilterInput("updatedOn", "true", FilterMod.EQ, null));
        filters.add(new FilterInput("createdOn", "true", FilterMod.EQ, null));
        //filters.add(new FilterInput("firstPlateName", "true", FilterMod.EQ, null));
        //filters.add(new FilterInput("lastPlateName", "true", FilterMod.EQ, null));
        filters.add(new FilterInput("serviceprovider", "true", FilterMod.EQ, null));
        filters.add(new FilterInput("servicetypes", "true", FilterMod.EQ, null));

        EbsUserProfile profile = new EbsUserProfile();
        profile.setServiceProviders(List.of(new ServiceProvider(11,"sp1"), new ServiceProvider(22, "sp2")));

        when(authorizationService.getUserProfile()).thenReturn(profile);
        when(batchRepository.findByCriteria(eq(BatchModel.class), anyList(), any(SortInput.class), any(PageInput.class), anyBoolean()))
            .thenReturn(new Connection<>( List.of(new BatchModel(), new BatchModel()), Pageable.ofSize(2), 2));
        when(converter.convert(any(BatchModel.class), eq(BatchTo.class)))
            .thenReturn(new BatchTo());

        Page<BatchTo> batches = subject.findBatchs(new PageInput(), new SortInput(), filters, false);

        assertEquals(2, batches.getContent().size());

    }

    @Test
    public void givenCreatedByOrUpdatedByFilterWhenFindBatchsThenReturnList() {
        List<FilterInput> filters = new ArrayList<>();
        filters.add(new FilterInput("createByUser", "true", FilterMod.EQ, null));
        filters.add(new FilterInput("updatedByUser", "true", FilterMod.EQ, null));

        EbsUserProfile profile = new EbsUserProfile();
        profile.setServiceProviders(List.of(new ServiceProvider(11,"sp1"), new ServiceProvider(22, "sp2")));

        when(authorizationService.getUserProfile()).thenReturn(profile);
        when(batchRepository.findByCriteria(eq(BatchModel.class), anyList(), any(SortInput.class), any(PageInput.class), anyBoolean()))
            .thenReturn(new Connection<>( List.of(new BatchModel(), new BatchModel()), Pageable.ofSize(2), 2));
        when(converter.convert(any(BatchModel.class), eq(BatchTo.class)))
            .thenReturn(new BatchTo());
        when(csUser.findUsers(any())).thenReturn(List.of(new UserTo(1,2,"userA", null),
            new UserTo(2,4,"userAB", null)));

        Page<BatchTo> batches = subject.findBatchs(new PageInput(), new SortInput(), filters, false);

        assertEquals(2, batches.getContent().size());

    }

    @Test
    public void givenPlateFilterWhenFindBatchsThenReturnList() {
        List<FilterInput> filters = new ArrayList<>();
        filters.add(new FilterInput("firstPlateName", "true", FilterMod.EQ, null));
        filters.add(new FilterInput("lastPlateName", "true", FilterMod.EQ, null));

        EbsUserProfile profile = new EbsUserProfile();
        profile.setServiceProviders(List.of(new ServiceProvider(11,"sp1"), new ServiceProvider(22, "sp2")));

        when(authorizationService.getUserProfile()).thenReturn(profile);
        when(batchRepository.findByCriteria(eq(BatchModel.class), anyList(), any(SortInput.class), any(PageInput.class), anyBoolean()))
            .thenReturn(new Connection<>( List.of(new BatchModel(), new BatchModel()), Pageable.ofSize(2), 2));
        when(converter.convert(any(BatchModel.class), eq(BatchTo.class)))
            .thenReturn(new BatchTo());
        when(batchcodeRepository.runNativeQuery(anyString())).thenReturn(List.of(new Object[]{1}, new Object[]{2}));

        Page<BatchTo> batches = subject.findBatchs(new PageInput(), new SortInput(), filters, false);

        assertEquals(2, batches.getContent().size());

    }

    @Test
    public void findCollectionLayoutTest() {
        BatchModel model = new BatchModel();
        model.setCollectionlayout(new CollectionLayoutModel());
        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(model));
        when(converter.convert(any(), eq(CollectionLayoutTo.class)))
            .thenReturn(new CollectionLayoutTo());

        Optional<CollectionLayoutTo> result = subject.findCollectionLayout(1);
        
        assertTrue(result.isPresent());
    }

    @Test
    public void findContainerSetsTest() {
        
        when(containersetRepository.findByBatchId(anyInt())).thenReturn(List.of(new ContainerSetModel()));
        when(converter.convert(any(), eq(ContainerSetTo.class)))
            .thenReturn(new ContainerSetTo());
            
        Set<ContainerSetTo> result = subject.findContainerSets(1);
        
        assertTrue(!result.isEmpty());
    }

    @Test
    public void findLoadTypeTest() {        
        BatchModel model = new BatchModel();
        model.setCollectionlayout(new CollectionLayoutModel());

        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(model));
        when(converter.convert(any(), eq(LoadTypeTo.class)))
            .thenReturn(new LoadTypeTo());
            
        Optional<LoadTypeTo> result = subject.findLoadType(1);
        
        assertTrue(!result.isEmpty());
    }

    @Test
    public void findRequestListMembersTest() {

        when(requestlistmemberRepository.findByBatchId(anyInt())).thenReturn(List.of(new RequestListMemberModel()));
        when(converter.convert(any(), eq(RequestListMemberTo.class)))
                .thenReturn(new RequestListMemberTo());

        Set<RequestListMemberTo> result = subject.findRequestListMembers(1);

        assertTrue(!result.isEmpty());
    }

    @Test
    public void findSampleDetailsTest() {

        when(sampledetailRepository.findByBatchId(anyInt())).thenReturn(List.of(new SampleDetailModel()));
        when(converter.convert(any(), eq(SampleDetailTo.class)))
                .thenReturn(new SampleDetailTo());

        Set<SampleDetailTo> result = subject.findSampleDetails(1);

        assertTrue(!result.isEmpty());
    }

    @Test
    public void findServiceProviderTest() {
        BatchModel model = new BatchModel();
        model.setServiceprovider(new ServiceProviderModel());

        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(model));
        when(converter.convert(any(), eq(ServiceProviderTo.class)))
                .thenReturn(new ServiceProviderTo());

        Optional<ServiceProviderTo> result = subject.findServiceProvider(1);

        assertTrue(!result.isEmpty());
    }

    @Test
    public void findTissueTypeTest() {
        BatchModel model = new BatchModel();
        model.setTissuetype(new TissueTypeModel());

        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(model));
        when(converter.convert(any(), eq(TissueTypeTo.class)))
                .thenReturn(new TissueTypeTo());

        Optional<TissueTypeTo> result = subject.findTissueType(1);

        assertTrue(!result.isEmpty());
    }
    

    @Test
    public void givenBatchExistsWhenModifyBatchThenSuccess() {
        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(new BatchModel()));
        when(batchRepository.save(any())).thenReturn(new BatchModel());
        when(converter.convert(any(), eq(BatchModel.class)))
                .thenReturn(new BatchModel());
        when(converter.convert(any(), eq(BatchTo.class)))
                .thenReturn(new BatchTo());
        BatchInput input = new BatchInput();
        input.setTechnologyPlatformId(1);
        input.setVendorId(1);

        BatchTo result = subject.modifyBatch(input);
        assertTrue(result != null);
        verify(batchRepository, times(1)).save(any());
    }

    
    @Test
    public void getRequesByBatchIDTest() {
        when(batchcodeRepository.findRequestsByBatchID(anyInt())).thenReturn(List.of(1, 2, 99));

        Page<RequestIDTo> result = subject.getRequesByBatchID(1);

        assertThat(result.getContent()).size().isEqualTo(3);
    }

    @Test
    public void givenBatchIsNot0WhengetListBatchForDesignThenReturnOne() {
        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(new BatchModel()));
        when(batchcodeRepository.getDistinctByParametersAndQueryString(anyString()))
            .thenReturn(List.of("plate1","plate2","plate3"));

        Page<DesignTo> result = subject.getListBatchForDesign(123);

        assertThat(result.getContent()).size().isEqualTo(1);
    }
    
    @Test
    public void givenBatchIs0WhengetListBatchForDesignThenReturnMany() {
        when(batchRepository.findByCriteria(eq(BatchModel.class), any(), any(SortInput.class), any(PageInput.class)))
                .thenReturn(new Connection<>(List.of(new BatchModel(), new BatchModel()), Pageable.ofSize(2), 2));

        Page<DesignTo> result = subject.getListBatchForDesign(0);

        assertThat(result.getContent()).size().isEqualTo(2);
    }

    @Test
    public void getBatchIdByRequestIdIntoRequestListMembersTest() {

        subject.getBatchIdByRequestIdIntoRequestListMembers(1);

        verify(batchcodeRepository, times(1)).getDistinctByParametersAndQueryById(anyString());
    }

    @Test
    public void getRequestListMemberByRequestIdTest() {
        when(batchcodeRepository.runNativeQuery(anyString()))
                .thenReturn(List.of(new Object[] { 11 }, new Object[] { 22 }));

        List<Integer> result = subject.getRequestListMemberByRequestId(1);
        assertThat(result).containsExactly(11, 22);

        verify(batchcodeRepository, times(1)).runNativeQuery(anyString());
    }

    @Test
    public void findBatchByRequestIdTest() {

        when(batchRepository.findById(anyInt())).thenReturn(Optional.of(new BatchModel()));
        when(converter.convert(any(), eq(BatchTo.class)))
                .thenReturn(new BatchTo());

        Optional<BatchTo> result = subject.findBatchByRequestId(1);

        assertTrue(!result.isEmpty());
    }

    @Test
    public void findBatchMatkerGroupsTest() {

        when(batchMarkerGroupRepository.findByBatchId(anyInt())).thenReturn(List.of(new BatchMarkerGroupModel()));
        when(converter.convert(any(), eq(BatchMarkerGroupTo.class)))
                .thenReturn(new BatchMarkerGroupTo());

        Set<BatchMarkerGroupTo> result = subject.findBatchMatkerGroups(1);

        assertTrue(!result.isEmpty());
    }

    @Test
    public void givenFixedMarkerGroupsWhenfindMarkersThenReturnFixed() {
        BatchMarkerGroupModel model = new BatchMarkerGroupModel();
        model.setFixed(true);

        when(batchMarkerGroupRepository.findByBatchId(anyInt()))
                .thenReturn(List.of(model));
        when(markerService.findAssayNamesByGroupAndTechServiceProv(anyInt(), anyInt()))
                .thenReturn(List.of("assayName1", "assayName2", "assayName3"));

        List<String> result = subject.findMarkers(1);

        assertThat(result).size().isEqualTo(3);
    }

    @Test
    public void givenNotFixedMarkerGroupsWhenfindMarkersThenReturnCustom() {
        BatchMarkerGroupModel model = new BatchMarkerGroupModel();
        model.setBatchMarkerGroupCustom(new HashSet<>());
        model.getBatchMarkerGroupCustom().add(new BatchMarkerGroupCustomModel());

        when(batchMarkerGroupRepository.findByBatchId(anyInt()))
                .thenReturn(List.of(model));
        when(markerService.findAssayNamesByMarkerIdsAndTechServiceProv(any(), anyInt()))
                .thenReturn(List.of("assayName1", "assayName2", "assayName3"));

        List<String> result = subject.findMarkers(1);

        assertThat(result).size().isEqualTo(3);
    }

    @Test
    public void givenBatchIdWhenfindSampleGermplasmDetailsThenReturnDetails() {
        SampleDetailModel model = new SampleDetailModel();
        model.setEntityId(3);//plot
        when(sampledetailRepository.findByBatchId(anyInt()))
                .thenReturn(List.of(model));

        List<SampleGermplasmDetailTo> result = subject.findSampleGermplasmDetails(1);

        assertThat(result).size().isEqualTo(1);
    }

    @Test
    public void givenPlateCodeWhenfindSampleGermplasmDetailsThenReturnDetails() {
        SampleDetailModel model = new SampleDetailModel();
        model.setEntityId(17);//package
        when(sampledetailRepository.findByPlateName(anyString()))
                .thenReturn(List.of(model));

        List<SampleGermplasmDetailTo> result = subject.findSampleGermplasmDetails("aPlate");

        assertThat(result).size().isEqualTo(1);
    }

    @Test
    public void givenNoEntityTypeWhenGetEntityTypeThenThrowException() {
        Exception e = assertThrows(RuntimeException.class,
                () -> subject.getEntityType(List.of(new SampleDetailModel()), "anEntity", "aKey"));
        assertEquals("Cannot infer sample type in anEntity : aKey", e.getMessage());
    }

    @Test
    public void givenPlotEntitiesWhenCreateGermplasmDetailMapThenReturnGermplasmDetailMap() {
        GermplasmDetailTo g1 = new GermplasmDetailTo(1, 11, 111,99),
            g2 = new GermplasmDetailTo(1, 11, 112, 99),
            g3 = new GermplasmDetailTo(2, 12, 112, 99),
            g4 = new GermplasmDetailTo(2, 13, 111, 99);

        when(cbGermplasmService.getGermplasmDetail(anySet(), eq("plot")))
                .thenReturn(List.of(g1, g2, g3, g4));

        Map<Integer, GermplasmDetailTo> result = subject.createGermplasmDetailMap(new HashSet<>(List.of(123)), "plot");

        assertThat(result.size()).isEqualTo(3);
        assertThat(result.keySet()).contains(11, 12, 13);
    }

    @Test
    public void givenPackageEntitiesWhenCreateGermplasmDetailMapThenReturnDetails() {
        GermplasmDetailTo g1 = new GermplasmDetailTo(1, 11, 111, 99), g2 = new GermplasmDetailTo(1, 11, 112, 99),
                g3 = new GermplasmDetailTo(2, 12, 112, 99), g4 = new GermplasmDetailTo(2, 13, 111, 99);

        when(cbGermplasmService.getGermplasmDetail(anySet(), eq("package")))
                .thenReturn(List.of(g1, g2, g3, g4));

        Map<Integer, GermplasmDetailTo> result = subject.createGermplasmDetailMap(new HashSet<>(List.of(123)),
                "package");

        assertThat(result.size()).isEqualTo(2);
        assertThat(result.keySet()).contains(111, 112);

    }

    @Test
    public void givenPlotsWhenFindSampleGermplasmDetailsThenReturnGermplasmDetailMap() {
        RequestModel req = new RequestModel(123);
        RequestListMemberModel member1 = new RequestListMemberModel(), member2 = new RequestListMemberModel();
        SampleDetailModel detail1 = new SampleDetailModel(1111), detail2 = new SampleDetailModel(2222);

        member1.setRequest(req);
        member1.setDataId(11);
        member1.setEntityId(3);
        member1.setId(1000);
        member1.setSampleDetail(detail1);
        member2.setRequest(req);
        member2.setDataId(12);
        member2.setEntityId(3);
        member2.setId(2000);
        member2.setSampleDetail(detail2);

        when(requestlistmemberRepository.findByCriteria(eq(RequestListMemberModel.class), any(), any(SortInput.class),
                any(PageInput.class)))
                .thenReturn(new Connection<>(List.of(member1, member2), Pageable.ofSize(2), 2))
                .thenReturn(new Connection<>(emptyList(), Pageable.unpaged(), 0));
        when(sampledetailRepository.findAllById(anyList()))
                .thenReturn(List.of(detail1, detail2));
        when(cbGermplasmService.getGermplasmDetail(anySet(), anySet(), eq(null)))
                .thenReturn(List.of(new GermplasmDetailTo(), new GermplasmDetailTo()));
        when(converter.convert(any(SampleDetailModel.class), eq(SampleGermplasmDetailTo.class)))
                .thenReturn(new SampleGermplasmDetailTo());

        GenotypeSearchParams params = new GenotypeSearchParams();
        params.setPlotIds(new HashSet<>(List.of(11, 12)));
        Map<Integer, List<SampleGermplasmDetailTo>> result = subject.findSampleGermplasmDetails(params);
    
        assertThat(result.keySet()).containsOnly(123);
        assertThat(result.get(123).size()).isEqualTo(2);
    }

    @Test
    public void givenPlotParamWhenCreateFiltersThenReturnPlotFilters() {
        GenotypeSearchParams input = new GenotypeSearchParams();
        input.setPlotIds(new HashSet<>(List.of(1, 2, 3)));

        List<FilterInput> result = subject.createFilters(input);

        assertThat(result.size()).isEqualTo(2);
        assertThat(result).extracting("col", "val", "vals", "mod").contains(
            Tuple.tuple("dataId", null, new String[] { "1", "2", "3" }, FilterMod.IN),
            Tuple.tuple("entityId", "" + PLOT_ENTITY_ID, null, FilterMod.EQ));
    }

    @Test
    public void givenPackageParamWhenCreateFiltersThenReturnPackageFilters() {
        GenotypeSearchParams input = new GenotypeSearchParams();
        input.setPackageIds(new HashSet<>(List.of(1, 2, 3)));

        List<FilterInput> result = subject.createFilters(input);

        assertThat(result.size()).isEqualTo(2);
        assertThat(result).extracting("col", "val", "vals", "mod").contains(
                Tuple.tuple("dataId", null, new String[] { "1", "2", "3" }, FilterMod.IN),
                Tuple.tuple("entityId", "" + PACKAGE_ENTITY_ID, null, FilterMod.EQ));
    }
    
    @Test
    public void givenRequestParamWhenCreateFiltersThenReturnPackageFilter() {
        GenotypeSearchParams input = new GenotypeSearchParams();
        input.setRequestId(123);

        List<FilterInput> result = subject.createFilters(input);

        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0)).extracting("col", "val", "vals", "mod").contains(
                "request.id", "123", null, FilterMod.EQ);
    }

    @Test
    public void givenGermplasmParamWhenCreateFiltersThenReturnGermplasmFilter() {
        GenotypeSearchParams input = new GenotypeSearchParams();
        input.setGermplasmIds(new HashSet<>(List.of(1, 2, 3)));

        List<FilterInput> result = subject.createFilters(input);

        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0)).extracting("col", "val", "vals", "mod").contains(
                "germplasmId", null, new String[] { "1", "2", "3" }, FilterMod.IN);
    }

    @Test
    public void givenSampleParamWhenCreateFiltersThenReturnSampleFilter() {
        GenotypeSearchParams input = new GenotypeSearchParams();
        input.setSampleIds(new HashSet<>(List.of("s1","s2","s3")));

        List<FilterInput> result = subject.createFilters(input);
        
        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0)).extracting("col", "val", "mod").contains(
                "sampleDetail.sampleCode", null, FilterMod.IN);
        assertThat(result.get(0).getVals()).contains("s1", "s2", "s3");
    }

}

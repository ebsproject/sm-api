package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class AgriplexToHapMapConverterTest {

    AgriplexToHapMapConverter subject;

    @BeforeEach
    public void init() {
        subject = new AgriplexToHapMapConverter();
        try {
            Files.createDirectories(Path.of("tmp"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    void canConvertFromAgriplexToHapmap() {
        Path p = Path.of("tmp", "agriplexSample.hapmap");
        try {
            subject.convert(234, p,
                    getClass().getResourceAsStream("/agriplexSample.xlsx").readAllBytes());

            assertThat(p.toFile().length()).isEqualTo(546);
            Files.deleteIfExists(p);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    void givenNumericIdsWhenConvertThenCanConvertFromAgriplexToHapmap() {
        Path p = Path.of("tmp", "agriplexSample2.hapmap");
        try {
            subject.convert(123, p,
                    getClass().getResourceAsStream("/agriplexSample2.xlsx").readAllBytes());
                    
            assertThat(p.toFile().length()).isEqualTo(486);       
            Files.deleteIfExists(p);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

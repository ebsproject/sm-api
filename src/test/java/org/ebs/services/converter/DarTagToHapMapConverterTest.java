package org.ebs.services.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class DarTagToHapMapConverterTest {

    DarTagToHapMapConverter subject;

    @BeforeEach
    public void init() {
        subject = new DarTagToHapMapConverter();
        try {
            Files.createDirectories(Path.of("tmp"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void test() {
       
        Path p = Path.of("tmp", "darTagSample.hapmap");
        try {
            subject.convert(123, p,
                    getClass().getResourceAsStream("/darTagSample.csv").readAllBytes());
                    
            assertThat(p.toFile().length()).isEqualTo(398);
            Files.deleteIfExists(p);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

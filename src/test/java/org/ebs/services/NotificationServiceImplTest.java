package org.ebs.services;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import org.ebs.util.brapi.TokenGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class NotificationServiceImplTest {

    @Mock
    private RestTemplateBuilder restTemplateBuilderMock;
    @Mock
    private TokenGenerator tokenGeneratorMock;
    @Mock
    private RestTemplate restTemplateMock;

    private NotificationServiceImpl subject;

    private ResponseEntity<NotificationResponse> response;



    @BeforeEach
    public void init() throws NoSuchFieldException, SecurityException {
        response = ResponseEntity.accepted().body(new NotificationResponse(true, ""));
        when(tokenGeneratorMock.getToken()).thenReturn("sometoken");
        when(restTemplateMock.exchange(anyString(), eq(HttpMethod.POST), any(), eq(NotificationResponse.class)))
            .thenReturn(response);
        when(restTemplateBuilderMock.build()).thenReturn(restTemplateMock);
        
        subject = new NotificationServiceImpl(restTemplateBuilderMock, tokenGeneratorMock);
        setField(subject, "csGraphqlEndpoint","http:8080//localhost/graphql");
        setField(subject, "gigwaApiEndpoint","http:8080//localhost/gigwa/rest");
    }

    @Test
    void test() {

        boolean response = subject.sendNotificationAsGigwa(1, 1, "an error happened in here!!");

        assertTrue(response);
    }

    private void setField(Object object, String fieldName, Object value) {
        try {
            var field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(object, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException("Failed to set " + fieldName + " of object", e);
        }
    }
}

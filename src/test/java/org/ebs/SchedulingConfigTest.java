package org.ebs;

import static org.ebs.services.GigwaService.DEFAULT_MODULE_NAME;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.ebs.services.GigwaLoader;
import org.ebs.util.client.GigwaClient;
import org.ebs.util.client.GigwaModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SchedulingConfigTest {
 
    @Mock
    private GigwaClient clientMock;

    private SchedulingConfig subject;

    @BeforeEach
    public void init() {
        subject = new SchedulingConfig(clientMock);
    }

    @Test
    public void givenGigwaModuleNotExistsWhenInitSchedulerThenCallCreateModule() {
        when(clientMock.listModules()).thenReturn(Map.of("something", new GigwaModule(0, true, false, "")));
        
        subject.initGigwaDb();

        verify(clientMock, times(1)).createModule(DEFAULT_MODULE_NAME);
        verify(clientMock, times(1)).setModuleVisibility(DEFAULT_MODULE_NAME, false, true);
    }

    @Test
    public void givenGigwaModuleExistsWhenInitSchedulerThenNotCreateModule() {
        when(clientMock.listModules()).thenReturn(Map.of(
            "something", new GigwaModule(0, false, true, ""),
            "ebs", new GigwaModule(0, false, true, "")));
        
        subject.initGigwaDb();

        verify(clientMock, times(0)).createModule(DEFAULT_MODULE_NAME);
        verify(clientMock, times(1)).setModuleVisibility(DEFAULT_MODULE_NAME, false, true);
    }
}
